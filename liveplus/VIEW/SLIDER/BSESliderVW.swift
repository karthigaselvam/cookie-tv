//
//  BSESliderVW.swift
//  liveplus
//
//  Created by BSEtec on 19/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage
import MaterialIconsSwift

protocol BSESliderVWDelegate:class {
    func hideLoader()->Void;
    func setLoader()->Void;
    func gotoProfile(model:BSEUserModel)->Void;
     func gotoBroadcast(model:BSEBroadcastModel)->Void;
}

class BSESliderVW: UIView, UIScrollViewDelegate {
    @IBOutlet var page_scroll : UIScrollView?;
    @IBOutlet var pager : UIPageControl?;
    var typer:String!;
    var userData = [AnyObject]();
    var broadcastData = [AnyObject]();
    var viewHeight: CGFloat!;
    var currentPage : CGFloat!;
    weak var delegate: BSESliderVWDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
    }

    
    convenience init(frame: CGRect, type: String, data: [AnyObject]) {
        self.init(frame: frame)
        let mainView: UIView = Bundle.main.loadNibNamed("BSESliderVW",
                                                        owner: self,
                                                        options: nil)?.first as! UIView
        mainView.frame.origin.x = 0;
        mainView.frame.origin.y = 0;
        mainView.frame.size.width = frame.size.width;
        mainView.frame.size.height = frame.size.height;
        self.addSubview(mainView);
        self.typer = type;
        self.viewHeight = frame.size.height;
        if(self.typer == "people") {
            self.userData = data;
            initUserFields();
        } else {
            self.broadcastData = data;
            currentPage = 0;
            initBroadcastFields();
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK : - custom function
    
    func initUserFields() -> Void {
        self.page_scroll?.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width, height: viewHeight - 50);
        
        let bottomView:UIView = UIView.init(frame: CGRect(x:0, y:viewHeight - 50, width:UIScreen.main.bounds.size.width, height:50));
        bottomView.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0);
        self.addSubview(bottomView);
        
        let borderView:UIView = UIView.init(frame: CGRect(x:0, y:48, width:UIScreen.main.bounds.size.width, height:2));
        borderView.backgroundColor = UIColor.init(red: 195.0/255.0, green: 195.0/255.0, blue: 195.0/255.0, alpha: 1.0);
        bottomView.addSubview(borderView);
        
        let header_label:UILabel = UILabel.init(frame: CGRect(x:12, y:0, width:UIScreen.main.bounds.size.width-24, height:48));
        header_label.font = UIFont.init(name: "Poppins-Regular", size: 17.0)
         header_label.text = "TRENDING".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        header_label.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0);
        bottomView.addSubview(header_label);
        
        let feature_label:UILabel = UILabel.init(frame: CGRect(x:(getBSEValue(_keyname: CURRENTLANG)=="ar")
            ? self.frame.size.width-80 : 0, y:viewHeight-129, width:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? 80 : 100, height:24));
         feature_label.text = "Featured".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        feature_label.backgroundColor = UIColor.init(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0);
        feature_label.font = UIFont.init(name: "Poppins-Medium", size: 15.0);
        feature_label.textColor = .white;
        feature_label.textAlignment = NSTextAlignment.center;
        self.addSubview(feature_label);
        
        var incr:Int! = 0;
        let screenWidth:CGFloat = UIScreen.main.bounds.size.width;
        for dic in self.userData {
            let model: BSEUserModel = dic as! BSEUserModel;
            let xAxis: CGFloat = screenWidth * CGFloat(incr)
            print("x axis is ",xAxis);
            self.addSlide(model: model, tag: incr)
            incr = incr + 1;
        }
        
        page_scroll?.contentSize = CGSize(width: screenWidth * CGFloat(self.userData.count), height: viewHeight-50);
        self.pager?.frame = CGRect(x:(screenWidth-91)/2, y:viewHeight - 135, width:91, height: 37);
    }
    
    func addSlide(model:BSEUserModel, tag:Int) -> Void {
        let screenWidth:CGFloat = UIScreen.main.bounds.size.width;
        let xAxis: CGFloat = screenWidth * CGFloat(tag)
        let sliderView:UIView =  UIView.init(frame: CGRect(x:xAxis, y:0, width:screenWidth, height:viewHeight-50));
        sliderView.backgroundColor = .white;
        sliderView.tag = tag;
        page_scroll?.addSubview(sliderView);
        
        let imageview : UIImageView = UIImageView.init(frame: CGRect(x:0, y:0, width:screenWidth, height:viewHeight-90));
        imageview.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        imageview.contentMode = .scaleAspectFill;
        imageview.layer.masksToBounds = true;
        sliderView.addSubview(imageview);
        
        let overlayView:UIView =  UIView.init(frame: CGRect(x:0, y:0, width:screenWidth, height:viewHeight-90));
        overlayView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7);
        sliderView.addSubview(overlayView);
        
        let user_label:UILabel = UILabel.init(frame: CGRect(x:12, y:12, width:screenWidth-24, height:24));
        user_label.text = model.first_name.count>0 ? (model.first_name+" "+model.last_name).uppercased() : model.username.uppercased();
        user_label.font = UIFont.init(name: "Poppins-Medium", size: 15.0)
        user_label.textColor = .white;
        user_label.textAlignment = (getBSEValue(_keyname: CURRENTLANG)=="ar") ? NSTextAlignment.left : NSTextAlignment.right;
        sliderView.addSubview(user_label);
        
        let userAction:UIButton =  UIButton.init(frame: CGRect(x:0, y:0, width:screenWidth, height:viewHeight-90));
        userAction.addTarget(self, action: #selector(self.userAction(_:)), for: .touchUpInside)
        userAction.tag = tag;
        sliderView.addSubview(userAction);
        
        let followView:UIView =  UIButton.init(frame: CGRect(x:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? 10 : screenWidth-44, y:viewHeight-134, width:32, height:32));
        followView.backgroundColor = .clear;
        sliderView.addSubview(followView);
        
        let followImage:UIImageView =  UIImageView.init(frame: CGRect(x:0, y:0, width:32, height:32));
        if(model.user_follow_status == 0) {
            followImage.image = UIImage(named: "Follow_white.png")
        } else if (model.user_follow_status == 1) {
            followImage.image = UIImage(named: "unFollow_white.png")
        } else if (model.user_follow_status == 2) {
            followImage.image = UIImage(named: "request_white.png")
        }
        followView.addSubview(followImage);
        
        let followAction:UIButton =  UIButton.init(frame: CGRect(x:0, y:0, width:32, height:32));
        followAction.addTarget(self, action: #selector(self.followAction(_:)), for: .touchUpInside)
        followAction.tag = tag;
        followView.addSubview(followAction);
        
        let follower_label:UILabel = UILabel.init(frame: CGRect(x:12, y:sliderView.frame.size.height-40, width:screenWidth-24, height:40));
        follower_label.font = UIFont.init(name: "Poppins-Medium", size: 15.0)
        follower_label.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0);
        sliderView.addSubview(follower_label);
        
        if(model.follower_count==0) {
            
           follower_label.text = "No Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.follower_count==1) {
          
            follower_label.text = "1 Follower".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
          follower_label.text = formatPoints(from: model.follower_count) +  "Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
    }
    
    func initBroadcastFields() -> Void {
        self.page_scroll?.frame = CGRect(x:0, y:0, width:self.frame.size.width, height: viewHeight);
        self.pager?.isHidden = true;
        
        let leftView:UIView = UIView.init(frame: CGRect(x:10, y:(viewHeight - 32)/2, width:32, height:32));
        leftView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7);
        self.addSubview(leftView);
        
        
        let leftView_label:UILabel = UILabel.init(frame: CGRect(x:0, y:0, width:32, height:32));
        leftView_label.font = MaterialIcons.fontOfSize(size:24);
        leftView_label.text = MaterialIcons.KeyboardArrowLeft
        leftView_label.textColor = .white;
        leftView_label.textAlignment = NSTextAlignment.center;
        leftView.addSubview(leftView_label);
        
        let leftView_action:UIButton =  UIButton.init(frame: CGRect(x:0, y:0, width:32, height:32));
        leftView_action.addTarget(self, action: #selector(self.prevAction(_:)), for: .touchUpInside)
        leftView.addSubview(leftView_action);
        
        let rightView:UIView = UIView.init(frame: CGRect(x:self.frame.size.width-42, y:(viewHeight - 32)/2, width:32, height:32));
        rightView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7);
        self.addSubview(rightView);
        
        
        let rightView_label:UILabel = UILabel.init(frame: CGRect(x:0, y:0, width:32, height:32));
        rightView_label.font = MaterialIcons.fontOfSize(size:24);
        rightView_label.text = MaterialIcons.KeyboardArrowRight
        rightView_label.textColor = .white;
        rightView_label.textAlignment = NSTextAlignment.center;
        rightView.addSubview(rightView_label);
        
        let rightView_action:UIButton =  UIButton.init(frame: CGRect(x:0, y:0, width:32, height:32));
        rightView_action.addTarget(self, action: #selector(self.nextAction(_:)), for: .touchUpInside)
        rightView.addSubview(rightView_action);
        

        var incr:Int! = 0;
        let screenWidth:CGFloat = self.frame.size.width;
        for dic in self.broadcastData {
            let model: BSEBroadcastModel = dic as! BSEBroadcastModel;
            let xAxis: CGFloat = screenWidth * CGFloat(incr)
            print("x axis is ",xAxis);
            self.addBroadcastSlide(model: model, tag: incr)
            incr = incr + 1;
        }
        page_scroll?.contentSize = CGSize(width: screenWidth * CGFloat(self.broadcastData.count), height: viewHeight);
    }
    
    func addBroadcastSlide(model:BSEBroadcastModel, tag:Int) -> Void {
        let screenWidth:CGFloat = self.frame.size.width;
        let xAxis: CGFloat = screenWidth * CGFloat(tag)
        let sliderView:UIView =  UIView.init(frame: CGRect(x:xAxis, y:0, width:screenWidth, height:viewHeight));
        sliderView.backgroundColor = .white;
        sliderView.tag = tag;
        page_scroll?.addSubview(sliderView);
        
        let imageview : UIImageView = UIImageView.init(frame: CGRect(x:0, y:0, width:screenWidth, height:viewHeight));
        imageview.sd_setImage(with: URL(string: BROADCASTIMGURL+String(model.broadcast_id)), placeholderImage: UIImage(named: "placeholder.png"))
        imageview.contentMode = .scaleAspectFill;
        imageview.layer.masksToBounds = true;
        sliderView.addSubview(imageview);
        
        let broadcastAction:UIButton =  UIButton.init(frame: CGRect(x:0, y:0, width:screenWidth, height:viewHeight));
        broadcastAction.addTarget(self, action: #selector(self.broadcastAction(_:)), for: .touchUpInside)
        broadcastAction.tag = tag;
        sliderView.addSubview(broadcastAction);

        
        let name_size = model.broadcast_name.capitalized.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
        var name_width: CGFloat = name_size.width+20
        if(name_width > self.frame.size.width) {
            name_width = self.frame.size.width
        }
        let name_label:UILabel = UILabel.init(frame: CGRect(x: (getBSEValue(_keyname: CURRENTLANG)=="ar") ?  self.frame.size.width-name_width :0, y:0, width:name_width, height:20));
        name_label.text = model.broadcast_name.capitalized;
        name_label.font = UIFont.init(name: "Poppins-Regular", size: 12.0)
        name_label.textColor = .white;
        name_label.textAlignment = NSTextAlignment.center;
        name_label.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7);
        sliderView.addSubview(name_label);
        
        let like_count:String;
        if(model.like_count==0) {
               like_count =  "No Likes".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.like_count==1) {
 like_count = "1 Like".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            
        } else  {
            like_count =  formatPoints(from: model.like_count) +  "Likes".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
        
        let view_count:String;
        if(model.viewer_count==0) {
            
            view_count =   "No Views".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.like_count==1) {
           
           view_count = "1 View".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
            
            view_count =  formatPoints(from: model.viewer_count) + "Views".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            
        }
        
        let view_count_size = view_count.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
        let like_count_size = like_count.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
        let statWidth : CGFloat = 14 + 14 + 2 + 2 + 5 + 5 + 4 +  4 + 1 + view_count_size.width + like_count_size.width
        
        let broadcastStats:UIView =  UIButton.init(frame: CGRect(x: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? 0 : self.frame.size.width-statWidth, y:viewHeight-20, width:statWidth, height:20));
        broadcastStats.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7);
        sliderView.addSubview(broadcastStats);
        
        let like_icon:UILabel = UILabel.init(frame: CGRect(x:5, y:3, width:14, height:14));
        like_icon.font = MaterialIcons.fontOfSize(size:14);
        like_icon.text = MaterialIcons.ThumbUp
        like_icon.textColor = .white;
        like_icon.textAlignment = NSTextAlignment.center;
        broadcastStats.addSubview(like_icon);
        
        let like_name:UILabel = UILabel.init(frame: CGRect(x:like_icon.frame.origin.x + like_icon.frame.size.width + 2, y:0, width:like_count_size.width, height:20));
        like_name.font = UIFont(name: "Poppins-Regular", size: 12);
        like_name.text = like_count
        like_name.textColor = .white;
        like_name.textAlignment = NSTextAlignment.center;
        broadcastStats.addSubview(like_name);
        
        let seperator:UILabel = UILabel.init(frame: CGRect(x:like_name.frame.origin.x + like_name.frame.size.width + 4, y:5, width:1, height:10));
        seperator.font = UIFont(name: "Poppins-Regular", size: 14);
        seperator.backgroundColor = .white;
        broadcastStats.addSubview(seperator);
        
        let view_icon:UILabel = UILabel.init(frame: CGRect(x:seperator.frame.origin.x + seperator.frame.size.width + 4, y:3, width:14, height:14));
        view_icon.font = MaterialIcons.fontOfSize(size:14);
        view_icon.text = MaterialIcons.RemoveRedEye
        view_icon.textColor = .white;
        view_icon.textAlignment = NSTextAlignment.center;
        broadcastStats.addSubview(view_icon);
        
        let view_name:UILabel = UILabel.init(frame: CGRect(x:view_icon.frame.origin.x + view_icon.frame.size.width + 2, y:0, width:view_count_size.width, height:20));
        view_name.font = UIFont(name: "Poppins-Regular", size: 12);
        view_name.text = view_count
        view_name.textColor = .white;
        view_name.textAlignment = NSTextAlignment.center;
        broadcastStats.addSubview(view_name);
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round((page_scroll?.contentOffset.x)! / (page_scroll?.frame.size.width)!)
        self.pager?.currentPage = Int(pageNumber)
    }
    
    func formatPoints(from: Int) -> String {
        let number = Double(from)
        let thousand = number / 1000
        let million = number / 1000000
        
        if million >= 1.0 { return "\(round(million*10)/10)M" }
        else if thousand >= 1.0 { return "\(round(thousand*10)/10)K" }
        else { return "\(Int(number))"}
    }
    
    // MARK: - control function
    @objc func userAction(_ sender: UIButton){
        let model:BSEUserModel = userData[sender.tag] as! BSEUserModel;
        delegate?.gotoProfile(model: model);
    }
    
    @objc func followAction(_ sender: UIButton){
        let model:BSEUserModel = userData[sender.tag] as! BSEUserModel
        if(model.user_follow_status==0 || model.user_follow_status==1) {
            delegate?.setLoader();
            var follower_status:Int!;
            follower_status = model.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(model.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(model.is_private==1) {
                model.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
 SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
                    follower_count = follower_count + 1;
    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    follower_count = follower_count - 1;
    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                model.user_follow_status = (follower_status==1) ? 1:0;
            }
            
            for V in (page_scroll?.subviews)! {
                if(V.tag==sender.tag) {
                    V.removeFromSuperview();
                    self.addSlide(model: model, tag: V.tag);
                }
            }
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
                 
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.delegate?.hideLoader();
            }) { (failure) -> Void in
                self.delegate?.hideLoader();
            }
        }
    }
    
    @objc func prevAction(_ sender: UIButton){
        if(Int(currentPage)>0) {
            currentPage = currentPage - 1;
            page_scroll?.setContentOffset(CGPoint.init(x: self.frame.size.width*currentPage, y: 0), animated: true);
        }
    }
    
    @objc func nextAction(_ sender: UIButton){
        let totalCount:Int = self.broadcastData.count - 1
        if(Int(currentPage)<totalCount) {
            currentPage = currentPage + 1;
            page_scroll?.setContentOffset(CGPoint.init(x: self.frame.size.width*currentPage, y: 0), animated: true);
            
        }
    }
    
    @objc func broadcastAction(_ sender: UIButton){
        let model:BSEBroadcastModel = broadcastData[sender.tag] as! BSEBroadcastModel;
        delegate?.gotoBroadcast(model: model);
    }

}

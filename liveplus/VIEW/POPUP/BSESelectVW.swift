//
//  BSESelectVW.swift
//  liveplus
//
//  Created by BSEtec on 26/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
protocol BSESelectVWDelegate:class {
    func closeItem(type:String);
    func selectedItem(model:BSEGroupModel, type:String);
}
class BSESelectVW: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var header_title:UILabel!
    @IBOutlet var close_icon:UILabel!
    @IBOutlet var select_icon:UILabel!
    @IBOutlet var closeView : UIView?;
    @IBOutlet var selectView : UIView?;
    weak var delegate: BSESelectVWDelegate?
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var s_type: String!
    var currentIndex: Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
    }
    
    
    convenience init(frame: CGRect, type: String, data: [AnyObject]) {
        self.init(frame: frame)
        let mainView: UIView = Bundle.main.loadNibNamed("BSESelectVW",
                                                        owner: self,
                                                        options: nil)?.first as! UIView
        mainView.frame.origin.x = 0;
        mainView.frame.origin.y = 0;
        mainView.frame.size.width = frame.size.width;
        mainView.frame.size.height = frame.size.height;
        self.addSubview(mainView);
        s_type = type;
        tableData.append(contentsOf: data);
        tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
        initFields();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - custom function
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = closeView!.frame;
            framer.origin.x =  self.frame.size.width - (closeView!.frame.size.width);
            closeView!.frame = framer;
            
            framer = selectView!.frame;
            framer.origin.x =  0;
            selectView!.frame = framer;
        }
        
        
        if(s_type=="broadcast") {
 header_title.text = "Broacast Type".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if (s_type=="group") {
  header_title.text = "Group List".localized(lang: getBSEValue(_keyname: CURRENTLANG))        }
        close_icon.font = MaterialIcons.fontOfSize(size:24);
        close_icon.text = MaterialIcons.Close;
        
        select_icon.font = MaterialIcons.fontOfSize(size:24);
        select_icon.text = MaterialIcons.Check;
        if(s_type=="broadcast") {
            var incr : Int = 0;
            for Dic in tableData {
                let model:BSEGroupModel = Dic as! BSEGroupModel;
                if(model.status==1) {
                    currentIndex = incr;
                }
                incr = incr + 1;
            }
        } else if (s_type=="group") {
            currentIndex = 0;
            var incr : Int = 0;
            for Dic in tableData {
                let model:BSEGroupModel = Dic as! BSEGroupModel;
                if(incr==0) {
                    model.status = 1;
                } else {
                    model.status = 0;
                }
                
                incr = incr + 1;
            }
        }
        tblView?.reloadData();
    }
    
    // MARK: - control function
    @IBAction func actionClose(sender: UIButton!) {
        self.delegate?.closeItem(type:s_type);
    }
    
    @IBAction func actionSelectedItem(sender: UIButton!) {
        var selectedModel:BSEGroupModel!;
        for Dic in tableData {
            let model:BSEGroupModel = Dic as! BSEGroupModel;
            if(model.status==1) {
                selectedModel = model;
                break;
            }
        }
        delegate?.selectedItem(model: selectedModel, type:s_type);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let uidentifier = "SelectCell"
        
        var cell: SelectCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? SelectCell
        if cell == nil {
            tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "SelectCellRTL" : "SelectCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? SelectCell;
        }
        if(s_type == "broadcast") {
            cell.setBroadcastValue(model: tableData[indexPath.row] as! BSEGroupModel)
        } else if (s_type == "group") {
            cell.setGroupValue(model: tableData[indexPath.row] as! BSEGroupModel)
        }
        
        cell.backgroundColor = .white
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let oldModel:BSEGroupModel = tableData[currentIndex] as! BSEGroupModel
        oldModel.status=0;
        tblView?.reloadRows(at:[IndexPath.init(row: currentIndex, section: 0)], with: .none)
        currentIndex = indexPath.row;
        
        let model:BSEGroupModel = tableData[indexPath.row] as! BSEGroupModel
        model.status=1;
        tblView?.reloadRows(at:[indexPath], with: .none)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

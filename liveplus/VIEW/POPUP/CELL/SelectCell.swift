//
//  SelectCell.swift
//  liveplus
//
//  Created by BSEtec on 26/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
class SelectCell: UITableViewCell {
    
    @IBOutlet var header_title:UILabel!
    @IBOutlet var select_icon:UILabel!
    @IBOutlet var select_btn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setBroadcastValue(model:BSEGroupModel) -> Void {
        header_title.text = model.group_name.capitalized
        select_icon.font = MaterialIcons.fontOfSize(size:20);
        if(model.status == 0) {
            select_icon.text = MaterialIcons.CheckBoxOutlineBlank;
        } else {
            select_icon.text = MaterialIcons.CheckBox;
        }
    }
    
    func setGroupValue(model:BSEGroupModel) -> Void {
        header_title.text = model.group_name.capitalized
        select_icon.font = MaterialIcons.fontOfSize(size:20);
        if(model.status == 0) {
            select_icon.text = MaterialIcons.CheckBoxOutlineBlank;
        } else {
            select_icon.text = MaterialIcons.CheckBox;
        }
    }
    
}

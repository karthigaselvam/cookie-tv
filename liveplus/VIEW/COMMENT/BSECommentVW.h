//
//  BSECommentVW.h
//  commentview
//
//  Created by jeyakumar on 28/02/18.
//  Copyright © 2018 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface BSECommentVW : UIView {
    IBOutlet UIImageView *userImage;
    IBOutlet UILabel *username;
    IBOutlet UILabel *comment_label;
    IBOutlet UIView *overlay;
         NSString *current_lang;
}
-(id)initWithFrame:(CGRect)frame andDic:(NSDictionary *) Dic andCurrentLangCode:(NSString *)LangCode;

@end

//
//  BSECommentVW.m
//  commentview
//
//  Created by jeyakumar on 28/02/18.
//  Copyright © 2018 bsetec. All rights reserved.
//

#import "BSECommentVW.h"


@implementation BSECommentVW

-(id)initWithFrame:(CGRect)frame andDic:(NSDictionary *) Dic andCurrentLangCode:(NSString *)LangCode {
    self = [super initWithFrame:frame];
    if (self) {
        
    UIView *listView = [[[NSBundle mainBundle] loadNibNamed:([LangCode isEqualToString:@"ar"])?@"BSECommentVWRTL":@"BSECommentVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
        [self addSubview:listView];
        
        userImage.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5.0f;
        self.layer.masksToBounds = YES;
        
        username.text = [Dic objectForKey:@"username"];
        userImage.imageURL = [NSURL URLWithString:[Dic objectForKey:@"userimage"]];
        comment_label.text = [Dic objectForKey:@"comment"];
        
        CGRect framer = comment_label.frame;
        framer.size.height = [self HeightCalculation:comment_label.text andWidth:comment_label.frame.size.width];
        comment_label.frame = framer;
        
        if([LangCode isEqualToString:@"ar"])
        {
            framer = userImage.frame;
            framer.size.height = self.frame.size.height;
            framer.origin.x=self.frame.size.width-userImage.frame.size.width;
            userImage.frame = framer;
            
            framer = overlay.frame;
            framer.size.height = self.frame.size.height;
            framer.origin.x=self.frame.size.width-overlay.frame.size.width;
            overlay.frame = framer;
            
            
        }
        else
        {
            framer = userImage.frame;
            framer.size.height = self.frame.size.height;
            userImage.frame = framer;
            
            overlay.frame = userImage.bounds;
        }
        
        
        overlay.backgroundColor = [self randomColor];
        
        [self initFields];
    }
    return self;
}

-(void) initFields {
    [UIView animateWithDuration:0.5
                          delay:8.0
                        options: UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         self.alpha=0.0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(CGFloat)HeightCalculation :(NSString *)StringData andWidth:(CGFloat) width {
    
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:12.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}

-(UIColor *)randomColor
{
    CGFloat red   = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue  = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:.25];
    return color;
}


@end

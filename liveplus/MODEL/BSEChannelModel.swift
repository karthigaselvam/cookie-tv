//
//  BSEChannelModel.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEChannelModel: NSObject {
    var channel_id:Int!;
    var channel_name:String!;
    var channel_count:Int!;
    var created_at:String!;
    var updated_at:String!;
    public override init() {
    }
    
    func setChannel(dic:Dictionary<String,Any>) -> AnyObject {
        self.channel_id = dic["channel_id"] as! Int;
        self.channel_name = dic["channel_name"] as! String;
        self.channel_count = dic["channel_count"] as! Int;
        self.created_at = dic["created_at"] as! String;
        self.updated_at = dic["updated_at"] as! String;
        return self;
    }
}

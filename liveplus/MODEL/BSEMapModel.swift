//
//  BSEMapModel.swift
//  liveplus
//
//  Created by BSEtec on 14/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MapKit

class BSEMapModel: NSObject, MKAnnotation {
    let title: String?
    let broadcast_id: Int!
    let user_id: Int!
    let broadcast_status: String!
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, broadcast_id: Int, user_id: Int, broadcast_status: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.broadcast_id = broadcast_id
        self.user_id = user_id
        self.broadcast_status = broadcast_status
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}

//
//  BSEUserModel.swift
//  liveplus
//
//  Created by BSEtec on 12/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEUserModel: NSObject {
    var access_token:String!;
    var country:String!;
    var state:String!;
    var email:String!;
    var username:String!;
    var first_name:String!;
    var last_name:String!;
    var follower_count:Int!;
    var following_count:Int!;
    var broadcast_count:Int!;
    var group_count:Int!;
    var user_id:Int!;
    var is_notify:Int!;
     var is_block:Int!;
      var blockedbyme:Int!;
    var is_private:Int!;
    var auto_invite:Int!;
    var role:Int!;
    var user_follow_status:Int!;
    var is_requested:Int!;
    var status:Int!;
    var is_broadcast_id:Int!;
    public override init() {
    }
    
    func setUserData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["id"] as! Int;
        self.access_token = Dic["access_token"] as! String;
        self.country = Dic["country"] as! String;
        self.state = Dic["state"] as! String;
        self.email = Dic["email"] as! String;
        self.username = Dic["username"] as! String;
        self.first_name = Dic["first_name"] as! String;
        self.last_name = Dic["last_name"] as! String;
        
        if Dic["follower_count"] is NSNumber {
            self.follower_count = Dic["follower_count"] as! Int
        } else {
            let f_count:String = Dic["follower_count"] as! String;
            self.follower_count = Int(f_count)
        }
        if Dic["following_count"] is NSNumber {
            self.following_count = Dic["following_count"] as! Int
        } else {
            let f_count:String = Dic["following_count"] as! String;
            self.following_count = Int(f_count)
        }
        
        if Dic["group_count"] is NSNumber {
            self.group_count = Dic["group_count"] as! Int
        } else {
            let f_count:String = Dic["group_count"] as! String;
            self.group_count = Int(f_count)
        }
        
        if Dic["broadcast_count"] is NSNumber {
            self.broadcast_count = Dic["broadcast_count"] as! Int
        } else {
            let f_count:String = Dic["broadcast_count"] as! String;
            self.broadcast_count = Int(f_count)
        }
        
        if Dic["is_notify"] is NSNumber {
            self.is_notify = Dic["is_notify"] as! Int
        } else {
            let f_count:String = Dic["is_notify"] as! String;
            self.is_notify = Int(f_count)
        }
        
        if Dic["is_private"] is NSNumber {
            self.is_private = Dic["is_private"] as! Int
        } else {
            let f_count:String = Dic["is_private"] as! String;
            self.is_private = Int(f_count)
        }
        
        if Dic["auto_invite"] is NSNumber {
            self.auto_invite = Dic["auto_invite"] as! Int
        } else {
            let f_count:String = Dic["auto_invite"] as! String;
            self.auto_invite = Int(f_count)
        }
        
        if Dic["role"] is NSNumber {
            self.role = Dic["role"] as! Int
        } else {
            let f_count:String = Dic["role"] as! String;
            self.role = Int(f_count)
        }
        

        if let _ = Dic["is_broadcast_id"] {
            if Dic["is_broadcast_id"] is NSNumber {
               self.is_broadcast_id = Dic["is_broadcast_id"] as! Int;
            } else {
                let f_count:String = Dic["is_broadcast_id"] as! String;
                self.is_broadcast_id = Int(f_count)
            }
        } else {
            self.is_broadcast_id = 0;
        }
        return self;
    }
    
    func setProfileData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["user_id"] as! Int;
        self.access_token = Dic["access_token"] as! String;
        self.country = Dic["country"] as! String;
        self.state = Dic["state"] as! String;
        self.email = Dic["email"] as! String;
        self.username = Dic["username"] as! String;
        self.first_name = Dic["first_name"] as! String;
        self.last_name = Dic["last_name"] as! String;
        if Dic["follower_count"] is NSNumber {
            self.follower_count = Dic["follower_count"] as! Int
        } else {
            let f_count:String = Dic["follower_count"] as! String;
            self.follower_count = Int(f_count)
        }
        if Dic["following_count"] is NSNumber {
            self.following_count = Dic["following_count"] as! Int
        } else {
            let f_count:String = Dic["following_count"] as! String;
            self.following_count = Int(f_count)
        }
        if Dic["group_count"] is NSNumber {
            self.group_count = Dic["group_count"] as! Int
        } else {
            let f_count:String = Dic["group_count"] as! String;
            self.group_count = Int(f_count)
        }
        if Dic["broadcast_count"] is NSNumber {
            self.broadcast_count = Dic["broadcast_count"] as! Int
        } else {
            let f_count:String = Dic["broadcast_count"] as! String;
            self.broadcast_count = Int(f_count)
        }
        if Dic["is_notify"] is NSNumber {
            self.is_notify = Dic["is_notify"] as! Int
        } else {
            let f_count:String = Dic["is_notify"] as! String;
            self.is_notify = Int(f_count)
        }
        
        if Dic["is_block"] is NSNumber {
            self.is_block = Dic["is_block"] as! Int
        } else {
            let f_count:String = Dic["is_block"] as! String;
            self.is_block = Int(f_count)
        }
        
        if Dic["blockedbyme"] is NSNumber {
            self.blockedbyme = Dic["blockedbyme"] as! Int
        } else {
            let f_count:String = Dic["blockedbyme"] as! String;
            self.blockedbyme = Int(f_count)
        }
        
        if Dic["is_private"] is NSNumber {
            self.is_private = Dic["is_private"] as! Int
        } else {
            let f_count:String = Dic["is_private"] as! String;
            self.is_private = Int(f_count)
        }
        
        if Dic["auto_invite"] is NSNumber {
            self.auto_invite = Dic["auto_invite"] as! Int
        } else {
            let f_count:String = Dic["auto_invite"] as! String;
            self.auto_invite = Int(f_count)
        }
        
        if Dic["role"] is NSNumber {
            self.role = Dic["role"] as! Int
        } else {
            let f_count:String = Dic["role"] as! String;
            self.role = Int(f_count)
        }
        
        if Dic["is_follow"] is NSNumber {
            self.user_follow_status = Dic["is_follow"] as! Int
        } else {
            let f_count:String = Dic["is_follow"] as! String;
            self.user_follow_status = Int(f_count)
        }
        
        if Dic["is_requested"] is NSNumber {
            self.is_requested = Dic["is_requested"] as! Int
        } else {
            let f_count:String = Dic["is_requested"] as! String;
            self.is_requested = Int(f_count)
        }

        if let _ = Dic["is_broadcast_id"] {
            if Dic["is_broadcast_id"] is NSNumber {
                self.is_broadcast_id = Dic["is_broadcast_id"] as! Int;
            } else {
                let f_count:String = Dic["is_broadcast_id"] as! String;
                self.is_broadcast_id = Int(f_count)
            }
        } else {
            self.is_broadcast_id = 0;
        }
        return self;
    }
    
    func setUserListData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["user_id"] as! Int;
        self.country = Dic["country"] as! String;
        self.state = Dic["state"] as! String;
        self.email = Dic["email"] as! String;
        self.username = Dic["username"] as! String;
        self.first_name = Dic["first_name"] as! String;
        self.last_name = Dic["last_name"] as! String;
        if Dic["follower_count"] is NSNumber {
            self.follower_count = Dic["follower_count"] as! Int
        } else {
            let f_count:String = Dic["follower_count"] as! String;
            self.follower_count = Int(f_count)
        }
        if Dic["following_count"] is NSNumber {
            self.following_count = Dic["following_count"] as! Int
        } else {
            let f_count:String = Dic["following_count"] as! String;
            self.following_count = Int(f_count)
        }
        
        if Dic["broadcast_count"] is NSNumber {
            self.broadcast_count = Dic["broadcast_count"] as! Int
        } else {
            let f_count:String = Dic["broadcast_count"] as! String;
            self.broadcast_count = Int(f_count)
        }
        
        if Dic["group_count"] is NSNumber {
            self.group_count = Dic["group_count"] as! Int
        } else {
            let f_count:String = Dic["group_count"] as! String;
            self.group_count = Int(f_count)
        }
        if Dic["is_notify"] is NSNumber {
            self.is_notify = Dic["is_notify"] as! Int
        } else {
            let f_count:String = Dic["is_notify"] as! String;
            self.is_notify = Int(f_count)
        }
        
        if Dic["is_private"] is NSNumber {
            self.is_private = Dic["is_private"] as! Int
        } else {
            let f_count:String = Dic["is_private"] as! String;
            self.is_private = Int(f_count)
        }
        if let _ = Dic["is_auto_group_invite"] {
            if Dic["is_auto_group_invite"] is NSNumber {
                self.auto_invite = Dic["is_auto_group_invite"] as! Int
            } else {
                let f_count:String = Dic["is_auto_group_invite"] as! String;
                self.auto_invite = Int(f_count)
            }
        }
        
        if Dic["role"] is NSNumber {
            self.role = Dic["role"] as! Int
        } else {
            let f_count:String = Dic["role"] as! String;
            self.role = Int(f_count)
        }
        
        if Dic["user_follow_status"] is NSNumber {
            self.user_follow_status = Dic["user_follow_status"] as! Int
        } else {
            let f_count:String = Dic["user_follow_status"] as! String;
            self.user_follow_status = Int(f_count)
        }
    
        return self;
    }
    func setRequestListData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["id"] as! Int;
        self.username = Dic["name"] as! String;
        if let _ = Dic["connect_id"] {
            self.user_follow_status = Dic["connect_id"] as! Int;
        }
        self.is_requested = 0;
        return self;
    }
    
    func setFollowListData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["user_id"] as! Int;
        self.username = Dic["name"] as! String;
        self.status = 0;
        return self;
    }
    
    func setGroupViewData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.user_id = Dic["user_id"] as! Int;
        self.username = Dic["user_name"] as! String;
        self.status = 0;
        return self;
    }
    
}

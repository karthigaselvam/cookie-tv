//
//  BSEBroadcastModel.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEBroadcastModel: NSObject {
    var broadcast_id:Int!;
    var broadcast_name:String!;
    var broadcast_status:String!;
    var author_id:Int!;
    var author_name:String!;
    var author_country:String!;
    var latitude:String!;
    var longitude:String!;
    var like_count:Int!;
    var viewer_count:Int!;
    var created_at:String!;
    
    public override init() {
    }
    
    func setBroadcastData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.broadcast_id = Dic["broadcast_id"] as! Int;
        self.broadcast_name = Dic["broadcast_name"] as! String;
        self.broadcast_status = Dic["broadcast_status"] as! String;
        self.author_id = Dic["author_id"] as! Int;
        if let _ = Dic["author_name"] {
            self.author_name = Dic["author_name"] as! String;
        }
       // self.author_country = Dic["author_country"] as! String;
        self.latitude = Dic["latitude"] as! String;
        self.longitude = Dic["longitude"] as! String;
        self.like_count = Dic["like_count"] as! Int;
        self.viewer_count = Dic["viewer_count"] as! Int;
        self.created_at = Dic["created_at"] as! String;
        return self;
    }
}

//
//  BSENotificationModel.swift
//  liveplus
//
//  Created by BseTec on 2/24/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSENotificationModel: NSObject {
    var notification_id:Int!;
    var title:String!;
    var type:String!;
    var objetc_id:Int!;
    var message:String!;
    var status:String!;
      var sender_id:Int!;
    var authorname:String!;
    var profile_pic:String!;
    var date:String!;
    
    public override init() {
    }
    
    func getNotificationList(Dic:Dictionary<String,Any>) -> AnyObject {
        self.title = Dic["title"] as! String;
        self.message = Dic["message"] as! String;
         self.authorname = Dic["author_name"] as! String;
         self.profile_pic = Dic["profile_pic"] as! String;
         self.date = Dic["created_at"] as! String;
         self.type = Dic["type"] as! String;
         if let _ = Dic["is_broadcast_id"] {
            self.status = Dic["status"] as! String;
         } else {
            self.status = "stop";
        }
        
        if Dic["object_id"] is NSNumber {
            self.objetc_id = Dic["object_id"] as! Int
        } else {
            let f_count:String = Dic["object_id"] as! String;
            self.objetc_id = Int(f_count)
        }
        
        if Dic["notification_id"] is NSNumber {
            self.notification_id = Dic["notification_id"] as! Int
        } else {
            let f_count:String = Dic["notification_id"] as! String;
            self.notification_id = Int(f_count)
        }
        if Dic["sender_id"] is NSNumber {
            self.sender_id = Dic["sender_id"] as! Int
        } else {
            let f_count:String = Dic["sender_id"] as! String;
            self.sender_id = Int(f_count)
        }
        return self
    }
}

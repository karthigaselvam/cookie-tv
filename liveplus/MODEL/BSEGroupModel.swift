//
//  BSEGroupModel.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEGroupModel: NSObject {
    var group_id:Int!;
    var group_name:String!;
    var owner_id:Int!;
    var owner_username:String!;
    var type:String!;
    var member_details = [AnyObject]();
    var broadcast_count:Int!;
    var status:Int!;
    public override init() {
    }
    
    func setJoinedData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.group_id = Dic["group_id"] as! Int;
        self.group_name = Dic["group_name"] as! String;
        if let _ = Dic["owner_id"] {
            self.owner_id = Dic["owner_id"] as! Int;
            self.type = Dic["type"] as! String;
            self.broadcast_count = Dic["broadcast_count"] as! Int;
        }
        return self
    }
    
    func setViewData(Dic:Dictionary<String,Any>) -> AnyObject {
        self.group_id = Dic["group_id"] as! Int;
        self.group_name = Dic["group_name"] as! String;
        self.owner_id = Dic["group_created_by"] as! Int;
        self.owner_username = Dic["owner_username"] as! String;
        self.type = Dic["type"] as! String;
        
        if let _ = Dic["member_details"] {
            let result : Array<Any> = Dic["member_details"] as! Array<Any>;
            if(result.count>0) {
                for dic in result {
                    let userModel : BSEUserModel = BSEUserModel().setGroupViewData(Dic: dic as! Dictionary<String, Any>) as! BSEUserModel;
                    self.member_details.append(userModel as AnyObject);
                }
            }
        }
        
        return self
    }
}

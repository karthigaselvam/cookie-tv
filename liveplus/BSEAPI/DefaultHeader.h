//
//  DefaultHeader.h
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#ifndef DefaultHeader_h
#define DefaultHeader_h

#define iPhonex ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhonexs_max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ?  CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

#define SOCKETURL @"wss://cookie-tv.net:3000/one2many"
#define APIURL @"https://cookie-tv.net/api/public/index.php/"
#define SHAREURL @"http://cookie-tv.net/media/"
#define VIDEOURL @"http://cookie-tv.net/media/"
#define METHODGETBROADCASTDETAILS @"broadcast/details"
#define METHODGETBROADCASTVIEWERS @"broadcast/participants"
#define METHODBROADCASTACTION @"broadcast/actionbt"
#define METHODREPORTBROADCAST @"broadcast/report"
#endif /* DefaultHeader_h */




//
//  apiservice.swift
//  liveplus
//
//  Created by BSEtec on 09/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SystemConfiguration

class apiservice: NSObject {
    // api tools
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func convertToDictionary(response: Data) -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: response, options: []) as? [String: Any];
        } catch {
            print(error.localizedDescription);
        }
        return nil;
    }
    
    func apiGetRequest (params: String, withMethod method: String, andCompletionBlock successBlock: @escaping ((AnyObject) -> Void), andFailureBlock failBlock: @escaping ((AnyObject) -> Void)) {
        if(!isInternetAvailable()) {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
            
            
    failBlock(NSError(domain: "com.cookietv.net", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) ]));
            
        }
        let originalString = APIURL + method + "?" + params;
        
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                failBlock(error as AnyObject);
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        let Dic = self.convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: method, result: Dic!);
                        successBlock(output as AnyObject);
                    } else {
                        
    failBlock(NSError(domain: "com.cookietv.net", code: httpResponse.statusCode, userInfo: [
        NSLocalizedDescriptionKey:
        "Error From API".localized(lang: getBSEValue(_keyname: CURRENTLANG)) ]));
                    }
                } else {
                    
                    failBlock(NSError(domain: "com.cookietv.net", code: 2, userInfo: [
                        NSLocalizedDescriptionKey:
                            "Error From API".localized(lang: getBSEValue(_keyname: CURRENTLANG))]));
                }
                
            }
        })
        task.resume();
    }
    
    func apiPostRequest (params: String, withMethod method: String, andCompletionBlock successBlock: @escaping ((AnyObject) -> Void), andFailureBlock failBlock: @escaping ((AnyObject) -> Void)) {
        if(!isInternetAvailable()) {
           
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
            
failBlock(NSError(domain: "com.cookietv.net", code: 1, userInfo: [ NSLocalizedDescriptionKey: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) ]));
        }
        var request = URLRequest(url: URL(string: APIURL + method)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        request.httpMethod = "POST"
        request.httpBody = params.data(using: .utf8)
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                failBlock(error as AnyObject);
            } else {
                let Dic = self.convertToDictionary(response: data!);
                let output = apiparser().sperateParams(method: method, result: Dic!);
                successBlock(output as AnyObject);
            }
        })
        task.resume();
    }
    
}

//
//  apiparser.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class apiparser: NSObject {
    func sperateParams(method: String,result: Dictionary<String, Any>) -> AnyObject {
        if(method == METHODCHANNELLIST) {
            return getChannelResponse(json: result);
        } else if (method == METHODLOGIN || method == METHODFACEBOOK || method == METHODREGISTER || method == METHODEDITPROFILE) {
            return getLoginResponse(json: result);
        } else if (method == METHODFORGOT) {
            return getForgotResponse(json: result);
        } else if(method == METHODCHANGEPASSWORD) {
             return getChangePasswordResponse(json: result);
        } else if(method == METHODGETPROFILE) {
            return getProfileResponse(json: result);
        } else if (method == METHODSEARCHPEOPLE) {
            return getUserListResponse(json: result);
        } else if (method == METHODBROADCASTLIST || method == METHODBROADCASTSUGGESTION || method==METHODBROADCASTLIVE || method == METHODRECENTLYWATCHED
            || method == METHODBROADCASTLISTER || method == METHODGROUPHISTORY) {
            return getBroadcastListResponse(json: result);
        } else if (method == METHODFOLLOWREQUEST || method == METHODBLOCKEDUSERS) {
            return getFollowRequestListResponse(json: result);
        } else if (method == METHODJOINEDGROUP || method == METHODMYGROUP) {
            return getJoinedGroupListResponse(json: result);
        } else if (method == METHODFOLLOWERLIST || method == METHODFOLLOWINGLIST) {
            return getFollowerListResponse(json: result);
        } else if (method == METHODADDGROUP) {
            return getCreateGroupResponse(json: result);
        } else if (method == METHODGROUPVIEW) {
            return getGroupViewResponse(json: result);
        } else if (method == METHODADDBROADCAST) {
            return getCreateBroadcastResponse(json: result);
        } else if (method==METHODFOLLOWACTION || method==METHODBROADCASTSETTINGS || method==METHODFOLLOWREQUESTACTION || method == METHODGROUPDELETE || method==METHODLEFTGROUP) {
            return getCommonResponse(json: result);
        } else if (method == METHODNOTIFICATIONLIST) {
            return getNotificationList(json: result);
        }
        else if (method==METHODNOTIFICATIONUPDATE) {
            return getNotificationListCountUpdate(json: result);
        }
        return result as AnyObject;
    }
    
    func getCommonResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("login response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = json["status_message"] as! String;
        }
        return commonModel;
    }
    
    
    func getLoginResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("login response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            let userModel : BSEUserModel = BSEUserModel().setUserData(Dic: json["details"] as! Dictionary<String, Any>) as! BSEUserModel;
            print("user data",userModel);
            commonModel.temparray.append(userModel as AnyObject);
            
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = json["status_message"] as! String;
        }
        return commonModel;
    }
    
    func getForgotResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("forgot response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
        } else {
            commonModel.status = "false";
        }
        commonModel.status_Msg = json["status_message"] as! String;
        return commonModel;
    }
    
    func getChangePasswordResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("forgot response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.return_id = json["access_token"] as? String
        } else {
            commonModel.status = "false";
        }
        commonModel.status_Msg = json["status_message"] as! String;
        return commonModel;
    }
    
    func getUserListResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("channel listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for dic in result {
                        let userModel : BSEUserModel = BSEUserModel().setUserListData(Dic: dic as! Dictionary<String, Any>) as! BSEUserModel;
                        commonModel.temparray.append(userModel as AnyObject);
                    }
                }
                
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    func getProfileResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("get profile data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["details"] {
                let result : Dictionary<String, Any> = json["details"] as! Dictionary<String, Any>;
                let userModel : BSEUserModel = BSEUserModel().setProfileData(Dic: result) as! BSEUserModel;
                commonModel.temparray.append(userModel as AnyObject)
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    //MARK: - Group
    func getJoinedGroupListResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for dic in result {
                        let userModel : BSEGroupModel = BSEGroupModel().setJoinedData(Dic: dic as! Dictionary<String, Any>) as! BSEGroupModel;
                        commonModel.temparray.append(userModel as AnyObject);
                    }
                }
                
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    func getCreateGroupResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            if json["group_id"] is NSNumber {
                let group_id:Int = json["group_id"] as! Int;
                commonModel.return_id = String(group_id)
            } else {
                commonModel.return_id = json["group_id"] as! String
            }
            commonModel.status = "true";
          
              commonModel.status_Msg =  "Group Created".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            
        } else {
            commonModel.status_Msg = "Error From API".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            commonModel.status = "false";
        }
        return commonModel;
    }
    
    
    func getGroupViewResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            if let _ = json["result"] {
                let result : Dictionary<String, Any> = json["result"] as! Dictionary<String, Any>
                if(result.count>0) {
                    let groupModel : BSEGroupModel = BSEGroupModel().setViewData(Dic: result) as! BSEGroupModel;
                    commonModel.temparray.append(groupModel as AnyObject);
                    
                }
                
            }
           commonModel.status_Msg =  "Group Created".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            
        } else {
            commonModel.status_Msg = "Error From API".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            commonModel.status = "false";
        }
        return commonModel;
    }
    
    
    
    // MARK: - follow
    
    func getFollowRequestListResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for dic in result {
                        let userModel : BSEUserModel = BSEUserModel().setRequestListData(Dic: dic as! Dictionary<String, Any>) as! BSEUserModel;
                        commonModel.temparray.append(userModel as AnyObject);
                    }
                }
                
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    func getFollowerListResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for dic in result {
                        let userModel : BSEUserModel = BSEUserModel().setFollowListData(Dic: dic as! Dictionary<String, Any>) as! BSEUserModel;
                        commonModel.temparray.append(userModel as AnyObject);
                    }
                }
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    //MARK: broadcast
    
    func getCreateBroadcastResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("response = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            if json["broadcast_id"] is NSNumber {
                let group_id:Int = json["broadcast_id"] as! Int;
                commonModel.return_id = String(group_id)
            } else {
                commonModel.return_id = json["broadcast_id"] as! String
            }
            commonModel.status = "true";
         commonModel.status_Msg = "Broadcast created".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            
        } else {
            commonModel.status_Msg = "Error From API".localized(lang: getBSEValue(_keyname: CURRENTLANG))
            commonModel.status = "false";
        }
        return commonModel;
    }
    
    func getBroadcastListResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("getBroadcastListResponse listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            
            if(( json["unread_count"]) != nil)
             {
            if json["unread_count"] is NSNumber {
                let group_id:Int = json["unread_count"] as! Int;
                commonModel.return_id = String(group_id)
            } else {
                commonModel.return_id = json["unread_count"] as! String
            }
            }
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for Dic in result {
                        let bModel : BSEBroadcastModel = BSEBroadcastModel().setBroadcastData(Dic: Dic as! Dictionary<String, Any>) as! BSEBroadcastModel;
                        commonModel.temparray.append(bModel as AnyObject);
                    }
                }
                
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    func getChannelResponse(json:Dictionary<String,Any>) -> BSECommonModel {
        print("channel listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for Dic in result {
                        let channelModel : BSEChannelModel = BSEChannelModel().setChannel(dic: Dic as! Dictionary<String, Any>) as! BSEChannelModel;
                        commonModel.temparray.append(channelModel as AnyObject);
                    }
                }
                
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    //MARK: - notification parser
    
    func getNotificationList(json:Dictionary<String,Any>) -> BSECommonModel {
        print("listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            
            if(( json["unread_count"]) != nil)
            {
                if json["unread_count"] is NSNumber {
                    let group_id:Int = json["unread_count"] as! Int;
                    commonModel.return_id = String(group_id)
                } else {
                    commonModel.return_id = json["unread_count"] as! String
                }
            }
            if let _ = json["result"] {
                let result : Array<Any> = json["result"] as! Array<Any>;
                if(result.count>0) {
                    for dic in result {
                        let userModel : BSENotificationModel = BSENotificationModel().getNotificationList(Dic: dic as! Dictionary<String, Any>) as! BSENotificationModel;
                        commonModel.temparray.append(userModel as AnyObject);
                    }
                }
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    
    func getNotificationListCountUpdate(json:Dictionary<String,Any>) -> BSECommonModel {
        print("listing data = ",json);
        let commonModel : BSECommonModel = BSECommonModel();
        if (json["status"] as? Bool)!{
            commonModel.status = "true";
            commonModel.status_Msg = "success";
            if(( json["unread_count"]) != nil)
            {
                if json["unread_count"] is NSNumber {
                    let group_id:Int = json["unread_count"] as! Int;
                    commonModel.return_id = String(group_id)
                } else {
                    commonModel.return_id = json["unread_count"] as! String
                }
            }
        } else {
            commonModel.status = "false";
            commonModel.status_Msg = "failure";
        }
        return commonModel;
    }
    
    
    
    
    
    
}

//
//  config.swift
//  liveplus
//
//  Created by BSEtec on 09/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//
import UIKit

public func getBSEValue(_keyname: String) -> String {
    return UserDefaults.standard.object(forKey: _keyname) as? String ?? "";
}

public func setBSEValue(_keyname: String, _value: String) -> Void {
    UserDefaults.standard.set(_value, forKey: _keyname);
}

public func removeBSEValue(_keyname: String) -> Void {
    UserDefaults.standard.removeObject(forKey: _keyname)
}



let APIURL = "https://cookie-tv.net/api/public/index.php/";
let BROADCASTIMGURL = "https://cookie-tv.net/api/public/index.php/image/broadcast/";
let BROADCASTVIDEOURL = "https://cookie-tv.net/api/public/index.php/video/broadcast/";
let USERIMGURL = "https://cookie-tv.net/api/public/index.php/image/user/";
let PRIVACYURL = "https://cookie-tv.net/";
let HELPURL = "https://cookie-tv.net/";

let METHODUPLOAD = "fileupload"
let METHODLOGIN = "user/login";
let METHODREGISTER = "user/register";
let METHODFORGOT = "user/forgotpassword";
let METHODEDITPROFILE = "user/edit_profile";
let METHODCHANGEPASSWORD = "user/change_password";
let METHODBROADCASTSETTINGS = "user/update_broadcast_settings";
let METHODFACEBOOK = "user/oauth";
let METHODSEARCHPEOPLE = "user/user_list";
let METHODGETPROFILE = "user/profileinfo"

let METHODFOLLOWACTION = "user/follow_unfollow_block"
let METHODFOLLOWREQUEST = "admin/getrequestlist";
let METHODBLOCKEDUSERS = "user/block_list";
let METHODFOLLOWREQUESTACTION = "admin/postresponsestatus";
let METHODFOLLOWERLIST = "user/followers_list"
let METHODFOLLOWINGLIST = "user/following_list"

let METHODADDBROADCAST = "broadcast/aebroadcast";
let METHODBROADCASTLIST = "broadcast/home";
let METHODBROADCASTLISTER = "broadcast/listing";
let METHODCHANNELLIST = "broadcast/channels";
let METHODBROADCASTSUGGESTION = "broadcast/suggested"
let METHODBROADCASTLIVE = "broadcast/live"
let METHODRECENTLYWATCHED = "broadcast/myrecentbt"

let METHODGROUPVIEW = "admin/groupview"
let METHODJOINEDGROUP = "broadcast/membergroup";
let METHODLEFTGROUP = "broadcast/leftgroup";
let METHODMYGROUP = "broadcast/mygroup";
let METHODADDGROUP = "broadcast/aegroup";
let METHODGROUPDELETE = "broadcast/group_delete";
let METHODGROUPHISTORY = "user/groupbroadcastlist";

let METHODNOTIFICATIONLIST = "user/notifications";
let METHODNOTIFICATIONUPDATE = "user/notifications_update";

let USERID = "userid" ;
let ACCESSTOKEN = "access_token";
let USERNAME = "username";
let FIRSTNAME = "first_name";
let LASTNAME = "last_name";
let COUNTRY = "country";
let STATE = "state";
let EMAIL = "email";
let FOLLOWERCOUNT = "follower_count";
let FOLLOWINGCOUNT = "following_count";
let GROUPCOUNT = "group_count";
let BROADCASTCOUNT = "broadcast_count";
let ISNOTIFY = "is_notify";
let ISPRIVATE = "is_private";
let AUTOINVITE = "auto_invite";
let ROLE = "role";
let ISBROADCASTID = "is_broadcast_id";
let DEVICETOKEN = "device_token";
let CAMERAPERMISSON = "camera_usage"
let MICPERMISSON = "mic_usage"
let LOCATIONPERMISSON = "location_usage"

let LOGINFROM = "is_loginFrom";
let CURRENTLANG = "Current_Language";

// method will be here



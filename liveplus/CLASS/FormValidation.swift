//
//  FormValidation.swift
//  Liveplus
//
//  Created by BseTec on 2/10/18.
//  Copyright © 2018 Pothi. All rights reserved.
//

import UIKit

class FormValidation: NSObject {
    
    func notEmpty(msg:String) -> Bool {
        if(msg.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased().count>0)
        {
            return true
        }
        return false
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func ValidateEqualString(str1:String,str2:String) -> Bool
    {
        if(str1.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased().count>0 && str2.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased().count>0)
        {
            if str1 == str2
            {
                return true
            }
        }
        return false
    }
    func validateStringContainsAlphabetsOnly(input: String) -> Bool {
        let emailRegEx = "[A-Za-z]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: input)
    }
    
    func passwordLength(msg:String,totalCount:Int) -> Bool {
    if msg.count >= totalCount
    {
        return true
    }
     return false
    }
    
}



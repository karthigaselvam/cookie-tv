//
//  AppDelegate.swift
//  liveplus
//
//  Created by BSEtec on 09/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation
import CoreLocation
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate, CLLocationManagerDelegate ,GIDSignInDelegate {

    var window: UIWindow?
    var permission : BSEPermissionVC?
    var tabcontroller : UITabBarController?;
    var navController : UINavigationController?;


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
  
FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = "925220581500-ggeo75i9ngg9q6kn0clktj6o5nu9atbq.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self

        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        application.registerForRemoteNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
         UITextView.appearance().linkTextAttributes = [ NSAttributedStringKey.foregroundColor.rawValue: UIColor.red]
        
        initConfig();
        
        if (getBSEValue(_keyname: CURRENTLANG) ).isEmpty {
        setBSEValue(_keyname:CURRENTLANG, _value:"en");
        print( getBSEValue(_keyname: CURRENTLANG))
        }
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if(!getBSEValue(_keyname: USERID).isEmpty) {
            login();
        } else {
            logout();
        }
        
        self.window?.makeKeyAndVisible()

        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {

      if (url.scheme == "925220581500-ggeo75i9ngg9q6kn0clktj6o5nu9atbq.apps.googleusercontent.com")
      {
        return GIDSignIn.sharedInstance().handle(url,
                                                    sourceApplication: sourceApplication,
                                                    annotation: annotation)

        }

        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
      
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            setBSEValue(_keyname: CAMERAPERMISSON, _value: "1")
        } else  if AVCaptureDevice.authorizationStatus(for: .video) ==  .notDetermined {
            setBSEValue(_keyname: CAMERAPERMISSON, _value: "0")
        } else  if AVCaptureDevice.authorizationStatus(for: .video) ==  .denied {
            setBSEValue(_keyname: CAMERAPERMISSON, _value: "2")
        }
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            setBSEValue(_keyname: MICPERMISSON, _value: "1")
        case AVAudioSessionRecordPermission.denied:
            setBSEValue(_keyname: MICPERMISSON, _value: "2")
        case AVAudioSessionRecordPermission.undetermined:
            setBSEValue(_keyname: MICPERMISSON, _value: "0")
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                setBSEValue(_keyname: LOCATIONPERMISSON, _value: "0")
            case .restricted:
                setBSEValue(_keyname: LOCATIONPERMISSON, _value: "2")
            case .denied:
                setBSEValue(_keyname: LOCATIONPERMISSON, _value: "2")
            case .authorizedAlways:
                setBSEValue(_keyname: LOCATIONPERMISSON, _value: "1")
            case .authorizedWhenInUse:
                setBSEValue(_keyname: LOCATIONPERMISSON, _value: "1")
            }
        } else {
            setBSEValue(_keyname: LOCATIONPERMISSON, _value: "2")
        }
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if let rootViewController = self.topViewControllerWithRootViewController(rootViewController: window?.rootViewController) {
            if (rootViewController.responds(to: Selector(("canRotate")))) {
                // Unlock landscape view orientations for this view controller if it is not currently being dismissed
                if !rootViewController.isBeingDismissed{
                    return .all
                }
            }
        }
        
        // Only allow portrait (standard behaviour)
        return .portrait
    }
    
    private func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
        if (rootViewController == nil) {
            return nil
        }
        if (rootViewController.isKind(of: UITabBarController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
        } else if (rootViewController.isKind(of: UINavigationController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
        } else if (rootViewController.presentedViewController != nil) {
            return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
        }
        return rootViewController
    }
    
    
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        setBSEValue(_keyname: DEVICETOKEN, _value: deviceTokenString)

        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
        if let notification = data["aps"] as? NSDictionary,
            let alert = notification["alert"] as? String {
            
            let alert = UIAlertController(title: "Notification", message: alert as String, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ignore", style: .default, handler: { _ in
                
            }))
            
            alert.addAction(UIAlertAction(title: "View", style: .default, handler: { _ in
                self.processNotification(dic: data as NSDictionary);
            }))
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
    }
    
    //MARK: custom functionabout
    
    func initConfig() -> Void {
        
    let config = MessageBarConfig.Builder()
            .withErrorColor(.red)
            .withSuccessColor(.green)
            .withTitleFont(UIFont(name: "Poppins-SemiBold", size: 16)!)
            .withMessageFont(UIFont(name: "Poppins-Regular", size: 14)!)
            .build()
        SwiftMessageBar.setSharedConfig(config)
    }
    
    func login() -> Void {
        
//        let v2 = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height-49, width: 100, height: 49))
//        v2.backgroundColor = UIColor.red
//        self.window!.addSubview(v2);
//
       
//        let v2 = UIView(frame: CGRect(x: 50, y: 50, width: 100, height: 50))
//        v2.backgroundColor = UIColor.black
//        window!.addSubview(v2);
        
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Main", bundle: nil)
        tabcontroller = mainView.instantiateViewController(withIdentifier: "main") as? UITabBarController;
        tabcontroller?.delegate = self;
        tabcontroller?.selectedIndex=0;
        self.window!.rootViewController = tabcontroller
    
        
       //  setupMiddleButton()
        
//        var mainVC: BSEHomeMainVC?
//        mainVC = BSEHomeMainVC(nibName:"BSEHomeMainVC",bundle:nil)
//        navController = UINavigationController.init(rootViewController: mainVC!)
//        navController?.isNavigationBarHidden=true
//        window!.rootViewController = navController

    }
   
    
    
    // MARK: - Actions
    @objc private func menuButtonAction(sender: UIButton) {
       // selectedIndex = 2
        tabcontroller?.selectedIndex = 2
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController == tabBarController.viewControllers?[2] {
            if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1") {
                return true
            } else {
                permission = BSEPermissionVC(nibName:"BSEPermissionVC",bundle:nil);
                permission?.modalPresentationStyle = .overCurrentContext
                if let window = self.window, let rootViewController = window.rootViewController {
                    var currentController = rootViewController
                    while let presentedController = currentController.presentedViewController {
                        currentController = presentedController
                    }
                    currentController.present(permission!, animated: false, completion: nil)
                    
                }
            }
            return false
        } else {
            return true
        }
    }
    
    func logout() -> Void {
        
        if window!.viewWithTag(101) != nil {
            window!.viewWithTag(101)?.removeFromSuperview()
        }
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("HomeMain"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("profileNotify"), object: nil)
      
        removeBSEValue(_keyname:USERID)
        removeBSEValue(_keyname:ACCESSTOKEN);
        removeBSEValue(_keyname:USERNAME);
        removeBSEValue(_keyname:FIRSTNAME);
        removeBSEValue(_keyname:LASTNAME);
        removeBSEValue(_keyname:COUNTRY);
        removeBSEValue(_keyname:STATE);
        removeBSEValue(_keyname:EMAIL);
        removeBSEValue(_keyname:FOLLOWERCOUNT);
        removeBSEValue(_keyname:FOLLOWINGCOUNT);
        removeBSEValue(_keyname:GROUPCOUNT);
        removeBSEValue(_keyname:ISNOTIFY);
        removeBSEValue(_keyname:ISPRIVATE);
        removeBSEValue(_keyname:AUTOINVITE);
        removeBSEValue(_keyname:ROLE);
         removeBSEValue(_keyname:LOGINFROM);
        
        var mainVC: BSELoginVC?
        mainVC = BSELoginVC(nibName:"BSELoginVC",bundle:nil);
        
//        let frame = UIScreen.main.bounds;
//        window = UIWindow(frame: frame);
        navController = UINavigationController.init(rootViewController: mainVC!);
        navController?.isNavigationBarHidden=true;
        window!.rootViewController = navController
        self.window?.makeKeyAndVisible()
        
        
        
    }
    
    func addBroadcast()->Void {
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        tabcontroller?.selectedIndex = 2;
      
        
    }
    
    func showAddCamera() -> Void {
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1") {
          addBroadcast();
            
//            let discover = BSELiveVC(nibName: "BSELiveVC", bundle: nil)
//            navController?.pushViewController(discover, animated: true)
            
        } else {
            
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) != nil {
                let foundView = window.viewWithTag(101)
                window.sendSubview(toBack: foundView!)
            }
            
           permission = BSEPermissionVC(nibName:"BSEPermissionVC",bundle:nil);
            permission?.modalPresentationStyle = .overCurrentContext
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(permission!, animated: false, completion: {
                    
                })
            }
        }
        
    }
    
    //MARK: Push Notification Function
    func processNotification(dic:NSDictionary) -> Void {
        
        if(!getBSEValue(_keyname: USERID).isEmpty) {
            tabcontroller?.selectedIndex=0;
            NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"push_notification"])
            print("Push notification data: \(dic)")
        }
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            self.window?.endEditing(true)
            print("\(error.localizedDescription)")
       
            
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: error.localizedDescription , type: .error);
            
        } else {
           
            let first_name = (user.profile.givenName != nil) ? user.profile.givenName as! String : "";
            let last_name = (user.profile.familyName != nil) ? user.profile.familyName as! String : "";
            
            
        NotificationCenter.default.post(name: Notification.Name("twitterOauthCallback"), object: nil, userInfo: ["type":"google", "id":user.userID, "name":user.profile.name , "email":user.profile.email,"first_name":first_name,"last_name":last_name,"image": user.profile.imageURL(withDimension: 200)])
            // Perform any operations on signed in user here.
            
            // ...
             GIDSignIn.sharedInstance().signOut()
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}


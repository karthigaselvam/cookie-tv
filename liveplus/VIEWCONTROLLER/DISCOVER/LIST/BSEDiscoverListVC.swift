//
//  BSEDiscoverListVC.swift
//  liveplus
//
//  Created by BSEtec on 14/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit


protocol BSEDiscoverListVCDelegate:class {
    func notifySelect(type:String,value:String);
    func profileDetails(userModel:BSEUserModel);
    func broadcastDetails(model:BSEBroadcastModel);
}

class BSEDiscoverListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var page_type: String!;
    var page_keyword: String!;
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var loader : UIActivityIndicatorView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var isFirst: Bool!;
    var keywordChanged: Bool!;
    var currentpage: Int!;
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
     var refreshControl: UIRefreshControl!
    weak var delegate: BSEDiscoverListVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(keywordChanged) {
            self.searchApi();
        }
    }
    
    // MARK: custom function
    func initFields() -> Void {
        isFirst = true;
        keywordChanged = true;
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        self.searchApi();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        


        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
        
        
        
    }
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.searchApi();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                   // self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    if(self.page_type=="broadcast") {
                    self.getSearchBroadcast();
                                        } else if(self.page_type=="channel"){
                                            self.getSearchChannel();
                                        } else if(self.page_type=="people"){
                                            self.getSearchPeople();
                                        };
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }

    func searchApi() -> Void {
        keywordChanged = false;
        self.isFirstTime = true;
        self.currentpage = 1;
        if(page_type=="broadcast") {
            getSearchBroadcast();
        } else if(page_type=="channel"){
            getSearchChannel();
        } else if(page_type=="people"){
            getSearchPeople();
        }
    }
    
    func setLoader() -> Void {
        loader?.isHidden = false;
    }
    
    func hideLoader() -> Void {
        loader?.isHidden = true;
    }
    
    func getSearchBroadcast() -> Void {
        isAPILoading = true;
        let parameter = "type=1&&page_no="+String(currentpage)+"&&keyword="+page_keyword+"&&hashtag=&&is_home_search=1"+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        if(!apiservice().isInternetAvailable()) {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODBROADCASTLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
                
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODBROADCASTLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
         SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        
                        self.tblView?.reloadData();
                        self.hideLoader();
                        
//                        if(self.isFirst) {
//                            self.tblView?.es.startPullToRefresh();
//                            self.isFirst = false;
//                        }
                        
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    func getSearchChannel() -> Void {
        isAPILoading = true;
        let parameter = "page_no="+String(currentpage)+"&&keyword="+page_keyword+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
         SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODCHANNELLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                       
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODCHANNELLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    func getSearchPeople() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&page_no="+String(currentpage)+"&&keyword="+page_keyword+"&&sort="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODSEARCHPEOPLE + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                       
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODSEARCHPEOPLE, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        let bidentifier = "BroadcastCell"
        let uidentifier = "UserCell"
        let cidentifier = "ChannelCell"
        
        if(page_type=="broadcast") {
            var cell: BroadcastCell! = tableView.dequeueReusableCell(withIdentifier: bidentifier) as? BroadcastCell
            if cell == nil {
                tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "BroadcastCellRTL" : "BroadcastCell", bundle: nil), forCellReuseIdentifier: bidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: bidentifier) as? BroadcastCell;
            }
            cell .setDefaultValue(model: tableData[indexPath.row] as! BSEBroadcastModel)
            cell.broadcast_user_btn.tag = indexPath.row;
            cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToProfile(_:)), for: .touchUpInside)
            if indexPath.row % 2 == 0 {
                cell.backgroundColor = .white
            } else {
                cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if (page_type == "people") {
            var cell: UserCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell
            if cell == nil {
               tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "UserCellRTL" : "UserCell", bundle: nil), forCellReuseIdentifier: uidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell;
            }
            cell.setDefaultValue(model: tableData[indexPath.row] as! BSEUserModel)
            if indexPath.row % 2 == 0 {
                 cell.backgroundColor = .white
            } else {
                cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.follow_action?.tag = indexPath.row;
            cell.follow_action?.addTarget(self, action: #selector(self.userAction(_:)), for: .touchUpInside)
            return cell
        } else {
            var cell: ChannelCell! = tableView.dequeueReusableCell(withIdentifier: cidentifier) as? ChannelCell
            if cell == nil {
                tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "ChannelCellRTL" : "ChannelCell", bundle: nil), forCellReuseIdentifier: cidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: cidentifier) as? ChannelCell;
            }
            cell .setDefaultValue(model: tableData[indexPath.row] as! BSEChannelModel, channel_indexer: indexPath.row)
            if indexPath.row % 2 == 0 {
                cell.backgroundColor = .white
            } else {
                cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(page_type=="broadcast") {
            var totalHeight:CGFloat = 100;
            let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
            let height = heightForView(text: model.broadcast_name, font: UIFont(name: "Poppins-Regular", size: 13)!, width: UIScreen.main.bounds.size.width-130)
            totalHeight = totalHeight+height;
            return totalHeight > 120 ? totalHeight : 120;
        } else if (page_type=="people")  {
            return 70;
        } else {
            return 60;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if(page_type=="broadcast") {
            let model : BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel;
            self.delegate?.broadcastDetails(model: model);
         } else if (page_type=="people")  {
            let userModel : BSEUserModel = tableData[indexPath.row] as! BSEUserModel;
            self.delegate?.profileDetails(userModel: userModel);
         } else {
            let model: BSEChannelModel = tableData[indexPath.row] as! BSEChannelModel
            delegate?.notifySelect(type: "channel", value: model.channel_name);
        }
        //itemId[1] - Item Id
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    
    
    // MARK: - control function
    @objc func userAction(_ sender: UIButton){ //<- needs `@objc`
        let model:BSEUserModel = tableData[sender.tag] as! BSEUserModel
        if(model.user_follow_status==0 || model.user_follow_status==1) {
            var follower_status:Int!;
            follower_status = model.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(model.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(model.is_private==1) {
                model.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                   SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);

                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    follower_count = follower_count + 1;
                } else {
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);

                    follower_count = follower_count - 1;
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                model.user_follow_status = (follower_status==1) ? 1:0;
            }
            self.tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                   
                } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    
    @objc func goToProfile(_ sender: UIButton){
        let model : BSEBroadcastModel = tableData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        self.delegate?.profileDetails(userModel: userModel);
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ChannelCell.swift
//  liveplus
//
//  Created by BSEtec on 15/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {
    @IBOutlet var channel_index : UILabel?;
    @IBOutlet var channel_name : UILabel?;
    @IBOutlet var channel_count : UILabel?;
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEChannelModel, channel_indexer:Int) -> Void {
        channel_index?.text = "#"+String(format: "%02d", channel_indexer+1)
        channel_name?.text = model.channel_name.capitalized;
       channel_count?.text = String(model.channel_count) +  "Live".localized(lang: getBSEValue(_keyname: CURRENTLANG))
    }
    
}

//
//  UserCell.swift
//  liveplus
//
//  Created by BSEtec on 15/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage

class UserCell: UITableViewCell {
    @IBOutlet var username : UILabel?;
    @IBOutlet var fullname : UILabel?;
    @IBOutlet var followerView : UIView?;
    
    @IBOutlet var follower_label : UILabel?;
      @IBOutlet var line_label : UILabel?;
    @IBOutlet var userimage : UIImageView?;
    @IBOutlet var followimage : UIImageView?;
    @IBOutlet var follow_action : UIButton?;
    @IBOutlet var shieldimage : UIImageView?;
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userimage?.layer.cornerRadius = 25.0;
        userimage?.layer.masksToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEUserModel) -> Void {
        userimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        if(model.user_follow_status == 0) {
            followimage?.image = UIImage(named: "Follow.png")
        } else if (model.user_follow_status == 1) {
            followimage?.image = UIImage(named: "unFollow.png")
        } else if (model.user_follow_status == 2) {
            followimage?.image = UIImage(named: "request.png")
        }
    
        username?.text = "@"+model.username;
        fullname?.text = (model.first_name.count > 0) ? (model.first_name+" "+model.last_name).capitalized : model.username.capitalized ;

        let size = username!.text?.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = username!.frame;
            framer.size.width = (size?.width)!;
            framer.origin.x = UIScreen.main.bounds.size.width - (framer.size.width+80)
            username?.frame = framer;
            
            framer = followerView!.frame;
            framer.size.width = UIScreen.main.bounds.size.width - (username!.frame.size.width+username!.frame.origin.x);
            framer.origin.x = UIScreen.main.bounds.size.width - (username!.frame.size.width+80+framer.size.width);
            followerView!.frame = framer;
        }
        else
        {
            var framer: CGRect = username!.frame;
            framer.size.width = (size?.width)!;
            username?.frame = framer;
            
            framer = followerView!.frame;
            framer.origin.x = username!.frame.size.width + username!.frame.origin.x + 5;
            framer.size.width = UIScreen.main.bounds.size.width - (username!.frame.size.width + username!.frame.origin.x + 5 + 60)
            followerView!.frame = framer;
        }
        
        
      
        
        if(model.follower_count==0) {
        follower_label?.text =  "No Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
        else if(model.follower_count==1) {
 follower_label?.text =  "1 Follower".localized(lang: getBSEValue(_keyname: CURRENTLANG))        }
        else  {
    follower_label?.text = formatPoints(from: model.follower_count) + "Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
    }
    
    func setFollowerValue(model:BSEUserModel) -> Void {
        userimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        if(model.user_follow_status == 0) {
            followimage?.image = UIImage(named: "Follow.png")
        } else if (model.user_follow_status == 1) {
            followimage?.image = UIImage(named: "unFollow.png")
        } else if (model.user_follow_status == 2) {
            followimage?.image = UIImage(named: "request.png")
        }
        
        username?.text = "@"+model.username;
        fullname?.text = model.username.capitalized ;
        
        let size = username!.text?.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = username!.frame;
            framer.size.width = (size?.width)!+10;
            framer.origin.x = UIScreen.main.bounds.size.width - (framer.size.width+80)
            username?.frame = framer;
        }
        else
        {
            var framer: CGRect = username!.frame;
            framer.size.width = (size?.width)!;
            username?.frame = framer;
        }
        followerView?.isHidden = true
    }
    
    
    func formatPoints(from: Int) -> String {
        let number = Double(from)
        let thousand = number / 1000
        let million = number / 1000000
        
        if million >= 1.0 { return "\(round(million*10)/10)M" }
        else if thousand >= 1.0 { return "\(round(thousand*10)/10)K" }
        else { return "\(Int(number))"}
    }
    
}



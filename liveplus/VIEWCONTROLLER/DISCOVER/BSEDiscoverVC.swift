//
//  BSEDiscoverVC.swift
//  liveplus
//
//  Created by BSEtec on 14/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
import PageMenu
class BSEDiscoverVC: UIViewController, CAPSPageMenuDelegate, UITextFieldDelegate, BSEDiscoverListVCDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var searchView: UIView!;
    @IBOutlet weak var mainView: UIView!;
    @IBOutlet weak var searchField: UITextField!;
    @IBOutlet weak var search_icon: UILabel!;
    @IBOutlet weak var search_btn: UIButton!;
    @IBOutlet weak var search_loader: UIActivityIndicatorView!;
    var pageMenu : CAPSPageMenu?
    var broadcasts : BSEDiscoverListVC?
    var channels : BSEDiscoverListVC?
    var people : BSEDiscoverListVC?
    var currentIndex: Int!
    @IBOutlet var backView : UIView?;
    @IBOutlet var searchIconView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(broadcasts?.task==nil) {
        } else {
            if(broadcasts?.task.state == .running) {
                broadcasts?.task.cancel();
            }
        }
        if(people?.task==nil) {
        } else {
            if(people?.task.state == .running) {
                people?.task.cancel();
            }
        }
        if(channels?.task==nil) {
        } else {
            if(channels?.task.state == .running) {
                channels?.task.cancel();
            }
        }
    }
    
    // MARK: - custom function
    
    func initFields() -> Void {
        
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            searchField.textAlignment = .right
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            framer = searchIconView!.frame;
            framer.origin.x = 0;
            searchIconView!.frame = framer;
            
            framer = searchField!.frame;
            framer.origin.x =  UIScreen.main.bounds.size.width - (searchField!.frame.size.width+20);
            searchField!.frame = framer;
        }
        else
        {
            var framer: CGRect = searchIconView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - 50;
            searchIconView!.frame = framer;
            searchField.textAlignment = .left
        }
 
        searchField.placeholder =    "Search".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        search_icon.font = MaterialIcons.fontOfSize(size:24);
        search_icon.text = MaterialIcons.Search
        currentIndex = 0;
        setLayout();
    }
    
    func setLayout() -> Void {
        var controllerArray : [UIViewController] = [];
        broadcasts = BSEDiscoverListVC(nibName: "BSEDiscoverListVC", bundle: nil)
        broadcasts?.delegate=self;
        broadcasts?.title =  "Broadcasts".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        broadcasts?.page_keyword = ""
        broadcasts?.page_type = "broadcast"
        
        channels = BSEDiscoverListVC(nibName: "BSEDiscoverListVC", bundle: nil)
        channels?.delegate=self;
        channels?.title = "Channels".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        channels?.page_keyword = ""
        channels?.page_type = "channel"
        
        people = BSEDiscoverListVC(nibName: "BSEDiscoverListVC", bundle: nil)
        people?.delegate=self;
        people?.title = "People".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        people?.page_keyword = ""
        people?.page_type = "people"
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor.init(red: 195.0/255.0, green: 195.0/255.0, blue: 195.0/255.0, alpha: 1.0)),
            .selectionIndicatorColor(UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)),
            .menuHeight(40.0),
            .menuItemFont(UIFont(name: "Poppins-Regular", size: 14)!),
            .useMenuLikeSegmentedControl(true),
            .enableHorizontalBounce(false),
            .selectionIndicatorHeight(2.0)
        ]
        
        
        controllerArray.append(broadcasts!)
        controllerArray.append(channels!)
        controllerArray.append(people!)
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0.0, y:0,width:self.view.frame.width, height:mainView.frame.size.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self;
        
        mainView.addSubview(pageMenu!.view)
        
    }
    
    func notifySelect(type: String, value: String) {
        if(type=="channel") {
            print("channel name is",value);
            self.view .endEditing(true)
            let channelDetails = BSEChannelDetailsVC(nibName: "BSEChannelDetailsVC", bundle: nil)
            channelDetails.channel_name = value
            self.navigationController?.pushViewController(channelDetails, animated: true)
        }
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        
        currentIndex = index;
    }
    
    func searchController(search:String) -> Void {
        broadcasts?.page_keyword = search;
        broadcasts?.keywordChanged = true;
        channels?.page_keyword = search;
        channels?.keywordChanged = true;
        people?.page_keyword = search;
        people?.keywordChanged = true;
        if(currentIndex == 0) {
            broadcasts?.searchApi()
        } else if(currentIndex==1) {
            channels?.searchApi()
        } else {
            people?.searchApi()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchField.resignFirstResponder();
        return true;
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionSearchCancel(sender: UIButton!) {
        let searchText: String = (searchField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!;
        if(searchText.count>0) {
            searchField.text = "";
            search_icon.text = MaterialIcons.Search
            searchController(search: searchField.text!);
        }
    }
    
    @IBAction func performSearch(sender: UITextField!) {
        var searchText: String = (sender.text?.trimmingCharacters(in: .whitespacesAndNewlines))!;
        searchText = searchText.replacingOccurrences(of: " ", with: "+")
        if(searchText.count>0) {
            search_icon.text = MaterialIcons.Close
        } else {
            search_icon.text = MaterialIcons.Search
        }
        searchController(search: searchText);
    }
    
    func profileDetails(userModel:BSEUserModel) {
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func broadcastDetails(model: BSEBroadcastModel) {
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"viewer_count":model.viewer_count];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
             presenter.page_from = "detail"
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

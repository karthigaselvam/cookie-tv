//
//  BSELiveVC.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
import AACameraView
import CoreLocation

class BSELiveVC: UIViewController, UITextFieldDelegate, BSESelectVWDelegate, CLLocationManagerDelegate {
    @IBOutlet var mainView: UIView!;
    @IBOutlet weak var cameraView: AACameraView!
    @IBOutlet var overlayView:UIView!;
    @IBOutlet var toolView:UIView!;
    @IBOutlet var close_icon:UILabel!;
    @IBOutlet var camera_icon:UILabel!;
    @IBOutlet var globe_icon:UILabel!;
    @IBOutlet var drop_icon:UILabel!;
    @IBOutlet var desc_field:UITextField!;
    @IBOutlet var loader:UIActivityIndicatorView!;
    var broadcastView:BSESelectVW!
    var groupView:BSESelectVW!
    var keyboard_duration: Double = 0.0;
    var progressBar:AMProgressBar!
    var broadcast_type:String!
    var camera_type:String!
    var broadcast_image: UIImage?
    var group_id:String!
    var broadcast_id:String!
    var latitude:String!
    var longitude:String!
    var locationManager:CLLocationManager!
    @IBOutlet var broadcast_label:UILabel!;
    @IBOutlet var globalView:UIView!;
    @IBOutlet var dropView:UIView!;
    @IBOutlet var closeView:UIView!;
    @IBOutlet var camerView:UIView!;
    @IBOutlet var btn_submit : UIButton?;
    @IBOutlet var audience_lbl:UILabel!;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboard Function
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        cameraView.startSession()
        determineMyCurrentLocation()
    }
   override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        cameraView.stopSession()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
       
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double {
            keyboard_duration = duration
            UIView.animate(withDuration: duration, animations: {
                var framer:CGRect = self.toolView.frame;
                framer.origin.y = self.mainView.frame.size.height - (self.toolView.frame.size.height+(keyboardSize?.height)!)
                self.toolView.frame = framer
            })
        }
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        UIView.animate(withDuration: keyboard_duration, animations: {
            var framer:CGRect = self.toolView.frame;
            framer.origin.y = self.mainView.frame.size.height - self.toolView.frame.size.height
            self.toolView.frame = framer
        })
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set { super.hidesBottomBarWhenPushed = newValue }
    }
    
    //MARK: - custom function
    
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            desc_field.textAlignment = .right
            var framer: CGRect = globalView!.frame;
            framer.origin.x = ((UIScreen.main.bounds.size.width)-32) - (globalView!.frame.size.width);
            globalView!.frame = framer;
            
            framer = dropView!.frame;
            framer.origin.x = 0;
            dropView!.frame = framer;
            
            dropView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            framer = closeView!.frame;
            framer.origin.x = (UIScreen.main.bounds.size.width) - (closeView!.frame.size.width);
            closeView!.frame = framer;
            
            framer = camerView!.frame;
            framer.origin.x = 0;
            camerView!.frame = framer;
            
            //   camerView?.autoresizingMask = [.flexibleTopMargin];
            
            framer = broadcast_label!.frame;
            framer.origin.x = (toolView.frame.size.width-16) - (globalView!.frame.size.width+broadcast_label!.frame.size.width+10);
            broadcast_label!.frame = framer;
            
            framer = audience_lbl!.frame;
            framer.origin.x = (toolView.frame.size.width-16) - (globalView!.frame.size.width+audience_lbl!.frame.size.width+10);
            audience_lbl!.frame = framer;
        }
        else
        {
            desc_field.textAlignment = .left
        }
        broadcast_label?.text =  "public".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        audience_lbl? .text = "tab".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        desc_field?.placeholder = "seeing".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        btn_submit?.setTitle( "startlive".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        close_icon.font = MaterialIcons.fontOfSize(size:32);
        close_icon.text = MaterialIcons.Close;
        
        camera_icon.font = MaterialIcons.fontOfSize(size:32);
        camera_icon.text = MaterialIcons.SwitchCamera;
        
        globe_icon.font = MaterialIcons.fontOfSize(size:50);
        globe_icon.text = MaterialIcons.Public;
        
        drop_icon.font = MaterialIcons.fontOfSize(size:30);
        drop_icon.text = MaterialIcons.KeyboardArrowDown;
        broadcast_type="public"
        camera_type = "front"
        group_id = "";
        loader.isHidden = true;
        
        // UIScreen.main.brightness = 1.0
        
        cameraView.response = { response in
            if let img = response as? UIImage {
                self.broadcast_image = self.resizeImage(image: img, newWidth: 200)
                var broadcast_name:String = getBSEValue(_keyname: USERNAME).capitalized + " is on live"
                if(FormValidation().notEmpty(msg: self.desc_field.text!)) {
                    broadcast_name = self.desc_field.text!
                }
                let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&lat="+self.latitude+"&&long="+self.longitude+"&&type="+self.broadcast_type+"&&broadcast_name="+broadcast_name+"&&invite_user_id=&&group_id="+self.group_id+"&&language="+getBSEValue(_keyname: CURRENTLANG);
                apiservice().apiPostRequest(params:parameter, withMethod: METHODADDBROADCAST,  andCompletionBlock: { (success) -> Void in
                    let commonModel : BSECommonModel = success as! BSECommonModel;
                    if(commonModel.status == "true") {
                        self.broadcast_id = commonModel.return_id;
                        self.uploadBroadcastImage(image:self.broadcast_image!)
                    } else {
                        self.hideLoader();
                    }

                }) { (failure) -> Void in
                    self.hideLoader();
                }
            } else if let error = response as? Error {
                print("Error: ", error.localizedDescription)
            }
        }
    }
    
    func setLoader() -> Void {
        loader.isHidden = false;
        mainView.isUserInteractionEnabled=false;
    }
    
    
    
    func hideLoader() -> Void {
       loader.isHidden = true;
       mainView.isUserInteractionEnabled=true;
    }
    
    func uploadBroadcastImage(image: UIImage) -> Void {
        let imageData = UIImagePNGRepresentation(image)
        
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        var request = URLRequest(url: URL(string: APIURL + METHODUPLOAD)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        
        request.httpMethod = "POST"
        let boundaryConstant = "----------------12345";
        let contentType = "multipart/form-data;boundary=" + boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        let filename:String =  self.broadcast_id
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"type\";\r\n\r\n".data(using: .utf8)!)
        uploadData.append("broadcast".data(using: .utf8)!);
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        uploadData.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        uploadData.append(imageData!)
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        request.httpBody = uploadData as Data
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
             self.hideLoader();
            if error != nil {
               SwiftMessageBar.showMessageWithTitle("Error",message:error?.localizedDescription, type: .error);
            } else {
                
    let Dic = apiservice().convertToDictionary(response: data!);
                print("upload data ", Dic);
                if(self.broadcastView != nil) {
                    self.broadcastView.removeFromSuperview();
                    self.broadcastView = nil;
                }
                
                if(self.groupView != nil) {
                    self.groupView.removeFromSuperview();
                    self.groupView = nil;
                }
                
                
                let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
                presenter.model = ["broadcast_id":String(self.broadcast_id),"status":"live", "broadcast_name":self.desc_field.text!];
                presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
                 presenter.page_from = "live"
                presenter.camera_type = self.camera_type
                presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
                
                self.desc_field.text="";
                self.broadcast_type = "public"
                self.broadcast_label.text = "public"
                setBSEValue(_keyname:ISBROADCASTID, _value:String(self.broadcast_id));
            }
        })
        
        task.resume()
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    
    
    func selectedItem(model: BSEGroupModel, type: String) {
        self.closeItem(type: type);
        if(type=="group") {
            group_id = String(model.group_id);
            broadcast_label.text = model.group_name.capitalized;
        } else if(type=="broadcast") {
            if(model.group_id==2) {
                self.showGroup()
                broadcast_type="group"
            } else if (model.group_id==1) {
                group_id = ""
                broadcast_type="users"
            } else {
                group_id = ""
                broadcast_type="public"
            }
            broadcast_label.text = model.group_name.capitalized;
        }
    }
    
    func closeItem(type: String) {
        if(type=="broadcast") {
            self.broadcastView.isHidden = true;
            var framer:CGRect = self.broadcastView.frame;
            framer.origin.y = -185;
            self.broadcastView.frame = framer;
        } else if (type=="group") {
            self.groupView.isHidden = true;
            var framer:CGRect = self.groupView.frame;
            framer.origin.y = -(UIScreen.main.bounds.size.height*0.6);
            self.groupView.frame = framer;
        }
    }
    
    func showGroup()->Void {
        if(groupView==nil) {
            setLoader();
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+getBSEValue(_keyname: USERID)+"&&username=&&type=all&&page_no="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            apiservice().apiGetRequest(params:parameter, withMethod: METHODMYGROUP,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    if(commonModel.temparray.count==0) {

                        let alert = UIAlertController(title:"Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , message:  "You Don't have Groups".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.groupView = BSESelectVW.init(frame: CGRect(x:(UIScreen.main.bounds.size.width*0.2)/2,y:-(UIScreen.main.bounds.size.height*0.6),width:UIScreen.main.bounds.size.width*0.8,height:UIScreen.main.bounds.size.height*0.6), type: "group", data: commonModel.temparray);
                        self.groupView.delegate=self;
                        self.groupView.isHidden = true;
                        self.view.addSubview(self.groupView);
                        
                        self.groupView.isHidden = false;
                        UIView.animate(withDuration: 0.5, animations: {
                            var framer:CGRect = self.groupView.frame;
                            framer.origin.y = ((UIScreen.main.bounds.size.height*0.4)/2)
                            self.groupView.frame = framer
                        })
                    }
                } else {
                    
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        } else {
            self.groupView.isHidden = false;
            UIView.animate(withDuration: 0.5, animations: {
                var framer:CGRect = self.groupView.frame;
                framer.origin.y = ((UIScreen.main.bounds.size.height*0.4)/2)
                self.groupView.frame = framer
            })
        }
    }
    
    
    // MARK: - custom function
    @IBAction func actionClose(sender: UIButton!) {
         //UIScreen.main.brightness = 0.3
        self.view.endEditing(true);
        NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"tab_icon"])
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.tabcontroller?.selectedIndex = 0;
      //  self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionSwitchCamera(sender: UIButton!) {
        self.view.endEditing(true);
        if(camera_type=="front") {
            camera_type = "back"
        } else {
            camera_type = "front";
        }
        cameraView.toggleCamera()
    }
    
    @IBAction func actionSubmit(sender: UIButton!) {
        self.view.endEditing(true);
        setLoader();
        self.cameraView.captureImage();
    }
    
    @IBAction func actionShowBroadcastType(sender: UIButton!)
    {
        self.view.endEditing(true);
        if(broadcastView==nil)
        {
            var incr : Int = 0;
            var broadcastData = [AnyObject]();

            for value in ["Public".localized(lang: getBSEValue(_keyname: CURRENTLANG)), "Only To Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG)),  "Only To Group".localized(lang: getBSEValue(_keyname: CURRENTLANG))] {
                let model:BSEGroupModel = BSEGroupModel();
                model.group_id = incr;
                model.group_name = value;
                model.status = (incr==0) ? 1:0
                broadcastData.append(model);
                incr = incr + 1
            }
            
            broadcastView = BSESelectVW.init(frame: CGRect(x:(UIScreen.main.bounds.size.width*0.2)/2,y:-185,width:UIScreen.main.bounds.size.width*0.8,height:185), type: "broadcast", data: broadcastData);
            broadcastView.delegate=self;
            broadcastView.isHidden = true;
            self.view.addSubview(broadcastView);
        }
        
        broadcastView.isHidden = false;
        UIView.animate(withDuration: 0.5, animations: {
            var framer:CGRect = self.broadcastView.frame;
            framer.origin.y = ((UIScreen.main.bounds.size.height-185)/2)
            self.broadcastView.frame = framer
        })
        
    }
    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - location update
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       let userLocation:CLLocation = locations[0] as CLLocation
       latitude = String(userLocation.coordinate.latitude)
       longitude = String(userLocation.coordinate.longitude)
       manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

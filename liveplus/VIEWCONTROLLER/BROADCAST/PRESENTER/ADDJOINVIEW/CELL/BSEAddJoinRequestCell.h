//
//  BSEAddJoinRequestCell.h
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
NS_ASSUME_NONNULL_BEGIN

@interface BSEAddJoinRequestCell : UICollectionViewCell
{
    IBOutlet AsyncImageView *user_image;
    IBOutlet UILabel *user_name,*user_guest_name;
}
@end

NS_ASSUME_NONNULL_END

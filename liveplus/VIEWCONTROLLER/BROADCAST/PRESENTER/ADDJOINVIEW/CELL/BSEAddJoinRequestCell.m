//
//  BSEAddJoinRequestCell.m
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSEAddJoinRequestCell.h"

@implementation BSEAddJoinRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    user_image.layer.masksToBounds=YES;
    user_image.layer.cornerRadius=user_image.frame.size.width/2;
    // Initialization code
}

@end

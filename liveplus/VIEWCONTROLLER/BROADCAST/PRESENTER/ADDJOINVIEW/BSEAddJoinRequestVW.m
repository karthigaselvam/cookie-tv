//
//  BSEAddJoinRequestVW.m
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSEAddJoinRequestVW.h"
#import "BSEAddJoinRequestCell.h"
@implementation BSEAddJoinRequestVW
-(id)initWithFrame:(CGRect)frame andAddJoinRequestDetails:(NSDictionary *)Dic andCurrentLangCode:(NSString *)LangCode{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *listView = [[[NSBundle mainBundle] loadNibNamed:@"BSEAddJoinRequestVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
        
        [self addSubview:listView];
        listView.layer.masksToBounds=YES;
        listView.layer.cornerRadius=5.0F;
        current_lang=LangCode;
        [self initFields];
        
    }
    return self;
}
-(void) initFields {
    
    [guest_collection registerNib:[UINib nibWithNibName:@"BSEAddJoinRequestCell" bundle:nil] forCellWithReuseIdentifier:@"BSEAddJoinRequestCell"];
    [waiting_collection registerNib:[UINib nibWithNibName:@"BSEAddJoinRequestCell" bundle:nil] forCellWithReuseIdentifier:@"BSEAddJoinRequestCell"];
    
}
#pragma mark -- collection view delegate --

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier1 = @"BSEAddJoinRequestCell";
    // BSEHomePageModel *model = [_Listary objectAtIndex:indexPath.row];
    BSEAddJoinRequestCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:MyIdentifier1 forIndexPath:indexPath];
 //   [cell setImageContent:[self.image_array objectAtIndex:indexPath.row]];
    return cell;
}

//- (UIEdgeInsets)collectionView:
//(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(5, 10, 5, 0);
//}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(60, 60);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

//
//  BSEAddJoinRequestVW.h
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol BSEAddJoinRequestVWDelegate<NSObject>
-(void) closeAddJoinRequestView;
@end
@interface BSEAddJoinRequestVW : UIView
{
     NSString *current_lang;
    IBOutlet UICollectionView *guest_collection,*waiting_collection;
}
@property (nonatomic,strong) id <BSEAddJoinRequestVWDelegate> delegate;

-(id) initWithFrame:(CGRect)frame andAddJoinRequestDetails:(NSDictionary *) Dic andCurrentLangCode:(NSString *)LangCode;
@end

NS_ASSUME_NONNULL_END

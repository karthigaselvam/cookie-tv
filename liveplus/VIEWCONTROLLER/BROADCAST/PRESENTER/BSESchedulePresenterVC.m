//
//  BSESchedulePresenterVC.m
//  Emenator
//
//  Created by JITENDRA on 10/12/17.
//

#import "BSESchedulePresenterVC.h"
#import "SRWebSocket.h"
#import <AVFoundation/AVFoundation.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCDataChannel.h>
#import <WebRTC/RTCEAGLVideoView.h>
#import <WebRTC/RTCVideoTrack.h>
#import <WebRTC/RTCIceCandidate.h>
#import <WebRTC/RTCSessionDescription.h>
#import "BSELiveUsersVW.h"
#import "BSELiveToolVW.h"
#import "BSELiveDetailVW.h"
#import "BSECommentVW.h"
#import "TSMessageView.h"
#import "JRMFloatingAnimationView.h"
#import "UIImage+FontAwesome.h"
#import <CoreMedia/CoreMedia.h>
#import "NSString+LocaliseLanguage.h"
#import "BSEJoinRequestVW.h"
#import "BSEAddJoinRequestVW.h"
@interface BSESchedulePresenterVC ()<SRWebSocketDelegate,NBMWebRTCPeerDelegate,NBMRendererDelegate,RTCEAGLVideoViewDelegate, NSURLSessionDelegate, BSELiveToolVWDelegate, BSELiveDetailVWDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,BSEJoinRequestVWDelegate,BSEAddJoinRequestVWDelegate> {
    SRWebSocket *_webSocket;
    BSELiveUsersVW *userList;
    BSELiveToolVW *toolView;
    BSEJoinRequestVW *joinRequestView;
       BSEAddJoinRequestVW *addJoinRequestView;
    BSELiveDetailVW *detailView;
    CGFloat screen_width, screen_height;
    NSString *user_id;
    NSString *access_token;
    NSString *username;
    NSString *broadcast_id;
     NSString *author_id;
   // AVPlayer *player;
    BOOL isnavigate;
    CGFloat duration;
}
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) AVPlayerItem *videoItem;
@property (nonatomic,strong) AVPlayerLayer *avLayer;
@property (nonatomic,strong) IBOutlet UIView *streamView;
@property (nonatomic,strong) AVPlayer *videoPlayer;
@property (nonatomic, copy, readonly) NBMPeer *localPeer;
@property (nonatomic, strong) NBMWebRTCPeer *webRTCPeer;
@property (nonatomic, strong) NBMMediaConfiguration *mediaConfiguration;
@property (nonatomic,strong) RTCVideoTrack *stream;
@property (strong, nonatomic) JRMFloatingAnimationView *floatingView;
@end

@implementation BSESchedulePresenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initFields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        if(self.player!=nil) {
            [self.player play];
        }
    }
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    // [[UIScreen mainScreen] setBrightness:0.5];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
     if(!isnavigate) {
        if(self.player!=nil) {
            [self.player pause];
        }
     }
}

- (void)orientationChanged:(NSNotification *)notification{

    

}

-(void) viewDidLayoutSubviews {
    if(screen_width  == [[UIScreen mainScreen] bounds].size.width) {
        return;
    }
    screen_width = [[UIScreen mainScreen] bounds].size.width;
    screen_height = [[UIScreen mainScreen] bounds].size.height;
    
    [userList removeFromSuperview];
    [self configureUserOnline];
    
    if(toolView!=nil) {
        if(!toolView.isHidden) {
            [self moreClose:nil];
        }
        [toolView removeFromSuperview];
        toolView = nil;
    }
    
    if(joinRequestView!=nil) {
        if(!joinRequestView.isHidden) {
            [self moreJoinRequestClose:nil];
        }
        [joinRequestView removeFromSuperview];
        joinRequestView = nil;
    }
    
    
    if(addJoinRequestView!=nil) {
        if(!addJoinRequestView.isHidden) {
            [self moreAddJoinRequestClose:nil];
        }
        [addJoinRequestView removeFromSuperview];
        addJoinRequestView = nil;
    }
    
    if(detailView!=nil) {
        if(!detailView.isHidden) {
            [self actionDetailClose:nil];
        }
        [detailView removeFromSuperview];
        detailView = nil;
    }
    
    CGRect framer  = commentView.frame;
    framer.size.height = (iPhonex || iPhonexs_max) ? screen_height - 178 : screen_height - 100;
    framer.size.width = screen_width - 40;
    commentView.frame = framer;
    
    if(comment_field.isFirstResponder) {
        [comment_field resignFirstResponder];
    }
    
    if(reportView!=nil) {
        if(!reportView.isHidden) {
            [self actionCloseReport:nil];
        }
        [reportView removeFromSuperview];
        [self configureReport];
    }
    
     if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
         CGRect stream_frame = CGRectMake([UIScreen mainScreen].bounds.origin.x, (iPhonexs_max||iPhonex)? [UIScreen mainScreen].bounds.origin.y-40:[UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, (iPhonexs_max||iPhonex)?[UIScreen mainScreen].bounds.size.height-40:[UIScreen mainScreen].bounds.size.height);
         [self.avLayer setFrame:stream_frame];
     }
    
    if(_floatingView!=nil) {
        [_floatingView removeFromSuperview];
        [self initFloatView];
    }
    [self UpdateFooterHeaderFunction];
}

-(BOOL) prefersStatusBarHidden {
    return YES;
}

-(BOOL) shouldAutorotate {
    return (iPhonex || iPhonexs_max) ? NO : YES;
}

-(void) canRotate {
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) hidesBottomBarWhenPushed {
    return YES;
}

#pragma mark -- custom function

-(void) initFields {
    
//    if([self.current_lang isEqualToString:@"ar"])
//    {
//        comment_field.textAlignment=NSTextAlignmentRight;
//
//        CGRect frame=userLikeView.frame;
//        frame.origin.x=10;
//        userLikeView.frame=frame;
//
//        frame=comment_field.frame;
//        frame.origin.x=  [[UIScreen mainScreen] bounds].size.width-(comment_field.frame.size.width+10);
//        comment_field.frame=frame;
//
//        frame=toogleView.frame;
//        frame.origin.x= 10;
//        toogleView.frame=frame;
//
//        frame=stop_btn.frame;
//        frame.origin.x=[[UIScreen mainScreen] bounds].size.width-(stop_btn.frame.size.width+10);
//        stop_btn.frame=frame;
//
//        frame=cameraView.frame;
//        frame.origin.x= 10;
//        cameraView.frame=frame;
//
////        frame=muteView.frame;
////        frame.origin.x=  cameraView.frame.size.width+cameraView.frame.origin.x+1;
////        muteView.frame=frame;
//
//    }
//    else
//    {
//        comment_field.textAlignment=NSTextAlignmentLeft;
//    }
    
    
    info_label.text =[@"Broacast Denied" LocalString:_current_lang];
    
   // [back_btn setTitle:[@"GO BACK" LocalString:_current_lang] forState:UIControlStateNormal];
    
   comment_field.placeholder= [@"Say Hi" LocalString:_current_lang];
    
    footerView.hidden=headerView.hidden=YES;
    overlay_btn.hidden = YES;
    broadcast_id = [self.model objectForKey:@"broadcast_id"];
    author_id = [self.model objectForKey:@"broadcast_userID"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    user_id = [prefs stringForKey:@"userid"];
    access_token = [prefs stringForKey:@"access_token"];
    username = [prefs stringForKey:@"username"];
    NSLog(@"userdetails %@",self.user);
    NSLog(@"broadcast details %@",self.model);
    
   // [self updateView];
    
    infoView.hidden = YES;
    screen_width = [[UIScreen mainScreen] bounds].size.width;
    screen_height = [[UIScreen mainScreen] bounds].size.height;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        screenWidth = 640.0f;
        screenHeight = 480.0f;
    }else {
        screenWidth = 480.0f;
        screenHeight = 640.0f;
    }
    
    onlineUSers = [NSMutableArray new];
    self.isPresented = YES;
   
    
 //   headerView.layer.masksToBounds = YES;
    
   // [self headerOn];
    
    [self configureUserOnline];
    [self initGetBroadcastDetails];
 //[[UIScreen mainScreen] setBrightness:1.0];
    
    CGRect framer  = commentView.frame;
   framer.size.height = (iPhonex || iPhonexs_max)?screen_height - 178:  screen_height - 100;
    framer.size.width = screen_width - 40;
    commentView.frame = framer;
     if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
         [self playerUpdate];
     }

    UIFont *font=[UIFont fontWithName:@"TSMessageView" size:14.0f];
    UIFontDescriptor * fontD = [font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    
    [[TSMessageView appearance] setTitleFont:[UIFont fontWithDescriptor:fontD size:14.0f]];
    
    [self configureReport];
    
    [self initFloatView];
    
}

-(void)UpdateFooterHeaderFunction
{
    
    mute_icon.text = [NSString fontAwesomeIconStringForEnum:FAVolumeUp];
    count_icon.text = [NSString fontAwesomeIconStringForEnum:FAUser];
    more_icon.text = [NSString fontAwesomeIconStringForEnum:FAInfo];
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    share_icon.text = [NSString fontAwesomeIconStringForEnum:FAShare];
    like_count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeartO];
    group_icon.text = [NSString fontAwesomeIconStringForEnum:FAUsers];
    
    like_count_icon.hidden =like_user_online.hidden =([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"] && ![author_id isEqualToString:user_id])?NO:YES;
    mute_icon.hidden=([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"] && ![author_id isEqualToString:user_id])?YES:NO;
    footerView.hidden=headerView.hidden=NO;
    if([author_id isEqualToString:user_id])
    {
    camera_icon.text = ([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"])?[NSString fontAwesomeIconStringForEnum:FATrashO]:[NSString fontAwesomeIconStringForEnum:FACamera];
    }
    else
    {
    camera_icon.text = [NSString fontAwesomeIconStringForEnum:FAFlag];
    }
    
    shareView.backgroundColor=(![[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"])?[UIColor colorWithRed:220.0F/255.0F green:3.0F/255.0F blue:22.0F/255.0F alpha:1.0F]:[UIColor colorWithRed:225.0F/255.0F green:226.0F/255.0F blue:227.0F/255.0F alpha:1.0F];
    
    camera_icon.textColor=(![[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"] )?[UIColor whiteColor]:[UIColor darkGrayColor];
    
    userImage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,author_id]];
    user_label.text = [[self.model objectForKey:@"author_name"] uppercaseString];
    [user_label sizeToFit];
    
    comment_NewView.layer.cornerRadius= 20.0F;
    comment_NewView.layer.masksToBounds=YES;
    
    user_headerView.layer.cornerRadius= 20.0F;
    user_headerView.layer.masksToBounds=YES;
    
    camerView.layer.cornerRadius= camerView.frame.size.width/2;
    camerView.layer.masksToBounds=YES;
    
    shareView.layer.cornerRadius= shareView.frame.size.width/2;
    shareView.layer.masksToBounds=YES;
    
    muteView.layer.cornerRadius= muteView.frame.size.width/2;
    muteView.layer.masksToBounds=YES;
    
    moreView.layer.cornerRadius= moreView.frame.size.width/2;
    moreView.layer.masksToBounds=YES;
    
    groupView.layer.cornerRadius= groupView.frame.size.width/2;
    groupView.layer.masksToBounds=YES;
    
    userImage.layer.cornerRadius= userImage.frame.size.width/2;
    userImage.layer.masksToBounds=YES;
    userImage.layer.borderColor=[UIColor whiteColor].CGColor;
    userImage.layer.borderWidth=2.0f;
    
    CGRect frame;
    muteView.hidden = NO;
    frame = muteView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width -  (muteView.frame.size.width+10);
    muteView.frame=frame;
    
    frame = moreView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (muteView.frame.size.width+moreView.frame.size.width+20);
    moreView.frame=frame;
    
    frame = shareView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (muteView.frame.size.width+moreView.frame.size.width+shareView.frame.size.width+30);
    shareView.frame=frame;
    
    frame = camerView.frame;
    frame.origin.x = UIScreen.mainScreen.bounds.size.width - (muteView.frame.size.width+moreView.frame.size.width+camerView.frame.size.width+shareView.frame.size.width+40);
    camerView.frame=frame;
    
//    frame = groupView.frame;
//    frame.origin.x = UIScreen.mainScreen.bounds.size.width - (muteView.frame.size.width+moreView.frame.size.width+camerView.frame.size.width+shareView.frame.size.width+groupView.frame.size.width+50);
//    groupView.frame=frame;
    
    frame = comment_NewView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - (muteView.frame.size.width+moreView.frame.size.width+camerView.frame.size.width+shareView.frame.size.width+60);
    frame.origin.x = 10;
    comment_NewView.frame=frame;
    
    if ([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"] && [author_id isEqualToString:user_id])
    {
        //cameraView.hidden = YES;
        muteView.hidden = YES;

        frame = moreView.frame;
        frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (moreView.frame.size.width+10);
        moreView.frame=frame;

        frame = shareView.frame;
        frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (moreView.frame.size.width+shareView.frame.size.width+20);
        shareView.frame=frame;

        frame = camerView.frame;
        frame.origin.x = UIScreen.mainScreen.bounds.size.width - (moreView.frame.size.width+camerView.frame.size.width+shareView.frame.size.width+30);
        camerView.frame=frame;

        frame = comment_NewView.frame;
        frame.size.width = UIScreen.mainScreen.bounds.size.width - (moreView.frame.size.width+camerView.frame.size.width+shareView.frame.size.width+50);
        frame.origin.x = 10;
        comment_NewView.frame=frame;

    }
    if([headerView.subviews containsObject :users_onlineView])
    {
        [users_onlineView removeFromSuperview];
    }
    
    // header view
    frame  = user_headerView.frame;
    frame.size.width= (user_label.frame.origin.x+user_label.frame.size.width+15);
    user_headerView.frame=frame;
    
    frame = user_MainheaderView.frame;
    frame.size.width=(user_headerView.frame.origin.x+user_headerView.frame.size.width)>180?user_headerView.frame.origin.x+user_headerView.frame.size.width:180;
    user_MainheaderView.frame=frame;
    
    users_onlineView=[[UIView alloc] initWithFrame:CGRectMake(user_headerView.frame.origin.x+user_headerView.frame.size.width+20,5,UIScreen.mainScreen.bounds.size.width-(user_headerView.frame.origin.x+user_headerView.frame.size.width+65),35)];
    users_onlineView.backgroundColor=[UIColor clearColor];
    [headerView addSubview:users_onlineView];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    onlineuser_collection=[[UICollectionView alloc] initWithFrame:users_onlineView.bounds collectionViewLayout:layout];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [onlineuser_collection setDataSource:self];
    [onlineuser_collection setDelegate:self];
    onlineuser_collection.backgroundColor=[UIColor clearColor];
    [onlineuser_collection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [users_onlineView addSubview:onlineuser_collection];
}
-(void) initFloatView {
    self.floatingView = [[JRMFloatingAnimationView alloc] initWithStartingPoint:CGPointMake(screen_width - 45, screen_height - 45)];
    self.floatingView.startingPointWidth = 45;
    self.floatingView.animationWidth = screen_width - 60;
    self.floatingView.pop = YES;
    self.floatingView.varyAlpha = YES;
    self.floatingView.fadeOut=YES;
    self.floatingView.maxFloatObjectSize = 32;
    self.floatingView.minFloatObjectSize = 32;
    
    self.floatingView.maxAnimationHeight = screen_height;
    self.floatingView.minAnimationHeight = screen_height/2;
    [likeView addSubview:self.floatingView];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero completionHandler:nil];
}

-(void) initGetBroadcastDetails {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *UrlString = [NSString stringWithFormat:@"%@%@?broadcast_id=%@&&language=%@", APIURL, METHODGETBROADCASTDETAILS, broadcast_id,self.current_lang];
    [request setURL:[NSURL URLWithString:UrlString]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            
            if([[Dic objectForKey:@"result"] isKindOfClass:[NSArray class]]) {
                self.model = [[NSMutableDictionary alloc] initWithDictionary:[[Dic objectForKey:@"result"]objectAtIndex:0]];
                NSLog(@"testing details %@", self.model);
             
                user_online.text = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"viewer_count"]];
                [self UpdateFooterHeaderFunction];
                NSLog(@"testing details %@", self.model);
                NSString *isLike  = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"is_like"]];
                if([isLike isEqualToString:@"0"]) {
                    like_count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeartO];
                } else {
                    like_count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeart];
                }
                NSString *like_count = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"like_count"]];
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber *myNumber = [f numberFromString:like_count];
                like_user_online.text = [self suffixNumber:myNumber];
                [self connectSocket:nil];
            }
        }
        
    }] resume];
}



-(void) updateView {
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        cameraView.hidden = NO;
        muteView.hidden = NO;
    [stop_btn setTitle:[@"STOP BROADCAST" LocalString:_current_lang]     forState:UIControlStateNormal];
       
    } else if ([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        cameraView.hidden = YES;
        muteView.hidden = YES;
       [stop_btn setTitle:[@"DELETE BROADCAST" LocalString:_current_lang]   forState:UIControlStateNormal];
    }
}

-(void) headerOff {
    CGRect framer = toogleView.frame;
    framer.origin.y = 10;
    toogleView.frame = framer;
    
    framer = headerView.frame;
    framer.size.height = 0;
    headerView.frame= framer;
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        toogle_icon.text = [NSString fontAwesomeIconStringForEnum:FAChevronDown];
    } else {
        toogle_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    }
}

-(void) headerOn {
    
    CGRect framer = headerView.frame;
    framer.size.height = 45;
    headerView.frame= framer;
    
    framer = toogleView.frame;
    framer.origin.y =  45;
    toogleView.frame = framer;
    toogle_icon.text = [NSString fontAwesomeIconStringForEnum:FAChevronUp];
}

-(CGFloat)HeightCalculation :(NSString *)StringData {
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:15.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake(([UIScreen mainScreen].bounds.size.width-20), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}

#pragma mark -- control function

-(IBAction)actionVideoShareAction:(id)sender {
     [self.view endEditing:YES];
[MBProgressHUD showHUDAddedTo:self.view animated:YES];
NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SHAREURL, broadcast_id]];
UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:nil];
[self presentViewController:activityViewController animated:YES completion:^{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}];

}
-(IBAction)actionShowDetailsView:(id)sender {
     [self.view endEditing:YES];
     [self showDetailView];
}


-(IBAction)actionToogleAction:(id)sender {
    [self.view endEditing:YES];
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        if(isHeaderOpen) {
            [UIView animateWithDuration:0.5 animations:^{
                [self headerOff];
            } completion:^(BOOL finished) {
                isHeaderOpen = NO;
            }];
        } else {
            [UIView animateWithDuration:0.5 animations:^{
                [self headerOn];
            } completion:^(BOOL finished) {
                isHeaderOpen = YES;
            }];
        }
    } else {
       // [self.player removePlayerNotificationTarget:self];
        [self.player removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.player pause];
        self.player = nil;
        NSDictionary *dic = @{@"id":@"leftofflineroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
        [_webSocket close];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(IBAction)actionMute:(id)sender {
    
    [self.view endEditing:YES];
    if([author_id isEqualToString:user_id])
    {
    if(![self.webRTCPeer videoAuthorized]) {
        return;
    }
    if([self.webRTCPeer isAudioEnabled]) {
        [self.webRTCPeer enableAudio:NO];
        mute_icon.text = [NSString fontAwesomeIconStringForEnum:FAVolumeDown];
    } else {
        [self.webRTCPeer enableAudio:YES];
        mute_icon.text = [NSString fontAwesomeIconStringForEnum:FAVolumeUp];
    }
    }
    else
    {
        [self actionLikeAction:nil];
    }
}

-(IBAction)actionCameraPosition:(id)sender {
    [self.view endEditing:YES];
    if([author_id isEqualToString:user_id])
    {
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSError *error;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:60.0];
                [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
                [request setHTTPMethod:@"POST"];
        
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"1",@"action_type",@"",@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
                NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
                [request setHTTPBody:postData];
                NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    NSDictionary *dic = @{@"id":@"leftofflineroom",@"name":broadcast_id};
                    NSString *jsonString = [self createJsonFromDic:dic];
                    [_webSocket sendString:jsonString error:nil];
                    [_webSocket close];
                    self.isPresented = NO;
                    [self.delegate deleteItem];
                    [self.navigationController popViewControllerAnimated:YES];
        
                }];
                [postDataTask resume];
    }
    else
    {
    if(![self.webRTCPeer videoAuthorized]) {
        return;
    }
    if(isBackCamera) {
        if([self.webRTCPeer hasCameraPositionAvailable:NBMCameraPositionFront]) {
            [self.webRTCPeer selectCameraPosition:NBMCameraPositionFront];
        }
        isBackCamera = NO;
        
    } else {
        if([self.webRTCPeer hasCameraPositionAvailable:NBMCameraPositionBack]) {
            [self.webRTCPeer selectCameraPosition:NBMCameraPositionBack];
        }
        isBackCamera = YES;
    }
    
    [[self.streamView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLive];
        // [self generateOfferForPeer:_localPeer];
    });
    }
    }
    else
    {
        [self actionShowReport:nil];
    }
}

-(IBAction)actionStop:(id)sender {
    [self.view endEditing:YES];
   
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"] && [author_id isEqualToString:user_id])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSError *error;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        
        NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"2",@"action_type",@"",@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
        NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
        [request setHTTPBody:postData];
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"is_broadcast_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSDictionary *dic = @{@"id":@"leftroom",@"name":broadcast_id};
            NSString *jsonString = [self createJsonFromDic:dic];
            [_webSocket sendString:jsonString error:nil];
            
            [self.webRTCPeer stopLocalMedia];
            [self.webRTCPeer closeConnectionWithConnectionId:[self connectionIdOfPeer:self.localPeer]];
            [[self.streamView subviews]
             makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [_webSocket close];
           [self actionGoBack:nil];

        }];
        [postDataTask resume];
    }
    else
    {
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        NSDictionary *dic = @{@"id":@"leftofflineroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
        NSDictionary *dic = @{@"id":@"leftroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    
    [self closeConnection];
    [_webSocket close];
        [self.player removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.player pause];
        self.player = nil;
        [self actionGoBack:nil];
    }
    
}

-(IBAction)actionGoBack:(id)sender {
    if(!([self.page_from isEqualToString:@"live"]))
    {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeMain" object:nil userInfo:@{@"type":@"tab_icon"}];
    }
    
    self.isPresented = NO;
    [self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)actionShowMenu:(id)sender {
    
}

-(IBAction)actionMore:(id)sender {
    [self.view endEditing:YES];
    NSArray *menu;
   // if(([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"])) {
 menu = @[[@"broadcast details" LocalString:_current_lang]];
//    } else {
//      menu = @[[@"broadcast details" LocalString:_current_lang],   [@"share broadcast" LocalString:_current_lang] , [@"delete broadcast" LocalString:_current_lang]];
//    }

    if(toolView==nil) {
        NSString *desc = [self.model objectForKey:@"broadcast_name"];
        desc = desc.length>300 ? [desc substringFromIndex:300] : desc;
        
        CGFloat heightCalc = 170 + 135 + [self HeightCalculation:desc];
        
        NSLog(@"testing width and height %f and %f", screen_width, screen_height);
        
     toolView = [[BSELiveToolVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, heightCalc) andData:menu andBroadcastDetails:self.model andCurrentLangCode:_current_lang];
        
        toolView.delegate = self;
        [self.view addSubview:toolView];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framer = toolView.frame;
            framer.origin.y = screen_height - toolView.frame.size.height;
            toolView.frame = framer;
        } completion:^(BOOL finished) {
        }];
    }
    overlay_btn.hidden = NO;
    toolView.hidden = NO;
    footerView.hidden = YES;
 //   headerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(moreClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = toolView.frame;
        framer.origin.y = screen_height - toolView.frame.size.height;
        toolView.frame = framer;
    } completion:^(BOOL finished) {
        
    }];
}

-(IBAction)moreClose:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = toolView.frame;
        framer.origin.y = screen_height;
        toolView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        toolView.hidden = YES;
        footerView.hidden = NO;
      //  headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
}

#pragma mark -- socket

- (IBAction)connectSocket:(id)sender
{
    _webSocket.delegate = nil;
    [_webSocket close];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:SOCKETURL]];
    _webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    _webSocket.delegate = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [_webSocket open];
}

///--------------------------------------
#pragma mark - SRWebSocketDelegate
///--------------------------------------

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    NSLog(@"Websocket Connected");
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSDictionary *dic = @{@"id":@"createofflineroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
         [self createWebrtcStream];
    }
   
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@":( Websocket Failed With Error %@", error);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(nonnull NSString *)string
{
    NSLog(@"Received \"%@\"", string);
    NSDictionary *socketDic = [self createDicfromJson:string];
    NSString *connectionId = [self connectionIdOfPeer:_localPeer];
    if([[socketDic objectForKey:@"id"] isEqualToString:@"iceCandidate"]) {
        RTCIceCandidate *candid = [[RTCIceCandidate alloc] initWithSdp:[[socketDic objectForKey:@"candidate"]objectForKey:@"candidate"] sdpMLineIndex:[[[socketDic objectForKey:@"candidate"]objectForKey:@"sdpMLineIndex"] intValue] sdpMid:[[socketDic objectForKey:@"candidate"]objectForKey:@"sdpMid"]];
        [self.webRTCPeer addICECandidate:candid connectionId:connectionId];
        
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"presenterResponse"]) {
        if([[socketDic objectForKey:@"response"] isEqualToString:@"accepted"]) {
            [self.localPeer addStream:username];
            [self.webRTCPeer processAnswer:[socketDic objectForKey:@"sdpAnswer"] connectionId:connectionId];
        } else {
            NSLog(@"error %@",[socketDic objectForKey:@"message"]);
        }
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"userJoined"])
    {
        NSLog(@"user joined %@",socketDic);
        [self userJoin:socketDic];
        
    }
    else if([[socketDic objectForKey:@"id"] isEqualToString:@"userLeft"]) {
        NSLog(@"user Left %@",socketDic);
        [self userLeft:socketDic];
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"disconnected"]) {
        [self deleteBroadcast];
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"commentMessage"]) {
        NSDictionary *commentDic = [socketDic objectForKey:@"cmt"];
        [self addCommentInUI:[commentDic objectForKey:@"cmt_text"] andUserID:[commentDic objectForKey:@"user_id"]  andUserName:[commentDic objectForKey:@"username"]];
    } else if ([[socketDic objectForKey:@"id"] isEqualToString:@"likeMessage"]) {
        if(![user_id isEqualToString:[socketDic objectForKey:@"userid"]]) {
            int liker = [[self.model objectForKey:@"like_count"] intValue];
            liker = liker + 1;
            NSString *liker_obj = [NSString stringWithFormat:@"%i",liker];
            [self.model setObject:liker_obj forKey:@"like_count"];
             like_user_online.text = [self.model objectForKey:@"like_count"];
            UIImage *bubble = [UIImage imageWithIcon:@"fa-heart" backgroundColor:[UIColor clearColor] iconColor:[self randomColor] fontSize:32.0f];
            [self.floatingView addImage:bubble];
            [self.floatingView animate];
        }
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"WebSocket closed");
    self.title = @"Connection Closed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
    NSLog(@"WebSocket received pong");
}

#pragma mark -- webrtc

-(void) createWebrtcStream {
    
    _localPeer = [[NBMPeer alloc] initWithId:username];
    
    _mediaConfiguration = [NBMMediaConfiguration defaultConfiguration];
    NBMWebRTCPeer *webSession = [[NBMWebRTCPeer alloc] initWithDelegate:self configuration:_mediaConfiguration];
    self.webRTCPeer = webSession;
    if([self.camera_type isEqualToString:@"back"]) {
        if([self.webRTCPeer hasCameraPositionAvailable:NBMCameraPositionBack]) {
            [self.webRTCPeer selectCameraPosition:NBMCameraPositionBack];
        }
        isBackCamera = YES;
    } else {
        if([self.webRTCPeer hasCameraPositionAvailable:NBMCameraPositionFront]) {
            [self.webRTCPeer selectCameraPosition:NBMCameraPositionFront];
        }
        isBackCamera = NO;
    }
    
    BOOL started = [self.webRTCPeer startLocalMedia];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (started) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showLive];
            [self generateOfferForPeer:_localPeer];
            
        });
    }
}

-(void) showLive {
    
    CGFloat rWidth;
    RTCEAGLVideoView *renderView;
    if(screen_width<screen_height) {
        rWidth= screen_height/screenHeight * screenWidth;
        CGFloat rOriginX = (rWidth - screen_width) / 2 ;
        renderView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectMake(-rOriginX, 0, rWidth, screen_height)];
    } else {
        rWidth= screen_width/screenWidth * screenHeight;
        CGFloat rOriginY = (rWidth - screen_height) / 2 ;
        renderView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectMake(0,-rOriginY, screen_width, rWidth)];
    }
    renderView.delegate=self;
    [self.webRTCPeer.localStream.videoTracks.lastObject addRenderer:renderView];
    [self.streamView addSubview:renderView];
}

- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size {
    if(screenHeight!=size.height && screenWidth!=size.width) {
        screenHeight = size.height;
        screenWidth = size.width;
        [[self.streamView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLive];
        });
    }
}

- (NSString *)connectionIdOfPeer:(NBMPeer *)peer {
    if (!peer) {
        peer = [self localPeer];
    }
    NSString *connectionId = peer.identifier;
    
    return connectionId;
}

- (void)generateOfferForPeer:(NBMPeer *)peer {
    NSString *connectionId = [self connectionIdOfPeer:peer];
    [self.webRTCPeer generateOffer:connectionId withDataChannels:YES];
    
    
}

-(NSString *) createJsonFromDic:(NSDictionary *) Dic {
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Dic options:0 error:&err];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

-(NSDictionary *) createDicfromJson : (NSString *) Json {
    NSError *err;
    NSData *jsonData = [Json dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&err];
}

#pragma mark -- handle admin report

-(void) deleteBroadcast {
   // headerView.hidden = YES;
    footerView.hidden = YES;
    [[self.streamView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    infoView.hidden = NO;
    
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //
    //    NSData *submitData = [[NSString stringWithFormat:@"userid=%ld&&access_token=%@&&type=stop&&id=%@",(long)user.intId,user.strPwdHash,self.model.sID]dataUsingEncoding:NSUTF8StringEncoding];
    //    [BSEAPICalls scheduleAction:submitData ForSuccessionBlock:^(id newResponse){
    //        [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self.webRTCPeer stopLocalMedia];
    [self.webRTCPeer closeConnectionWithConnectionId:[self connectionIdOfPeer:self.localPeer]];
    [[self.streamView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_webSocket close];
    
    //    }andFailureBlock:^(NSError *error){
    //        NSLog(@"Error %@",error);
    //        [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    }];
    
}




#pragma mark -- webrtc delegate

- (void)webRTCPeer:(NBMWebRTCPeer *)peer hasICECandidate:(RTCIceCandidate *)candidate forConnection:(NBMPeerConnection *)connection {
    
    NSDictionary *params = @{@"candidate": candidate.sdp,
                             @"sdpMid": candidate.sdpMid ?: @"",
                             @"sdpMLineIndex": @(candidate.sdpMLineIndex)};
    
    NSDictionary *dic = @{@"id":@"onIceCandidateForRoom",@"name":broadcast_id,@"candidate":params};
    NSString *jsonString = [self createJsonFromDic:dic];
    [_webSocket sendString:jsonString error:nil];
    NSLog(@"local connection %@",candidate);
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didGenerateOffer:(RTCSessionDescription *)sdpOffer forConnection:(NBMPeerConnection *)connection {
    NSDictionary *dic = @{@"id":@"createroom",@"name":broadcast_id,@"sdpOffer":sdpOffer.sdp};
    NSString *jsonString = [self createJsonFromDic:dic];
    [_webSocket sendString:jsonString error:nil];
    
    NSLog(@"offer generated %@",sdpOffer);
    
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didAddStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
    
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didRemoveStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
}

- (void)webrtcPeer:(NBMWebRTCPeer *)peer iceStatusChanged:(RTCIceConnectionState)state ofConnection:(NBMPeerConnection *)connection {
    
}

#pragma mark -- online users

-(void) configureUserOnline {
    
    NSLog(@"screen Width %f",screen_width);
    NSLog(@"screen height %f",screen_height);
    
    userList =[[BSELiveUsersVW alloc] initWithFrame:CGRectMake((screen_width*.1)/2, (screen_height*.3)/2, screen_width*.9, screen_height*.7) andCurrentLangCode:self.current_lang];
    [userList.online_users_close_btn addTarget:self action:@selector(actionCloseUserList:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:userList];
    
    CGRect framer = userList.frame;
    framer.origin.y = -(screen_height*.3);
    userList.frame = framer;
    userList.hidden = YES;
    if([onlineUSers count]>0) {
        [userList setTableData:onlineUSers];
    }
    
}

-(IBAction)actionOpenUserList:(id)sender {
    userList.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = userList.frame;
        framer.origin.y = (screen_height*.3)/2;
        userList.frame = framer;
    } completion:^(BOOL finished) {
    }];
}

-(IBAction)actionCloseUserList:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = userList.frame;
        framer.origin.y = screen_height;
        userList.frame = framer;
        
    } completion:^(BOOL finished) {
        userList.hidden = YES;
        CGRect framer = userList.frame;
        framer.origin.y = -(screen_height*.3);
        userList.frame = framer;
    }];
}

-(void) userJoin : (NSDictionary *)data {
    if([onlineUSers count]==0) {
        [onlineUSers addObject:@{@"userID":[data objectForKey:@"userID"],@"userName":[data objectForKey:@"userName"]}];
    } else {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"userID == %@",[data objectForKey:@"userID"]];
        
        NSArray *result = [onlineUSers filteredArrayUsingPredicate:pred];
        if([result count]==0) {
            [onlineUSers addObject:@{@"userID":[data objectForKey:@"userID"],@"userName":[data objectForKey:@"userName"]}];
        }
    }
    
    [self updateUserOnline];
    
}

-(void) userLeft : (NSDictionary *)data {
    
    if([onlineUSers count]>0) {
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"userID == %@",[data objectForKey:@"userID"]];
        
        NSArray *result = [onlineUSers filteredArrayUsingPredicate:pred];
        if([result count]>0) {
            for (NSDictionary *Dic in result) {
                [onlineUSers removeObject:Dic];
            }
        }
        
    }
    [self updateUserOnline];
}

-(void) updateUserOnline {
    
    user_online.text = [NSString stringWithFormat:@"%li",[onlineUSers count]];
    [userList setTableData:onlineUSers];
    [onlineuser_collection reloadData];
    
    
//    for(int i=0;i<(onlineUSers.count);i++)
//    {
//        UIView *inner_View=[[UIView alloc] initWithFrame:CGRectMake((i==0)?0:i*45,0,35,35)];
//        inner_View.backgroundColor=[UIColor blackColor];
//        inner_View.tag = (long)[[onlineUSers objectAtIndex:i]objectForKey:@"userID"];
//        inner_View.layer.cornerRadius= inner_View.frame.size.width/2;
//        inner_View.layer.masksToBounds=YES;
//
//        AsyncImageView *online_user_image;
//        online_user_image =[[AsyncImageView alloc] initWithFrame:CGRectMake((i==0)?0:i*45,0,35,35)];
//        if(i<4)
//        {
//            online_user_image.imageURL=nil;
//            online_user_image.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,[[onlineUSers objectAtIndex:i]objectForKey:@"userID"]]];
//            [inner_View addSubview:online_user_image];
//        }
//        if(i==4)
//        {
//            online_user_image.image=[UIImage imageNamed:@"view_more.png"];
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            [button addTarget:self
//                       action:@selector(configureUserOnline)
//             forControlEvents:UIControlEventTouchUpInside];
//            button.frame = CGRectMake(i*45,0,35,35);
//            button.backgroundColor=[UIColor purpleColor];
//            [inner_View addSubview:button];
//            [inner_View addSubview:online_user_image];
//            break;
//        }
//
//        [users_onlineView addSubview:inner_View];
//    }

}

#pragma mark -- api parsing

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            dictionary = (NSDictionary*)object;
        }
        else
        {
            dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    
    return [dictionary dictionaryByReplacingNullsWithStrings];
}

-(NSString*) suffixNumber:(NSNumber*)number
{
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    
    num = llabs(num);
    
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log10l(num) / 3.f); //log10l(1000));
    
    NSArray* units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)]];
}

#pragma mark -- toolview delegate

-(void) selectedItem:(NSString *)selectedValue {
    overlay_btn.hidden = YES;
    toolView.hidden = YES;
    footerView.hidden = NO;
  //  headerView.hidden = NO;
    toogleView.hidden = NO;
    CGRect framer = toolView.frame;
    framer.origin.y = screen_height;
    toolView.frame = framer;

    if([selectedValue isEqualToString:[@"broadcast details" LocalString:_current_lang]]) {
        [self showDetailView];
    } else if ([selectedValue isEqualToString:[@"share broadcast" LocalString:_current_lang]]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SHAREURL, broadcast_id]];
        UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:nil];
        [self presentViewController:activityViewController animated:YES completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    } else if ([selectedValue isEqualToString:[@"view online users" LocalString:_current_lang]]) {
        [self actionOpenUserList:nil];
    } else if ([selectedValue isEqualToString:[@"delete broadcast" LocalString:_current_lang]]) {
        [self actionStop:nil];
    }
}
-(void) closeToolView {
    [self moreClose:nil];
}

#pragma mark -- show detail view

-(void) showDetailView {
    if(detailView==nil) {
        if(iPhonex || iPhonexs_max) {
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            if (UIInterfaceOrientationIsLandscape(orientation)) {
                 detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
            } else {
                 detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height-40) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
            }
        } else {
             detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
        }
        detailView.delegate = self;
        [self.view addSubview:detailView];
    }
    overlay_btn.hidden = NO;
    detailView.hidden = NO;
    toolView.hidden = YES;
    footerView.hidden = YES;
   headerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(actionDetailClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = detailView.frame;
        if( iPhonex || iPhonexs_max) {
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            if (UIInterfaceOrientationIsLandscape(orientation)) {
                 framer.origin.y = 0;
            } else {
               framer.origin.y = 40;
            }
        } else {
             framer.origin.y = 0;
        }
       
        detailView.frame = framer;
    } completion:^(BOOL finished) {
        
    }];
}

-(IBAction)actionDetailClose:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = detailView.frame;
        framer.origin.y = screen_height;
        detailView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        detailView.hidden = YES;
        footerView.hidden = NO;
        headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
}

-(void) closeDetailView {
    [self actionDetailClose:nil];
}
-(void)updateViewerCount:(NSString *)Count
{
    
}

#pragma mark -- keyboard event

- (void)keyboardWasShown:(NSNotification *)notification
{
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSDictionary *info = [notification userInfo];
    NSNumber *number = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    duration = [number doubleValue];
    //[commentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [UIView animateWithDuration:duration
                          delay:0.00f
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         CGRect framer = footerView.frame;
                         framer.origin.y = (iPhonex || iPhonexs_max) ? (screen_height-45) - (keyboardSize.height+45): screen_height - (keyboardSize.height+45);
                         footerView.frame = framer;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    if(!reportView.isHidden) {
        reportView.frame = CGRectMake((screen_width-300)/2,100, 300, 235);
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    // [commentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [UIView animateWithDuration:duration
                          delay:0.00f
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         CGRect framer = footerView.frame;
                         framer.origin.y = (iPhonex || iPhonexs_max)? (screen_height-78) - 45 : screen_height - 45;
                         footerView.frame = framer;
                     }
                     completion:^(BOOL finished) {
                     }];
}


#pragma mark -- comment functions

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 100;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    NSString *commentText = [comment_field.text stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
    if(commentText.length>0) {
        [self addComment:commentText];
    }
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextView *)textView
{
    CGRect frame;
    frame = comment_NewView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - 20;
    frame.origin.x = 10;
    comment_NewView.frame=frame;
    
shareView.hidden=camerView.hidden=moreView.hidden=muteView.hidden=groupView.hidden=YES;
}
- (void)textFieldDidEndEditing:(UITextView *)textView
{
    CGRect frame;
    frame = comment_NewView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - (shareView.frame.size.width+camerView.frame.size.width+moreView.frame.size.width+muteView.frame.size.width+groupView.frame.size.width+70);
    frame.origin.x = 10;
    comment_NewView.frame=frame;
shareView.hidden=camerView.hidden=moreView.hidden=muteView.hidden=groupView.hidden=NO;
    
}
-(CGFloat)HeightCalculation :(NSString *)StringData andWidth:(CGFloat) width {
    
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:12.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake((width-56), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}


-(void) addComment: (NSString *) message {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"5",@"action_type",message,@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            NSString *comment_id = [NSString stringWithFormat:@"%@",[Dic objectForKey:@"broacast_id"]];
            NSString *picture = [NSString stringWithFormat:@"%@image/user/%@",APIURL,user_id];
             if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
                
                NSDictionary *dic = @{@"id":@"sendMessageToRoom",@"name":broadcast_id,@"comment":@{@"cmt_id":comment_id,@"user_id":user_id,@"author_pic":picture,@"broadcast_id":broadcast_id,@"cmt_text":message,@"created_at":@"Just Now",@"username":username}};
                NSString *jsonString = [self createJsonFromDic:dic];
                [_webSocket sendString:jsonString error:nil];
             } else {
                 NSDictionary *dic = @{@"id":@"sendMessageToOfflineRoom",@"name":broadcast_id,@"comment":@{@"cmt_id":comment_id,@"user_id":user_id,@"author_pic":picture,@"broadcast_id":broadcast_id,@"cmt_text":message,@"created_at":@"Just Now",@"username":username}};
                 NSString *jsonString = [self createJsonFromDic:dic];
                 [_webSocket sendString:jsonString error:nil];
             }
            comment_field.text = @"";
            [self addCommentInUI:message andUserID:user_id andUserName:username];
        }
        
        
    }];
    [postDataTask resume];
}

-(void) addCommentInUI:(NSString *) message andUserID : (NSString *) userID andUserName: (NSString *) userName {
    
    NSString *userImage = [NSString stringWithFormat:@"%@image/user/%@",APIURL,userID];
    CGFloat width, height;
    UIFont *font = [UIFont fontWithName:@"Poppins-Regular" size:12.0];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    width = [message sizeWithAttributes:attributes].width + 58;
    CGFloat userWidth =  [[NSString stringWithFormat:@"@%@",userName] sizeWithAttributes:attributes].width + 58;
    
    if(userWidth>=width) {
        width = userWidth;
    }
    
    if(width>=commentView.frame.size.width) {
        width = commentView.frame.size.width;
    }
    
    height = 26 + [self HeightCalculation:message andWidth:width];
    CGRect framer;
    if(commentView.subviews.count>0) {
        UIView *lastView = commentView.subviews.lastObject;
        framer.origin.x = 0;
        framer.size.height = height;
        framer.size.width = width;
        framer.origin.y = lastView.frame.origin.y + lastView.frame.size.height + 8;
    } else {
        framer.origin.x = 0;
        framer.size.height = height;
        framer.size.width = width;
        framer.origin.y = commentView.frame.size.height - height;
    }
    
    BSECommentVW *comment = [[BSECommentVW alloc] initWithFrame:framer andDic:@{@"username":[NSString stringWithFormat:@"@%@",userName],@"userimage":userImage,@"comment":message} andCurrentLangCode:self.current_lang];
    [commentView addSubview:comment];
    
    UIView *lastView = commentView.subviews.lastObject;
    
    CGPoint bottomOffset = CGPointMake(0, (lastView.frame.size.height + lastView.frame.origin.y) - commentView.bounds.size.height);
    [commentView setContentOffset:bottomOffset animated:YES];
    
}

-(UIColor *)randomColor
{
    CGFloat red   = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue  = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}


#pragma mark -- player

-(void) playerUpdate {
   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mp4", VIDEOURL, broadcast_id]];
    
//    self.player = [SGPlayer player];
//    self.player.view.frame = [UIScreen mainScreen].bounds;
//
//    [self.player registerPlayerNotificationTarget:self
//                                      stateAction:@selector(stateAction:)
//                                   progressAction:@selector(progressAction:)
//                                   playableAction:@selector(playableAction:)
//                                      errorAction:@selector(errorAction:)];
//    [self.player setViewTapAction:^(SGPlayer * _Nonnull player, SGPLFView * _Nonnull view) {
//        NSLog(@"player display view did click!");
//    }];
//    [self.streamView insertSubview:self.player.view atIndex:0];
//
//    self.player.decoder = [SGPlayerDecoder decoderByFFmpeg];
//    [self.player replaceVideoWithURL:videoURL];
    
    _videoItem = [AVPlayerItem playerItemWithURL:videoURL];
    self.player = [AVPlayer playerWithPlayerItem:_videoItem];
    CALayer *superlayer = self.streamView.layer;
    
    self.avLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    //[self.avLayer setFrame:[UIScreen mainScreen].bounds];
    
    CGRect stream_frame = CGRectMake([UIScreen mainScreen].bounds.origin.x, (iPhonexs_max||iPhonex)? [UIScreen mainScreen].bounds.origin.y-40:[UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, (iPhonexs_max||iPhonex)?[UIScreen mainScreen].bounds.size.height-40:[UIScreen mainScreen].bounds.size.height);
    [self.avLayer setFrame:stream_frame];
    [superlayer addSublayer:self.avLayer];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:_videoItem];
    [_player addObserver:self
              forKeyPath:@"status"
                 options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial)
                 context:nil];
}
-(void)playerDidFinishPlaying:(NSNotification *)notification {
    //[self performSegueWithIdentifier:@"YourIdentifier" sender:self];
    [self.player seekToTime:kCMTimeZero];
    [self.player play];
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == _player && [keyPath isEqualToString:@"status"]) {
        if (_player.status == AVPlayerStatusReadyToPlay) {
            [self.player seekToTime:kCMTimeZero];
            [self.player play];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"PLAYING");
        }
        else if (_player.status == AVPlayerStatusFailed) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"FAILURE");
        }
        else if (_player.status == AVPlayerStatusUnknown) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"UNKNOW");
        }
    }
    if (object == self.videoItem && [keyPath isEqualToString:@"playbackBufferEmpty"]) {
        if (self.videoItem.playbackBufferEmpty) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        if (self.videoItem.playbackLikelyToKeepUp) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        if (self.videoItem.playbackBufferFull) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}

//- (void)stateAction:(NSNotification *)notification
//{
//    SGState * state = [SGState stateFromUserInfo:notification.userInfo];
//    NSString * text;
//    switch (state.current) {
//        case SGPlayerStateNone:
//            break;
//        case SGPlayerStateBuffering:
//            break;
//        case SGPlayerStateReadyToPlay:
//            [self.player play];
//            break;
//        case SGPlayerStatePlaying:
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            text = @"Playing";
//            break;
//        case SGPlayerStateSuspend:
//            text = @"Suspend";
//            break;
//        case SGPlayerStateFinished:
//            text = @"Finished";
//            [self.player seekToTime:0.0];
//            [self.player play];
//            break;
//        case SGPlayerStateFailed:
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            text = @"Error";
//            break;
//    }
//}
//
//- (void)progressAction:(NSNotification *)notification
//{
//    SGProgress * progress = [SGProgress progressFromUserInfo:notification.userInfo];
//
//}
//
//- (void)playableAction:(NSNotification *)notification
//{
//    SGPlayable * playable = [SGPlayable playableFromUserInfo:notification.userInfo];
//    NSLog(@"playable time : %f", playable.current);
//}
//
//- (void)errorAction:(NSNotification *)notification
//{
//    SGError * error = [SGError errorFromUserInfo:notification.userInfo];
//    NSLog(@"player did error : %@", error.error);
//}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return onlineUSers.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    AsyncImageView *online_user_image;
    online_user_image =[[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,35,35)];
    online_user_image.layer.cornerRadius= online_user_image.frame.size.width/2;
    online_user_image.layer.masksToBounds=YES;
    online_user_image.imageURL=nil;
    online_user_image.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,[[onlineUSers objectAtIndex:indexPath.row]objectForKey:@"userID"]]];
    online_user_image.backgroundColor=[UIColor darkGrayColor];
    [cell addSubview:online_user_image];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(35, 35);
}

#pragma mark -- viewer functions

-(void) closeConnection {
   // self.stream = nil;
    [self.webRTCPeer closeConnectionWithConnectionId:[self connectionIdOfPeer:self.localPeer]];
    [[self.streamView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
}
#pragma mark -- report

-(void) configureReport {
    
    lbl_report_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    reportView.frame = CGRectMake((screen_width-300)/2,-235, 300, 235);
    [self.view addSubview:reportView];
    reportView.hidden = YES;
    
    report_field.textColor = [UIColor darkGrayColor];
    report_field.text =  [@"Report Message" LocalString:_current_lang];
    
    
}

-(IBAction)actionCloseReport:(id)sender {
    
    report_field.text = @"";
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        reportView.frame = CGRectMake((screen_width-300)/2,screen_height, 300, 235);
    } completion:^(BOOL finished) {
        footerView.hidden = NO;
        headerView.hidden = NO;
        reportView.hidden = YES;
        reportView.frame = CGRectMake((screen_width-300)/2,-235, 300, 235);
    }];
}

-(IBAction)actionReportStream:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *desc =  [report_field.text isEqualToString:[@"Report Message" LocalString:_current_lang]] ? @"" : report_field.text;
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODREPORTBROADCAST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"userid",broadcast_id, @"broadcast_id",desc,@"description",self.current_lang,@"language",nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [TSMessage showNotificationWithTitle:[@"Report sent" LocalString:_current_lang] type:TSMessageNotificationTypeSuccess];
    [self actionCloseReport:nil];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        NSLog(@"testing dic %@",Dic);
        if ([[Dic objectForKey:@"status"] integerValue]) {
        }
    }];
    [postDataTask resume];
    
}

-(IBAction)actionShowReport:(id)sender {
    [self.view bringSubviewToFront:reportView];
    footerView.hidden = YES;
    headerView.hidden = YES;
    reportView.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        reportView.frame = CGRectMake((screen_width-300)/2,(screen_height-235)/2, 300, 235);
    } completion:^(BOOL finished) {
    }];
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    
    if([textView.text isEqualToString:[@"Report Message" LocalString:_current_lang]]) {
        textView.text = @"";
        report_field.textColor = [UIColor colorWithRed:193.0f/255.0f green:193.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
    }
}

-(void) textViewDidEndEditing:(UITextView *)textView {
    if([textView.text isEqualToString:@""]) {
        textView.text = [@"Report Message" LocalString:_current_lang];
        report_field.textColor = [UIColor darkGrayColor];
    }
}
#pragma mark -- like action
-(IBAction)actionLikeAction:(id)sender {
    
    UIImage *bubble = [UIImage imageWithIcon:@"fa-heart" backgroundColor:[UIColor clearColor] iconColor:[self randomColor] fontSize:32.0f];
    [self.floatingView addImage:bubble];
    [self.floatingView animate];
    
    like_count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeart];
    int liker = [[self.model objectForKey:@"like_count"] intValue];
    liker = liker + 1;
    NSString *liker_obj = [NSString stringWithFormat:@"%i",liker];
    [self.model setObject:liker_obj forKey:@"like_count"];
    
    NSString *like_count = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"like_count"]];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:like_count];
    like_user_online.text = [self suffixNumber:myNumber];
    
    
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        NSDictionary *dic = @{@"id":@"sendLikeToRoom",@"name":broadcast_id, @"userid":user_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
        NSDictionary *dic = @{@"id":@"sendLikeToOfflineRoom",@"name":broadcast_id, @"userid":user_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    }
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"4",@"action_type",@"",@"cmt_desc",@"1",@"like_count",self.current_lang,@"language",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            NSLog(@"liked in background");
        }
    }];
    [postDataTask resume];
}

#pragma mark -- Join request action
-(IBAction)actionJoinRequest:(id)sender {
    [self.view endEditing:YES];
    if([author_id isEqualToString:broadcast_id])
    {
        [self ConfigureJoinRequestView];
    }
    else
    {[self ConfigureAddJoinRequestView];
        
    }
}
-(void)ConfigureAddJoinRequestView
{
    if(addJoinRequestView==nil) {
        
        CGFloat heightCalc = 225;
        
        NSLog(@"testing width and height %f and %f", screen_width, screen_height);
        
        addJoinRequestView = [[BSEAddJoinRequestVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, heightCalc) andAddJoinRequestDetails:self.model andCurrentLangCode:_current_lang];
        
        addJoinRequestView.delegate = self;
        [self.view addSubview:addJoinRequestView];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framer = addJoinRequestView.frame;
            framer.origin.y = screen_height - addJoinRequestView.frame.size.height;
            addJoinRequestView.frame = framer;
        } completion:^(BOOL finished) {
        }];
    }
    overlay_btn.hidden = NO;
    addJoinRequestView.hidden = NO;
    footerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(moreAddJoinRequestClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = addJoinRequestView.frame;
        framer.origin.y = screen_height - addJoinRequestView.frame.size.height;
        addJoinRequestView.frame = framer;
    } completion:^(BOOL finished) {
        
    }];
}
-(void)ConfigureJoinRequestView
{
    if(joinRequestView==nil) {
        
        CGFloat heightCalc = 170 + 135 ;
        
        NSLog(@"testing width and height %f and %f", screen_width, screen_height);
        
        joinRequestView = [[BSEJoinRequestVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, heightCalc) andJoinRequestDetails:self.model andCurrentLangCode:_current_lang];
        
        joinRequestView.delegate = self;
        [self.view addSubview:joinRequestView];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framer = joinRequestView.frame;
            framer.origin.y = screen_height - joinRequestView.frame.size.height;
            joinRequestView.frame = framer;
        } completion:^(BOOL finished) {
        }];
    }
    overlay_btn.hidden = NO;
    joinRequestView.hidden = NO;
    footerView.hidden = YES;
    
    //   headerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(moreJoinRequestClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = joinRequestView.frame;
        framer.origin.y = screen_height - joinRequestView.frame.size.height;
        joinRequestView.frame = framer;
    } completion:^(BOOL finished) {
        
    }];
}
-(IBAction)moreJoinRequestClose:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = joinRequestView.frame;
        framer.origin.y = screen_height;
        joinRequestView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        joinRequestView.hidden = YES;
        footerView.hidden = NO;
        //  headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
    
}
-(IBAction)moreAddJoinRequestClose:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = addJoinRequestView.frame;
        framer.origin.y = screen_height;
        addJoinRequestView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        addJoinRequestView.hidden = YES;
        footerView.hidden = NO;
        //  headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
    
}
-(void) closeJoinRequestView
{
    [self moreJoinRequestClose:nil];
}
-(void) closeAddJoinRequestView
{
    [self moreAddJoinRequestClose:nil];
}

@end


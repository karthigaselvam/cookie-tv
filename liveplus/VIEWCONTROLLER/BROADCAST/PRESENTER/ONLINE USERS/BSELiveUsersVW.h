//
//  BSELiveUsersVW.h
//  Emenator
//
//  Created by JITENDRA on 10/14/17.
//

#import <UIKit/UIKit.h>
@interface BSELiveUsersVW : UIView <UITableViewDelegate, UITableViewDataSource>{
    IBOutlet UIView *headerView, *notData,*closeView;
    IBOutlet UILabel *close_icon,*lbl_title,*lbl_nodata;
    IBOutlet UITableView *tblView;
    NSMutableArray *peopleData;
      NSString *current_lang;
}
@property (nonatomic,strong) IBOutlet UIButton *online_users_close_btn;
-(void) setTableData : (NSMutableArray *) data;
-(id) initWithFrame:(CGRect)frame andCurrentLangCode:(NSString *)LangCode;
@end

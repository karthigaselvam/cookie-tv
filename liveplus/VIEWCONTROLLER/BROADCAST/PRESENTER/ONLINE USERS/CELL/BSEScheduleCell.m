//
//  BSEScheduleCell.m
//  Emenator
//
//  Created by JITENDRA on 10/11/17.
//

#import "BSEScheduleCell.h"

@implementation BSEScheduleCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    user_image.layer.masksToBounds=YES;
    user_image.layer.cornerRadius=30.0f;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
-(void) setDicValue:(NSDictionary *) model {
    user_image.imageURL=nil;
    user_image.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,[model objectForKey:@"userID"]]];
    user_name.text = [[model objectForKey:@"userName"] capitalizedString];
}
@end

//
//  BSEScheduleCell.h
//  Emenator
//
//  Created by JITENDRA on 10/11/17.
//

#import <UIKit/UIKit.h>

#import "AsyncImageView.h"

@interface BSEScheduleCell : UITableViewCell {
    IBOutlet AsyncImageView *user_image;
    IBOutlet UILabel *user_name;
}
-(void) setDicValue:(NSDictionary *) model;
@end

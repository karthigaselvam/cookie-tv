//
//  BSELiveUsersVW.m
//  Emenator
//
//  Created by JITENDRA on 10/14/17.
//

#import "BSELiveUsersVW.h"
#import "BSEScheduleCell.h"
#import "NSString+LocaliseLanguage.h"
@implementation BSELiveUsersVW
-(id) initWithFrame:(CGRect)frame andCurrentLangCode:(NSString *)LangCode {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *listView = [[[NSBundle mainBundle] loadNibNamed:@"BSELiveUsersVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
                current_lang=LangCode;
        [self addSubview:listView];
        [self initFields];
    }
    return self;
}

-(void) initFields {
    
    if(([current_lang isEqualToString:@"ar"]))
    {
        CGRect frame=closeView.frame;
        frame.origin.x=5;
        closeView.frame=frame;
        
        frame=lbl_title.frame;
        frame.origin.x=  self.frame.size.width-(lbl_title.frame.size.width+10);
        lbl_title.frame=frame;
        
    }
    
    
    lbl_title.text= [@"ONLINE USERS" LocalString:current_lang];
    lbl_nodata.text=[@"Users Not Available" LocalString:current_lang];
    
    peopleData = [NSMutableArray new];
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    notData.hidden = NO;
    tblView.hidden = YES;
}

-(void) setTableData:(NSMutableArray *)data {
    if([data count]>0) {
        peopleData = [[NSMutableArray alloc] initWithArray:data];
        notData.hidden = YES;
        tblView.hidden = NO;
    } else {
        [peopleData removeAllObjects];
        notData.hidden = NO;
        tblView.hidden = YES;
    }
    [tblView reloadData];
}

#pragma mark - table view

#pragma mark -- table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.backgroundColor = [UIColor blackColor];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [peopleData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    BSEUserFeed *model=[peopleData objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"BSEScheduleCell";
    BSEScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
       [tableView registerNib:[UINib nibWithNibName:(([current_lang isEqualToString:@"ar"]))?@"BSEScheduleCellRTL":@"BSEScheduleCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    }
    
    [cell setDicValue:[peopleData objectAtIndex:indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

@end

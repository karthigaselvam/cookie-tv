//
//  BSESchedulePresenterVC.h
//  Emenator
//
//  Created by JITENDRA on 10/12/17.
//

#import <UIKit/UIKit.h>
#import "NSDictionary+ReplaceNull.h"
#import "AsyncImageView.h"
@protocol BSESchedulePresenterVCDelegate <NSObject>
@optional
-(void) deleteItem;
@end
@interface BSESchedulePresenterVC : UIViewController <UITextFieldDelegate> {
IBOutlet UIView *headerView, *toogleView, *footerView, *infoView, *cameraView, *muteView, *moreView, *countView, *likeView, *mainView,*userLikeView,*comment_NewView,*shareView,*camerView,*user_headerView,*user_MainheaderView, *reportView,*closeView,*groupView;
IBOutlet UILabel *camera_icon, *mute_icon, *visitor_icon, *toogle_icon, *user_online, *info_label, *more_icon, *count_icon,*close_icon,*share_icon,*reportTitle_lbl,*lbl_repost,*lbl_report_icon,*like_user_online, *like_count_icon,*group_icon;
  IBOutlet UIButton *stop_btn, *overlay_btn,*submit_btn;
    IBOutlet UITextField *comment_field;
    IBOutlet UIScrollView *commentView;
    BOOL isHeaderOpen, isBackCamera;
    NSMutableArray *onlineUSers;
    CGFloat screenWidth, screenHeight;
    IBOutlet UIButton *back_btn;
     IBOutlet UIImageView *userImage;
    IBOutlet UILabel *user_label;
    UIView *users_onlineView;
    UICollectionView *onlineuser_collection;
     IBOutlet UITextView *report_field;
}
@property (nonatomic,strong) id <BSESchedulePresenterVCDelegate> delegate;
@property (nonatomic) BOOL isPresented;
@property (nonatomic,strong) NSString *camera_type;
@property (nonatomic,strong) NSMutableDictionary *model;
@property (nonatomic,strong) NSMutableDictionary *user;
@property (nonatomic,strong) NSString *current_lang;
@property (nonatomic,strong) NSString *page_from;

@end


//
//  BSEJoinRequestVW.h
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@protocol BSEJoinRequestVWDelegate<NSObject>
-(void) closeJoinRequestView;
@end
@interface BSEJoinRequestVW : UIView
{
     IBOutlet UITableView *tblView;
        IBOutlet UIView *tblViewHeader,*closeView;
     IBOutlet UILabel  *close_icon,*lbl_title;
     NSString *current_lang;
      NSDictionary *joinRequestDetails;
}
@property (nonatomic,strong) id <BSEJoinRequestVWDelegate> delegate;
-(id) initWithFrame:(CGRect)frame andJoinRequestDetails:(NSDictionary *) Dic andCurrentLangCode:(NSString *)LangCode;
@end

NS_ASSUME_NONNULL_END

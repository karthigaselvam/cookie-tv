//
//  BSEJoinRequestVW.m
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSEJoinRequestVW.h"
#import "BSEJoinRequestCell.h"
@implementation BSEJoinRequestVW

-(id)initWithFrame:(CGRect)frame andJoinRequestDetails:(NSDictionary *)Dic andCurrentLangCode:(NSString *)LangCode{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *listView = [[[NSBundle mainBundle] loadNibNamed:@"BSEJoinRequestVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
        
        [self addSubview:listView];
        listView.layer.masksToBounds=YES;
        listView.layer.cornerRadius=5.0F;
        
        joinRequestDetails = [[NSDictionary alloc] initWithDictionary:Dic];
        current_lang=LangCode;
        [self initFields];
    }
    return self;
}



-(void) initFields {
    
    lbl_title.text=[NSString stringWithFormat:@"Waiting List(1)"];
    
    tblView.tableFooterView = nil;
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
}
-(IBAction)actionClose:(id)sender {
    [self.delegate closeJoinRequestView];
}
#pragma mark -- table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.backgroundColor = [UIColor blackColor];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    BSEUserFeed *model=[peopleData objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"BSEJoinRequestCell";
    BSEJoinRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:(([current_lang isEqualToString:@"ar"]))?@"BSEJoinRequestCell":@"BSEJoinRequestCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    }
    
   // [cell setDicValue:[peopleData objectAtIndex:indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}









/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

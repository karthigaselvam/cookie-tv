//
//  BSEJoinRequestCell.h
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
NS_ASSUME_NONNULL_BEGIN

@interface BSEJoinRequestCell : UITableViewCell
{
    IBOutlet AsyncImageView *user_image;
    IBOutlet UILabel *user_name,*delete_icon;
}
@property(strong,nonatomic) IBOutlet UIButton *delete_btn,*accept_btn;
@end

NS_ASSUME_NONNULL_END

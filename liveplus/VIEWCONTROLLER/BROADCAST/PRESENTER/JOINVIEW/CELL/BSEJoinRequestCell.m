//
//  BSEJoinRequestCell.m
//  CookieTV
//
//  Created by BseTec on 10/29/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSEJoinRequestCell.h"

@implementation BSEJoinRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    user_image.layer.masksToBounds=YES;
    user_image.layer.cornerRadius=user_image.frame.size.width/2;
    
    self.accept_btn.layer.masksToBounds=YES;
    self.accept_btn.layer.cornerRadius=15.0F;
    
    delete_icon.text = [NSString fontAwesomeIconStringForEnum:FATrashO];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  BSEActionCell.m
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSEActionCell.h"
#import "NSString+LocaliseLanguage.h"
@implementation BSEActionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setDefaultValue:(NSString *) heading {
    if([heading isEqualToString:[@"broadcast details" LocalString:_current_lang]]) {
        action_icon.text = [NSString fontAwesomeIconStringForEnum:FAInfo];
    } else if ([heading isEqualToString:[@"share broadcast" LocalString:_current_lang]]) {
        action_icon.text = [NSString fontAwesomeIconStringForEnum:FAShare];
    } else if ([heading isEqualToString:[@"delete broadcast" LocalString:_current_lang]]) {
        action_icon.text = [NSString fontAwesomeIconStringForEnum:FABan];
    } else if ([heading isEqualToString:[@"view online users" LocalString:_current_lang]]) {
        action_icon.text = [NSString fontAwesomeIconStringForEnum:FAUsers];
    } else if ([heading isEqualToString: [@"report broadcast" LocalString:_current_lang]]) {
        action_icon.text = [NSString fontAwesomeIconStringForEnum:FAFlag];
    }
    action_label.text = [heading capitalizedString];
}

@end

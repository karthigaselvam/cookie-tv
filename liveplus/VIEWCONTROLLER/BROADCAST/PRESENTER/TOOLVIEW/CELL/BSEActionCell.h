//
//  BSEActionCell.h
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSEActionCell : UITableViewCell {
    IBOutlet UILabel *action_icon, *action_label;
}
-(void) setDefaultValue:(NSString *) heading;
@property (nonatomic,strong) NSString *current_lang;
@end

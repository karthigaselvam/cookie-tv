//
//  BSELiveToolVW.m
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSELiveToolVW.h"
#import <CoreLocation/CoreLocation.h>
#import "BSEActionCell.h"
#import "NSString+LocaliseLanguage.h"
@implementation BSELiveToolVW

-(id)initWithFrame:(CGRect)frame andData:(NSArray *)data andBroadcastDetails:(NSDictionary *)Dic andCurrentLangCode:(NSString *)LangCode {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *listView = [[[NSBundle mainBundle] loadNibNamed:@"BSELiveToolVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
        [self addSubview:listView];
        tableData = [[NSMutableArray alloc] initWithArray:data];
        broadcastDetails = [[NSDictionary alloc] initWithDictionary:Dic];
            current_lang=LangCode;
        [self initFields];
    }
    return self;
}

-(void) initFields {
    
    if(([current_lang isEqualToString:@"ar"]))
    {
        desc_label.textAlignment = created_label.textAlignment=user_label.textAlignment = location_label.textAlignment = NSTextAlignmentRight;
        
        CGRect frame=closeView.frame;
        frame.origin.x=0;
        closeView.frame=frame;
        
        closeView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        frame=status_label.frame;
        frame.origin.x=  [[UIScreen mainScreen] bounds].size.width-(status_label.frame.size.width+10);
        status_label.frame=frame;
        
        frame=user_label.frame;
        frame.origin.x= 10;
        user_label.frame=frame;
        
        frame=userImage.frame;
        frame.origin.x= [[UIScreen mainScreen] bounds].size.width-(userImage.frame.size.width+10) ;
        userImage.frame=frame;
        
    }
    if([[broadcastDetails objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        status_label.text = [@"COMPLETED" LocalString:current_lang];
    } else {
        status_label.text = [@"Broacast Denied" LocalString:current_lang];
    }
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    created_label.text = [NSString stringWithFormat: [@"Created At:" LocalString:current_lang] , [broadcastDetails objectForKey:@"created_at"]];
    NSString *desc = [broadcastDetails objectForKey:@"broadcast_name"];
    desc = desc.length>300 ? [desc substringFromIndex:300] : desc;
    desc_label.text = desc;
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:[[broadcastDetails objectForKey:@"latitude"] floatValue] longitude:[[broadcastDetails objectForKey:@"longitude"] floatValue]];
    CLGeocoder* geocoder = [CLGeocoder new];
    __block CLPlacemark* placemark;
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             location_label.text = [[NSString stringWithFormat:@"%@, %@",placemark.locality, placemark.country] capitalizedString];
             
         }
     }];
    
    CGRect framer = desc_label.frame;
    framer.size.height = [self HeightCalculation:desc];
    desc_label.frame = framer;
    
    framer = created_label.frame;
    framer.origin.y = desc_label.frame.origin.y + desc_label.frame.size.height + 4;
    created_label.frame = framer;
    
    framer = location_label.frame;
    framer.origin.y = created_label.frame.origin.y + created_label.frame.size.height + 4;
    location_label.frame = framer;
    
    framer = broadcastView.frame;
    framer.size.height = location_label.frame.origin.y + location_label.frame.size.height + 8;
    broadcastView.frame = framer;
    
    userImage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,[broadcastDetails objectForKey:@"author_id"]]];
    user_label.text = [[broadcastDetails objectForKey:@"author_name"] capitalizedString];
    
    framer = userView.frame;
    framer.size.height = broadcastView.frame.origin.y + broadcastView.frame.size.height;
    userView.frame = framer;
    
    framer = tblViewHeader.frame;
    framer.size.height = 170 + [self HeightCalculation:desc];
    tblViewHeader.frame = framer;
    
    tblView.tableHeaderView = tblViewHeader;
    tblView.tableFooterView = nil;
}

-(CGFloat)HeightCalculation :(NSString *)StringData {
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:15.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake(([UIScreen mainScreen].bounds.size.width-20), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}

-(IBAction)actionClose:(id)sender {
    [self.delegate closeToolView];
}

#pragma mark - table view

#pragma mark -- table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    BSEUserFeed *model=[peopleData objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"BSEActionCell";
    BSEActionCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
       [tableView registerNib:[UINib nibWithNibName:(([current_lang isEqualToString:@"ar"]))?@"BSEActionCellRTL": @"BSEActionCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    }
       cell.current_lang=current_lang;
    [cell setDefaultValue:[tableData objectAtIndex:indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate selectedItem:[tableData objectAtIndex:indexPath.row]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

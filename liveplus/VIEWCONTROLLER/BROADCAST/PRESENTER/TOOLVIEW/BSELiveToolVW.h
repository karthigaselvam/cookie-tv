//
//  BSELiveToolVW.h
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@protocol BSELiveToolVWDelegate<NSObject>
-(void) selectedItem:(NSString *) selectedValue;
-(void) closeToolView;
@end
@interface BSELiveToolVW : UIView<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tblView;
    IBOutlet UIView *tblViewHeader, *userView, *broadcastView,*closeView;
    IBOutlet UILabel *desc_label, *location_label, *created_label, *user_label, *status_label, *close_icon;
    IBOutlet UIImageView *userImage;
    NSDictionary *broadcastDetails;
    NSMutableArray *tableData;
     NSString *current_lang;
}
@property (nonatomic,strong) id <BSELiveToolVWDelegate> delegate;
-(id)initWithFrame:(CGRect)frame andData:(NSArray *)data andBroadcastDetails:(NSDictionary *)Dic andCurrentLangCode:(NSString *)LangCode;

@end

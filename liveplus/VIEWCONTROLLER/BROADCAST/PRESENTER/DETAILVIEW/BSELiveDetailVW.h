//
//  BSELiveDetailVW.h
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BSEScheduleCell.h"
#import "NSDictionary+ReplaceNull.h"
@protocol BSELiveDetailVWDelegate<NSObject>
-(void) closeDetailView;
-(void) updateViewerCount:(NSString *)Count;
@end

@interface BSELiveDetailVW : UIView <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate> {
    IBOutlet UITableView *tblView;
    IBOutlet UIView *tblViewHeader, *noData, *loadMoreView,*closeView;
    IBOutlet MKMapView *mapView;
    NSMutableArray *peopleData;
    IBOutlet UILabel *viewer_count, *liker_count, *close_icon;
    IBOutlet UILabel *lbl_title, *lbl_likes, *lbl_views,*lbl_viewers, *lbl_click, *lbl_nodata;

    NSDictionary *broadcastDetails;
    int currentpage;
    BOOL isapiloading;
     NSString *current_lang;
}
@property (nonatomic,strong) id <BSELiveDetailVWDelegate> delegate;
-(id) initWithFrame:(CGRect)frame andBroadcastDetails:(NSDictionary *) Dic andCurrentLangCode:(NSString *)LangCode;


@end

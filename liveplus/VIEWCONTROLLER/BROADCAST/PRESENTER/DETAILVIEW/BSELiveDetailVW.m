//
//  BSELiveDetailVW.m
//  liveplus
//
//  Created by BSEtec on 27/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "BSELiveDetailVW.h"
#import "NSString+LocaliseLanguage.h"
@implementation BSELiveDetailVW

-(id)initWithFrame:(CGRect)frame andBroadcastDetails:(NSDictionary *)Dic andCurrentLangCode:(NSString *)LangCode{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *listView = [[[NSBundle mainBundle] loadNibNamed:@"BSELiveDetailVW" owner:self options:nil] objectAtIndex:0];
        CGRect frame= listView.frame;
        frame.size.width=self.frame.size.width;
        frame.size.height=self.frame.size.height;
        listView.frame=frame;
        [self addSubview:listView];
        broadcastDetails = [[NSDictionary alloc] initWithDictionary:Dic];
        current_lang=LangCode;
        [self initFields];
    }
    return self;
}

-(void) initFields {
   
    if(([current_lang isEqualToString:@"ar"]))
    {
        
        
        CGRect frame=closeView.frame;
        frame.origin.x=10;
        closeView.frame=frame;
        
        frame=lbl_title.frame;
        frame.origin.x=  [[UIScreen mainScreen] bounds].size.width-(lbl_title.frame.size.width+10);
        lbl_title.frame=frame;
        
    }
    
    lbl_title.text=[@"broadcast details" LocalString:current_lang];
    lbl_likes.text=[@"TOTAL LIKES" LocalString:current_lang];
    lbl_views.text= [@"TOTAL VIEWS" LocalString:current_lang];
    lbl_viewers.text=[@"Broadcast Viewers" LocalString:current_lang];
    lbl_click.text=[@"Click Here To View More" LocalString:current_lang];
    lbl_nodata.text=[@"No Data" LocalString:current_lang];
    
    

    tblView.tableFooterView = nil;
    currentpage = 1;
    isapiloading = NO;
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    tblView.tableHeaderView = tblViewHeader;
    tblView.tableFooterView = nil;
    
    
    MKCoordinateRegion region = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[broadcastDetails objectForKey:@"latitude"] floatValue], [[broadcastDetails objectForKey:@"longitude"] floatValue]), 200, 200)];
    [mapView setRegion:region animated:YES];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = CLLocationCoordinate2DMake([[broadcastDetails objectForKey:@"latitude"] floatValue], [[broadcastDetails objectForKey:@"longitude"] floatValue]);
    [mapView addAnnotation:point];
    
    viewer_count.text = [NSString stringWithFormat:@"%@",[broadcastDetails objectForKey:@"viewer_count"]];
    liker_count.text = [NSString stringWithFormat:@"%@",[broadcastDetails objectForKey:@"like_count"]];
    [self initParticipants];
}

-(void) initParticipants {
    isapiloading = YES;
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *userID = [prefs stringForKey:@"userid"];
    NSString *access_token = [prefs stringForKey:@"access_token"];
    NSString *broadcast_id = [broadcastDetails objectForKey:@"broadcast_id"];
    
    NSString *UrlString = [NSString stringWithFormat:@"%@%@?user_id=%@&&access_token=%@&&broadcast_id=%@&&page_no=%i&&language=%@", APIURL, METHODGETBROADCASTVIEWERS, userID, access_token, broadcast_id,currentpage,current_lang];
    [request setURL:[NSURL URLWithString:UrlString]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        NSLog(@"testing details %@", Dic);
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
        [self.delegate updateViewerCount:[NSString stringWithFormat:@"%@",[Dic objectForKey:@"total_count"]]];
            if([[Dic objectForKey:@"result"] isKindOfClass:[NSArray class]]) {
                if([[Dic objectForKey:@"result"] count]>0) {
                    if(currentpage ==1) {
                        peopleData = [NSMutableArray new];
                    }
                    for (NSDictionary *Arr in [Dic objectForKey:@"result"]) {
                        NSString *user_id = [NSString stringWithFormat:@"%@",[Arr objectForKey:@"userID"]];
                        [peopleData addObject:@{@"userID":user_id, @"userName":[Arr objectForKey:@"userName"]}];
                    }
                    isapiloading = NO;
                }
            }
        }
        
        if([peopleData count]==0) {
            tblView.tableFooterView = noData;
        } else {
            if(!isapiloading) {
               tblView.tableFooterView = loadMoreView;
            } else {
                tblView.tableFooterView = nil;
            }
        }
        [tblView reloadData];
        
    }] resume];
}
-(IBAction)actionClose:(id)sender {
    [self.delegate closeDetailView];
}

-(IBAction)actionloadMore:(id)sender {
    if(!isapiloading) {
        currentpage = currentpage + 1;
        [self initParticipants];
    }
}

#pragma mark -- table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.backgroundColor = [UIColor blackColor];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [peopleData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    BSEUserFeed *model=[peopleData objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"BSEScheduleCell";
    BSEScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:(([current_lang isEqualToString:@"ar"]))?@"BSEScheduleCellRTL":@"BSEScheduleCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    }
    
    [cell setDicValue:[peopleData objectAtIndex:indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark -- api parsing

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            dictionary = (NSDictionary*)object;
        }
        else
        {
            dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    
    return [dictionary dictionaryByReplacingNullsWithStrings];
}
@end


//
//  BSESchedulePresenterVC.m
//  Emenator
//
//  Created by JITENDRA on 10/12/17.
//

#import "BSEScheduleViewerVC.h"
#import "SRWebSocket.h"
#import "UIImage+FontAwesome.h"
#import <AVFoundation/AVFoundation.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCDataChannel.h>
#import <WebRTC/RTCEAGLVideoView.h>
#import <WebRTC/RTCVideoTrack.h>
#import <WebRTC/RTCIceCandidate.h>
#import <WebRTC/RTCSessionDescription.h>
#import "BSELiveToolVW.h"
#import "BSELiveDetailVW.h"
#import "BSECommentVW.h"
#import "TSMessageView.h"
#import "JRMFloatingAnimationView.h"
//#import <SGPlayer/SGPlayer.h>
#import <CoreMedia/CoreMedia.h>
#import "NSString+LocaliseLanguage.h"
@interface BSEScheduleViewerVC ()<SRWebSocketDelegate,NBMWebRTCPeerDelegate,NBMRendererDelegate,RTCEAGLVideoViewDelegate, NSURLSessionDelegate, BSELiveToolVWDelegate, BSELiveDetailVWDelegate> {
    SRWebSocket *_webSocket;
    BSELiveToolVW *toolView;
    BSELiveDetailVW *detailView;
    CGFloat screen_width, screen_height;
    NSString *user_id;
    NSString *access_token;
    NSString *username;
    NSString *broadcast_id;
   // AVPlayer *player;
    BOOL isnavigate;
    CGFloat duration;
}
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) AVPlayerItem *videoItem;
@property (nonatomic,strong) AVPlayerLayer *avLayer;
@property (nonatomic,strong) IBOutlet UIView *streamView;
@property (nonatomic, copy, readonly) NBMPeer *localPeer;
@property (nonatomic, strong) NBMWebRTCPeer *webRTCPeer;
@property (nonatomic, strong) NBMMediaConfiguration *mediaConfiguration;
@property (nonatomic,strong) RTCVideoTrack *stream;
@property (strong, nonatomic) JRMFloatingAnimationView *floatingView;
@end

@implementation BSEScheduleViewerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initFields];
}



- (void)viewWillAppear:(BOOL)animated{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [super viewWillAppear:animated];
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        if(self.player!=nil) {
            [self.player play];
        }
    }
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        [self setAudioOutputSpeaker:YES];
    }
    
}




- (void)viewWillDisappear:(BOOL)animated{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    // [[UIScreen mainScreen] setBrightness:0.5];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
     if(!isnavigate) {
         if(self.player!=nil) {
             [self.player pause];
         }
     }
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
       [self setAudioOutputSpeaker:NO];
    }
}

- (void)orientationChanged:(NSNotification *)notification{

    

}

-(void) viewDidLayoutSubviews {
    if(screen_width  == [[UIScreen mainScreen] bounds].size.width) {
        return;
    }
    screen_width = [[UIScreen mainScreen] bounds].size.width;
    screen_height = [[UIScreen mainScreen] bounds].size.height;
    
    
    if(toolView!=nil) {
        if(!toolView.isHidden) {
            [self moreClose:nil];
        }
        [toolView removeFromSuperview];
        toolView = nil;
    }
    
    if(detailView!=nil) {
        if(!detailView.isHidden) {
            [self actionDetailClose:nil];
        }
        [detailView removeFromSuperview];
        detailView = nil;
    }
    
    CGRect framer  = commentView.frame;
    framer.size.height = (iPhonex || iPhonexs_max)?screen_height - 178:  screen_height - 100;
    framer.size.width = screen_width - 40;
    commentView.frame = framer;
    
    if(comment_field.isFirstResponder) {
        [comment_field resignFirstResponder];

    }
    
    if(reportView!=nil) {
        if(!reportView.isHidden) {
            [self actionCloseReport:nil];
        }
        [reportView removeFromSuperview];
        [self configureReport];
    }
    
     if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
         NSLog(@"screen width %f and screen height %f",screen_width,screen_height);
         if(self.player!=nil) {
             CGRect stream_frame = CGRectMake([UIScreen mainScreen].bounds.origin.x, (iPhonexs_max||iPhonex)? [UIScreen mainScreen].bounds.origin.y-40:[UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, (iPhonexs_max||iPhonex)?[UIScreen mainScreen].bounds.size.height-40:[UIScreen mainScreen].bounds.size.height);
             [self.avLayer setFrame:stream_frame];
         }
     }
    
    [self updateStreamFrame];
    [self UpdateFooterHeaderView];
}

-(void) updateStreamFrame {
    CGFloat rWidth;
    RTCEAGLVideoView *renderView;
    if([self.streamView.subviews count]==0) {
        return;
    } else {
        renderView = (RTCEAGLVideoView *)[self.streamView.subviews objectAtIndex:0];
    }
    if(screenWidth<screenHeight) {
        if(iPhonex || iPhonexs_max) {
            CGFloat screenXheight = screen_height - 45;
            rWidth= screenXheight/screenHeight * screenWidth;
            CGFloat rOriginX = (rWidth - screen_width) / 2 ;
            renderView.frame = CGRectMake(-rOriginX, 0, rWidth, screenXheight);
        } else {
            rWidth= screen_height/screenHeight * screenWidth;
            CGFloat rOriginX = (rWidth - screen_width) / 2 ;
            renderView.frame = CGRectMake(-rOriginX, 0, rWidth, screen_height);
        }
    } else {
        if(iPhonex || iPhonexs_max) {
            CGFloat screenXWidth = screen_width - 45;
            rWidth= screenXWidth/screenWidth * screenHeight;
            CGFloat rOriginY = (rWidth - screen_height) / 2 ;
            renderView.frame = CGRectMake(0,-rOriginY, screenXWidth, rWidth);
        } else {
            rWidth= screen_width/screenWidth * screenHeight;
            CGFloat rOriginY = (rWidth - screen_height) / 2 ;
            renderView.frame = CGRectMake(0,-rOriginY, screen_width, rWidth);
        }
    }
}

-(BOOL) prefersStatusBarHidden {
    return (iPhonex || iPhonexs_max) ? NO : YES;
}

-(BOOL) shouldAutorotate {
    return YES;
}

-(void) canRotate {
    
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) hidesBottomBarWhenPushed {
    return YES;
}

#pragma mark -- custom function

-(void) initFields {

    if([self.current_lang isEqualToString:@"ar"])
    {
        comment_field.textAlignment=NSTextAlignmentRight;
        
        CGRect frame=userLikeView.frame;
        frame.origin.x=10;
        userLikeView.frame=frame;
        
        userLikeView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        frame=comment_field.frame;
        frame.origin.x=  [[UIScreen mainScreen] bounds].size.width-(comment_field.frame.size.width+10);
        comment_field.frame=frame;
        
        frame=cameraView.frame;
        frame.origin.x= 10;
        cameraView.frame=frame;
        
        frame=closeView.frame;
        frame.origin.x= 10;
        closeView.frame=frame;
        
        cameraView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        frame=reportTitle_lbl.frame;
        frame.origin.x= reportView.frame.size.width-(reportTitle_lbl.frame.size.width+10);
        reportTitle_lbl.frame=frame;
        
    }
    else
    {
        comment_field.textAlignment=NSTextAlignmentLeft;
    }

    comment_field.placeholder = [@"Say Hi" LocalString:_current_lang];
    
    reportTitle_lbl.text=[ [@"report broadcast" LocalString:_current_lang] uppercaseString];
    
    [submit_btn setTitle: [@"SUBMIT" LocalString:_current_lang] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSessionRouteChange:) name:AVAudioSessionRouteChangeNotification object:nil];
    overlay_btn.hidden =  footerView.hidden = headerView.hidden =YES;
    broadcast_id = [self.model objectForKey:@"broadcast_id"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    user_id = [prefs stringForKey:@"userid"];
    access_token = [prefs stringForKey:@"access_token"];
    username = [prefs stringForKey:@"username"];
    NSLog(@"userdetails %@",self.user);
    NSLog(@"broadcast details %@",self.model);

    infoView.hidden = YES;
    screen_width = [[UIScreen mainScreen] bounds].size.width;
    screen_height =  [[UIScreen mainScreen] bounds].size.height;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        screenWidth = 640.0f;
        screenHeight = 480.0f;
    }else {
        screenWidth = 480.0f;
        screenHeight = 640.0f;
    }
    
    self.isPresented = YES;
    camera_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeartO];
    more_icon.text = [NSString fontAwesomeIconStringForEnum:FAInfo];
    lbl_share.text = [NSString fontAwesomeIconStringForEnum:FAShare];
    lbl_repost.text = [NSString fontAwesomeIconStringForEnum:FAFlag];
    viewer_icon.text = [NSString fontAwesomeIconStringForEnum:FAUser];

    headerView.layer.masksToBounds = YES;
  
    [self initGetBroadcastDetails];
   // [[UIScreen mainScreen] setBrightness:1.0];
    CGRect framer  = commentView.frame;
    framer.size.height = (iPhonex || iPhonexs_max)?screen_height - 178:  screen_height - 100;
    framer.size.width = screen_width - 40;
    commentView.frame = framer;
    
     if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
         [self playerUpdate];
        // [self.player play];
     }
    
    UIFont *font=[UIFont fontWithName:@"TSMessageView" size:14.0f];
    UIFontDescriptor * fontD = [font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    
    [[TSMessageView appearance] setTitleFont:[UIFont fontWithDescriptor:fontD size:14.0f]];
    
    [self configureReport];
    
    self.floatingView = [[JRMFloatingAnimationView alloc] initWithStartingPoint:CGPointMake(screen_width - 45, screen_height - 45)];
    self.floatingView.startingPointWidth = 45;
    self.floatingView.animationWidth = screen_width - 60;
    self.floatingView.pop = YES;
    self.floatingView.varyAlpha = YES;
    self.floatingView.fadeOut=YES;
    self.floatingView.maxFloatObjectSize = 32;
    self.floatingView.minFloatObjectSize = 32;
    
    self.floatingView.maxAnimationHeight = screen_height;
    self.floatingView.minAnimationHeight = screen_height/2;
    [likeView addSubview:self.floatingView];
    
    
    
}
-(void)UpdateFooterHeaderView
{
   footerView.hidden = headerView.hidden =NO;
    footer_commentView.layer.cornerRadius= 20.0F;
    footer_commentView.layer.masksToBounds=YES;
    
    user_headerView.layer.cornerRadius= 20.0F;
    user_headerView.layer.masksToBounds=YES;
    
    footer_shareView.layer.cornerRadius= footer_shareView.frame.size.width/2;
    footer_shareView.layer.masksToBounds=YES;
    
    footer_repostView.layer.cornerRadius= footer_repostView.frame.size.width/2;
    footer_repostView.layer.masksToBounds=YES;
    
    moreView.layer.cornerRadius= moreView.frame.size.width/2;
    moreView.layer.masksToBounds=YES;
    
    
    userLikeView.layer.cornerRadius= userLikeView.frame.size.width/2;
    userLikeView.layer.masksToBounds=YES;
    
    userImage.layer.cornerRadius= userImage.frame.size.width/2;
    userImage.layer.masksToBounds=YES;
    userImage.layer.borderColor=[UIColor whiteColor].CGColor;
    userImage.layer.borderWidth=2.0f;
    
    
    // footerView configuration
    CGRect frame;
    // footer view
    frame = userLikeView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width -  (userLikeView.frame.size.width+10);
    userLikeView.frame=frame;
    
    frame = moreView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (userLikeView.frame.size.width+moreView.frame.size.width+20);
    moreView.frame=frame;
    
    frame = footer_repostView.frame;
    frame.origin.x =  UIScreen.mainScreen.bounds.size.width - (userLikeView.frame.size.width+moreView.frame.size.width+footer_repostView.frame.size.width+30);
    footer_repostView.frame=frame;
    
    frame = footer_shareView.frame;
    frame.origin.x = UIScreen.mainScreen.bounds.size.width - (userLikeView.frame.size.width+moreView.frame.size.width+footer_repostView.frame.size.width+footer_shareView.frame.size.width+40);
    footer_shareView.frame=frame;
    
    frame = footer_commentView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - (userLikeView.frame.size.width+moreView.frame.size.width+footer_repostView.frame.size.width+footer_shareView.frame.size.width+60);
    frame.origin.x = 10;
    footer_commentView.frame=frame;
    
    userImage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@image/user/%@",APIURL,[self.model objectForKey:@"author_id"]]];
    user_label.text = [[self.model objectForKey:@"author_name"] uppercaseString];
    viewer_count.text = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"viewer_count"]];
    [user_label sizeToFit];
    
    frame  = user_headerView.frame;
    frame.size.width= (user_label.frame.origin.x+user_label.frame.size.width+10);
    user_headerView.frame=frame;
    
    frame = user_MainheaderView.frame;
frame.size.width=(user_headerView.frame.origin.x+user_headerView.frame.size.width)>180?user_headerView.frame.origin.x+user_headerView.frame.size.width:180;
    user_MainheaderView.frame=frame;
}
-(void) playerUpdate {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mp4", VIDEOURL, broadcast_id]];
    
//    self.player = [SGPlayer player];
//    self.player.view.frame = [UIScreen mainScreen].bounds;
//
//    [self.player registerPlayerNotificationTarget:self
//                                      stateAction:@selector(stateAction:)
//                                   progressAction:@selector(progressAction:)
//                                   playableAction:@selector(playableAction:)
//                                      errorAction:@selector(errorAction:)];
//    [self.player setViewTapAction:^(SGPlayer * _Nonnull player, SGPLFView * _Nonnull view) {
//        NSLog(@"player display view did click!");
//    }];
//    [self.streamView insertSubview:self.player.view atIndex:0];
//
//    self.player.decoder = [SGPlayerDecoder decoderByFFmpeg];
//    [self.player replaceVideoWithURL:videoURL];
    _videoItem = [AVPlayerItem playerItemWithURL:videoURL];
    self.player = [AVPlayer playerWithPlayerItem:_videoItem];
    CALayer *superlayer = self.streamView.layer;
    self.avLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    CGRect stream_frame = CGRectMake([UIScreen mainScreen].bounds.origin.x, (iPhonexs_max||iPhonex)? [UIScreen mainScreen].bounds.origin.y-40:[UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, (iPhonexs_max||iPhonex)?[UIScreen mainScreen].bounds.size.height-40:[UIScreen mainScreen].bounds.size.height);
    [self.avLayer setFrame:stream_frame];
    // playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [superlayer addSublayer:self.avLayer];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:_videoItem];
    
    //    [self.player seekToTime:kCMTimeZero];
    //    [self.player play];
    
    [_player addObserver:self
              forKeyPath:@"status"
                 options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial)
                 context:nil];
}
-(void)playerDidFinishPlaying:(NSNotification *)notification {
    //[self performSegueWithIdentifier:@"YourIdentifier" sender:self];
    [self.player seekToTime:kCMTimeZero];
    [self.player play];
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == _player && [keyPath isEqualToString:@"status"]) {
        if (_player.status == AVPlayerStatusReadyToPlay) {
            [self.player seekToTime:kCMTimeZero];
            [self.player play];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"PLAYING");
        }
        else if (_player.status == AVPlayerStatusFailed) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"FAILURE");
        }
        else if (_player.status == AVPlayerStatusUnknown) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"UNKNOW");
        }
    }
    if (object == self.videoItem && [keyPath isEqualToString:@"playbackBufferEmpty"]) {
        if (self.videoItem.playbackBufferEmpty) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        if (self.videoItem.playbackLikelyToKeepUp) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        if (self.videoItem.playbackBufferFull) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}
//- (void)stateAction:(NSNotification *)notification
//{
//    SGState * state = [SGState stateFromUserInfo:notification.userInfo];
//
//    NSString * text;
//    switch (state.current) {
//        case SGPlayerStateNone:
//            break;
//        case SGPlayerStateBuffering:
//            break;
//        case SGPlayerStateReadyToPlay:
//            [self.player play];
//            break;
//        case SGPlayerStatePlaying:
//           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            text = @"Playing";
//            break;
//        case SGPlayerStateSuspend:
//            text = @"Suspend";
//            break;
//        case SGPlayerStateFinished:
//            text = @"Finished";
//            [self.player seekToTime:0.0];
//            [self.player play];
//            break;
//        case SGPlayerStateFailed:
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            text = @"Error";
//            break;
//    }
//}
//
//- (void)progressAction:(NSNotification *)notification
//{
//    SGProgress * progress = [SGProgress progressFromUserInfo:notification.userInfo];
//
//}
//
//- (void)playableAction:(NSNotification *)notification
//{
//    SGPlayable * playable = [SGPlayable playableFromUserInfo:notification.userInfo];
//    NSLog(@"playable time : %f", playable.current);
//}
//
//- (void)errorAction:(NSNotification *)notification
//{
//    SGError * error = [SGError errorFromUserInfo:notification.userInfo];
//    NSLog(@"player did error : %@", error.error);
//}


-(void) initGetBroadcastDetails {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *UrlString = [NSString stringWithFormat:@"%@%@?broadcast_id=%@&&user_id=%@&&access_token=%@&&language=%@", APIURL, METHODGETBROADCASTDETAILS, broadcast_id,user_id, access_token,self.current_lang];
    [request setURL:[NSURL URLWithString:UrlString]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            [self initBroadcastView];
            if([[Dic objectForKey:@"result"] isKindOfClass:[NSArray class]]) {
              
                self.model = [[NSMutableDictionary alloc] initWithDictionary:[[Dic objectForKey:@"result"]objectAtIndex:0]];
                [self UpdateFooterHeaderView];
                NSLog(@"testing details %@", self.model);
                NSString *isLike  = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"is_like"]];
                if([isLike isEqualToString:@"0"]) {
                    count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeartO];
                } else {
                    count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeart];
                }
                NSString *like_count = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"like_count"]];
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber *myNumber = [f numberFromString:like_count];
                user_online.text = [self suffixNumber:myNumber];
                [self connectSocket:nil];
            }
        }
        
    }] resume];
}

-(void) initBroadcastView {
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"6",@"action_type",@"",@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            int viewer = [[self.model objectForKey:@"viewer_count"] intValue];
            viewer = viewer + 1;
            NSString *view_obj = [NSString stringWithFormat:@"%i",viewer];
            [self.model setObject:view_obj forKey:@"viewer_count"];
        }
    }];
    [postDataTask resume];
}

-(CGFloat)HeightCalculation :(NSString *)StringData {
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:15.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake(([UIScreen mainScreen].bounds.size.width-20), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}

#pragma mark -- viewer functions

-(void) closeConnection {
    self.stream = nil;
    [self.webRTCPeer closeConnectionWithConnectionId:[self connectionIdOfPeer:self.localPeer]];
    [[self.streamView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
}

-(void) checkScheduleDone {
    [self.view endEditing:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"3",@"action_type",@"",@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
        
             [self configureInfoLabel: [@"Connection Error" LocalString:_current_lang]];
            [self closeConnection];
        } else {
           [self configureInfoLabel:[@"Thank you" LocalString:_current_lang]];
            [self closeConnection];
            [_webSocket close];
        }
        infoView.hidden = NO;
        footerView.hidden = YES;
        
    }];
    [postDataTask resume];
}

-(void) configureInfoLabel : (NSString *) info_text {
    CGFloat height = [self HeightCalculationForText:info_text width:screen_width-30 FontSize:14.0f FontStyle:@"Poppins-Regular"];
    CGRect framer = info_label.frame;
    framer.size.height =height;
    info_label.frame = framer;
    
    info_label.text = info_text;
    
    framer = infoView.frame;
    framer.origin.y = (screen_height - height) /2;
    framer.size.height=height;
    infoView.frame = framer;
    
    [mainView bringSubviewToFront:infoView];
}

-(CGFloat)HeightCalculationForText:(NSString *)StringData width:(CGFloat)WithVal FontSize: (CGFloat) sizeFont FontStyle:(NSString *) style
{
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:style size:sizeFont];//
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake(WithVal, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}

#pragma mark -- control function
-(IBAction)actionProfile:(id)sender {
    //[self.delegate goToPeopleProfile:[NSString stringWithFormat:@"%@",[self.model objectForKey:@"author_id"]] andUsername:[self.model objectForKey:@"author_name"]];
}
-(IBAction)actionClose:(id)sender {
    
    [self.view endEditing:YES];
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        NSDictionary *dic = @{@"id":@"leftofflineroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
        NSDictionary *dic = @{@"id":@"leftroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    
    [self.view endEditing:YES];
    [self closeConnection];
    [_webSocket close];
  //  [self.player removePlayerNotificationTarget:self];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeMain" object:nil userInfo:@{@"type":@"tab_icon"}];
    
    [self.player removeObserver:self forKeyPath:@"status"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player pause];
    self.player = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)actionShare:(id)sender {
    [self.view endEditing:YES];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SHAREURL, broadcast_id]];
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}
-(IBAction)actionReport:(id)sender {
    [self.view endEditing:YES];
     [self actionShowReport:nil];
}
-(IBAction)actionDetails:(id)sender {
    [self.view endEditing:YES];
    [self showDetailView];
}
-(IBAction)actionMore:(id)sender {
    [self.view endEditing:YES];
    
     NSArray *menu = @[ [@"broadcast details" LocalString:_current_lang], [@"share broadcast" LocalString:_current_lang],[@"report broadcast" LocalString:_current_lang]];
    
    if(toolView==nil) {
        NSString *desc = [self.model objectForKey:@"broadcast_name"];
        desc = desc.length>300 ? [desc substringFromIndex:300] : desc;
        
        CGFloat heightCalc = 170 + 135 + [self HeightCalculation:desc];
        
        NSLog(@"testing width and height %f and %f", screen_width, screen_height);
        
        toolView = [[BSELiveToolVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, heightCalc) andData:menu andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
        toolView.delegate = self;
        [self.view addSubview:toolView];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framer = toolView.frame;
            framer.origin.y = screen_height - toolView.frame.size.height;
            toolView.frame = framer;
        } completion:^(BOOL finished) {
        }];
    }
    overlay_btn.hidden = NO;
    toolView.hidden = NO;
    footerView.hidden = YES;
    headerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(moreClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = toolView.frame;
        framer.origin.y = screen_height - toolView.frame.size.height;
        toolView.frame = framer;
    } completion:^(BOOL finished) {
        
    }];
}

-(IBAction)moreClose:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = toolView.frame;
        framer.origin.y = screen_height;
        toolView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        toolView.hidden = YES;
        footerView.hidden = NO;
        headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
}



#pragma mark -- socket

- (IBAction)connectSocket:(id)sender
{
    
    _webSocket.delegate = nil;
    [_webSocket close];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:SOCKETURL]];
    _webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    _webSocket.delegate = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_webSocket open];
}

///--------------------------------------
#pragma mark - SRWebSocketDelegate
///--------------------------------------

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    NSLog(@"Websocket Connected");
    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"stop"]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSDictionary *dic = @{@"id":@"joinofflineroom",@"name":broadcast_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
        [self createWebrtcStream];
    }
    
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@":( Websocket Failed With Error %@", error);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(nonnull NSString *)string
{
    NSLog(@"Received \"%@\"", string);
    NSDictionary *socketDic = [self createDicfromJson:string];
    NSString *connectionId = [self connectionIdOfPeer:_localPeer];
    if([[socketDic objectForKey:@"id"] isEqualToString:@"iceCandidate"]) {
        RTCIceCandidate *candid = [[RTCIceCandidate alloc] initWithSdp:[[socketDic objectForKey:@"candidate"]objectForKey:@"candidate"] sdpMLineIndex:[[[socketDic objectForKey:@"candidate"]objectForKey:@"sdpMLineIndex"] intValue] sdpMid:[[socketDic objectForKey:@"candidate"]objectForKey:@"sdpMid"]];
        [self.webRTCPeer addICECandidate:candid connectionId:connectionId];
        
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"viewerResponse"]) {
        if([[socketDic objectForKey:@"response"] isEqualToString:@"accepted"]) {
            [self.localPeer addStream:username];
            [self.webRTCPeer processAnswer:[socketDic objectForKey:@"sdpAnswer"] connectionId:connectionId];
        } else {
            [self configureInfoLabel:[socketDic objectForKey:@"message"]];
            [self closeConnection];
            infoView.hidden = NO;
            footerView.hidden = YES;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"stopCommunication"]) {
        [self checkScheduleDone];
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"nopresenter"]) {
  [self configureInfoLabel:[@"Presenter Not Available" LocalString:_current_lang] ];
        [self closeConnection];
        infoView.hidden = NO;
        footerView.hidden = YES;
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"connectionError"]) {
        [self configureInfoLabel: [@"Connection Error" LocalString:_current_lang]];
        [self closeConnection];
        infoView.hidden = NO;
        footerView.hidden = YES;
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"startCommunication"]) {
        infoView.hidden = YES;
        footerView.hidden = NO;
        [self createWebrtcStream];
    } else if([[socketDic objectForKey:@"id"] isEqualToString:@"commentMessage"]) {
        NSDictionary *commentDic = [socketDic objectForKey:@"cmt"];
        if(![user_id isEqualToString:[commentDic objectForKey:@"user_id"]]) {
             [self addCommentInUI:[commentDic objectForKey:@"cmt_text"] andUserID:[commentDic objectForKey:@"user_id"]  andUserName:[commentDic objectForKey:@"username"]];
        }
       
    } else if ([[socketDic objectForKey:@"id"] isEqualToString:@"likeMessage"]) {
        if(![user_id isEqualToString:[socketDic objectForKey:@"userid"]]) {
            int liker = [[self.model objectForKey:@"like_count"] intValue];
            liker = liker + 1;
            NSString *liker_obj = [NSString stringWithFormat:@"%i",liker];
            [self.model setObject:liker_obj forKey:@"like_count"];
            user_online.text = [self.model objectForKey:@"like_count"];
            UIImage *bubble = [UIImage imageWithIcon:@"fa-heart" backgroundColor:[UIColor clearColor] iconColor:[self randomColor] fontSize:32.0f];
            [self.floatingView addImage:bubble];
            [self.floatingView animate];
        }
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"WebSocket closed");
    self.title = @"Connection Closed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
    NSLog(@"WebSocket received pong");
}

#pragma mark -- webrtc

-(void) createWebrtcStream {
    
    _localPeer = [[NBMPeer alloc] initWithId:username];
    
    _mediaConfiguration = [NBMMediaConfiguration defaultConfiguration];
    NBMWebRTCPeer *webSession = [[NBMWebRTCPeer alloc] initWithDelegate:self configuration:_mediaConfiguration];
    self.webRTCPeer = webSession;
    BOOL started = [self.webRTCPeer startLocalMedia];
    
    if (started) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self generateOfferForPeer:_localPeer];
        });
    }
}

-(void) showLive : (RTCVideoTrack *) track {
    
    CGFloat rWidth;
    RTCEAGLVideoView *renderView;
    
    if(screenWidth<screenHeight) {
        rWidth= screen_height/screenHeight * screenWidth;
        CGFloat rOriginX = (rWidth - screen_width) / 2 ;
        renderView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectMake(-rOriginX, 0, rWidth, screen_height)];
    } else {
        rWidth= screen_width/screenWidth * screenHeight;
        CGFloat rOriginY = (rWidth - screen_height) / 2 ;
        renderView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectMake(0,-rOriginY, screen_width, rWidth)];
    }
    renderView.backgroundColor = [UIColor colorWithRed:85.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1.0f];
    
    renderView.delegate=self;
    [track addRenderer:renderView];
    [self.streamView addSubview:renderView];
}

- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size {
    if(screenHeight!=size.height && screenWidth!=size.width) {
        screenHeight = size.height;
        screenWidth = size.width;
        [[self.streamView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLive:_stream];
        });
    }
}

- (NSString *)connectionIdOfPeer:(NBMPeer *)peer {
    if (!peer) {
        peer = [self localPeer];
    }
    NSString *connectionId = peer.identifier;
    
    return connectionId;
}

- (void)generateOfferForPeer:(NBMPeer *)peer {
    NSString *connectionId = [self connectionIdOfPeer:peer];
    [self.webRTCPeer generateOffer:connectionId withDataChannels:YES];
    
    
}

-(NSString *) createJsonFromDic:(NSDictionary *) Dic {
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Dic options:0 error:&err];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

-(NSDictionary *) createDicfromJson : (NSString *) Json {
    NSError *err;
    NSData *jsonData = [Json dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&err];
}

- (void)setAudioOutputSpeaker:(BOOL)enabled
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    AVAudioSessionCategoryOptions options = audioSession.categoryOptions;
    if (options & AVAudioSessionCategoryOptionDefaultToSpeaker) return;
    options |= AVAudioSessionCategoryOptionDefaultToSpeaker;
    [audioSession setActive:YES error:nil];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:options error:nil];
}

- (void)didSessionRouteChange:(NSNotification *)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
        case AVAudioSessionRouteChangeReasonCategoryChange: {
            // Set speaker as default route
            NSError* error;
            [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        }
            break;
            
        default:
            break;
    }
}


#pragma mark -- webrtc delegate

- (void)webRTCPeer:(NBMWebRTCPeer *)peer hasICECandidate:(RTCIceCandidate *)candidate forConnection:(NBMPeerConnection *)connection {
    
    NSDictionary *params = @{@"candidate": candidate.sdp,
                             @"sdpMid": candidate.sdpMid ?: @"",
                             @"sdpMLineIndex": @(candidate.sdpMLineIndex)};
    
    NSDictionary *dic = @{@"id":@"onIceCandidateForRoom",@"name":broadcast_id,@"candidate":params};
    NSString *jsonString = [self createJsonFromDic:dic];
    [_webSocket sendString:jsonString error:nil];
    NSLog(@"local connection %@",candidate);
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didGenerateOffer:(RTCSessionDescription *)sdpOffer forConnection:(NBMPeerConnection *)connection {
    NSDictionary *dic = @{@"id":@"joinroom",@"name":broadcast_id,@"sdpOffer":sdpOffer.sdp,@"userID":user_id,@"userName":username};
    NSString *jsonString = [self createJsonFromDic:dic];
    [_webSocket sendString:jsonString error:nil];
    
    NSLog(@"offer generated %@",sdpOffer);
    
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didAddStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
    self.stream = remoteStream.videoTracks.lastObject;
    [self showLive:remoteStream.videoTracks.lastObject];
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didRemoveStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
}

- (void)webrtcPeer:(NBMWebRTCPeer *)peer iceStatusChanged:(RTCIceConnectionState)state ofConnection:(NBMPeerConnection *)connection {
    
}


#pragma mark -- api parsing

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            dictionary = (NSDictionary*)object;
        }
        else
        {
            dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    
    return [dictionary dictionaryByReplacingNullsWithStrings];
}

-(NSString*) suffixNumber:(NSNumber*)number
{
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    
    num = llabs(num);
    
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log10l(num) / 3.f); //log10l(1000));
    
    NSArray* units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)]];
}

#pragma mark -- toolview delegate

-(void) selectedItem:(NSString *)selectedValue {
    overlay_btn.hidden = YES;
    toolView.hidden = YES;
    footerView.hidden = NO;
    headerView.hidden = NO;
    toogleView.hidden = NO;
    CGRect framer = toolView.frame;
    framer.origin.y = screen_height;
    toolView.frame = framer;

    if([selectedValue isEqualToString:[@"broadcast details" LocalString:_current_lang]] ) {
        [self showDetailView];
    } else if ([selectedValue isEqualToString:[@"share broadcast" LocalString:_current_lang]]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SHAREURL, broadcast_id]];
        UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:nil];
        [self presentViewController:activityViewController animated:YES completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    } else if ([selectedValue isEqualToString:[@"report broadcast" LocalString:_current_lang]]) {
        [self actionShowReport:nil];
    }
}
-(void) closeToolView {
    [self moreClose:nil];
}

#pragma mark -- show detail view

-(void) showDetailView {
    if(detailView==nil) {
        if(iPhonex || iPhonexs_max) {
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                 detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height-40) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
            } else {
                detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
            }
        } else {
            detailView = [[BSELiveDetailVW alloc] initWithFrame:CGRectMake(0, screen_height, screen_width, screen_height) andBroadcastDetails:self.model andCurrentLangCode:self.current_lang];
        }
        
        detailView.delegate = self;
        [self.view addSubview:detailView];
    }
    overlay_btn.hidden = NO;
    detailView.hidden =  NO;
    toolView.hidden =   YES;
    footerView.hidden = YES;
    headerView.hidden = YES;
    toogleView.hidden = YES;
    [overlay_btn addTarget:self action:@selector(actionDetailClose:) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = detailView.frame;
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if(iPhonex || iPhonexs_max) {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                framer.origin.y = 40;
            } else {
                framer.origin.y = 0;
            }
        } else {
            framer.origin.y = 0;
        }
        detailView.frame = framer;
    } completion:^(BOOL finished) {
    }];
}

-(IBAction)actionDetailClose:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect framer = detailView.frame;
        framer.origin.y = screen_height;
        detailView.frame = framer;
    } completion:^(BOOL finished) {
        overlay_btn.hidden = YES;
        detailView.hidden = YES;
        footerView.hidden = NO;
        headerView.hidden = NO;
        toogleView.hidden = NO;
    }];
}

-(void) closeDetailView {
    [self actionDetailClose:nil];
}

-(void)updateViewerCount:(NSString *)Count
{
    [self.model setObject:Count forKey:@"viewer_count"];
    viewer_count.text = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"viewer_count"]];
}



#pragma mark -- keyboard event

- (void)keyboardWasShown:(NSNotification *)notification
{
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSDictionary *info = [notification userInfo];
    NSNumber *number = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    duration = [number doubleValue];
    //[commentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [UIView animateWithDuration:duration
                          delay:0.00f
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                        CGRect framer = footerView.frame;
                        framer.origin.y = (iPhonex || iPhonexs_max)?(screen_height-45) - (keyboardSize.height+45):screen_height - (keyboardSize.height+45);
                        footerView.frame = framer;
                     }
                     completion:^(BOOL finished) {
                     }];
    
    if(!reportView.isHidden) {
        reportView.frame = CGRectMake((screen_width-300)/2,100, 300, 235);
    }
 
}

- (void) keyboardWillHide:(NSNotification *)notification {
    // [commentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [UIView animateWithDuration:duration
                          delay:0.00f
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         CGRect framer = footerView.frame;
                         framer.origin.y = (iPhonex || iPhonexs_max)? (screen_height-78) - 45 : screen_height - 45;
                         footerView.frame = framer;
                     }
                     completion:^(BOOL finished) {
                     }];
}


#pragma mark -- comment functions

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 100;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    NSString *commentText = [comment_field.text stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
    if(commentText.length>0) {

        [self addComment:commentText];
        
    }
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextView *)textView
{
    CGRect frame;
    frame = footer_commentView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - 20;
    frame.origin.x = 10;
    footer_commentView.frame=frame;
    
    footer_shareView.hidden=footer_repostView.hidden=moreView.hidden=userLikeView.hidden=YES;
}
- (void)textFieldDidEndEditing:(UITextView *)textView
{
    CGRect frame;
    frame = footer_commentView.frame;
    frame.size.width = UIScreen.mainScreen.bounds.size.width - (userLikeView.frame.size.width+moreView.frame.size.width+footer_repostView.frame.size.width+footer_shareView.frame.size.width+60);
    frame.origin.x = 10;
    footer_commentView.frame=frame;
footer_shareView.hidden=footer_repostView.hidden=moreView.hidden=userLikeView.hidden=NO;
}

-(CGFloat)HeightCalculation :(NSString *)StringData andWidth:(CGFloat) width {
    
    NSString *OriginalFrontendText    = StringData;
    UIFont *font                      = [UIFont fontWithName:@"Poppins-Regular" size:12.0];
    CGFloat descriptionHeight         = [OriginalFrontendText boundingRectWithSize:CGSizeMake((width-56), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}


-(void) addComment: (NSString *) message {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"5",@"action_type",message,@"cmt_desc",@"",@"like_count",self.current_lang,@"language",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            NSString *comment_id = [NSString stringWithFormat:@"%@",[Dic objectForKey:@"broacast_id"]];
            NSString *picture = [NSString stringWithFormat:@"%@image/user/%@",APIURL,user_id];
             if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
                NSDictionary *dic = @{@"id":@"sendMessageToRoom",@"name":broadcast_id,@"comment":@{@"cmt_id":comment_id,@"user_id":user_id,@"author_pic":picture,@"broadcast_id":broadcast_id,@"cmt_text":message,@"created_at":@"Just Now",@"username":username}};
                NSString *jsonString = [self createJsonFromDic:dic];
                [_webSocket sendString:jsonString error:nil];
             } else {
                 NSDictionary *dic = @{@"id":@"sendMessageToOfflineRoom",@"name":broadcast_id,@"comment":@{@"cmt_id":comment_id,@"user_id":user_id,@"author_pic":picture,@"broadcast_id":broadcast_id,@"cmt_text":message,@"created_at":@"Just Now",@"username":username}};
                 NSString *jsonString = [self createJsonFromDic:dic];
                 [_webSocket sendString:jsonString error:nil];
             }
            comment_field.text = @"";
            [self addCommentInUI:message andUserID:user_id andUserName:username];
        }
        
        
    }];
    [postDataTask resume];
}

-(void) addCommentInUI:(NSString *) message andUserID : (NSString *) userID andUserName: (NSString *) userName {
    
    NSString *userImage = [NSString stringWithFormat:@"%@image/user/%@",APIURL,userID];
    CGFloat width, height;
    UIFont *font = [UIFont fontWithName:@"Poppins-Regular" size:12.0];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    width = [message sizeWithAttributes:attributes].width + 58;
    CGFloat userWidth =  [[NSString stringWithFormat:@"@%@",userName] sizeWithAttributes:attributes].width + 58;
    
    if(userWidth>=width) {
        width = userWidth;
    }
    
    if(width>=commentView.frame.size.width) {
        width = commentView.frame.size.width;
    }
    
    height = 26 + [self HeightCalculation:message andWidth:width];
    CGRect framer;
    if(commentView.subviews.count>0) {
        UIView *lastView = commentView.subviews.lastObject;
        framer.origin.x = 0;
        framer.size.height = height;
        framer.size.width = width;
        framer.origin.y = lastView.frame.origin.y + lastView.frame.size.height + 8;
    } else {
        framer.origin.x = 0;
        framer.size.height = height;
        framer.size.width = width;
        framer.origin.y = commentView.frame.size.height - height;
    }
    BSECommentVW *comment = [[BSECommentVW alloc] initWithFrame:framer andDic:@{@"username":[NSString stringWithFormat:@"@%@",userName],@"userimage":userImage,@"comment":message} andCurrentLangCode:self.current_lang];
    [commentView addSubview:comment];
    
    UIView *lastView = commentView.subviews.lastObject;
    
    CGPoint bottomOffset = CGPointMake(0, (lastView.frame.size.height + lastView.frame.origin.y) - commentView.bounds.size.height);
    [commentView setContentOffset:bottomOffset animated:YES];
    
}

#pragma mark -- report

-(void) configureReport {
    
    close_icon.text = [NSString fontAwesomeIconStringForEnum:FATimes];
    reportView.frame = CGRectMake((screen_width-300)/2,-235, 300, 235);
    [self.view addSubview:reportView];
    reportView.hidden = YES;
    
    report_field.textColor = [UIColor darkGrayColor];
    report_field.text =  [@"Report Message" LocalString:_current_lang];
   
    
}

-(IBAction)actionCloseReport:(id)sender {
    
    report_field.text = @"";
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        reportView.frame = CGRectMake((screen_width-300)/2,screen_height, 300, 235);
    } completion:^(BOOL finished) {
        footerView.hidden = NO;
        headerView.hidden = NO;
        reportView.hidden = YES;
        reportView.frame = CGRectMake((screen_width-300)/2,-235, 300, 235);
    }];
}

-(IBAction)actionReportStream:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 NSString *desc =  [report_field.text isEqualToString:[@"Report Message" LocalString:_current_lang]] ? @"" : report_field.text;
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODREPORTBROADCAST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"userid",broadcast_id, @"broadcast_id",desc,@"description",self.current_lang,@"language",nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
   
 [TSMessage showNotificationWithTitle:[@"Report sent" LocalString:_current_lang] type:TSMessageNotificationTypeSuccess];
    [self actionCloseReport:nil];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        NSLog(@"testing dic %@",Dic);
        if ([[Dic objectForKey:@"status"] integerValue]) {
        }
    }];
    [postDataTask resume];
    
}

-(IBAction)actionShowReport:(id)sender {
    [self.view bringSubviewToFront:reportView];
    footerView.hidden = YES;
    headerView.hidden = YES;
    reportView.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        reportView.frame = CGRectMake((screen_width-300)/2,(screen_height-235)/2, 300, 235);
    } completion:^(BOOL finished) {
    }];
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    
    if([textView.text isEqualToString:[@"Report Message" LocalString:_current_lang]]) {
        textView.text = @"";
        report_field.textColor = [UIColor colorWithRed:193.0f/255.0f green:193.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
    }
}

-(void) textViewDidEndEditing:(UITextView *)textView {
    if([textView.text isEqualToString:@""]) {
       textView.text = [@"Report Message" LocalString:_current_lang];
        report_field.textColor = [UIColor darkGrayColor];
    }
}

#pragma mark -- like action
-(IBAction)actionLikeAction:(id)sender {
    
    UIImage *bubble = [UIImage imageWithIcon:@"fa-heart" backgroundColor:[UIColor clearColor] iconColor:[self randomColor] fontSize:32.0f];
    [self.floatingView addImage:bubble];
    [self.floatingView animate];
    
    count_icon.text = [NSString fontAwesomeIconStringForEnum:FAHeart];
    int liker = [[self.model objectForKey:@"like_count"] intValue];
    liker = liker + 1;
    NSString *liker_obj = [NSString stringWithFormat:@"%i",liker];
    [self.model setObject:liker_obj forKey:@"like_count"];
    
    NSString *like_count = [NSString stringWithFormat:@"%@",[self.model objectForKey:@"like_count"]];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:like_count];
    user_online.text = [self suffixNumber:myNumber];
    

    if([[self.model objectForKey:@"broadcast_status"] isEqualToString:@"live"]) {
        NSDictionary *dic = @{@"id":@"sendLikeToRoom",@"name":broadcast_id, @"userid":user_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    } else {
        NSDictionary *dic = @{@"id":@"sendLikeToOfflineRoom",@"name":broadcast_id, @"userid":user_id};
        NSString *jsonString = [self createJsonFromDic:dic];
        [_webSocket sendString:jsonString error:nil];
    }

    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:NSOperationQueue.mainQueue];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APIURL,METHODBROADCASTACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [request setHTTPMethod:@"POST"];

    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: access_token, @"access_token",user_id, @"user_id",broadcast_id, @"broadcast_id",@"4",@"action_type",@"",@"cmt_desc",@"1",@"like_count",self.current_lang,@"language",nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *Dic = [self dictionaryFromResponseData:data];
        if ([[Dic objectForKey:@"status"] integerValue]) {
            NSLog(@"liked in background");
        }
    }];
    [postDataTask resume];
}

-(UIColor *)randomColor
{
    CGFloat red   = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue  = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}


@end


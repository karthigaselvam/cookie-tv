//
//  BSEScheduleViewerVC.h
//  Emenator
//
//  Created by JITENDRA on 10/12/17.
//

#import <UIKit/UIKit.h>
#import "NSDictionary+ReplaceNull.h"

@interface BSEScheduleViewerVC : UIViewController <UITextFieldDelegate> {
    IBOutlet UIView *headerView, *toogleView, *footerView, *infoView, *cameraView, *muteView, *moreView, *countView, *reportView, *likeView, *mainView,*userLikeView,*closeView,*footer_repostView,*footer_shareView,*footer_commentView,*user_headerView,*user_MainheaderView;
    IBOutlet UILabel *camera_icon, *mute_icon, *visitor_icon, *toogle_icon, *user_online, *info_label, *more_icon, *count_icon, *close_icon,*reportTitle_lbl,*lbl_repost,*lbl_share,*lbl_info;
    IBOutlet UIButton *stop_btn, *overlay_btn,*submit_btn;
    IBOutlet UITextField *comment_field;
    IBOutlet UITextView *report_field;
    IBOutlet UIScrollView *commentView;
    BOOL isHeaderOpen, isBackCamera, isReportOpen;
    NSMutableArray *onlineUSers;
    CGFloat screenWidth, screenHeight;
    IBOutlet UIImageView *userImage;
    IBOutlet UILabel *user_label,*viewer_count,*viewer_icon;
}
@property (nonatomic) BOOL isPresented;
@property (nonatomic,strong) NSMutableDictionary *model;
@property (nonatomic,strong) NSMutableDictionary *user;
@property (nonatomic,strong) NSString *current_lang;

@end


//
//  BSEFeaturedVC.swift
//  liveplus
//
//  Created by BSEtec on 19/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit




class BSEFeaturedVC: UIViewController, UITableViewDataSource, UITableViewDelegate, BSESliderVWDelegate {
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var header_title : UILabel?;
    @IBOutlet var headerView : UIView?;
    var tableData = [AnyObject]();
    var headerData = [AnyObject]();
    var isAPILoading: Bool!;
    @IBOutlet var searchView : UIView?;
    @IBOutlet var ProfileView : UIView?;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    //MARK: - custom function
    
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = ProfileView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (ProfileView!.frame.size.width);
            ProfileView!.frame = framer;
            
            framer = searchView!.frame;
            framer.origin.x = 0;
            searchView!.frame = framer;
            
            searchView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
        }
        isAPILoading = false;
        self.isFirstTime = true;
        self.currentpage = 1;
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        self.getTrendingUsers();
        
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
    }
    
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getTrendingUsers();
            self.isPullToRefresh = true;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getTrendingUsers();
                }
            }
        }
    }
    
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    
    func getTrendingUsers() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&page_no="+String(currentpage)+"&&keyword=&&sort=2"+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        apiservice().apiGetRequest(params:parameter, withMethod: METHODSEARCHPEOPLE,  andCompletionBlock: { (success) -> Void in
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                if(self.isFirstTime) {
                    self.tableData.removeAll();
                    self.headerData.removeAll();
                }
                if(commonModel.temparray.count>0) {
                    if(self.isFirstTime) {
                        var incr:Int! = 1;
                        for dic in commonModel.temparray {
                            if(incr<=4) {
                                self.headerData.append(dic);
                            } else {
                                self.tableData.append(dic);
                            }
                            incr = incr + 1;
                        }
                        self.isFirstTime = false;
                    } else {
                       self.tableData.append(contentsOf:commonModel.temparray);
                    }
                    self.isAPILoading = false;
                } 
            } else {
                self.isAPILoading = false;
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
            }
            
            if(self.tableData.count==0) {
                self.tblView?.tableFooterView = self.noDataView();
            }
            if(self.headerData.count>0) {
                self.tblView?.tableFooterView = UIView.init();
            }
            self.tblView?.reloadData();
            self.hideLoader();
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
        }) { (failure) -> Void in
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.isAPILoading = false;
            self.hideLoader();
        }
    }
    
    func setDefaultHeader() -> Void {
        let slider:BSESliderVW = BSESliderVW.init(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.width*0.8), type: "people", data: headerData);
        slider.delegate=self;
        tblView?.tableHeaderView = slider
        
    }
    
    func setLoader() {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    
    func hideLoader() {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func gotoProfile(model: BSEUserModel) {
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = model
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func gotoBroadcast(model: BSEBroadcastModel) {
        
    }
    
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
 
    @objc func userAction(_ sender: UIButton){ //<- needs `@objc`
        let model:BSEUserModel = tableData[sender.tag] as! BSEUserModel
        if(model.user_follow_status==0 || model.user_follow_status==1) {
            var follower_status:Int!;
            follower_status = model.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(model.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(model.is_private==1) {
                model.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
                  SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                    
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);

                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
                    follower_count = follower_count + 1;
                   SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    follower_count = follower_count - 1;
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                model.user_follow_status = (follower_status==1) ? 1:0;
            }
            self.tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }
    
    @IBAction func actionProfile(sender: UIButton!) {
//        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
//        let model:BSEUserModel = BSEUserModel();
//        model.user_id = Int(getBSEValue(_keyname: USERID));
//        model.access_token = getBSEValue(_keyname: ACCESSTOKEN);
//        model.username = getBSEValue(_keyname: USERNAME);
//        model.first_name = getBSEValue(_keyname: FIRSTNAME);
//        model.last_name = getBSEValue(_keyname: LASTNAME);
//        model.country = getBSEValue(_keyname: COUNTRY);
//        model.state = getBSEValue(_keyname: STATE);
//        model.email = getBSEValue(_keyname: EMAIL);
//        model.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
//        model.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
//        model.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
//        model.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
//        model.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
//        model.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
//        model.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
//        model.role = Int(getBSEValue(_keyname: ROLE));
//        profile.model = model
//        self.navigationController?.pushViewController(profile, animated: true)
        
        let Jgroup = BSEHomeMainVC(nibName: "BSEHomeMainVC", bundle: nil)
        self.navigationController?.pushViewController(Jgroup, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        let uidentifier = "UserCell"
        var cell: UserCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell
        if cell == nil {
             tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "UserCellRTL" : "UserCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell;
        }
        cell.setDefaultValue(model: tableData[indexPath.row] as! BSEUserModel)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.follow_action?.tag = indexPath.row;
        cell.follow_action?.addTarget(self, action: #selector(self.userAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: BSEUserModel = tableData[indexPath.row] as! BSEUserModel
        self.gotoProfile(model: model);
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

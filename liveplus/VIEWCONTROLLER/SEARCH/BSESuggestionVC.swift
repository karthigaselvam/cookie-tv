//
//  BSESuggestionVC.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MapKit
class BSESuggestionVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var header_title : UILabel?;
    @IBOutlet var headerView : UIView?;
    @IBOutlet var leftView : UIView?;
    @IBOutlet var rightView : UIView?;
    @IBOutlet var suggestion_btn : UIButton?;
    @IBOutlet var map_btn : UIButton?;
    @IBOutlet var map_border : UIView?;
    @IBOutlet var suggestion_border : UIView?;
    @IBOutlet var mapView: MKMapView!
    
    var tableData = [AnyObject]();
    var mapData = [BSEMapModel]();
    var isAPILoading: Bool!;
    var page_type:String!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var isInfiniteScrolling: Bool!;
     var isPullToRefresh: Bool!
        var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!
    @IBOutlet var backView : UIView?;
    
    @IBOutlet var featured_title : UILabel?;
    @IBOutlet var nearBy_title : UILabel?;
    @IBOutlet var new_title : UILabel?;

    @IBOutlet var featured_border : UILabel?;
    @IBOutlet var nearBy_border : UILabel?;
    @IBOutlet var new_border : UILabel?;
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: custom function
    func initFields() -> Void {
        
        suggestion_btn?.setTitle( "suggestion".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        map_btn?.setTitle( "map".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = 10;
            backView!.frame = framer;
            backView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
        }
        
        var framer:CGRect! = leftView?.frame;
        framer.size.width = UIScreen.main.bounds.size.width/2;
        leftView?.frame = framer;
        
        framer = rightView?.frame;
        framer.origin.x = UIScreen.main.bounds.size.width/2
        framer.size.width = UIScreen.main.bounds.size.width/2;
        rightView?.frame = framer;
        
        
        tblView?.isHidden=false;
        mapView?.isHidden=true;
        map_border?.isHidden = true;
       // map_btn?.setTitleColor(UIColor.init(red: 177.0/255.0, green: 191.0/255.0, blue: 199.0/255.0, alpha: 1.0), for: .normal)
        suggestion_border?.isHidden = false;
       // suggestion_btn?.setTitleColor(UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0), for: .normal)
        
        page_type = "map";
        isFirstTime = true;
        currentpage = 1;
       
        CommonFunction(1)

        mapView.delegate = self;
        mapView.register(BSESuggestionMapView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//        let mapModel = BSEMapModel(title: "King David Kalakaua",
//                              locationName: "Waikiki Gateway Park",
//                              discipline: "Sculpture",
//                              coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
//        mapView.addAnnotation(mapModel)
        
        let span = MKCoordinateSpanMake(180, 360)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0000, longitude: 0.0000), span: span)
        mapView.setRegion(region, animated: true)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
        
        
    }
    
    func getSuggestionBroadcast() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&page_no="+String(currentpage)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        leftView?.isUserInteractionEnabled = false;
        rightView?.isUserInteractionEnabled = false;
        apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTSUGGESTION,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                if(self.isFirstTime) {
                    self.tableData.removeAll();
                    self.isFirstTime = false;
                }
                if(commonModel.temparray.count>0) {
                    self.tableData.append(contentsOf:commonModel.temparray);
                    self.isAPILoading = false;
                }
            } else {
                self.isAPILoading = false;
                SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            
            if(self.tableData.count==0) {
                self.tblView?.tableFooterView = self.noDataView();
            } else {
                self.tblView?.tableFooterView = UIView.init();
            }
            self.tblView?.reloadData();
            self.hideLoader();
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.leftView?.isUserInteractionEnabled = true;
            self.rightView?.isUserInteractionEnabled = true;
        }) { (failure) -> Void in
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.isAPILoading = false;
            self.hideLoader();
        }
    }
    
    func getLiveBroadcast() -> Void {
        leftView?.isUserInteractionEnabled = false;
        rightView?.isUserInteractionEnabled = false;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTLIVE,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                if(commonModel.temparray.count>0) {
                    for Dic in commonModel.temparray {
                        let broadcast: BSEBroadcastModel = Dic as! BSEBroadcastModel;
                        let locationName: String = "Post by "+broadcast.author_name.capitalized;
                        let lat = (broadcast.latitude as NSString).doubleValue
                        let longit = (broadcast.longitude as NSString).doubleValue
                        let mapModel: BSEMapModel = BSEMapModel(title: broadcast.broadcast_name,
                                                   broadcast_id: broadcast.broadcast_id,
                                                   user_id: broadcast.author_id,
                                                   broadcast_status: broadcast.broadcast_status,
                                                   locationName: locationName,
                                                   discipline: "Sculpture",
                                                   coordinate: CLLocationCoordinate2D(latitude: lat, longitude: longit))
                        self.mapData.append(mapModel);
                    }
                    self.mapView.addAnnotations(self.mapData);
                }
            } else {
                SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            self.leftView?.isUserInteractionEnabled = true;
            self.rightView?.isUserInteractionEnabled = true;
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
            self.leftView?.isUserInteractionEnabled = true;
            self.rightView?.isUserInteractionEnabled = true;
        }
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    //MARK: control function
    @IBAction func actionListSelect(sender: UIButton!) {
       CommonFunction(sender.tag)
    }
    
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }
    
    
    @IBAction func actionMainMenu(sender: UIButton!) {
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(sender.tag==0 )
        {
             NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"feature"])
            appDelegate.tabcontroller?.selectedIndex = 0
        }
        if(sender.tag==2)
        {
            NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"new"])
            appDelegate.tabcontroller?.selectedIndex = 0
        }
    }
    
    
    @objc func CommonFunction(_ sender: NSInteger){
        suggestion_border?.isHidden = true;
        map_border?.isHidden = true;
        map_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        suggestion_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        if(sender == 0) {
            tblView?.isHidden=false;
            mapView?.isHidden=true;
            suggestion_border?.isHidden = false;
            suggestion_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "suggestion";
            isPullToRefresh = false;
            isInfiniteScrolling = false;
            self.isFirstTime = true;
            self.currentpage = 1;
            getSuggestionBroadcast()
            tblView?.tableFooterView = UIView (frame: CGRect.zero)
        } else {
            tblView?.isHidden=true;
            mapView?.isHidden=false;
            map_border?.isHidden = false;
            map_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "map";
            getLiveBroadcast();
        }
    }
    
    /**
     This is the function that help refresh UITableView through pull to refresh action
     */
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getSuggestionBroadcast();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getSuggestionBroadcast();
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    @objc func goToProfile(_ sender: UIButton){
        let model : BSEBroadcastModel = tableData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        let identifier = "BroadcastCell"
        var cell: BroadcastCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell
        if cell == nil {
            tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "BroadcastCellRTL" : "BroadcastCell", bundle: nil), forCellReuseIdentifier: identifier);
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell;
        }
        cell .setDefaultValue(model: tableData[indexPath.row] as! BSEBroadcastModel)
        cell.broadcast_user_btn.tag = indexPath.row;
        cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToProfile(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var totalHeight:CGFloat = 100;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        let height = heightForView(text: model.broadcast_name, font: UIFont(name: "Poppins-Regular", size: 13)!, width: UIScreen.main.bounds.size.width-130)
        totalHeight = totalHeight+height;
        
        return totalHeight > 120 ? totalHeight : 120;
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
             presenter.page_from = "detail"
            presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"viewer_count":model.viewer_count];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }

}

// MARK: - MKMapViewDelegate

extension BSESuggestionVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location : BSEMapModel = view.annotation as! BSEMapModel
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        if(String(describing: location.user_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.page_from = "detail"
            presenter.model = ["broadcast_id":String(location.broadcast_id),"broadcast_status":location.broadcast_status];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(location.broadcast_id),"broadcast_status":location.broadcast_status];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
             viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    
}

// MARK: - mapview annotation
class BSESuggestionMapView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? BSEMapModel else {return}
            
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "navigate.png"), for: UIControlState())
            rightCalloutAccessoryView = mapsButton
            image = UIImage(named: "location.png")
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = artwork.subtitle
            detailCalloutAccessoryView = detailLabel
        }
    }
    
}



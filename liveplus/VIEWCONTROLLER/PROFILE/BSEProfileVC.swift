//
//  BSEProfileVC.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage
import MaterialIconsSwift

class BSEProfileVC: UIViewController, BSESliderVWDelegate {
    @IBOutlet var headerView : UIView?;
    @IBOutlet var page_scroll : UIScrollView?;
    @IBOutlet var menuIcon : UIView?;
    @IBOutlet var userName : UILabel?;
    @IBOutlet var userEmail : UILabel?;
    @IBOutlet var userImage : UIImageView?;
    @IBOutlet var avatarImage : UIImageView?;
    @IBOutlet var editView : UIView?;
    @IBOutlet var edit_icon : UILabel?;
    @IBOutlet var avatarView : UIView?;
    @IBOutlet var avatarEditView : UIView?;
    @IBOutlet var camera_icon : UILabel?;
    @IBOutlet var actionView : UIView?;
    @IBOutlet var broadcast_label : UILabel?;
    @IBOutlet var follower_label : UILabel?;
    @IBOutlet var following_label : UILabel?;
    @IBOutlet var group_label : UILabel?;
    @IBOutlet var broadcastView : UIView?;
    @IBOutlet var followerView : UIView?;
    @IBOutlet var followingView : UIView?;
    @IBOutlet var groupView : UIView?;
    @IBOutlet var notificationView : UIView?;
     @IBOutlet var featureView : UIView?;
    @IBOutlet var recentView : UIView?;
    @IBOutlet var recentView_header : UIView?;
    @IBOutlet var recentView_content : UIView?;
    @IBOutlet var recentView_noData: UILabel?;
    
    @IBOutlet var followActionView : UIView?;
    @IBOutlet var followActionImage : UIImageView?;
    
    @IBOutlet var lbl_broadcast: UILabel?;
    @IBOutlet var lbl_followers: UILabel?;
    @IBOutlet var lbl_following: UILabel?;
    @IBOutlet var lbl_group: UILabel?;
    @IBOutlet var lbl_recent: UILabel?;
      @IBOutlet var backView : UIView?;
    
    
    var progressBar:AMProgressBar!
    var task: URLSessionDataTask!;
    var model: BSEUserModel!;
    var isOwner: Bool!;
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.task==nil) {
        } else {
            if(self.task.state == .running) {
                self.task.cancel();
            }
        }
    }
    
    //MARK: -- custom function
    func initFields() -> Void {

        lbl_broadcast?.text =  "BROADCASTS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_followers?.text = "FOLLOWERS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_following?.text = "FOLLOWING".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_group?.text =  "GROUPS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_recent?.text = "RECENTLY WATCHED".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        recentView_noData?.text = "No Recent".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            framer = menuIcon!.frame;
            framer.origin.x = 0;
            menuIcon!.frame = framer;
            
            menuIcon?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            
            framer = followActionView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (followActionView!.frame.size.width);
            followActionView!.frame = framer;
            
            framer = editView!.frame;
            framer.origin.x = 0;
            editView!.frame = framer;
            
            //editView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
        } else {
            var framer: CGRect = editView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (editView!.frame.size.width);
            editView!.frame = framer;
        }
        
        if(model == nil)
        {
             let model1:BSEUserModel = BSEUserModel();
            model1.user_id = Int(getBSEValue(_keyname: USERID));
                        model1.access_token = getBSEValue(_keyname: ACCESSTOKEN);
                        model1.username = getBSEValue(_keyname: USERNAME);
                        model1.first_name = getBSEValue(_keyname: FIRSTNAME);
                        model1.last_name = getBSEValue(_keyname: LASTNAME);
                        model1.country = getBSEValue(_keyname: COUNTRY);
                        model1.state = getBSEValue(_keyname: STATE);
                        model1.email = getBSEValue(_keyname: EMAIL);
                        model1.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
                        model1.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
                        model1.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
                        model1.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
                        model1.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
                        model1.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
                        model1.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
                        model1.role = Int(getBSEValue(_keyname: ROLE));
            model = model1;
        }
        
        let userID:String = String(model.user_id)
        if(userID == getBSEValue(_keyname: USERID)) {
            menuIcon?.isHidden = false;
            editView?.isHidden = false;
            avatarEditView?.isHidden = false;
            isOwner = true;
            followActionView?.isHidden = true;
            NotificationCenter.default.addObserver(self, selector: #selector(self.profileNotify(notification:)), name: Notification.Name("profileNotify"), object: nil)
        notificationView?.isHidden = false;
            featureView?.isHidden = false;
            
        } else {
            menuIcon?.isHidden = true;
            editView?.isHidden = true;
            avatarEditView?.isHidden = true;
            isOwner = false;
            followActionView?.isHidden = false;
        notificationView?.isHidden = true;
        featureView?.isHidden = true;
        }
        recentView_noData?.isHidden = true;
        recentView_content?.isHidden = true;
        userImage?.layer.masksToBounds = true;
        
        edit_icon?.font = MaterialIcons.fontOfSize(size:20);
        edit_icon?.text = MaterialIcons.Edit
        
        avatarView?.layer.borderColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0).cgColor;
        avatarView?.layer.borderWidth = 2.0
        avatarView?.layer.cornerRadius = 64;
        avatarView?.layer.masksToBounds = true;
        
        avatarImage?.layer.cornerRadius = 60;
        avatarImage?.layer.masksToBounds = true;
        
        avatarEditView?.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor;
        avatarEditView?.layer.borderWidth = 2.0
        avatarEditView?.layer.cornerRadius = 20;
        avatarEditView?.layer.masksToBounds = true;
        
        camera_icon?.font = MaterialIcons.fontOfSize(size:20);
        camera_icon?.text = MaterialIcons.CameraAlt
        
        
        let itemWidth : CGFloat = (UIScreen.main.bounds.size.width - 15) / ((isOwner) ? 4 : 3)
        var framer: CGRect = broadcastView!.frame;
        framer.size.width = itemWidth;
        broadcastView?.frame = framer;
        
        framer = followerView!.frame;
        framer.origin.x = itemWidth * 1;
        framer.size.width = itemWidth;
        followerView?.frame = framer;
        
        framer = followingView!.frame;
        framer.origin.x = itemWidth * 2;
        framer.size.width = itemWidth;
        followingView?.frame = framer;
        
        if(isOwner) {
            framer = groupView!.frame;
            framer.origin.x = itemWidth * 3;
            framer.size.width = itemWidth;
            groupView?.frame = framer;
        } else {
            groupView?.isHidden = true;
        }
        
        
//        userImage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
//        avatarImage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        userName?.text = model.username.uppercased();
        
        self.avatarImage?.image = nil;
        self.userImage?.image = nil;
        SDImageCache.shared().removeImage(forKey: USERIMGURL+String(model.user_id), withCompletion: {
            self.userImage?.sd_setImage(with: URL(string: USERIMGURL+String(self.model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
            self.avatarImage?.sd_setImage(with: URL(string: USERIMGURL+String(self.model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        })
       // updateProfileFields();
        getProfileInfo();
        
    }
    
    func getProfileInfo() -> Void {
        let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+String(model.user_id)+"&&username="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
          
   SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
            hideLoader();
        }
        let originalString = APIURL + METHODGETPROFILE + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODGETPROFILE, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            let userModel : BSEUserModel = commonModel.temparray[0] as! BSEUserModel;
                            if(self.isOwner) {
                                self.saveUser(model: userModel);
                            }
                            self.model = userModel;
                            self.updateProfileFields();
                        } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        self.getRecentlyWatched();
                    } else {
                        self.hideLoader();
                    }
                } else {
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    func getRecentlyWatched() -> Void {
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+String(model.user_id)+"&&page_no="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        
        if(!apiservice().isInternetAvailable()) {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
            hideLoader();
            self.recentView_noData?.isHidden = false;
            self.recentView_content?.isHidden = true;
        }
        let originalString = APIURL + METHODRECENTLYWATCHED + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoader();
                self.recentView_noData?.isHidden = false;
                self.recentView_content?.isHidden = true;
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODRECENTLYWATCHED, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(commonModel.temparray.count==0) {
                                self.recentView_noData?.isHidden = false;
                                self.recentView_content?.isHidden = true;
                                var framer:CGRect = (self.recentView?.frame)!;
                                framer.size.height = (self.recentView_noData?.frame.size.height)! + (self.recentView_noData?.frame.origin.y)! + 10;
                                self.recentView?.frame = framer;
                            } else {
                                self.recentView_noData?.isHidden = true;
                                self.recentView_content?.isHidden = false;
                                for V in (self.recentView_content?.subviews)!{
                                    V.removeFromSuperview()
                                }
                                let slider:BSESliderVW = BSESliderVW.init(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width-20,height:UIScreen.main.bounds.size.width*0.7), type: "broadcast", data: commonModel.temparray);
                                slider.delegate=self;
                                self.recentView_content?.addSubview(slider);
                                
                                var framer:CGRect = (self.recentView_content?.frame)!;
                                framer.size.height = UIScreen.main.bounds.size.width*0.7;
                                self.recentView_content?.frame = framer;
                                
                                framer = (self.recentView?.frame)!;
                                framer.size.height = (self.recentView_content?.frame.size.height)! + (self.recentView_content?.frame.origin.y)! + 10;
                                self.recentView?.frame = framer;
                            }
                        } else {
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                        }
                        self.page_scroll?.contentSize = CGSize(width:UIScreen.main.bounds.size.width, height:(self.recentView?.frame.size.height)! + (self.recentView?.frame.origin.y)!);
                        
                        self.hideLoader();
                    } else {
                        self.recentView_noData?.isHidden = false;
                        self.recentView_content?.isHidden = true;
                        self.hideLoader();
                    }
                } else {
                    self.recentView_noData?.isHidden = false;
                    self.recentView_content?.isHidden = true;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    
    func updateProfileFields() -> Void {
        if(model.group_count==0) {
            group_label?.text =  "No".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.group_count==1) {
          group_label?.text =  "1".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
           group_label?.text = formatPoints(from: model.group_count)
        }
        
        userImage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        avatarImage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        userName?.text = (model.first_name.count > 0) ? (model.first_name+" "+model.last_name).uppercased() : model.username.uppercased();
        userEmail?.text = model.email == "null" ? "@"+model.username : model.email;
        
        if(model.follower_count==0) {
            
            follower_label?.text = "No".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.follower_count==1) {
            follower_label?.text =  "1".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
            follower_label?.text = formatPoints(from: model.follower_count)
        }
        
        if(model.broadcast_count==0) {
            broadcast_label?.text = "No".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.follower_count==1) {
            broadcast_label?.text =  "1".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
            broadcast_label?.text = formatPoints(from: model.broadcast_count)
        }
        
        if(model.following_count==0) {
            following_label?.text = "No".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(model.following_count==1) {
            following_label?.text = "1".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else  {
            following_label?.text = formatPoints(from: model.following_count)
        }
        
        if(model.user_follow_status == 0) {
            followActionImage?.image = UIImage(named: "Follow_white.png")
        } else if (model.user_follow_status == 1) {
            followActionImage?.image = UIImage(named: "unFollow_white.png")
        } else if (model.user_follow_status == 2) {
            followActionImage?.image = UIImage(named: "request_white.png")
        }
        
        if(!self.isOwner) {
            
            editView?.isHidden = false;
            if(model.is_block == 0) {
                edit_icon?.text = MaterialIcons.LockOpen
            } else {
                edit_icon?.text = MaterialIcons.LockOutline
            }
            
            if(model.blockedbyme == 1) {
                page_scroll?.isHidden = true;
           
                let alert = UIAlertController(title:"Message".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"blocked by user".localized(lang: getBSEValue(_keyname: CURRENTLANG)), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok".localized(lang: getBSEValue(_keyname: CURRENTLANG)), style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true);
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func formatPoints(from: Int) -> String {
        let number = Double(from)
        let thousand = number / 1000
        let million = number / 1000000
        
        if million >= 1.0 { return "\(round(million*10)/10)M" }
        else if thousand >= 1.0 { return "\(round(thousand*10)/10)K" }
        else { return "\(Int(number))"}
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func gotoProfile(model: BSEUserModel) {
        
    }
    func gotoBroadcast(model: BSEBroadcastModel) {
        
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
             presenter.page_from = "detail"
            presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"viewer_count":model.viewer_count];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    func saveUser(model:BSEUserModel) -> Void {
        setBSEValue(_keyname:USERID, _value:String(model.user_id))
        setBSEValue(_keyname:ACCESSTOKEN, _value:model.access_token);
        setBSEValue(_keyname:USERNAME, _value:model.username);
        setBSEValue(_keyname:FIRSTNAME, _value:model.first_name);
        setBSEValue(_keyname:LASTNAME, _value:model.last_name);
        setBSEValue(_keyname:COUNTRY, _value:model.country);
        setBSEValue(_keyname:STATE, _value:model.state);
        setBSEValue(_keyname:EMAIL, _value:model.email);
        setBSEValue(_keyname:FOLLOWERCOUNT, _value:String(model.follower_count));
        setBSEValue(_keyname:FOLLOWINGCOUNT, _value:String(model.following_count));
        setBSEValue(_keyname:GROUPCOUNT, _value:String(model.group_count));
        setBSEValue(_keyname:BROADCASTCOUNT, _value:String(model.broadcast_count));
        setBSEValue(_keyname:ISNOTIFY, _value:String(model.is_notify));
        setBSEValue(_keyname:ISPRIVATE, _value:String(model.is_private));
        setBSEValue(_keyname:AUTOINVITE, _value:String(model.auto_invite));
        setBSEValue(_keyname:ROLE, _value:String(model.role));
        setBSEValue(_keyname:ISBROADCASTID, _value:String(model.is_broadcast_id));
    }
    
    @objc func profileNotify(notification: Notification){
        let type : String = notification.userInfo!["type"] as! String
        if( type == "updateprofile") {
            self.model = notification.userInfo!["value"] as! BSEUserModel;
            self.avatarImage?.image = nil;
            self.userImage?.image = nil;
            SDImageCache.shared().removeImage(forKey: USERIMGURL+String(model.user_id), withCompletion: {
                self.updateProfileFields();
            })
        }
       else  if( type == "push_notification") {
            actionNotification(sender: nil)
        }
    }
    
    func uploadImage(image: UIImage) -> Void {
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        var request = URLRequest(url: URL(string: APIURL + METHODUPLOAD)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        
        request.httpMethod = "POST"
        let boundaryConstant = "----------------12345";
        let contentType = "multipart/form-data;boundary=" + boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        let filename = randomString(length: 8) + ".jpg"
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"type\";\r\n\r\n".data(using: .utf8)!)
        uploadData.append("user".data(using: .utf8)!);
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
        uploadData.append(imageData!)
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        request.httpBody = uploadData as Data
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoader();
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: error?.localizedDescription , type: .error);
            } else {
                
                let Dic = apiservice().convertToDictionary(response: data!);
                self.updateProfileImage(profile_img:Dic!["fileurl"] as! String);
            }
        })
        
        task.resume()
    }
    
    func updateProfileImage(profile_img: String) {

        let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&first_name="+getBSEValue(_keyname: FIRSTNAME)+"&&last_name="+getBSEValue(_keyname: LASTNAME)+"&&state="+getBSEValue(_keyname: STATE)+"&&country="+getBSEValue(_keyname: COUNTRY)+"&&profile_pic="+profile_img+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        print("parameter are: \(parameter)")
        
        apiservice().apiPostRequest(params:parameter, withMethod: METHODEDITPROFILE,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                self.avatarImage?.image = nil;
                self.userImage?.image = nil;
                SDImageCache.shared().removeImage(forKey: USERIMGURL+String(self.model.user_id), withCompletion: {
                    self.updateProfileFields();
                })
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Profile Updated".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);

            } else {
                
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                
            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
        }
   
    }
    
    func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            let alert  = UIAlertController(title: "Warning".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "No camera".localized(lang: getBSEValue(_keyname: CURRENTLANG)), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK".localized(lang: getBSEValue(_keyname: CURRENTLANG)), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - control function
    @IBAction func actionBack(sender: UIButton!) {
        if(isOwner) {
            NotificationCenter.default.removeObserver(self, name: Notification.Name("profileNotify"), object: nil)
        }
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionMenu(sender: UIButton!) {
        let settings = BSEMenuVC(nibName: "BSEMenuVC", bundle: nil)
        self.navigationController?.pushViewController(settings, animated: true)
    }
    @IBAction func actionPeople(sender: UIButton!) {
        let settings = BSEFeaturedVC(nibName: "BSEFeaturedVC", bundle: nil)
        self.navigationController?.pushViewController(settings, animated: true)
    }
    @IBAction func actionNotification(sender: UIButton!) {
        let settings = BSENotifyVC(nibName: "BSENotifyVC", bundle: nil)
        self.navigationController?.pushViewController(settings, animated: true)
    }
    @IBAction func actionEditProfile(sender: UIButton!) {
        if(self.isOwner) {
            let editProfile = BSEEditProfileVC(nibName: "BSEEditProfileVC", bundle: nil)
            self.navigationController?.pushViewController(editProfile, animated: true)
        } else {
            var block_status:Int!;
            if(model.is_block==0) {
                block_status = 2
               
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "User Blocked".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                model.is_block = 1;
                followActionView?.isHidden = true;
                edit_icon?.text = MaterialIcons.LockOutline
            } else {
                block_status = 4
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "User UnBlocked".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                model.is_block = 0;
                followActionView?.isHidden = false;
                edit_icon?.text = MaterialIcons.LockOpen
            }
            
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(block_status)+"&&member_id="+String(model.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                //  self.hideLoader();
            }) { (failure) -> Void in
                //  self.hideLoader();
            }
        }
    }
    
    @IBAction func actionAvatarUpload(sender: UIButton!) {
        
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        let alert = UIAlertController(title:"Choose Image".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , message: nil, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Camera".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
            self.openCamera()
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title:   "Cancel".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , style: .cancel, handler: { _ in
            //self.openGallary()
              NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"tab_icon"])
        }))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionFollowAction(sender: UIButton!) {
        if(model.user_follow_status==0 || model.user_follow_status==1) {
            var follower_status:Int!;
            follower_status = model.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(model.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(model.is_private==1) {
                model.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    
                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
                    follower_count = follower_count + 1;
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    follower_count = follower_count - 1;
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                model.user_follow_status = (follower_status==1) ? 1:0;
            }
            
            if(model.user_follow_status == 0) {
                followActionImage?.image = UIImage(named: "Follow_white.png")
            } else if (model.user_follow_status == 1) {
                followActionImage?.image = UIImage(named: "unFollow_white.png")
            } else if (model.user_follow_status == 2) {
                followActionImage?.image = UIImage(named: "request_white.png")
            }
            
            if(!isOwner) {
                let userModel:BSEUserModel = BSEUserModel();
                userModel.user_id = Int(getBSEValue(_keyname: USERID));
                userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
                userModel.username = getBSEValue(_keyname: USERNAME);
                userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
                userModel.last_name = getBSEValue(_keyname: LASTNAME);
                userModel.country = getBSEValue(_keyname: COUNTRY);
                userModel.state = getBSEValue(_keyname: STATE);
                userModel.email = getBSEValue(_keyname: EMAIL);
                userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
                userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
                userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
                userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
                userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
                userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
                userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
                userModel.role = Int(getBSEValue(_keyname: ROLE));
                NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
            }
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    @IBAction func actionToolView(sender: UIButton!) {
        if(sender.tag==0) {
            let mybroadcast = BSEMyBroadcastVC(nibName: "BSEMyBroadcastVC", bundle: nil)
            mybroadcast.model = model;
            mybroadcast.type = "my"
            self.navigationController?.pushViewController(mybroadcast, animated: true)
        } else if (sender.tag==1) {
            let follower = BSEFollowersVC(nibName: "BSEFollowersVC", bundle: nil)
            follower.model = model;
            self.navigationController?.pushViewController(follower, animated: true)
        } else if (sender.tag==2) {
            let following = BSEFollowingVC(nibName: "BSEFollowingVC", bundle: nil)
            following.model = model;
            self.navigationController?.pushViewController(following, animated: true)
        } else if (sender.tag==3) {
            let mygroup = BSEMyGroupVC(nibName: "BSEMyGroupVC", bundle: nil)
            self.navigationController?.pushViewController(mygroup, animated: true)
        }
    }
        
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}

//MARK: - UIImagePickerControllerDelegate
extension BSEProfileVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.avatarImage?.image = editedImage
            self.userImage?.image = editedImage
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: {
            self.setLoader();
            self.uploadImage(image: (self.avatarImage?.image)!);
        })
        
          NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"tab_icon"])
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: {
        });
        
          NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"tab_icon"])
    }
    
}

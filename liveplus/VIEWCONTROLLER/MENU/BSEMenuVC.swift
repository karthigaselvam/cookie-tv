//
//  BSEMenuVC.swift
//  liveplus
//
//  Created by BSEtec on 19/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var backView: UIView!
    var userData = [AnyObject]();
    var privateData = [AnyObject]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
        }
         lbl_menuTitle.text =  "USER SETTINGS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        if(getBSEValue(_keyname: ISPRIVATE) == "1") {
            
            if(getBSEValue(_keyname: LOGINFROM) == "userlogin") {
 
                userData = [ "Edit Profile".localized(lang: getBSEValue(_keyname: CURRENTLANG))as AnyObject,
                             "Update Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Follow Request".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Joined Group".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Blocked Users".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject];
            }
            else
            {
                userData = [ "Edit Profile".localized(lang: getBSEValue(_keyname: CURRENTLANG))as AnyObject,
                             "Update Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Follow Request".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Joined Group".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Blocked Users".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject];

            }
        } else {
            if(getBSEValue(_keyname: LOGINFROM) == "userlogin") {
                
                userData = [ "Edit Profile".localized(lang: getBSEValue(_keyname: CURRENTLANG))as AnyObject,
                             "Change Password".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Update Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Joined Group".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Blocked Users".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject];

            }
            else
            {
                userData = [ "Edit Profile".localized(lang: getBSEValue(_keyname: CURRENTLANG))as AnyObject,
                             "Update Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Joined Group".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,
                             "Blocked Users".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject];
              
            }
        }
  
    privateData = ["Privacy Policy".localized(lang: getBSEValue(_keyname: CURRENTLANG))     as AnyObject,"Terms".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,"Help Center".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,"Language".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject,"Logout".localized(lang: getBSEValue(_keyname: CURRENTLANG)) as AnyObject];
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.navigationController?.popViewController(animated: true);
    }
    
    
    // MARK: table Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0) {
           return userData.count;
        } else {
           return privateData.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Menu")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Menu")
        }
        if(indexPath.section==0) {
            cell!.textLabel?.text = userData[indexPath.row] as? String;
        } else {
            cell!.textLabel?.text = privateData[indexPath.row] as? String;
        }
        cell!.textLabel?.font = UIFont.init(name: "Poppins-Regular", size: 14.0)
        cell?.backgroundColor = .white;
        cell?.selectionStyle = UITableViewCellSelectionStyle.none;
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuname : String;
        if(indexPath.section==0) {
            menuname = (userData[indexPath.row] as? String)!;
        } else {
            menuname = (privateData[indexPath.row] as? String)!;
        }
 
        if(menuname ==  "Privacy Policy".localized(lang: getBSEValue(_keyname: CURRENTLANG)) || menuname == "Terms".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            UIApplication.shared.open(URL(string:PRIVACYURL)!, options: [:], completionHandler: nil)
        } else if (menuname == "Help Center".localized(lang: getBSEValue(_keyname: CURRENTLANG))){
            UIApplication.shared.open(URL(string:HELPURL)!, options: [:], completionHandler: nil)
        } else if (menuname ==  "Logout".localized(lang: getBSEValue(_keyname: CURRENTLANG))){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.logout();
        } else if (menuname ==  "Edit Profile".localized(lang: getBSEValue(_keyname: CURRENTLANG))){
            let editProfile = BSEEditProfileVC(nibName: "BSEEditProfileVC", bundle: nil)
            self.navigationController?.pushViewController(editProfile, animated: true)
        } else if (menuname ==  "Change Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            let changePassword = BSEChangePasswordVC(nibName: "BSEChangePasswordVC", bundle: nil)
            self.navigationController?.pushViewController(changePassword, animated: true)
        } else if (menuname ==  "Update Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            let settings = BSEBroadcastSettingsVC(nibName: "BSEBroadcastSettingsVC", bundle: nil)
            self.navigationController?.pushViewController(settings, animated: true)
        } else if (menuname ==  "Follow Request".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            let request = BSERequestVC(nibName: "BSERequestVC", bundle: nil)
            self.navigationController?.pushViewController(request, animated: true)
        } else if (menuname ==  "Joined Group".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            let Jgroup = BSEJoinedGroupVC(nibName: "BSEJoinedGroupVC", bundle: nil)
            self.navigationController?.pushViewController(Jgroup, animated: true)
        }
        else if (menuname == "Blocked Users".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            let blockObj = BSEBlockedVC(nibName: "BSEBlockedVC", bundle: nil)
            self.navigationController?.pushViewController(blockObj, animated: true)
        }
        else if (menuname == "Language".localized(lang: getBSEValue(_keyname: CURRENTLANG))) {
            //
            let Jgroup = LanguageVC(nibName: "LanguageVC", bundle: nil)
            self.navigationController?.pushViewController(Jgroup, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

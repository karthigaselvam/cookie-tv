//
//  BSEFollowListVC.swift
//  liveplus
//
//  Created by BseTec on 10/16/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEFollowListVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var header_title : UILabel?;
    @IBOutlet var headerView : UIView?;
    @IBOutlet var leftView : UIView?;
    @IBOutlet var rightView : UIView?;
    @IBOutlet var followers_btn : UIButton?;
    @IBOutlet var following_btn : UIButton?;
    @IBOutlet var followers_border : UIView?;
    @IBOutlet var following_border : UIView?;

    
    var tableData = [AnyObject]();
    var mapData = [BSEMapModel]();
    var isAPILoading: Bool!;
    var page_type:String!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!
    @IBOutlet var backView : UIView?;
    var model: BSEUserModel!
     var task: URLSessionDataTask!;
    
    
    @IBOutlet var featured_title : UILabel?;
    @IBOutlet var nearBy_title : UILabel?;
    @IBOutlet var new_title : UILabel?;

    
    @IBOutlet var featured_border : UILabel?;
    @IBOutlet var nearBy_border : UILabel?;
    @IBOutlet var new_border : UILabel?;
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 initFields();
        // Do any additional setup after loading the view.
    }
    
    // MARK: custom function
    func initFields() -> Void {
        
        followers_btn?.setTitle( "FOLLOWERS".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        following_btn?.setTitle( "FOLLOWING".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = 10;
            backView!.frame = framer;
            backView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
        }
        
        var framer:CGRect! = leftView?.frame;
        framer.size.width = UIScreen.main.bounds.size.width/2;
        leftView?.frame = framer;
        
        framer = rightView?.frame;
        framer.origin.x = UIScreen.main.bounds.size.width/2
        framer.size.width = UIScreen.main.bounds.size.width/2;
        rightView?.frame = framer;
        
        tblView?.isHidden=false;
        followers_border?.isHidden = true;
        // map_btn?.setTitleColor(UIColor.init(red: 177.0/255.0, green: 191.0/255.0, blue: 199.0/255.0, alpha: 1.0), for: .normal)
        following_border?.isHidden = false;
        // suggestion_btn?.setTitleColor(UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0), for: .normal)
        
        featured_border?.isHidden = true;
        nearBy_border?.isHidden = true;
        new_border?.isHidden = true;
        
        // NotificationCenter.default.addObserver(self, selector: #selector(self.FollowNotify(notification:)), name: Notification.Name("FollowList"), object: nil)
        
       
        
        if(model == nil)
        {
            let model1:BSEUserModel = BSEUserModel();
            model1.user_id = Int(getBSEValue(_keyname: USERID));
            model1.access_token = getBSEValue(_keyname: ACCESSTOKEN);
            model1.username = getBSEValue(_keyname: USERNAME);
            model1.first_name = getBSEValue(_keyname: FIRSTNAME);
            model1.last_name = getBSEValue(_keyname: LASTNAME);
            model1.country = getBSEValue(_keyname: COUNTRY);
            model1.state = getBSEValue(_keyname: STATE);
            model1.email = getBSEValue(_keyname: EMAIL);
            model1.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
            model1.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
            model1.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
            model1.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
            model1.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
            model1.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
            model1.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
            model1.role = Int(getBSEValue(_keyname: ROLE));
            model = model1;
            
            // NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"follow"])
        }
        
        
        
        
        page_type = "followers";
        CommonFunction(0)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
      
        
    }
    // MARK: - Notification function
//    @objc func FollowNotify(notification: Notification){
//        let type : String = notification.userInfo!["type"] as! String
//        if( type == "feature") {
//            CommonMainHeaderFunction(0)
//        }
//        else
//        {
//            CommonMainHeaderFunction(2)
//        }
//    }
    @objc func CommonMainHeaderFunction(_ sender: NSInteger){
        featured_border?.isHidden = true;
        nearBy_border?.isHidden = true;
        new_border?.isHidden = true;
        
        if(sender == 0 || sender == 2) {
            if(sender == 0)
            {
                featured_border?.isHidden = false;
                featured_title?.textColor = UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                nearBy_title?.textColor = UIColor.white
                new_title?.textColor = UIColor.white
                featured_border?.backgroundColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
            }
            else
            {
                new_border?.isHidden = false;
                new_title?.textColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                featured_title?.textColor = UIColor.white
                nearBy_title?.textColor = UIColor.white
                new_border?.backgroundColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                
            }
        }
        
        
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    //MARK: control function
    @IBAction func actionListSelect(sender: UIButton!) {
        CommonFunction(sender.tag)
    }
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }
    @IBAction func actionMainMenu(sender: UIButton!) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(sender.tag==0 )
        {
            NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"feature"])
            appDelegate.tabcontroller?.selectedIndex = 0
        }
        else if(sender.tag==2)
        {
            NotificationCenter.default.post(name: Notification.Name("HomeMain"), object: nil, userInfo: ["type":"new"])
            appDelegate.tabcontroller?.selectedIndex = 0
        }
        else
        {
            appDelegate.tabcontroller?.selectedIndex = 1
        }
    }
    @objc func CommonFunction(_ sender: NSInteger){
        followers_border?.isHidden = true;
        following_border?.isHidden = true;
        followers_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        following_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        if(sender == 0) {
            tblView?.isHidden=false;
            followers_border?.isHidden = false;
            followers_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "followers";
            isPullToRefresh = false;
            isInfiniteScrolling = false;
            self.isFirstTime = true;
            self.currentpage = 1;
            getMyFollowers()
            tblView?.tableFooterView = UIView (frame: CGRect.zero)
        } else {
            
            tblView?.isHidden=false;
            following_border?.isHidden = false;
            following_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "following";
            isPullToRefresh = false;
            isInfiniteScrolling = false;
            self.isFirstTime = true;
            self.currentpage = 1;
            getMyFollowing();
            tblView?.tableFooterView = UIView (frame: CGRect.zero)
       
        }
    }
    
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            if(self.page_type == "followers")
            {
                 self.getMyFollowers();
            }
           else
            {
                 self.getMyFollowing();
            }
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    if(self.page_type == "followers")
                    {
                    self.getMyFollowers();
                    }
                    else
                    {
                         self.getMyFollowing();
                    }
                    
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = (self.page_type == "followers") ? "No Followers List Available":  "No Following List Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    
    
    
    func getMyFollowers() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+String(model.user_id)+"&&page_no="+String(currentpage)+"&&username=&&record="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODFOLLOWERLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODFOLLOWERLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
                            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                    if(self.tableData.count==0) {
                        self.tblView?.tableFooterView = self.noDataView();
                    } else {
                        self.tblView?.tableFooterView = UIView.init();
                    }
                }
                
            }
        })
        task.resume();
    }
    
    func getMyFollowing() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+String(model.user_id)+"&&page_no="+String(currentpage)+"&&username=&&record="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODFOLLOWINGLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODFOLLOWINGLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
                            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                        
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                    if(self.tableData.count==0) {
                        self.tblView?.tableFooterView = self.noDataView();
                    } else {
                        self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
                    }
                }
                
            }
        })
        task.resume();
    }
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let uidentifier = "UserCell"
        
        var cell: UserCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell
        if cell == nil {
            tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "UserCellRTL" : "UserCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell;
        }
        cell.setFollowerValue(model: tableData[indexPath.row] as! BSEUserModel)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.follow_action?.tag = indexPath.row;
        cell.follow_action?.addTarget(self, action: #selector(self.userAction(_:)), for: .touchUpInside)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model:BSEUserModel = tableData[indexPath.row] as! BSEUserModel
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = model
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    @objc func userAction(_ sender: UIButton){ //<- needs `@objc`
        let modeler:BSEUserModel = tableData[sender.tag] as! BSEUserModel
        if(modeler.user_follow_status==0 || modeler.user_follow_status==1) {
            var follower_status:Int!;
            follower_status = modeler.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(modeler.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(modeler.is_private==1) {
                modeler.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    follower_count = follower_count + 1;
                } else {
                    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    follower_count = follower_count - 1;
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                modeler.user_follow_status = (follower_status==1) ? 1:0;
            }
            self.tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            
            self.updateCount();
            
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
                    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    
    func updateCount() -> Void {
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  BSEChangePasswordVC.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage
import MaterialIconsSwift

class BSEChangePasswordVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var mainView: UIView!;
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var oldpassField: UITextField!;
    @IBOutlet weak var newpassField: UITextField!;
    @IBOutlet weak var confirmpassField: UITextField!;
    @IBOutlet weak var page_scroll: UIScrollView!;
    @IBOutlet var submit_btn: UIButton!;
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var backView : UIView?;
    var progressBar:AMProgressBar!;

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }
    // MARK: - Custom Function
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            oldpassField.textAlignment = .right
            newpassField.textAlignment = .right
            confirmpassField.textAlignment = .right
        }
        else
        {
            oldpassField.textAlignment = .left
            newpassField.textAlignment = .left
            confirmpassField.textAlignment = .left
            
        }
        
        lbl_menuTitle.text =  "CHANGE PASSWORD".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        submit_btn?.setTitle( "UPDATE".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        oldpassField.placeholder =   "Old Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        newpassField.placeholder =   "New Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        confirmpassField.placeholder = "Confirm Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
    }
    
    func setLoader() -> Void {
        headerView.isUserInteractionEnabled = false;
        submit_btn.isUserInteractionEnabled = false;
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        

    }
    
    
    func hideLoader() -> Void {
        headerView.isUserInteractionEnabled = true;
        submit_btn.isUserInteractionEnabled = true;
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    

    func validateFields() -> Bool {
        if  FormValidation().notEmpty(msg: oldpassField.text!) && FormValidation().notEmpty(msg: newpassField.text!) && FormValidation().notEmpty(msg: confirmpassField.text!) {
            
            if !FormValidation().passwordLength(msg: newpassField.text!,totalCount:6){

SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "InValid Password".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                return false;
            }
            
            if(!FormValidation().ValidateEqualString(str1: newpassField.text!, str2: confirmpassField.text!)) {
                
   SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Password MisMatch".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                return false;
            }
            return true;
        }
        else {
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    
    func saveUser(access_token:String) -> Void {
        setBSEValue(_keyname:ACCESSTOKEN, _value:access_token);

    }
    

    
    
    // MARK: - Control function
    @IBAction func actionSubmit(sender: UIButton!) {
        self.view .endEditing(true)
        if self.validateFields(){
            setLoader();
            let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&old_password="+(oldpassField.text)!+"&&new_password="+(confirmpassField.text)!+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            apiservice().apiPostRequest(params:parameter, withMethod: METHODCHANGEPASSWORD,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    self.saveUser(access_token: commonModel.return_id);
                    
 SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Password Updated".localized(lang: getBSEValue(_keyname: CURRENTLANG)), type: .success);
                    
                } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    


    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==oldpassField{
            newpassField.becomeFirstResponder()
        } else if textField==newpassField{
            confirmpassField.becomeFirstResponder()
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    
    // MARK: - Keyboard Function
    override func viewDidAppear(_ animated: Bool)
    {
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    
    // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}



//
//  BSEMyGroupVC.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEMyGroupVC: UIViewController, UITableViewDataSource, UITableViewDelegate, BSECreateGroupVCDelegate, BSGroupDetailVCDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
     var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!;
    var currentIndex: Int!
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var lbl_noData: UILabel!
    @IBOutlet var backView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(task==nil) {
        } else {
            if(task.state == .running) {
                task.cancel();
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - custom function
    
    func initFields() -> Void {
       
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
        }
        
        
        lbl_menuTitle.text = "My Groups".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        lbl_noData.text = "No Data".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
 
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        self.isFirstTime = true;
        self.currentpage = 1;
        self.getMyGroup();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)

    }
    
    
   /** This is the function that help refresh UITableView through pull to refresh action
    */
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getMyGroup();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getMyGroup();
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Groups Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    func getMyGroup() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+getBSEValue(_keyname: USERID)+"&&username=&&type=&&page_no="+String(currentpage)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
           SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODMYGROUP + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                       
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODMYGROUP, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
    SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)),  message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                       if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.hideLoader();
                    } else {
                      if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                    if(self.tableData.count==0) {
                        self.tblView?.tableFooterView = self.noDataView();
                    } else {
                        self.tblView?.tableFooterView = UIView.init();
                    }
                }
                
            }
        })
        task.resume();
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func newGroup(model: BSEGroupModel) {
        tableData.append(model);
        tblView?.reloadData();
        if(self.tableData.count==0) {
            self.tblView?.tableFooterView = self.noHeaderView();
        } else {
            self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
        }
    }
    
    func updateItem(model: BSEGroupModel) {
        let groupModel:BSEGroupModel = tableData[currentIndex] as! BSEGroupModel
        groupModel.group_name = model.group_name;
        tblView?.reloadData()
    }
    
    func removeItem(model: BSEGroupModel) {
        tableData.remove(at: currentIndex)
        tblView?.reloadData()
        if(self.tableData.count==0) {
            self.tblView?.tableFooterView = self.noHeaderView();
        } else {
            self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
        }
    }
    
    func noHeaderView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Groups Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }

    
    // MARK: table Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count+1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row==0) {
            let aidentifier = "AddCell"
            var cell: AddCell! = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell
            if cell == nil {
                tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "AddCellRTL" : "AddCell", bundle: nil), forCellReuseIdentifier: aidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell;
            }
            cell.setDefaultValue(type:"add group")
            cell.backgroundColor = .white
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else {
            let uidentifier = "GroupCell"
            var cell: GroupCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell
            if cell == nil {
                tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "GroupCellRTL":  "GroupCell", bundle: nil), forCellReuseIdentifier: uidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell;
            }
            cell.setDefaultValue(model: tableData[indexPath.row-1] as! BSEGroupModel)
            cell.backgroundColor = .white
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row==0) {
            let createGroup = BSECreateGroupVC(nibName: "BSECreateGroupVC", bundle: nil)
            createGroup.delegate = self;
            self.navigationController?.pushViewController(createGroup, animated: true)
        } else {
            currentIndex = indexPath.row - 1
            let groupModel : BSEGroupModel = tableData[indexPath.row-1] as! BSEGroupModel
            let groupView = BSGroupDetailVC(nibName: "BSGroupDetailVC", bundle: nil)
            groupView.groupModel = groupModel;
            groupView.isowner = true;
            groupView.delegate = self;
            self.navigationController?.pushViewController(groupView, animated: true)
        }
        
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
   

}

//
//  AddCell.swift
//  liveplus
//
//  Created by BSEtec on 22/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift

class AddCell: UITableViewCell {
    
    @IBOutlet var add_title:UILabel!;
    @IBOutlet var add_icon:UILabel!;

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultValue(type:String) -> Void {
        
        if(type == "add group") {
            add_title.text = "Add New Group".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else if(type == "add member") {
            add_title.text = "Add Member".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
        
        add_icon.font = MaterialIcons.fontOfSize(size:21);
        add_icon.text = MaterialIcons.Add
    }
    
}

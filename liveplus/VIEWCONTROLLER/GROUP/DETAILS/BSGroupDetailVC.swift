//
//  BSGroupDetailVC.swift
//  liveplus
//
//  Created by BSEtec on 22/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

protocol BSGroupDetailVCDelegate:class {
    func removeItem(model:BSEGroupModel);
    func updateItem(model:BSEGroupModel);
}
class BSGroupDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, BSEFollowSearchVCDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var header_title: UITextField!;
    @IBOutlet weak var header_btn: UIButton!;
    @IBOutlet weak var backView: UIView!;
    @IBOutlet weak var cancelView: UIView!;
    @IBOutlet weak var moreView: UIView!;
    @IBOutlet weak var updateView: UIView!;
    @IBOutlet var lbl_cancel: UILabel!
    @IBOutlet var lbl_update: UILabel!
    @IBOutlet var lbl_desc: UILabel!
    @IBOutlet var live_btn: UIButton!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var task: URLSessionDataTask!;
    var progressBar:AMProgressBar!;
    var isowner:Bool!;
    weak var delegate: BSGroupDetailVCDelegate?
    var groupModel:BSEGroupModel!;
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initFields();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: custom function
    func initFields() -> Void {
        
       
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            
            
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            framer = moreView!.frame;
            framer.origin.x = 0;
            moreView!.frame = framer;
            
            framer = updateView!.frame;
            framer.origin.x = 0;
            updateView!.frame = framer;
            
            moreView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            updateView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            framer = cancelView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            cancelView!.frame = framer;
        }
       

       

       

        
        lbl_cancel.text = "CANCEL".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_update.text = "UPDATE".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_desc.text = "Create group desc".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        live_btn?.setTitle("Go Live".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        
        
        backView.isHidden = false;
        moreView.isHidden = false;
        cancelView.isHidden = true;
        updateView.isHidden = true;
        
        header_title.text = self.groupModel.group_name
        initGroupView();

        if(isowner) {
            let firstModel:BSEUserModel = BSEUserModel();
            firstModel.user_id = Int(getBSEValue(_keyname: USERID))
            firstModel.username = getBSEValue(_keyname: USERNAME)
            tableData.append(firstModel);
            self.tblView?.tableHeaderView = self.tblHeaderView;
            header_btn.isHidden = false;
        } else {
            self.tblView?.tableHeaderView = UIView.init(frame: CGRect.zero);
            header_btn.isHidden = true;
        }
        self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);

    }
    
    func initGroupView() -> Void {
        setLoader();
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&id="+String(groupModel.group_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG)
        print("parameter are: \(parameter)")
        
        apiservice().apiGetRequest(params:parameter, withMethod: METHODGROUPVIEW,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                self.groupModel = commonModel.temparray[0] as! BSEGroupModel;
                self.tableData.append(contentsOf:self.groupModel.member_details);
                
                self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
                self.tblView?.reloadData();
            } else {
               
        SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
        }
    }
    
    func validateFields() -> Bool {
        if  FormValidation().notEmpty(msg: header_title.text!)  {
            return true;
        }
        else {
           
SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    
    func setLoader() -> Void {
        headerView.isUserInteractionEnabled = false;
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    
    func hideLoader() -> Void {
        headerView.isUserInteractionEnabled = true;
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func updateUserCount() -> Void {
        var group_count:Int = Int(getBSEValue(_keyname: GROUPCOUNT))!
        group_count = group_count - 1;
        setBSEValue(_keyname: GROUPCOUNT, _value: String(group_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    
    func deleteGroup() -> Void {
        // setLoader();
        self.delegate?.removeItem(model: self.groupModel);
        self.updateUserCount();
        SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"Group Updated".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
        
        self.navigationController?.popViewController(animated: true);
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&group_id="+String(groupModel.group_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        print("parameter are: \(parameter)")
        
        apiservice().apiPostRequest(params:parameter, withMethod: METHODGROUPDELETE,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
            } else {
            }
            // self.hideLoader();
        }) { (failure) -> Void in
            // self.hideLoader();
        }
    }
    
    func leaveGroup() -> Void {
        // setLoader();
        self.delegate?.removeItem(model: self.groupModel);
        
         SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"You Left".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
        self.navigationController?.popViewController(animated: true);
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&group_id="+String(groupModel.group_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        print("parameter are: \(parameter)")
        
        apiservice().apiPostRequest(params:parameter, withMethod: METHODLEFTGROUP,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
            } else {
            }
            // self.hideLoader();
        }) { (failure) -> Void in
            // self.hideLoader();
        }
    }

    
    func viewHistory() -> Void {
        let mybroadcast = BSEMyBroadcastVC(nibName: "BSEMyBroadcastVC", bundle: nil)
        mybroadcast.groupModel = groupModel;
        mybroadcast.type = "history"
        self.navigationController?.pushViewController(mybroadcast, animated: true)
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionUpdateGroup(sender: UIButton!) {
        self.view .endEditing(true)
        self.actionCancelUpdateTitle(sender: nil);
        if self.validateFields(){
            var user_ids : String="";
            var incr: Int = 0;
            var selectedData = [AnyObject]();
            selectedData.append(contentsOf: tableData);
            selectedData.removeFirst()
            for Dic in selectedData {
                let userModel:BSEUserModel = Dic as! BSEUserModel
                if(incr == 0) {
                    user_ids = String(userModel.user_id)
                } else {
                    user_ids = user_ids+","+String(userModel.user_id)
                }
                incr = incr + 1;
            }
            self.groupModel.group_name = self.header_title.text;
            self.groupModel.member_details.append(contentsOf: self.tableData);
            self.delegate?.updateItem(model: self.groupModel);

           // setLoader();
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&group_name="+(header_title.text)!+"&&type=users&&user_ids="+(user_ids)+"&&group_id="+String(groupModel.group_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODADDGROUP,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                } else {
            SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                }
               // self.hideLoader();
            }) { (failure) -> Void in
               // self.hideLoader();
            }
        }
    }
    
    @IBAction func actionUpdateTitle(sender: UIButton!) {
        header_title.becomeFirstResponder()
        backView.isHidden = true;
        moreView.isHidden = true;
        cancelView.isHidden = false;
        updateView.isHidden = false;
        header_btn.isHidden = true;
    }
    
    @IBAction func actionCancelUpdateTitle(sender: UIButton!) {
        backView.isHidden = false;
        moreView.isHidden = false;
        cancelView.isHidden = true;
        updateView.isHidden = true;
        header_btn.isHidden = false;
    }
    
    @IBAction func actionMoreMenu(sender: UIButton!) {
        
        
        
        let alert = UIAlertController(title:  "Group Menu".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , message: nil, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "View Group History".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
            self.viewHistory();
        }))
        if(isowner) {
            
            alert.addAction(UIAlertAction(title: "Close Group".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
                self.deleteGroup();
            }))
        } else {
            
            alert.addAction(UIAlertAction(title: "Leave Group".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , style: .default, handler: { _ in
                self.leaveGroup();
            }))
        }
        
 alert.addAction(UIAlertAction.init(title: "Cancel".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionGoLive(sender: UIButton!) {
        self.navigationController?.popToRootViewController(animated: false)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAddCamera()
     //  showAddCamera()
    }
    func showAddCamera() -> Void {
        
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1")
        {
            let discover = BSELiveVC(nibName: "BSELiveVC", bundle: nil)
            self.navigationController?.pushViewController(discover, animated: true)
        }
        else
        {
            let permission = BSEPermissionVC(nibName:"BSEPermissionVC",bundle:nil);
            permission.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(permission, animated: false, completion: nil)
        }
    }
    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    // MARK: table Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isowner) {
            return tableData.count+1;
        } else {
            return tableData.count;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(isowner) {
            if(indexPath.row==0) {
                let aidentifier = "AddCell"
                var cell: AddCell! = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell
                if cell == nil {
                  tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "AddCellRTL" : "AddCell", bundle: nil), forCellReuseIdentifier: aidentifier);
                    cell = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell;
                }
                cell.setDefaultValue(type:"add member")
                cell.backgroundColor = .white
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else {
                let uidentifier = "GroupCell"
                var cell: GroupCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell
                if cell == nil {
                   tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "GroupCellRTL":  "GroupCell", bundle: nil), forCellReuseIdentifier: uidentifier);
                    cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell;
                }
                cell.setUserValue(model: tableData[indexPath.row-1] as! BSEUserModel)
                cell.backgroundColor = .white
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        } else {
            let uidentifier = "GroupCell"
            var cell: GroupCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell
            if cell == nil {
                  tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "GroupCellRTL":  "GroupCell", bundle: nil), forCellReuseIdentifier: uidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell;
            }
            cell.setUserValue(model: tableData[indexPath.row] as! BSEUserModel)
            cell.backgroundColor = .white
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isowner) {
            if(indexPath.row==0) {
                let searchUser = BSEFollowSearchVC(nibName: "BSEFollowSearchVC", bundle: nil)
                var selectedData = [AnyObject]();
                selectedData.append(contentsOf: tableData);
                selectedData.removeFirst()
                searchUser.delegate = self;
                
                searchUser.selectedData = selectedData;
                self.navigationController?.pushViewController(searchUser, animated: true)
            } else {
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        var userModel : BSEUserModel = BSEUserModel();
        if(isowner) {
            if(indexPath.row != 0) {
                userModel = tableData[indexPath.row-1] as! BSEUserModel;
                if(getBSEValue(_keyname: USERID) == String(userModel.user_id)) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
        
        
        return false;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if(isowner) {
                self.tableData.remove(at: indexPath.row-1);
                self.tblView?.reloadData()
                self.actionUpdateGroup(sender: nil);
            }
        }
    }
    
    func selectedUsers(data:[AnyObject]) -> Void {
        tableData.removeAll()
        let firstModel:BSEUserModel = BSEUserModel();
        firstModel.user_id = Int(getBSEValue(_keyname: USERID))
        firstModel.username = getBSEValue(_keyname: USERNAME)
        tableData.append(firstModel);
        
        tableData.append(contentsOf: data);
        tblView?.reloadData();
        self.navigationController?.popViewController(animated: true);
        self.actionUpdateGroup(sender: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

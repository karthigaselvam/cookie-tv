//
//  BSECreateGroupVC.swift
//  liveplus
//
//  Created by BSEtec on 22/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

protocol BSECreateGroupVCDelegate:class {
    func newGroup(model:BSEGroupModel);
}
class BSECreateGroupVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, BSEFollowSearchVCDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet weak var groupnameField: UITextField!;
    @IBOutlet weak var create_btn: UIButton!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var task: URLSessionDataTask!;
    var progressBar:AMProgressBar!;
    weak var delegate: BSECreateGroupVCDelegate?
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var lbl_create: UILabel!
    @IBOutlet var lbl_desc: UILabel!
    @IBOutlet var backView : UIView?;
    @IBOutlet var createView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initFields();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: custom function
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            framer = createView!.frame;
            framer.origin.x = 10;
            createView!.frame = framer;
            
            createView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            lbl_create.textAlignment = .left
            groupnameField.textAlignment = .right
        }
        
        
        
        lbl_menuTitle.text = "CREATE GROUP".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_create.text = "CREATE".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_desc.text = "Create group desc".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        groupnameField.placeholder = "Group Name".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        let firstModel:BSEUserModel = BSEUserModel();
        firstModel.user_id = Int(getBSEValue(_keyname: USERID))
        firstModel.username = getBSEValue(_keyname: USERNAME)
        tableData.append(firstModel);
        
        tblView?.tableHeaderView = tblHeaderView;
        tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
        tblView?.reloadData();
    }
    
    func validateFields() -> Bool {
        if  FormValidation().notEmpty(msg: groupnameField.text!)  {
            return true;
        }
        else {
        
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    
    func setLoader() -> Void {
        headerView.isUserInteractionEnabled = false;
        create_btn.isUserInteractionEnabled = false;
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
        
    }
    
    
    func hideLoader() -> Void {
        headerView.isUserInteractionEnabled = true;
        create_btn.isUserInteractionEnabled = true;
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func updateUserCount() -> Void {
        var group_count:Int = Int(getBSEValue(_keyname: GROUPCOUNT))!
        group_count = group_count + 1;
        setBSEValue(_keyname: GROUPCOUNT, _value: String(group_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionCreateGroup(sender: UIButton!) {
        self.view .endEditing(true)
       
        if self.validateFields(){
            var user_ids : String="";
            var incr: Int = 0;
            var selectedData = [AnyObject]();
            selectedData.append(contentsOf: tableData);
            selectedData.removeFirst()
            for Dic in selectedData {
                let userModel:BSEUserModel = Dic as! BSEUserModel
                if(incr == 0) {
                    user_ids = String(userModel.user_id)
                } else {
                    user_ids = user_ids+","+String(userModel.user_id)
                }
                incr = incr + 1;
            }
            setLoader();
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&group_name="+(groupnameField.text)!+"&&type=users&&user_ids="+(user_ids)+"&&group_id="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODADDGROUP,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    self.updateUserCount();
                    let group:BSEGroupModel = BSEGroupModel();
                    group.group_id = Int(commonModel.return_id);
                    group.group_name = self.groupnameField.text;
                    group.broadcast_count = 0;
                    group.owner_id = Int(getBSEValue(_keyname: USERID))
                    group.owner_username = getBSEValue(_keyname: USERNAME)
                    group.member_details.append(contentsOf: self.tableData);
                    self.delegate?.newGroup(model: group);
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Group Created".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    
                    self.navigationController?.popViewController(animated: true);
                } else {
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }

    
    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    // MARK: table Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count+1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row==0) {
            let aidentifier = "AddCell"
            var cell: AddCell! = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell
            if cell == nil {
               tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "AddCellRTL" : "AddCell", bundle: nil), forCellReuseIdentifier: aidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: aidentifier) as? AddCell;
            }
            cell.setDefaultValue(type:"add member")
            cell.backgroundColor = .white
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else {
            let uidentifier = "GroupCell"
            var cell: GroupCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell
            if cell == nil {
                tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "GroupCellRTL":  "GroupCell", bundle: nil), forCellReuseIdentifier: uidentifier);
                cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell;
            }
            cell.setUserValue(model: tableData[indexPath.row-1] as! BSEUserModel)
            cell.backgroundColor = .white
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row==0) {
            let searchUser = BSEFollowSearchVC(nibName: "BSEFollowSearchVC", bundle: nil)
            var selectedData = [AnyObject]();
            selectedData.append(contentsOf: tableData);
            selectedData.removeFirst()
            searchUser.delegate = self;
            
            searchUser.selectedData = selectedData;
            self.navigationController?.pushViewController(searchUser, animated: true)
        } else {
            
        }
        
    }
    
    func selectedUsers(data:[AnyObject]) -> Void {
        tableData.removeAll()
        let firstModel:BSEUserModel = BSEUserModel();
        firstModel.user_id = Int(getBSEValue(_keyname: USERID))
        firstModel.username = getBSEValue(_keyname: USERNAME)
        tableData.append(firstModel);
        
        tableData.append(contentsOf: data);
        tblView?.reloadData();
        self.navigationController?.popViewController(animated: true);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

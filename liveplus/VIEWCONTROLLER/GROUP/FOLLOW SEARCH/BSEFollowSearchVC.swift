//
//  BSEFollowSearchVC.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift

protocol BSEFollowSearchVCDelegate:class {
    func selectedUsers(data:[AnyObject]);
}

class BSEFollowSearchVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var header_label: UILabel!;
    @IBOutlet weak var done_icon: UILabel!;
    @IBOutlet weak var select_icon: UILabel!;
    @IBOutlet weak var search_icon: UILabel!;
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet weak var searchField: UITextField!;
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var backView : UIView?;
    @IBOutlet var checkBoxView : UIView?;
    @IBOutlet var selecctView : UIView?;
    var mainData = [AnyObject]();
    var tableData = [AnyObject]();
    var task: URLSessionDataTask!;
    var isSelectall: Bool!
    var progressBar:AMProgressBar!;
    var selectedData = [AnyObject]();
     @IBOutlet var lbl_noData: UILabel!
    weak var delegate: BSEFollowSearchVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(task==nil) {
        } else {
            if(task.state == .running) {
                task.cancel();
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - custom function
    
    func initFields() -> Void {
        
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            searchField.textAlignment = .right
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            framer = selecctView!.frame;
            framer.origin.x = 10;
            selecctView!.frame = framer;
            selecctView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            framer = checkBoxView!.frame;
            framer.origin.x = (selecctView!.frame.size.width+selecctView!.frame.origin.x);
            checkBoxView!.frame = framer;
            checkBoxView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            framer = search_icon!.frame;
            framer.origin.x = 0;
            search_icon!.frame = framer;
            
            search_icon?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            
            framer = searchField!.frame;
            framer.origin.x = (search_icon!.frame.size.width+search_icon!.frame.origin.x+10);
            searchField!.frame = framer;
        }
        else
        {
            searchField.textAlignment = .left
        }
        
        
        
        isSelectall = false;
        done_icon.font = MaterialIcons.fontOfSize(size:24);
        done_icon.text = MaterialIcons.Done
        
        select_icon.font = MaterialIcons.fontOfSize(size:24);
        select_icon.text = MaterialIcons.CheckBoxOutlineBlank
        
        search_icon.font = MaterialIcons.fontOfSize(size:24);
        search_icon.text = MaterialIcons.Search
        
        header_label.text = "My Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        lbl_noData.text = "No Data".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        searchField.placeholder =  "Search Followers".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        self.getMyFollowers();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        
    }
    
    func getMyFollowers() -> Void {
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+getBSEValue(_keyname: USERID)+"&&page_no=&&username=&&record=all"+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
           SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODFOLLOWERLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODFOLLOWERLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(commonModel.temparray.count>0) {
                                self.tableData.append(contentsOf:commonModel.temparray);
                                self.mainData.append(contentsOf:commonModel.temparray);
                                if(self.tableData.count>0 && self.selectedData.count>0) {
                                    for Dic in self.tableData {
                                        let userModel : BSEUserModel = Dic as! BSEUserModel
                                        for selectDic in self.selectedData {
                                            let sModel : BSEUserModel = selectDic as! BSEUserModel
                                            if(sModel.user_id == userModel.user_id) {
                                                userModel.status = 1;
                                            }
                                        }
                                    }
                                }
                                
                            }
                        } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noHeaderView(txt:"No Followers Available");
                        } else {
                            self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                    } else {
                        self.hideLoader();
                    }
                } else {
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func noHeaderView(txt:String) -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = txt;
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    

    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let uidentifier = "GroupCell"
         
        var cell: GroupCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell
        if cell == nil {
             tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "GroupCellRTL":  "GroupCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? GroupCell;
        }
        cell.setFollowSearchValue(model: tableData[indexPath.row] as! BSEUserModel)
        cell.backgroundColor = .white
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model:BSEUserModel = tableData[indexPath.row] as! BSEUserModel
        model.status = model.status==0 ? 1 : 0;
        tblView?.reloadRows(at:[indexPath], with: .none)
    }
    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionPerformSearch(sender: UITextField!) {
        let search:String = (searchField.text?.lowercased())!;
        if(search.count==0) {
            self.tableData .removeAll();
            self.tableData.append(contentsOf: mainData)
        } else {
            self.tableData = mainData.filter { item in
                let userModel: BSEUserModel = item as! BSEUserModel;
                return userModel.username.lowercased().contains(search)
            }
        }
        if(self.tableData.count==0) {
            self.tblView?.tableFooterView = self.noHeaderView(txt:"No Data Available");
        } else {
            self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
        }
        tblView?.reloadData();
    }
    
    @IBAction func actionSearchButton(sender: UIButton!) {
        searchField.resignFirstResponder();
        self.actionPerformSearch(sender: nil);
    }
    
    @IBAction func actionSelectAll(sender: UIButton!) {
        for Dic in mainData {
            let userModel:BSEUserModel = Dic as! BSEUserModel
            userModel.status = (isSelectall) ? 0 : 1;
        }
        tableData.removeAll()
        tableData.append(contentsOf: mainData)
        tblView?.reloadData();
        if(isSelectall) {
            isSelectall = false;
            select_icon.text = MaterialIcons.CheckBoxOutlineBlank;
        } else {
            isSelectall = true;
            select_icon.text = MaterialIcons.CheckBox;
        }
    }
    
    @IBAction func actionChoose(sender: UIButton!) {
        selectedData.removeAll()
        for Dic in mainData {
            let userModel:BSEUserModel = Dic as! BSEUserModel
            if(userModel.status == 1) {
                selectedData.append(userModel);
            }
        }
        self.delegate?.selectedUsers(data: selectedData);
    }


}

//
//  GroupCell.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
import SDWebImage

class GroupCell: UITableViewCell {
    @IBOutlet var groupimage : UIImageView?;
    @IBOutlet var groupname : UILabel?;
    @IBOutlet var group_right_icon : UILabel?;
    override func awakeFromNib() {
        super.awakeFromNib()
        groupimage?.layer.cornerRadius = 20.0;
        groupimage?.layer.masksToBounds = true;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEGroupModel) -> Void {
        groupimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.owner_id)), placeholderImage: UIImage(named: "placeholder.png"))
        groupname?.text = model.group_name.capitalized;
        group_right_icon?.font = MaterialIcons.fontOfSize(size:20);
        group_right_icon?.text = MaterialIcons.KeyboardArrowRight
    }
    
    func setUserValue(model:BSEUserModel) -> Void {
        groupimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        groupname?.text = model.username.capitalized;
        group_right_icon?.isHidden = true;
    }
    
    func setFollowSearchValue(model:BSEUserModel) -> Void {
        groupimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        groupname?.text = model.username.capitalized;
        group_right_icon?.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        group_right_icon?.font = MaterialIcons.fontOfSize(size:24);
        group_right_icon?.text = model.status==0 ?  MaterialIcons.CheckBoxOutlineBlank : MaterialIcons.CheckBox
    }
    
}

//
//  BSERegisterVC.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import CountryList

class BSERegisterVC: UIViewController, UITextFieldDelegate, CountryListDelegate, UITextViewDelegate {
    @IBOutlet weak var mainView: UIView!;
    @IBOutlet weak var passField: UITextField!;
    @IBOutlet weak var confirmpassField: UITextField!;
    @IBOutlet weak var emailField: UITextField!;
    @IBOutlet weak var firstnameField: UITextField!;
    @IBOutlet weak var lastnameField: UITextField!;
    @IBOutlet weak var usernameField: UITextField!;
    @IBOutlet weak var stateField: UITextField!;
    @IBOutlet weak var countryField: UITextField!;
    @IBOutlet weak var page_scroll: UIScrollView!;
    @IBOutlet weak var header_img: UIImageView!;
    @IBOutlet var submit_btn: UIButton!;
     @IBOutlet var back_btn: UIButton!;
    var progressBar:AMProgressBar!;
    var countryCode:String!;
    var countryList = CountryList();
    @IBOutlet var termsView:UITextView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
    // MARK: - Custom Function
    func initFields() -> Void {
       
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            emailField.textAlignment = .right
            usernameField.textAlignment = .right
            firstnameField.textAlignment = .right
            lastnameField.textAlignment = .right
            stateField.textAlignment = .right
            countryField.textAlignment = .right
            passField.textAlignment = .right
             confirmpassField.textAlignment = .right
        }
        else
        {
            emailField.textAlignment = .left
            usernameField.textAlignment = .left
            firstnameField.textAlignment = .left
            lastnameField.textAlignment = .left
            stateField.textAlignment = .left
            countryField.textAlignment = .left
            passField.textAlignment = .left
            passField.textAlignment = .left
        }
        
        emailField.placeholder =  "Email Address".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        passField.placeholder =  "Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        confirmpassField.placeholder =  "Confirm Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        usernameField.placeholder =  "UserName".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        firstnameField.placeholder =  "FirstName".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lastnameField.placeholder = "LastName".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        stateField.placeholder =  "State".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        countryField.placeholder = "Country".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        submit_btn?.setTitle("CREATE ACCOUNT".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        back_btn?.setTitle( "Back".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        let termString : String =  "Regitser Terms and Privacy Policy".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        let myAttribute = [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 13.0)!];
        let myString = NSMutableAttributedString(string: termString, attributes: myAttribute)
        
myString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: termString.count))
myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 50, length: 5))
myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 59, length: 14))
        termsView.attributedText = myString;
        
        header_img.layer.masksToBounds=false
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        countryField.inputView = nil;
        countryList.delegate = self;
    }
    
    func setLoader() -> Void {
        
        progressBar = AMProgressBar(frame: submit_btn.bounds)
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 10
        progressBar.barColor = .white
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 0.5)
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = "CREATING ACCOUNT".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        submit_btn.addSubview(progressBar)
    
    }
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if submit_btn.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    

    func validateFields() -> Bool {
        if FormValidation().notEmpty(msg: emailField.text!) && FormValidation().notEmpty(msg: passField.text!) && FormValidation().notEmpty(msg: confirmpassField.text!) && FormValidation().notEmpty(msg: usernameField.text!) && FormValidation().notEmpty(msg: firstnameField.text!) && FormValidation().notEmpty(msg: lastnameField.text!)  && FormValidation().notEmpty(msg: stateField.text!) && FormValidation().notEmpty(msg: countryField.text!)
        {
            if !FormValidation().isValidEmail(testStr: emailField.text!){

SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"InValid Email".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , type: .error);
                return false;
            }
            if !FormValidation().passwordLength(msg: passField.text!,totalCount:6){
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "InValid Password".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                return false;
            }
            
            if(!FormValidation().ValidateEqualString(str1: passField.text!, str2: confirmpassField.text!)) {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Password MisMatch".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                return false;
            }
            return true;
        }
        else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    
    func saveUser(model:BSEUserModel) -> Void {
        setBSEValue(_keyname:USERID, _value:String(model.user_id))
        setBSEValue(_keyname:ACCESSTOKEN, _value:model.access_token);
        setBSEValue(_keyname:USERNAME, _value:model.username);
        setBSEValue(_keyname:FIRSTNAME, _value:model.first_name);
        setBSEValue(_keyname:LASTNAME, _value:model.last_name);
        setBSEValue(_keyname:COUNTRY, _value:model.country);
        setBSEValue(_keyname:STATE, _value:model.state);
        setBSEValue(_keyname:EMAIL, _value:model.email);
        setBSEValue(_keyname:FOLLOWERCOUNT, _value:String(model.follower_count));
        setBSEValue(_keyname:FOLLOWINGCOUNT, _value:String(model.following_count));
        setBSEValue(_keyname:GROUPCOUNT, _value:String(model.group_count));
        setBSEValue(_keyname:BROADCASTCOUNT, _value:String(model.broadcast_count));
        setBSEValue(_keyname:ISNOTIFY, _value:String(model.is_notify));
        setBSEValue(_keyname:ISPRIVATE, _value:String(model.is_private));
        setBSEValue(_keyname:AUTOINVITE, _value:String(model.auto_invite));
        setBSEValue(_keyname:ROLE, _value:String(model.role));
        setBSEValue(_keyname:ISBROADCASTID, _value:String(model.is_broadcast_id));
    }
    
    func selectedCountry(country: Country) {
        countryCode = country.countryCode;
        countryField.text = country.name;
    }
    
    
    // MARK: - Control function

    @IBAction func actionRegister(sender: UIButton!) {
        self.view .endEditing(true)
        if self.validateFields(){
            setLoader();
            let device_token = getBSEValue(_keyname: DEVICETOKEN).isEmpty ? "" :  getBSEValue(_keyname: DEVICETOKEN)
            let parameter = "email="+(emailField.text)!+"&&password="+(passField.text)!+"&&first_name="+(firstnameField.text)!+"&&last_name="+(lastnameField.text)!+"&&username="+(usernameField.text)!+"&&state="+(stateField.text)!+"&&country="+(countryCode)+"&&device_type=iphone&&device_token="+(device_token)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODREGISTER,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    let userModel : BSEUserModel = commonModel.temparray[0] as! BSEUserModel;
                    self.saveUser(model: userModel);
                     setBSEValue(_keyname:LOGINFROM, _value:"userlogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.login();
                } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionShowCountry(sender: UIButton!) {
        let navController = UINavigationController(rootViewController: countryList);
        self.present(navController, animated: true, completion: nil);
    }

    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==emailField{
            passField.becomeFirstResponder()
        } else if textField==passField{
            confirmpassField.becomeFirstResponder()
        } else if textField==confirmpassField{
            usernameField.becomeFirstResponder()
        } else if textField==usernameField{
            firstnameField.becomeFirstResponder()
        } else if textField==firstnameField{
            lastnameField.becomeFirstResponder()
        } else if textField==lastnameField{
            stateField.becomeFirstResponder()
        } else if textField==stateField{
            textField.resignFirstResponder();
            let navController = UINavigationController(rootViewController: countryList);
            self.present(navController, animated: true, completion: nil);
            
        } else{
            textField.resignFirstResponder()
            
        }
         return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == countryField) {
            countryField.resignFirstResponder();
            let navController = UINavigationController(rootViewController: countryList);
            self.present(navController, animated: true, completion: nil);
        }
    }
    
    
    // MARK: - Keyboard Function
    override func viewDidAppear(_ animated: Bool)
    {
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    
    // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - textview
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("testing clicked url",URL.absoluteString);
        return true;
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true;
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

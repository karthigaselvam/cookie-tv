//
//  RequestedCell.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class RequestedCell: UITableViewCell {
    
    @IBOutlet var fullname : UILabel?;
    @IBOutlet var userimage : UIImageView?;
    @IBOutlet var accept_btn: UIButton?;
    @IBOutlet var cancel_btn: UIButton?;
    @IBOutlet var confirmed_btn: UIButton?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userimage?.layer.cornerRadius = 25.0;
        userimage?.layer.masksToBounds = true;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEUserModel) -> Void {
        
  
        
        accept_btn?.setTitle( "ACCEPT".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        cancel_btn?.setTitle("CANCEL".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        confirmed_btn?.setTitle("ACCEPTED".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
            
 
        userimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        if(model.is_requested == 0) {
            accept_btn?.isHidden = false;
            cancel_btn?.isHidden = false;
            confirmed_btn?.isHidden = true;
        } else if (model.is_requested == 1) {
            accept_btn?.isHidden = true;
            cancel_btn?.isHidden = true;
            confirmed_btn?.isHidden = false;
            confirmed_btn?.setTitle("Approved", for: UIControlState.normal)
        } else if (model.is_requested == 2) {
            accept_btn?.isHidden = true;
            cancel_btn?.isHidden = true;
            confirmed_btn?.isHidden = false;
            confirmed_btn?.setTitle("Cancelled", for: UIControlState.normal)
        }
        fullname?.text = model.username.capitalized;
        
    }
}

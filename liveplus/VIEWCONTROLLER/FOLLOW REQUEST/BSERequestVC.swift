//
//  BSERequestVC.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit


class BSERequestVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
        var refreshControl: UIRefreshControl!
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    
    var progressBar:AMProgressBar!;
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var lbl_noData: UILabel!
      @IBOutlet var backView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(task==nil) {
        } else {
            if(task.state == .running) {
                task.cancel();
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - custom function
    
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
        }
        
        
        
        lbl_menuTitle.text = "FOLLOW REQUESTS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        lbl_noData.text =  "No Data".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        self.isFirstTime = true;
        self.currentpage = 1;
        self.getFollowerRequest();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
        
        
    }
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getFollowerRequest();
            self.isPullToRefresh = true;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getFollowerRequest();
                }
            }
        }
    }
    
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Request Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    func getFollowerRequest() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&page_no="+String(currentpage)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);

        }
        let originalString = APIURL + METHODFOLLOWREQUEST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }

                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODFOLLOWREQUEST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                        self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
        SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                         self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    func setLoader() -> Void {
        
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func requestAPI(parameter:String) -> Void {
        print("parameter are: \(parameter)")
        apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWREQUESTACTION,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                
            } else {
    SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
        }
    }
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let uidentifier = "RequestedCell"
        

        var cell: RequestedCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? RequestedCell
        if cell == nil {
              tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar")  ?  "RequestedCellRTL" : "RequestedCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? RequestedCell;
        }
        cell.setDefaultValue(model: tableData[indexPath.row] as! BSEUserModel)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.accept_btn?.tag = indexPath.row;
        cell.accept_btn?.addTarget(self, action: #selector(self.acceptAction(_:)), for: .touchUpInside)
        cell.cancel_btn?.tag = indexPath.row;
        cell.cancel_btn?.addTarget(self, action: #selector(self.cancelAction(_:)), for: .touchUpInside)
        return cell
    
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @objc func acceptAction(_ sender: UIButton){
        let model : BSEUserModel = tableData[sender.tag] as! BSEUserModel;
        
        let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&connect_id="+String(model.user_follow_status)+"&&follower_id="+String(model.user_id)+"&&status=1"+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        model.is_requested = 1;
        tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
        
        var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWERCOUNT))!
        follower_count = follower_count + 1;
        setBSEValue(_keyname: FOLLOWERCOUNT, _value: String(follower_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
        
        self.requestAPI(parameter: parameter);
        
    }
    
    @objc func cancelAction(_ sender: UIButton){
        let model : BSEUserModel = tableData[sender.tag] as! BSEUserModel;
        let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&connect_id="+String(model.user_follow_status)+"&&follower_id="+String(model.user_id)+"&&status=0";
        model.is_requested = 2;
        tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
        self.requestAPI(parameter: parameter);
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

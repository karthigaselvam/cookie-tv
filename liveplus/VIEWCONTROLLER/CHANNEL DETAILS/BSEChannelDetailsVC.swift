//
//  BSEChannelDetailsVC.swift
//  liveplus
//
//  Created by BSEtec on 15/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEChannelDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, BSESchedulePresenterVCDelegate {
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblView: UITableView!;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var keywordChanged: Bool!;
    var currentpage: Int!;
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!
    var channel_name: String!;
    var currentIndex:Int!
     @IBOutlet var backView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: custom function
    func initFields() -> Void {
        
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
        }
        
        
        
        
        self.isFirstTime = true;
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        currentpage = 0;
        self.getSearchChannel();
        
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
    }
    /**
     This is the function that help refresh UITableView through pull to refresh action
     */
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getSearchChannel();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getSearchChannel();
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
        
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func getSearchChannel() -> Void {
        isAPILoading = true;
        let parameter = "type=1&&page_no="+String(currentpage)+"&&keyword=&&hashtag="+channel_name+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        if(!apiservice().isInternetAvailable()) {
          SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODBROADCASTLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
                
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                     
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODBROADCASTLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                    self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noDataView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        
                        self.tblView?.reloadData();
                        self.hideLoader();
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                }
                
            }
        })
        task.resume();
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @objc func goToProfile(_ sender: UIButton){
        let model : BSEBroadcastModel = tableData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        let bidentifier = "BroadcastCell"

        var cell: BroadcastCell! = tableView.dequeueReusableCell(withIdentifier: bidentifier) as? BroadcastCell
        if cell == nil {
            tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "BroadcastCellRTL" : "BroadcastCell", bundle: nil), forCellReuseIdentifier: bidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: bidentifier) as? BroadcastCell;
        }
        cell.setDefaultValue(model: tableData[indexPath.row] as! BSEBroadcastModel)
        cell.broadcast_user_btn.tag = indexPath.row;
        cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToProfile(_:)), for: .touchUpInside)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var totalHeight:CGFloat = 100;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        let height = heightForView(text: model.broadcast_name, font: UIFont(name: "Poppins-Regular", size: 13)!, width: UIScreen.main.bounds.size.width-130)
        totalHeight = totalHeight+height;
        return totalHeight > 120 ? totalHeight : 120;

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        
        currentIndex = indexPath.row;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"viewer_count":model.viewer_count];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.delegate = self;
             presenter.page_from = "detail"
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func deleteItem() {
        tableData.remove(at: currentIndex);
        tblView?.reloadData();
        
        var broadcast_count:Int = Int(getBSEValue(_keyname: BROADCASTCOUNT))!
        broadcast_count = broadcast_count - 1;
        setBSEValue(_keyname: BROADCASTCOUNT, _value: String(broadcast_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

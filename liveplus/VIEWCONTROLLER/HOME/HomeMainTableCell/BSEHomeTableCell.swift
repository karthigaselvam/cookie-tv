//
//  BSEHomeTableCell.swift
//  liveplus
//
//  Created by BseTec on 10/4/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEHomeTableCell: UITableViewCell {
    
    @IBOutlet var broadcast_img1: UIImageView!;
    @IBOutlet var broadcast_title1: UILabel!;
    @IBOutlet var broadcast_tag1: UILabel!;
    @IBOutlet var broadcast_count1: UILabel!;
    
    
    @IBOutlet var broadcast_img2: UIImageView!;
    @IBOutlet var broadcast_title3: UILabel!;
    @IBOutlet var broadcast_tag3: UILabel!;
    @IBOutlet var broadcast_count2: UILabel!;
    
    
    @IBOutlet var broadcast_user_btn: UIButton!;
     @IBOutlet var broadcast_user_btn1: UIButton!;
    
    @IBOutlet var first_View: UIView!;
       @IBOutlet var seond_View: UIView!;
    
    @IBOutlet var first_LayerView: UIView!;
    @IBOutlet var second_LayerView: UIView!;
    override func awakeFromNib() {
        super.awakeFromNib()
        
        first_LayerView.layer.cornerRadius = 10.0;
        first_LayerView.layer.masksToBounds = true;
        
        first_View.layer.cornerRadius = 8.0;
        first_View.layer.masksToBounds = true;
        
        
        second_LayerView.layer.cornerRadius = 10.0;
        second_LayerView.layer.masksToBounds = true;
        
        seond_View.layer.cornerRadius = 8.0;
        seond_View.layer.masksToBounds = true;
        
        
//        let path = UIBezierPath(roundedRect:first_LayerView.bounds,
//                                byRoundingCorners:[.topRight, .bottomLeft],
//                                cornerRadii: CGSize(width: 20, height:  20))
//
//        let maskLayer = CAShapeLayer()
//
//        maskLayer.path = path.cgPath
//        first_LayerView.layer.mask = maskLayer
        
        
//        let path = UIBezierPath(roundedRect:  first_LayerView.bounds, byRoundingCorners: [], cornerRadii: CGSize(width: 10.0, height: 10.0))
//        let maskLayer = CAShapeLayer()
//        maskLayer.frame = self.bounds
//        maskLayer.path = path.cgPath
//         first_LayerView.layer.mask = maskLayer
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEBroadcastModel ) -> Void {
        
        broadcast_img1.sd_setImage(with: URL(string: BROADCASTIMGURL+String(model.broadcast_id)), placeholderImage: UIImage(named: "placeholder.png"))
        broadcast_count1.text = String(model.viewer_count)
        broadcast_count1.sizeToFit()
        
        var framer: CGRect = first_LayerView.frame;
        framer.size.width = broadcast_count1.frame.size.width +  broadcast_count1.frame.origin.x+8;
        first_LayerView.frame = framer;
        broadcast_title1.text = model.author_name.capitalized;
        let arr  = model.broadcast_name.components(separatedBy: " ")
        
        for var i in (0..<arr.count)
        {
            if (arr[i].contains("#"))
            {
                print(arr[i])
                broadcast_tag1.text = arr[i].capitalized;
                break;
            }
            else
            {
                broadcast_tag1.text = "";
            }
        }
        
        
        if broadcast_tag1.text == ""
        {
            var framer:CGRect! = broadcast_title1?.frame;
            framer.origin.y = first_View.frame.size.height-(broadcast_tag1.frame.size.height+8);
            broadcast_title1?.frame = framer;
        }
        else
        {
            var framer:CGRect! = broadcast_title1?.frame;
            framer.origin.y = first_View.frame.size.height-(broadcast_title1.frame.size.height+broadcast_tag1.frame.size.height+8);
            broadcast_title1?.frame = framer;
        }
    }
        func setDefaultSecondValue(andModel2:BSEBroadcastModel ) -> Void {
            broadcast_img2.sd_setImage(with: URL(string: BROADCASTIMGURL+String(andModel2.broadcast_id)), placeholderImage: UIImage(named: "placeholder.png"))
            broadcast_count2.text = String(andModel2.viewer_count)
            broadcast_count2.sizeToFit()
            
            var framer: CGRect = second_LayerView.frame;
            framer.size.width = broadcast_count2.frame.size.width +  broadcast_count1.frame.origin.x+8;
            second_LayerView.frame = framer;
            broadcast_title3.text = andModel2.author_name.capitalized;
            let arr  = andModel2.broadcast_name.components(separatedBy: " ")
            
            for var i in (0..<arr.count)
            {
                if (arr[i].contains("#"))
                {
                    print(arr[i])
                    broadcast_tag3.text = arr[i].capitalized;
                    break;
                }
                else
                {
                     broadcast_tag3.text = "";
                }
            }
            
            if broadcast_tag3.text == ""
            {
                
                
                
                var framer:CGRect! = broadcast_title3?.frame;
                framer.origin.y = seond_View.frame.size.height-(broadcast_tag3.frame.size.height+8);
                broadcast_title3?.frame = framer;
                
                
            }
            else
            {
                var framer:CGRect! = broadcast_title3?.frame;
                framer.origin.y = seond_View.frame.size.height-(broadcast_title3.frame.size.height+broadcast_tag3.frame.size.height+8);
                broadcast_title3?.frame = framer;
            }
            
            
            
            
            
        }
    }


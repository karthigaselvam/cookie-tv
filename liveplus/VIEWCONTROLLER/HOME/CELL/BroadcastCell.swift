//
//  BroadcastCell.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage

class BroadcastCell: UITableViewCell {
    
    @IBOutlet var broadcast_leftview: UIView!;
    @IBOutlet var top_View: UIView!;
    @IBOutlet var bottom_View: UIView!;
    @IBOutlet var broadcast_img: UIImageView!;
    @IBOutlet var broadcast_play_icon: UIImageView!;
    @IBOutlet var broadcast_btn: UIButton!;
    @IBOutlet var broadcast_status: UILabel!;
    @IBOutlet var broadcast_view_count: UIButton!;
    @IBOutlet var broadcast_title: UILabel!;
    @IBOutlet var broadcast_user_btn: UIButton!;
    @IBOutlet var broadcast_user_img: UIImageView!;
    @IBOutlet var broadcast_user_title: UILabel!;
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        broadcast_leftview.layer.cornerRadius = 5.0;
        broadcast_leftview.layer.masksToBounds = true;
        broadcast_user_img.layer.cornerRadius = 21.0;
        broadcast_user_img.layer.masksToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEBroadcastModel) -> Void {
        broadcast_img.sd_setImage(with: URL(string: BROADCASTIMGURL+String(model.broadcast_id)), placeholderImage: UIImage(named: "placeholder.png"))
        broadcast_user_img.sd_setImage(with: URL(string: USERIMGURL+String(model.author_id)), placeholderImage: UIImage(named: "placeholder.png"))
broadcast_status.text = (model.broadcast_status == "stop") ?
            "Completed".localized(lang: getBSEValue(_keyname: CURRENTLANG)): model.broadcast_status.capitalized;        broadcast_view_count.isHidden = (model.viewer_count>0) ? false : true;
        broadcast_view_count.setTitle("("+String(model.viewer_count)+")", for: .normal)
        broadcast_title.text = model.broadcast_name;
        broadcast_user_title.text = model.author_name.capitalized;
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            let title: NSString = broadcast_status.text! as NSString
            let view_count: NSString = "("+String(model.viewer_count)+")" as NSString
            let view_count_size = view_count.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
            
            broadcast_view_count.frame = CGRect(x:top_View.frame.size.width - (view_count_size.width+15), y:0, width: view_count_size.width+17, height:21);
            
            let size = title.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 13)!])
            
            var framer: CGRect = broadcast_status.frame;
            framer.size.width = size.width;
            framer.origin.x = (broadcast_view_count.isHidden) ? top_View.frame.size.width - (size.width+5): top_View.frame.size.width - (broadcast_view_count.frame.size.width+size.width);
            broadcast_status.frame = framer;
        }
        else
        {
            let title: NSString = broadcast_status.text! as NSString
            let size = title.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 13)!])
            var framer: CGRect = broadcast_status.frame;
            framer.size.width = size.width;
            broadcast_status.frame = framer;
            
            let view_count: NSString = "("+String(model.viewer_count)+")" as NSString
            let view_count_size = view_count.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 12)!])
            
            broadcast_view_count.frame = CGRect(x:broadcast_status.frame.size.width + broadcast_status.frame.origin.x + 4, y:0, width: view_count_size.width+17, height:21);
        }


    }
    
}

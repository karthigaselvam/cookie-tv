//
//  BSEHomeMainCollectionCell.swift
//  liveplus
//
//  Created by BseTec on 10/4/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import SDWebImage
class BSEHomeMainCollectionCell: UICollectionViewCell {

    @IBOutlet var tag_title: UILabel!;
        @IBOutlet var tag_image: UIImageView!;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setDefaultValue(model:BSEChannelModel) -> Void {
        
        if  model.channel_name.caseInsensitiveCompare("travel") == .orderedSame
        {
            tag_image.image=UIImage(named: "chn_1.png")
        }
        else if model.channel_name.caseInsensitiveCompare("party") == .orderedSame
        {
            tag_image.image=UIImage(named: "chn_2.png")
        }
        else if model.channel_name.caseInsensitiveCompare("music") == .orderedSame
        {
            tag_image.image=UIImage(named: "chn_3.png")
        }
        else if model.channel_name.caseInsensitiveCompare("game") == .orderedSame
        {
            tag_image.image=UIImage(named: "chn_4.png")
        }
        else if model.channel_name.caseInsensitiveCompare("concert") == .orderedSame
        {
            tag_image.image=UIImage(named: "chn_5.png")
        }
        else
        {
            tag_image.image=UIImage(named: "video.png")
        }
        
        tag_title.text =  model.channel_name.capitalized;
    }

}

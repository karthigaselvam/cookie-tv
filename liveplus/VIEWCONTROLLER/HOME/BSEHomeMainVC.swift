//
//  BSEHomeMainVC.swift
//  liveplus
//
//  Created by BseTec on 10/2/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import PageMenu
import MaterialIconsSwift
import MapKit

class BSEHomeMainVC: UIViewController,CAPSPageMenuDelegate,UITableViewDataSource, UITableViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegate,BSESchedulePresenterVCDelegate,UIScrollViewDelegate{
    
var pageMenu : CAPSPageMenu?
@IBOutlet var safeView : UIView?;
    
    @IBOutlet var featured_title : UILabel?;
    @IBOutlet var nearBy_title : UILabel?;
    @IBOutlet var new_title : UILabel?;
    @IBOutlet var video_title : UILabel?;
    
    @IBOutlet var headerView : UIView?;
    @IBOutlet var firstView : UIView?;
    @IBOutlet var secondView : UIView?;
    @IBOutlet var thirdView : UIView?;
    @IBOutlet var fourView : UIView?;
    @IBOutlet var fifthView : UIView?;
    
    @IBOutlet var featured_border : UILabel?;
    @IBOutlet var nearBy_border : UILabel?;
    @IBOutlet var new_border : UILabel?;
    
    @IBOutlet var nearByView : UIView?;
    @IBOutlet var tblView : UITableView?;
    @IBOutlet var feature_tblView : UITableView?;
    @IBOutlet var header_title : UILabel?;
    @IBOutlet var leftView : UIView?;
    @IBOutlet var rightView : UIView?;
    @IBOutlet var suggestion_btn : UIButton?;
    @IBOutlet var map_btn : UIButton?;
    @IBOutlet var map_border : UIView?;
      @IBOutlet var tag_view : UIView?;
    @IBOutlet var suggestion_border : UIView?;
    @IBOutlet var mapView: MKMapView!
    
    
    var tableData = [AnyObject]();
    var featureData = [AnyObject]();
    var tagData = [AnyObject]();
    var mapData = [BSEMapModel]();
    var isAPILoading: Bool!;
    var page_type:String!;
    var main_page_type:String!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var progressBar:AMProgressBar!
      var page_keyword: String!;
    var currentIndex : Int!
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var pageControl : UIPageControl?
    @IBOutlet var featureView : UIView?;
     @IBOutlet var no_DataView : UIView?;
    @IBOutlet var pageScroll : UIScrollView?;
    @IBOutlet var feature_headerView : UIView?;
     var items: [UIImage] = [];
    @IBOutlet var no_dataLbl : UILabel?;
    
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    var refreshControl: UIRefreshControl!
    
    
    
var controllerArray : [UIViewController] = []
override func viewDidLoad() {
super.viewDidLoad()
  initFields();
        // Do any additional setup after loading the view.
    }
    // MARK: - custom function
    
    func initFields() -> Void {
        
        setupMiddleButton()
        nearByView?.isHidden = true;
        featureView?.isHidden = true;
        no_DataView?.isHidden = true;
        
        nearBy_border?.isHidden = true;
        featured_border?.isHidden = true;
        new_border?.isHidden = true;
           page_keyword = ""
        collection.register(UINib(nibName: "BSEHomeMainCollectionCell", bundle: nil), forCellWithReuseIdentifier: "collectioncell")
      
        featured_title?.text =  "Featured".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        new_title?.text =  "New".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        nearBy_title?.text =  "NearBy".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        video_title?.text =  "Video".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tabMenuNotify(notification:)), name: Notification.Name("HomeMain"), object: nil)
        CommonFunction(0)
        getChannelsList()
        
        items =  [
            UIImage(named: "banner_1.jpg")!,
            UIImage(named: "banner_2.jpg")!,
        ]
        
        pageControl?.numberOfPages = items.count
        pageControl?.currentPage = 0
       // pageScroll?.bringSubview(toFront:pageControl!);
        pageScroll?.delegate = self
        
        for i in 0..<items.count {
            let imageView = UIImageView()
            let x = UIScreen.main.bounds.size.width * CGFloat(i)
            imageView.frame = CGRect(x: x, y: 0, width: UIScreen.main.bounds.size.width, height:120)
            imageView.contentMode = .scaleAspectFill
            imageView.image = items[i]
            imageView.layer.masksToBounds =  true
            pageScroll?.contentSize.width = UIScreen.main.bounds.size.width * CGFloat(i + 1)
            pageScroll?.addSubview(imageView)
        }
 
    }


  

    func setupMiddleButton() {
        
        let menuButton = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 || UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2688) ? (view.bounds.height-34) - menuButtonFrame.height:view.bounds.height - menuButtonFrame.height
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        menuButton.tag = 101

        let cam_Icon = UIImageView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
        cam_Icon.image = UIImage(named:"Live_Selected.png")
        menuButton.addSubview(cam_Icon)
        

        let menu_btn = UIButton(frame: menuButton.bounds)
        menu_btn.addTarget(self, action: #selector(menuButtonAction(sender:)), for: .touchUpInside)
        menuButton.addSubview(menu_btn)

        let window = UIApplication.shared.keyWindow!
        window.addSubview(menuButton);

//        var semiCircleLayer   = CAShapeLayer()
//        let center = CGPoint (x: menuButton.frame.size.width / 2, y: menuButton.frame.size.height / 2)
//        let circleRadius = menuButton.frame.size.width / 2
//        let circlePath = UIBezierPath(arcCenter: center, radius: circleRadius, startAngle: CGFloat(M_PI_2 * 2), endAngle: CGFloat(M_PI_2 * 2), clockwise: true)
//
//        semiCircleLayer.path = circlePath.cgPath
//        semiCircleLayer.strokeColor = UIColor.darkGray.cgColor
//        semiCircleLayer.fillColor = UIColor.clear.cgColor
//        semiCircleLayer.lineWidth = 1
//        semiCircleLayer.strokeStart = 0
//        semiCircleLayer.strokeEnd  = 1
//        menuButton.layer.addSublayer(semiCircleLayer)
        
        
        menuButton.backgroundColor = UIColor.white
//        menuButton.layer.borderColor = UIColor.white.cgColor
//        menuButton.layer.borderWidth = 5.0
        menuButton.layer.cornerRadius = menuButtonFrame.height/2;
        
        menuButton.layer.shadowColor = UIColor.black.cgColor
        menuButton.layer.shadowOpacity = 0.5
        menuButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        menuButton.layer.shadowRadius = 10
//        //menuButton.layer.shadowPath = UIBezierPath(rect: menuButton.bounds).cgPath
//
        let path = UIBezierPath()

        // Start at the Top Left Corner
        path.move(to: CGPoint(x: 0.0, y: 0.0))

        // Move to the Top Right Corner
        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: 0.0))

        //        // Move to the Bottom Right Corner
        //        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: menuButton.frame.size.height))

        // This is the extra point in the middle :) Its the secret sauce.
        path.addLine(to: CGPoint(x: menuButton.frame.size.width/2.0, y: menuButton.frame.size.height/2.0))

//        // Move to the Bottom Left Corner
//        path.addLine(to: CGPoint(x: 0.0, y: menuButton.frame.size.height))

        path.close()

        menuButton.layer.shadowPath = path.cgPath
      menuButton.layer.masksToBounds = true;
        menuButton.layer.shouldRasterize = true
        view.layoutIfNeeded()
    }
    
    // MARK: - Actions
    @objc private func menuButtonAction(sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAddCamera()
    }
    func noHeaderView() -> UIView
    {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Data Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            if(self.main_page_type == "featured" || self.main_page_type == "new")
            {
            self.page_keyword = ""
                self.getChannelsList();
            self.getRecentBroadcast();
            }
            else
            {
            self.getSuggestionBroadcast()
            }
            self.isPullToRefresh = true;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if(scrollView == pageScroll)
        {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl?.currentPage = Int(pageNumber)
        }
        else{
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.currentpage = self.currentpage + 1;
                    if(self.main_page_type == "featured" || self.main_page_type == "new")
                    {
                     self.feature_tblView?.tableFooterView = self.tableCustomFooterLoader();
                         self.getRecentBroadcast();
                    }
                    else
                    {
                        self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                           self.getSuggestionBroadcast()
                    }
                   
                }
            }
        }
    }
    }
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    // MARK: - Notification function
    @objc func tabMenuNotify(notification: Notification){
        let type : String = notification.userInfo!["type"] as! String
        if( type == "feature") {
            CommonFunction(0)
        }
        else if( type == "new")
        {
            CommonFunction(2)
        }
        else if( type == "tab_icon")
        {
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) == nil {
                 setupMiddleButton()
            }
        }
        else  if( type == "push_notification") {
            actionNotification(sender: nil)
        }
        else
        {
            
//NotificationCenter.default.post(name: Notification.Name("FollowList"), object: nil, userInfo: ["type": (main_page_type == "featured") ?  "feature" : "new"])
        }
    }
    
    //MARK: Featured and New function
   
    @objc func CommonFunction(_ sender: NSInteger){
        featured_border?.isHidden = true;
        nearBy_border?.isHidden = true;
        new_border?.isHidden = true;
        
        if(sender == 0 || sender == 2) {
            if(sender == 0)
            {
                featured_border?.isHidden = false;
                featured_title?.textColor = UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                nearBy_title?.textColor = UIColor.white
                new_title?.textColor = UIColor.white
                featured_border?.backgroundColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                featureView?.isHidden = false;
                nearByView?.isHidden = true;
                main_page_type = "featured"
               // NotificationCenter.default.post(name: Notification.Name("FollowList"), object: nil, userInfo: ["type":"feature"])
            }
            else
            {
                new_border?.isHidden = false;
                new_title?.textColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                featured_title?.textColor = UIColor.white
                nearBy_title?.textColor = UIColor.white
                new_border?.backgroundColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
                featureView?.isHidden = false;
                nearByView?.isHidden = true;
                main_page_type = "new"
                self.page_keyword = ""
                self.featureData.removeAll();
                feature_tblView?.reloadData()
               // NotificationCenter.default.post(name: Notification.Name("FollowList"), object: nil, userInfo: ["type":"new"])
            }
            feature_tblView?.tableHeaderView =  (sender == 0) ?  feature_headerView :UIView (frame: CGRect.zero);
            
            feature_tblView?.tableFooterView = UIView (frame: CGRect.zero)
            isPullToRefresh = false;
            isInfiniteScrolling = false;
            self.isFirstTime = true;
            self.currentpage = 1;
            getRecentBroadcast()
            feature_tblView?.tableFooterView = UIView (frame: CGRect.zero)
            
            refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
            self.feature_tblView?.insertSubview(refreshControl, at: 0)
            
        }
        else if(sender == 1) {
//            nearBy_border?.isHidden = false;
//            nearBy_title?.textColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
//            featured_title?.textColor = UIColor.white
//            new_title?.textColor = UIColor.white
//            nearBy_border?.backgroundColor =  UIColor.init(red: 243.0/255.0, green: 153.0/255.0, blue: 33.0/255.0, alpha: 1.0)
//            featureView?.isHidden = true;
//            main_page_type = "nearby"
//            CommonSuggestedFunction()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.tabcontroller?.selectedIndex = 1
        }
        
    }
    
    func getRecentBroadcast() -> Void
    {
        isAPILoading = true;
        let parameter = "type=1&&page_no="+String(currentpage)+"&&keyword=&&hashtag="+page_keyword+"&&language="+getBSEValue(_keyname: CURRENTLANG)+"&&user_id="+getBSEValue(_keyname: USERID);
        setLoader();
        apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTLIST,  andCompletionBlock: { (success) -> Void in
            
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                print(commonModel.return_id);
                
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
              appDelegate.tabcontroller?.tabBar.items?[4].badgeValue = (commonModel.return_id == "0") ? nil : commonModel.return_id;
                
                if(self.isFirstTime) {
                    self.featureData.removeAll();
                }
                if(commonModel.temparray.count>0) {
                    self.featureData.append(contentsOf:commonModel.temparray);
                    self.isAPILoading = false;
                }
            }
            else {
                self.isAPILoading = false;
                SwiftMessageBar.showMessageWithTitle("Error", message:
                    commonModel.status_Msg, type: .error);
            }
            if(self.featureData.count==0)
            {
            self.feature_tblView?.tableFooterView = self.noHeaderView();
            }
            else
            {
            self.feature_tblView?.tableFooterView = UIView.init();
            }
            self.feature_tblView?.reloadData();
            if(self.isFirstTime) {
                self.showBroadcastController();
                self.isFirstTime=false
            }
            self.hideLoader();
        }) {
            (failure) -> Void in
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.isAPILoading = false;
            self.hideLoader();
        }
    }
    
    func getChannelsList() -> Void {
        isAPILoading = true;
        let parameter = "page_no="+String(currentpage)+"&&keyword="+page_keyword+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        // setLoader();
        
        apiservice().apiGetRequest(params:parameter, withMethod: METHODCHANNELLIST,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                if(self.isFirstTime) {
                    self.tagData.removeAll();
                }
                if(commonModel.temparray.count>0) {
                    self.tagData.append(contentsOf:commonModel.temparray);
                    self.isAPILoading = false;
                }
            } else {
                self.isAPILoading = false;
                SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            
            if(self.tagData.count==0) {
                self.no_dataLbl?.isHidden = false;
            }
            else
            {
                self.no_dataLbl?.isHidden = true;
            }
            self.collection?.reloadData();
            if(self.isFirstTime) {
                self.isFirstTime=false
            }
            //  self.hideLoader();
        }) { (failure) -> Void in
            self.isAPILoading = false;
            // self.hideLoader();
        }
    }
    
    func showBroadcastController() -> Void {
        
        
        
        let is_current_broadcast:Int = Int(getBSEValue(_keyname: ISBROADCASTID))!
        if(is_current_broadcast > 0) {
            
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) != nil {
                window.viewWithTag(101)?.removeFromSuperview()
            }
            
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(is_current_broadcast),"broadcast_status":"live"];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        }
    }
    @objc func goToFeaturedProfile(_ sender: UIButton){
        let model : BSEBroadcastModel = featureData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }

    
     //MARK: Nearby Function
    
     @objc func CommonSuggestedFunction()
     {
        nearByView?.isHidden = false;
        suggestion_btn?.setTitle( "suggestion".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        map_btn?.setTitle( "map".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        var framer:CGRect! = leftView?.frame;
        framer.size.width = UIScreen.main.bounds.size.width/2;
        leftView?.frame = framer;
        
        framer = rightView?.frame;
        framer.origin.x = UIScreen.main.bounds.size.width/2
        framer.size.width = UIScreen.main.bounds.size.width/2;
        rightView?.frame = framer;
        
        tblView?.isHidden=false;
        mapView?.isHidden=true;
        map_border?.isHidden = true;
      
        suggestion_border?.isHidden = false;
        
        
        page_type = "map";
         CommonButtonFunction(1)
        
            self.mapView.delegate = self;
            self.mapView.register(BSEHomeMapView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
            let span = MKCoordinateSpanMake(180, 360)
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0000, longitude: 0.0000), span: span)
            self.mapView.setRegion(region, animated: true)
    
    }
    
        func getSuggestionBroadcast() -> Void {
            isAPILoading = true;
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&page_no="+String(currentpage)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            setLoader();
            leftView?.isUserInteractionEnabled = false;
            rightView?.isUserInteractionEnabled = false;
            apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTSUGGESTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    if(self.isFirstTime) {
                        self.tableData.removeAll();
                        self.isFirstTime = false;
                    }
                    if(commonModel.temparray.count>0) {
                        self.tableData.append(contentsOf:commonModel.temparray);
                        self.isAPILoading = false;
                    }
                } else {
                    self.isAPILoading = false;
                    SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
                }
                
                if(self.tableData.count==0) {
                     self.tblView?.tableFooterView = self.noHeaderView();
                }
                else
                {
                    self.tblView?.tableFooterView = UIView.init();

                }
                self.tblView?.reloadData();
                self.hideLoader();
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.leftView?.isUserInteractionEnabled = true;
                self.rightView?.isUserInteractionEnabled = true;
            }) { (failure) -> Void in
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                
                self.isAPILoading = false;
                self.hideLoader();
                self.leftView?.isUserInteractionEnabled = true;
                self.rightView?.isUserInteractionEnabled = true;
            }
        }
        
        func getLiveBroadcast() -> Void {
            leftView?.isUserInteractionEnabled = false;
            rightView?.isUserInteractionEnabled = false;
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            setLoader();
            apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTLIVE,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    if(commonModel.temparray.count>0) {
                        for Dic in commonModel.temparray {
                            let broadcast: BSEBroadcastModel = Dic as! BSEBroadcastModel;
                            let locationName: String = "Post by "+broadcast.author_name.capitalized;
                            let lat = (broadcast.latitude as NSString).doubleValue
                            let longit = (broadcast.longitude as NSString).doubleValue
                            let mapModel: BSEMapModel = BSEMapModel(title: broadcast.broadcast_name,
                                                                    broadcast_id: broadcast.broadcast_id,
                                                                    user_id: broadcast.author_id,
                                                                    broadcast_status: broadcast.broadcast_status,
                                                                    locationName: locationName,
                                                                    discipline: "Sculpture",
                                                                    coordinate: CLLocationCoordinate2D(latitude: lat, longitude: longit))
                            self.mapData.append(mapModel);
                        }
                        self.mapView.addAnnotations(self.mapData);
                    }
                } else {
                    SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
                }
                self.leftView?.isUserInteractionEnabled = true;
                self.rightView?.isUserInteractionEnabled = true;
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
                self.leftView?.isUserInteractionEnabled = true;
                self.rightView?.isUserInteractionEnabled = true;
            }
        }
        
        func setLoader() -> Void {
           
            progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
            
            progressBar.cornerRadius = 0
            progressBar.borderColor = .clear
            progressBar.borderWidth = 0
            
            progressBar.barCornerRadius = 0
            progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
            progressBar.barMode = AMProgressBarMode.determined.rawValue
            
            progressBar.hideStripes = false
            progressBar.stripesColor = .white
            progressBar.stripesWidth = 30
            progressBar.stripesDelta = 100
            progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
            progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
            progressBar.stripesSpacing = 30
            
            progressBar.textColor = .white
            progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
            progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
            progressBar.loaderText = ""
            progressBar.progressValue = 1
            progressBar.setProgress(progress: 1, animated: true)
            headerView?.addSubview(progressBar)
            headerView?.sendSubview(toBack: progressBar);
         
    }
        
        func hideLoader() -> Void
        {
            if progressBar != nil { // Make sure the view exists
                if (headerView?.subviews.contains(progressBar))! {
                    progressBar.removeFromSuperview() // Remove it
                }
            }
        }
        
 
    @IBAction func actionListSelect(sender: UIButton!) {
        CommonButtonFunction(sender.tag)
    }
    
    @objc func CommonButtonFunction(_ sender: NSInteger){
        suggestion_border?.isHidden = true;
        map_border?.isHidden = true;
        map_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        suggestion_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
        if(sender == 0) {
            tblView?.isHidden=false;
            mapView?.isHidden=true;
            suggestion_border?.isHidden = false;
            suggestion_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "suggestion";
            isPullToRefresh = false;
            isInfiniteScrolling = false;
            self.isFirstTime = true;
            self.currentpage = 1;
            getSuggestionBroadcast()
            tblView?.tableFooterView = UIView (frame: CGRect.zero)
            
            refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
            self.tblView?.insertSubview(refreshControl, at: 0)
            
        } else {
            tblView?.isHidden=true;
            mapView?.isHidden=false;
            map_border?.isHidden = false;
            map_btn?.setTitleColor(UIColor.init(red: 196.0/255.0, green: 12.0/255.0, blue: 35.0/255.0, alpha: 1.0), for: .normal)
            page_type = "map";
            getLiveBroadcast();
        }
    }
    
    
    @objc func goToProfile(_ sender: UIButton)
    {
        let model : BSEBroadcastModel = tableData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }
   
    
    // MARK: table Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableView ==  feature_tblView) ? Int(round(Float(featureData.count) / Float(2)))  : tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        
        if (tableView ==  feature_tblView)
        {
            let identifier = "BSEHomeTableCell"
            var cell: BSEHomeTableCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BSEHomeTableCell
            if cell == nil {
                tableView.register(UINib(nibName: "BSEHomeTableCell", bundle: nil), forCellReuseIdentifier: identifier);
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BSEHomeTableCell;
            }
            
              var startIndex: NSInteger!;
              var endIndex: NSInteger!;
            startIndex = indexPath.row * 2;
            endIndex = startIndex + 1;
            var model1: BSEBroadcastModel
            var model2: BSEBroadcastModel

            if startIndex < featureData.count
            {
            model1 = featureData[startIndex] as! BSEBroadcastModel
            cell.setDefaultValue(model: model1)
            cell.broadcast_user_btn.tag = startIndex;
            cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToDetail(_:)), for: .touchUpInside)
            }

            if endIndex < featureData.count
            {
                cell.seond_View.isHidden = false ;
                model2 = featureData[endIndex] as! BSEBroadcastModel
                cell.setDefaultSecondValue(andModel2: model2)
                cell.broadcast_user_btn1.tag = endIndex;
                cell.broadcast_user_btn1.addTarget(self, action: #selector(self.goToDetail(_:)), for: .touchUpInside)
            }
            else
            {
                cell.seond_View.isHidden = true ;
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else
        {
            let identifier = "BroadcastCell"
            var cell: BroadcastCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell
            if cell == nil {
                tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "BroadcastCellRTL" : "BroadcastCell", bundle: nil), forCellReuseIdentifier: identifier);
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell;
            }
            cell .setDefaultValue(model: tableData[indexPath.row] as! BSEBroadcastModel)
            cell.broadcast_user_btn.tag = indexPath.row;
            cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToProfile(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (tableView ==  feature_tblView)
        {
            return 160;
        }
        
        else
        {
        var totalHeight:CGFloat = 100;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        let height = heightForView(text: model.broadcast_name, font: UIFont(name: "Poppins-Regular", size: 13)!, width: UIScreen.main.bounds.size.width-130)
        totalHeight = totalHeight+height;
        
        return totalHeight > 120 ? totalHeight : 120;
        }
        
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        if (tableView ==  feature_tblView && self.main_page_type=="featured")
//        {
//        return tag_view
//        }
//        else
//        {
//        return nil
//        }
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if (tableView ==  feature_tblView && self.main_page_type=="featured")
//        {
//        return 85.0
//        }
//        else
//        {
//        return 0.0
//        }
//    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView ==  tblView)
        {
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) != nil {
                window.viewWithTag(101)?.removeFromSuperview()
            }
            
        let model: BSEBroadcastModel
        currentIndex=indexPath.row;
        model = tableData[indexPath.row] as! BSEBroadcastModel
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
           presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"viewer_count":model.viewer_count];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
             presenter.delegate = self;
             presenter.page_from = "detail"
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    }
    
    @objc func goToDetail(_ sender: UIButton){
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        currentIndex = sender.tag;
        let model : BSEBroadcastModel = featureData[sender.tag] as! BSEBroadcastModel;
       if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
        let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
        presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":String(model.author_id),"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
        presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
        presenter.delegate = self;
        presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status,"broadcast_userID":model.author_id,"broadcast_username":model.author_name,"viewer_count":model.viewer_count];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    func deleteItem() {
        
        if(self.main_page_type=="featured" || self.main_page_type=="new")
        {
            featureData.remove(at: currentIndex);
            feature_tblView?.reloadData();
        }
        else
        {
            tableData.remove(at: currentIndex);
            tblView?.reloadData();
        }
        
        var broadcast_count:Int = Int(getBSEValue(_keyname: BROADCASTCOUNT))!
        broadcast_count = broadcast_count - 1;
        setBSEValue(_keyname: BROADCASTCOUNT, _value: String(broadcast_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: control function
    @IBAction func actionNotification(sender: UIButton!) {
        let settings = BSENotifyVC(nibName: "BSENotifyVC", bundle: nil)
        self.navigationController?.pushViewController(settings, animated: true)
    }
    @IBAction func actionList(sender: UIButton!) {
         CommonFunction(sender.tag)
    }
    @IBAction func actionNearBy(sender: UIButton!) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.tabcontroller?.selectedIndex = 1
    }
    
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }
  
    
    
    @IBAction func actionVideo(sender: UIButton!) {
//
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1")
        {
            let discover = BSELiveVC(nibName: "BSELiveVC", bundle: nil)
            self.navigationController?.pushViewController(discover, animated: true)
        }
        else
        {
           let permission = BSEPermissionVC(nibName:"BSEPermissionVC",bundle:nil);
            permission.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(permission, animated: false, completion: nil)
//            if let window = self.window, let rootViewController = window.rootViewController {
//                var currentController = rootViewController
//                while let presentedController = currentController.presentedViewController {
//                    currentController = presentedController
//                }
//                currentController.present(permission!, animated: false, completion: nil)
//            }
        }
        

    }
    @IBAction func actionProfile(sender: UIButton!) {
        
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        let model:BSEUserModel = BSEUserModel();
        model.user_id = Int(getBSEValue(_keyname: USERID));
        model.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        model.username = getBSEValue(_keyname: USERNAME);
        model.first_name = getBSEValue(_keyname: FIRSTNAME);
        model.last_name = getBSEValue(_keyname: LASTNAME);
        model.country = getBSEValue(_keyname: COUNTRY);
        model.state = getBSEValue(_keyname: STATE);
        model.email = getBSEValue(_keyname: EMAIL);
        model.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        model.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        model.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        model.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        model.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        model.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        model.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        model.role = Int(getBSEValue(_keyname: ROLE));
        profile.model = model
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - MKMapViewDelegate

extension BSEHomeMainVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location : BSEMapModel = view.annotation as! BSEMapModel
        
        let window = UIApplication.shared.keyWindow!
        if window.viewWithTag(101) != nil {
            window.viewWithTag(101)?.removeFromSuperview()
        }
        
        if(String(describing: location.user_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(location.broadcast_id),"broadcast_status":location.broadcast_status];
             presenter.page_from = "detail"
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(location.broadcast_id),"broadcast_status":location.broadcast_status];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
    }
    
}

// MARK: - mapview annotation
class BSEHomeMapView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? BSEMapModel else {return}
            
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "navigate.png"), for: UIControlState())
            rightCalloutAccessoryView = mapsButton
            image = UIImage(named: "location.png")
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = artwork.subtitle
            detailCalloutAccessoryView = detailLabel
        }
    }
}


// MARK: - UICollectionViewDataSource
extension BSEHomeMainVC {
    //1
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return tagData.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  "collectioncell", for: indexPath) as! BSEHomeMainCollectionCell
        cell.setDefaultValue(model: tagData[indexPath.row] as! BSEChannelModel)
      //  cell.backgroundColor = .clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          let model: BSEChannelModel = tagData[indexPath.row] as! BSEChannelModel
        page_keyword =  model.channel_name;
        CommonFunction(0)
    }
    
}




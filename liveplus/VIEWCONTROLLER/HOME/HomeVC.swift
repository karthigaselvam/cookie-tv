//
//  HomeVC.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit


class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, BSESchedulePresenterVCDelegate {

    @IBOutlet var tblView : UITableView?;
    @IBOutlet var header_title : UILabel?;
    @IBOutlet var headerView : UIView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
@IBOutlet var backView : UIView?;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var progressBar:AMProgressBar!
    var currentIndex : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {

    }
    override func viewWillDisappear(_ animated: Bool)
    {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: custom function
    func initFields() -> Void {
        
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = 10;
            backView!.frame = framer;
            
            backView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            header_title?.textAlignment = .right
            
        }
        
        
        
        isFirstTime = true;
        currentpage = 1;
        getRecentBroadcast();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
          header_title? .text = "recent".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
    }
    
    func getRecentBroadcast() -> Void {
        isAPILoading = true;
        let parameter = "type=1&&page_no="+String(currentpage)+"&&keyword=&&hashtag="+"&&language="+getBSEValue(_keyname: CURRENTLANG)+"&&user_id="+getBSEValue(_keyname: USERID);
        setLoader();
        apiservice().apiGetRequest(params:parameter, withMethod: METHODBROADCASTLIST,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                if(self.isFirstTime) {
                    self.tableData.removeAll();
                    
                }
                if(commonModel.temparray.count>0) {
    self.tableData.append(contentsOf:commonModel.temparray);
                    self.isAPILoading = false;
                } else {
                   
                }
            } else {
                self.isAPILoading = false;
                SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            
            if(self.tableData.count==0) {
                
            }
            self.tblView?.reloadData();
            if(self.isFirstTime) {
                self.showBroadcastController();
                self.isFirstTime=false
            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.isAPILoading = false;
            self.hideLoader();
        }
    }
    
    func showBroadcastController() -> Void {
        let is_current_broadcast:Int = Int(getBSEValue(_keyname: ISBROADCASTID))!
        if(is_current_broadcast > 0) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(is_current_broadcast),"broadcast_status":"live"];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        }
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = .white
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        
    }
    
    
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if (headerView?.subviews.contains(progressBar))! {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }
    @objc func goToProfile(_ sender: UIButton){
        let model : BSEBroadcastModel = tableData[sender.tag] as! BSEBroadcastModel;
        let userModel : BSEUserModel = BSEUserModel();
        userModel.user_id = model.author_id;
        userModel.username = model.author_name;
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = userModel;
        self.navigationController?.pushViewController(profile, animated: true)
    }

    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //variable type is inferred
        let identifier = "BroadcastCell"
        var cell: BroadcastCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell
        if cell == nil {
             tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "BroadcastCellRTL" : "BroadcastCell", bundle: nil), forCellReuseIdentifier: identifier);
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BroadcastCell;
        }
        cell.setDefaultValue(model: tableData[indexPath.row] as! BSEBroadcastModel)
        cell.broadcast_user_btn.tag = indexPath.row;
        cell.broadcast_user_btn.addTarget(self, action: #selector(self.goToProfile(_:)), for: .touchUpInside)
        cell.backgroundColor = .clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var totalHeight:CGFloat = 100;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        let height = heightForView(text: model.broadcast_name, font: UIFont(name: "Poppins-Regular", size: 13)!, width: UIScreen.main.bounds.size.width-130)
        totalHeight = totalHeight+height;
        
        return totalHeight > 120 ? totalHeight : 120;
   
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentIndex=indexPath.row;
        let model: BSEBroadcastModel = tableData[indexPath.row] as! BSEBroadcastModel
        if(String(model.author_id) == getBSEValue(_keyname: USERID)) {
            let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
            presenter.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status];
            presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
            presenter.delegate = self;
            presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
        } else {
            let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
            viewer.model = ["broadcast_id":String(model.broadcast_id),"broadcast_status":model.broadcast_status];
            viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
             viewer.current_lang = getBSEValue(_keyname: CURRENTLANG)
            self.navigationController?.pushViewController(viewer, animated: true)
        }
        
    }
    
    func deleteItem() {
        tableData.remove(at: currentIndex);
        tblView?.reloadData();
        
        var broadcast_count:Int = Int(getBSEValue(_keyname: BROADCASTCOUNT))!
        broadcast_count = broadcast_count - 1;
        setBSEValue(_keyname: BROADCASTCOUNT, _value: String(broadcast_count));
        
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }

}

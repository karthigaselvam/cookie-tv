//
//  BSEForgotVC.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEForgotVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var page_scroll: UIScrollView!
    @IBOutlet weak var header_img: UIImageView!
    @IBOutlet var submit_btn: UIButton!
         @IBOutlet var back_btn: UIButton!;
    var progressBar:AMProgressBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
    // MARK: - Custom Function
    func initFields() -> Void {
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            emailField.textAlignment = .right
            
        }
        else
        {
            emailField.textAlignment = .left
        }
            
        emailField.placeholder =  "Username or Email Address".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        submit_btn?.setTitle("SUBMIT".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        back_btn?.setTitle("Back".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        
        
        header_img.layer.masksToBounds=false
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: submit_btn.bounds)
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 10
        progressBar.barColor = .white
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 0.5)
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = "SENDING".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        submit_btn.addSubview(progressBar)
    }
    

    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if submit_btn.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func validateFields() -> Bool {
        if (FormValidation().notEmpty(msg: emailField.text!)) {
            if !FormValidation().isValidEmail(testStr: emailField.text!){
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"InValid Email".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , type: .error);
                return false;
            }
            return true;
        } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    

    // MARK: - Control function
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);

    }
    @IBAction func actionSubmit(sender: UIButton!) {
        self.view .endEditing(true)
        if self.validateFields()
        {
            setLoader();
            let parameter = "email="+(emailField.text)!+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFORGOT,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:commonModel.status_Msg  , type: .success);
                } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    

    
    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // MARK: - Keyboard Function
    override func viewWillAppear(_ animated: Bool)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    
    // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

//
//  LanguageVC.swift
//  majlis
//
//  Created by BseTec on 4/18/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
class LanguageVC: UIViewController {
    
    @IBOutlet var lbl_english: UILabel!;
    @IBOutlet var lbl_arabic: UILabel!;
    @IBOutlet var lbl_indoesia: UILabel!;
    @IBOutlet var lbl_french: UILabel!;
    @IBOutlet var lbl_germen: UILabel!;
    @IBOutlet var lbl_icon1: UILabel!;
    @IBOutlet var lbl_icon2: UILabel!;
    @IBOutlet var lbl_icon3: UILabel!;
    @IBOutlet var lbl_icon4: UILabel!;
    @IBOutlet var lbl_icon5: UILabel!;
    @IBOutlet var confirm_Title: UILabel!;
    @IBOutlet var menu_Title: UILabel!;
    
    var is_currentLang:String!;
    @IBOutlet var overLay_Btn : UIButton?;
    @IBOutlet var delete_Btn : UIButton?;
    @IBOutlet var cancel_Btn : UIButton?;
    @IBOutlet var popupView : UIView?;
    @IBOutlet var backView : UIView?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
                initFields();
   
        // Do any additional setup after loading the view.
    }
    func initFields() -> Void {
         overLay_Btn?.isHidden = true;
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            // lbl_icon1?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            
            framer = lbl_icon1!.frame;
            framer.origin.x = (UIScreen.main.bounds.origin.x)+20;
            lbl_icon1!.frame = framer;
            
//            framer = lbl_icon2!.frame;
//            framer.origin.x = (UIScreen.main.bounds.origin.x)+20;
//            lbl_icon2!.frame = framer;
            
            framer = lbl_icon3!.frame;
            framer.origin.x = (UIScreen.main.bounds.origin.x)+20;
            lbl_icon3!.frame = framer;
            
            framer = lbl_icon4!.frame;
            framer.origin.x = (UIScreen.main.bounds.origin.x)+20;
            lbl_icon4!.frame = framer;
            
            framer = lbl_icon5!.frame;
            framer.origin.x = (UIScreen.main.bounds.origin.x)+20;
            lbl_icon5!.frame = framer;
            
            
            framer = lbl_english!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (lbl_english!.frame.size.width+10);
            lbl_english!.frame = framer;
            
//            framer = lbl_arabic!.frame;
//            framer.origin.x = UIScreen.main.bounds.size.width - (lbl_arabic!.frame.size.width+10);
//            lbl_arabic!.frame = framer;
            
            framer = lbl_indoesia!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (lbl_indoesia!.frame.size.width+10);
            lbl_indoesia!.frame = framer;
            
            
            framer = lbl_french!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (lbl_french!.frame.size.width+10);
            lbl_french!.frame = framer;
            
            framer = lbl_germen!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (lbl_germen!.frame.size.width+10);
            lbl_germen!.frame = framer;
            
            
            lbl_icon1?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
           // lbl_icon2?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            lbl_icon3?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            lbl_icon4?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            lbl_icon5?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
            lbl_english?.textAlignment = .right;
           // lbl_arabic?.textAlignment = .right;
            lbl_indoesia?.textAlignment = .right;
            lbl_germen?.textAlignment = .right;
            lbl_french?.textAlignment = .right;
            
            framer = cancel_Btn!.frame;
            framer.origin.x = ((self.view.frame.size.width-40) - (delete_Btn!.frame.size.width+cancel_Btn!.frame.size.width))/2;
            cancel_Btn!.frame = framer;
            
            
            framer = delete_Btn!.frame;
            framer.origin.x =  (cancel_Btn!.frame.size.width+cancel_Btn!.frame.origin.x)+8;
            delete_Btn!.frame = framer;
        }
        
        popupView?.layer.cornerRadius = 5;
        popupView?.layer.masksToBounds = true;
        
        delete_Btn?.layer.cornerRadius = 5;
        delete_Btn?.layer.masksToBounds = true;
        
        cancel_Btn?.layer.cornerRadius = 5;
        cancel_Btn?.layer.masksToBounds = true;
        
  
        lbl_icon1.font = MaterialIcons.fontOfSize(size:20);
       // lbl_icon2.font = MaterialIcons.fontOfSize(size:20);
        lbl_icon3.font = MaterialIcons.fontOfSize(size:20);
        lbl_icon4.font = MaterialIcons.fontOfSize(size:20);
        lbl_icon5.font = MaterialIcons.fontOfSize(size:20);
        
        var framer: CGRect = popupView!.frame;
        framer.origin.x = (UIScreen.main.bounds.size.width-(popupView?.frame.size.width)!)/2
        framer.origin.y = (UIScreen.main.bounds.size.height-(popupView?.frame.size.height)!)/2
        framer.size.width = (popupView?.frame.size.width)!;
        framer.size.height = (popupView?.frame.size.height)!;
        popupView?.frame = framer;
        self.view .addSubview(popupView!);
        popupView?.isHidden=true;
        
        is_currentLang = getBSEValue(_keyname: CURRENTLANG)
        self.UpdateCheckBox(parameter: getBSEValue(_keyname: CURRENTLANG))

        delete_Btn?.setTitle("Ok".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)

        cancel_Btn?.setTitle( "Cancel".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)

        menu_Title.text = "LANGUAGE".localized(lang: getBSEValue(_keyname: CURRENTLANG))

        lbl_english.text = "English".localized(lang: getBSEValue(_keyname: CURRENTLANG))

      //  lbl_arabic.text =  "Arabic".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_french.text = "French".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_germen.text =  "German".localized(lang: getBSEValue(_keyname: CURRENTLANG))

        lbl_indoesia.text =  "Indonesia".localized(lang: getBSEValue(_keyname: CURRENTLANG))

        confirm_Title.text = "Language_Confirm".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
    }
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    @IBAction func setPermission(sender: UIButton!) {
        
         overLay_Btn?.isHidden = false;
       popupView?.isHidden=false;
        if(sender.tag==0) {
            is_currentLang = "en"
        }
       else if(sender.tag==1) {
            is_currentLang = "ar"
        }
        else if(sender.tag==2)
        {
        is_currentLang = "id"
        }
        else if(sender.tag==3)
        {
            is_currentLang = "fr"
        }
        else if(sender.tag==4)
        {
            is_currentLang = "de"
        }
    }
    @IBAction func actionOK(sender: UIButton!) {
        
        setBSEValue(_keyname:CURRENTLANG, _value:is_currentLang);
        print( getBSEValue(_keyname: CURRENTLANG))
       confirm_Title.text = "Language_Confirm".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.login();
       self.UpdateCheckBox(parameter: is_currentLang)
        self .actionClose(sender: nil)
    }
    
     @IBAction func actionClose(sender: UIButton!) {
        overLay_Btn?.isHidden = true;
          popupView?.isHidden=true;
     }
    
     func UpdateCheckBox(parameter:String) -> Void {
        if(is_currentLang == "en")
        {
            lbl_icon1.text = MaterialIcons.RadioButtonChecked;
           // lbl_icon2.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon3.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon4.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon5.text = MaterialIcons.RadioButtonUnchecked;
        }
        else if(is_currentLang == "ar")
        {
           // lbl_icon2.text = MaterialIcons.RadioButtonChecked;
            lbl_icon1.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon3.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon4.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon5.text = MaterialIcons.RadioButtonUnchecked;
        }
        else if(is_currentLang == "id")
        {
            lbl_icon3.text = MaterialIcons.RadioButtonChecked;
           // lbl_icon2.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon1.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon4.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon5.text = MaterialIcons.RadioButtonUnchecked;
        }
        else if(is_currentLang == "ge")
        {
            lbl_icon5.text = MaterialIcons.RadioButtonChecked;
           // lbl_icon2.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon1.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon4.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon3.text = MaterialIcons.RadioButtonUnchecked;
        }
        else if(is_currentLang == "fr")
        {
            lbl_icon4.text = MaterialIcons.RadioButtonChecked;
           // lbl_icon2.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon1.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon3.text = MaterialIcons.RadioButtonUnchecked;
            lbl_icon5.text = MaterialIcons.RadioButtonUnchecked;
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

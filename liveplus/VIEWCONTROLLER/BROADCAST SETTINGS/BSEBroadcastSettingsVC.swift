//
//  BSEBroadcastSettingsVC.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//
import UIKit
import SDWebImage
import MaterialIconsSwift

class BSEBroadcastSettingsVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var mainView: UIView!;
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var isnotifyField: UISwitch!;
    @IBOutlet weak var isprivateField: UISwitch!;
    @IBOutlet weak var page_scroll: UIScrollView!;
    @IBOutlet var submit_btn: UIButton!;
    @IBOutlet var lbl_menuTitle: UILabel!
    @IBOutlet var lbl_private: UILabel!
    @IBOutlet var lbl_notifi: UILabel!
    @IBOutlet var backView : UIView?;
    var progressBar:AMProgressBar!;
    var is_notify:String!;
    var is_private:String!;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }
    // MARK: - Custom Function
    func initFields() -> Void {
        
        
        
        lbl_menuTitle.text = "UPDATE SETTINGS".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        submit_btn?.setTitle( "UPDATE".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        lbl_private.text = "Private Account".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_notifi.text =  "Notification Settings".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        is_private = getBSEValue(_keyname: ISPRIVATE)
        is_notify = getBSEValue(_keyname: ISNOTIFY)
        if(is_notify=="1") {
            isnotifyField.setOn(true, animated: false)
        } else {
            isnotifyField.setOn(false, animated: false)
        }
        
        if(is_private=="1") {
            isprivateField.setOn(true, animated: false)
        } else {
            isprivateField.setOn(false, animated: false)
        }
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            isnotifyField?.frame = CGRect(x:0 , y:6, width:(isprivateField?.frame.size.width)!, height:(isprivateField?.frame.size.height)!);
            isprivateField?.frame = CGRect(x:0 , y:6, width:(isprivateField?.frame.size.width)!, height:(isprivateField?.frame.size.height)!);
            
            framer = lbl_private!.frame;
            framer.origin.x = (UIScreen.main.bounds.size.width - 40) - (lbl_private!.frame.size.width);
            lbl_private!.frame = framer;
            
            framer = lbl_notifi!.frame;
            framer.origin.x = (UIScreen.main.bounds.size.width - 40) - (lbl_notifi!.frame.size.width);
            lbl_notifi!.frame = framer;
            
            lbl_private.textAlignment = .right
            lbl_notifi.textAlignment = .right
        }
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
    }
    
    func setLoader() -> Void {
        headerView.isUserInteractionEnabled = false;
        submit_btn.isUserInteractionEnabled = false;
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        headerView.isUserInteractionEnabled = true;
        submit_btn.isUserInteractionEnabled = true;
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    

    // MARK: - Control function
    @IBAction func switchChanged(sender: UISwitch!) {
        if(sender.tag==0) {
            if(sender.isOn) {
                is_private = "1"
            } else {
                is_private = "0"
            }
        } else {
            if(sender.isOn) {
                is_notify = "1"
            } else {
                is_notify = "0"
            }
        }
        
    }
    
    @IBAction func actionSubmit(sender: UIButton!) {
        self.view .endEditing(true)
        setLoader();
        let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&is_notify="+is_notify+"&&is_private="+is_private+"&&is_auto_group_invite="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        print("parameter are: \(parameter)")
        apiservice().apiPostRequest(params:parameter, withMethod: METHODBROADCASTSETTINGS,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                setBSEValue(_keyname: ISNOTIFY, _value: self.is_notify)
                setBSEValue(_keyname: ISPRIVATE, _value: self.is_private)
SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Broadcast Updated".localized(lang: getBSEValue(_keyname: CURRENTLANG)), type: .success);
            } else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg, type: .error);
            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
        }
        
    }
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    
    
    // MARK: - Keyboard Function
    override func viewDidAppear(_ animated: Bool)
    {
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    
    // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

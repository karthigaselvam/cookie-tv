//
//  BSEFollowingVC.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BSEFollowingVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var header_label: UILabel!;
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
    var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!;
    var model: BSEUserModel!
     @IBOutlet var lbl_noData: UILabel!
    @IBOutlet var backView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(task==nil) {
        } else {
            if(task.state == .running) {
                task.cancel();
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - custom function
    
    func initFields() -> Void {
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
        }
        
        
        
        lbl_noData.text = "No Data".localized(lang: getBSEValue(_keyname: CURRENTLANG))

        let userID:String = String(model.user_id)
        if(userID == getBSEValue(_keyname: USERID)) {
          header_label.text =  "Following".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        } else {
              header_label.text = model.username.capitalized  +  "Following".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        }
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        self.isFirstTime = true;
        self.currentpage = 1;
        self.getMyFollowers();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
    }
    
    
    /**
     This is the function that help refresh UITableView through pull to refresh action
     */
    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getMyFollowers();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getMyFollowers();
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    
    func getMyFollowers() -> Void {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&member_id="+String(model.user_id)+"&&page_no="+String(currentpage)+"&&username=&&record="+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        if(!apiservice().isInternetAvailable()) {
            SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "No Connection".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        let originalString = APIURL + METHODFOLLOWINGLIST + "?" + parameter;
        let request = URLRequest(url: URL(string: originalString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        
        if(task != nil) {
            if(task.state == .running) {
                task.cancel();
            }
        }
        
        task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                if(self.isPullToRefresh) {
                    self.isPullToRefresh = false;
                    self.refreshControl.endRefreshing()
                }
                self.isAPILoading = false;
                self.hideLoader();
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if(httpResponse.statusCode==200) {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                       
                        let Dic = apiservice().convertToDictionary(response: data!);
                        let output = apiparser().sperateParams(method: METHODFOLLOWINGLIST, result: Dic!);
                        let commonModel : BSECommonModel = output as! BSECommonModel;
                        if(commonModel.status == "true") {
                            if(self.isFirstTime) {
                                self.tableData.removeAll();
                                self.isFirstTime = false;
                            }
                            if(commonModel.temparray.count>0) {
                    self.tableData.append(contentsOf:commonModel.temparray);
                                self.isAPILoading = false;
                            }
                        } else {
                            self.isAPILoading = false;
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                        }
                        
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noHeaderView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                        self.tblView?.reloadData();
                        self.hideLoader();
                        
                    } else {
                        if(self.isPullToRefresh) {
                            self.isPullToRefresh = false;
                            self.refreshControl.endRefreshing()
                        }
                        self.isAPILoading = false;
                        self.hideLoader();
                        if(self.tableData.count==0) {
                            self.tblView?.tableFooterView = self.noHeaderView();
                        } else {
                            self.tblView?.tableFooterView = UIView.init();
                        }
                    }
                } else {
                    if(self.isPullToRefresh) {
                        self.isPullToRefresh = false;
                        self.refreshControl.endRefreshing()
                    }
                    self.isAPILoading = false;
                    self.hideLoader();
                    if(self.tableData.count==0) {
                        self.tblView?.tableFooterView = self.noHeaderView();
                    } else {
                        self.tblView?.tableFooterView = UIView.init(frame: CGRect.zero);
                    }
                }
                
            }
        })
        task.resume();
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func noHeaderView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Follow List Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }
    

    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let uidentifier = "UserCell"
         
        var cell: UserCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell
        if cell == nil {
           tableView.register(UINib(nibName: (getBSEValue(_keyname: CURRENTLANG)=="ar") ? "UserCellRTL" : "UserCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? UserCell;
        }
        cell.setFollowerValue(model: tableData[indexPath.row] as! BSEUserModel)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.follow_action?.tag = indexPath.row;
        cell.follow_action?.addTarget(self, action: #selector(self.userAction(_:)), for: .touchUpInside)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model:BSEUserModel = tableData[indexPath.row] as! BSEUserModel
        let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
        profile.model = model
        self.navigationController?.pushViewController(profile, animated: true)
    }
    

    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @objc func userAction(_ sender: UIButton){ //<- needs `@objc`
        let modeler:BSEUserModel = tableData[sender.tag] as! BSEUserModel
        if(modeler.user_follow_status==0 || modeler.user_follow_status==1) {
            var follower_status:Int!;
            follower_status = modeler.user_follow_status == 0 ? 1 : 3
            let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&type="+String(follower_status)+"&&member_id="+String(modeler.user_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            if(modeler.is_private==1) {
                modeler.user_follow_status = (follower_status==1) ? 2:0;
                if(follower_status==1) {
    SwiftMessageBar.showMessageWithTitle(  "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Follow Sent".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                } else {
                    var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                    follower_count = follower_count - 1;
                    setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
    SwiftMessageBar.showMessageWithTitle( "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                }
            } else {
                var follower_count:Int = Int(getBSEValue(_keyname: FOLLOWINGCOUNT))!
                if(follower_status==1) {
    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "Followed user".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    follower_count = follower_count + 1;
                } else {
    SwiftMessageBar.showMessageWithTitle(  "Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "unFollowed".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);
                    follower_count = follower_count - 1;
                }
                setBSEValue(_keyname: FOLLOWINGCOUNT, _value: String(follower_count));
                modeler.user_follow_status = (follower_status==1) ? 1:0;
            }
            self.tblView?.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)

            self.updateCount();
            
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODFOLLOWACTION,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    
                } else {
        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg, type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    
    func updateCount() -> Void {
        let userModel:BSEUserModel = BSEUserModel();
        userModel.user_id = Int(getBSEValue(_keyname: USERID));
        userModel.access_token = getBSEValue(_keyname: ACCESSTOKEN);
        userModel.username = getBSEValue(_keyname: USERNAME);
        userModel.first_name = getBSEValue(_keyname: FIRSTNAME);
        userModel.last_name = getBSEValue(_keyname: LASTNAME);
        userModel.country = getBSEValue(_keyname: COUNTRY);
        userModel.state = getBSEValue(_keyname: STATE);
        userModel.email = getBSEValue(_keyname: EMAIL);
        userModel.follower_count = Int(getBSEValue(_keyname: FOLLOWERCOUNT));
        userModel.following_count = Int(getBSEValue(_keyname: FOLLOWINGCOUNT));
        userModel.group_count = Int(getBSEValue(_keyname: GROUPCOUNT));
        userModel.broadcast_count = Int(getBSEValue(_keyname: BROADCASTCOUNT));
        userModel.is_notify = Int(getBSEValue(_keyname: ISNOTIFY));
        userModel.is_private = Int(getBSEValue(_keyname: ISPRIVATE));
        userModel.auto_invite = Int(getBSEValue(_keyname: AUTOINVITE));
        userModel.role = Int(getBSEValue(_keyname: ROLE));
        NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    }
    
   

}

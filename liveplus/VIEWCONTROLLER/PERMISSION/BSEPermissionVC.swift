//
//  BSEPermissionVC.swift
//  liveplus
//
//  Created by BSEtec on 23/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import MaterialIconsSwift
import AVFoundation
import CoreLocation

class BSEPermissionVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var close_icon: UILabel!;
    @IBOutlet var mainView: UIView!;
    @IBOutlet var closeView: UIView!;
    @IBOutlet var cameraView: UIView!;
    @IBOutlet var camera_label: UILabel!;
    @IBOutlet var camera_icon: UILabel!;
    @IBOutlet var camera_btn: UILabel!;
    
    @IBOutlet var micView: UIView!;
    @IBOutlet var mic_label: UILabel!;
    @IBOutlet var mic_icon: UILabel!;
    @IBOutlet var mic_btn: UILabel!;
    
    @IBOutlet var locationView: UIView!;
    @IBOutlet var location_label: UILabel!;
    @IBOutlet var location_icon: UILabel!;
    @IBOutlet var location_btn: UILabel!;
    @IBOutlet var lbl_title: UILabel!;
    @IBOutlet var lbl_desc: UILabel!;
    @IBOutlet var menuButton : UIView!;
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields();
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            var framer:CGRect = self.mainView.frame;
            framer.origin.y = (UIScreen.main.bounds.size.height - 272) / 2;
            self.mainView.frame = framer;
        }, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - custom function
    func initFields() -> Void {
        
       // setupMiddleButton();
        
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = camera_icon!.frame;
            framer.origin.x = cameraView.frame.size.width - (camera_icon!.frame.size.width+20);
            camera_icon!.frame = framer;
            
            framer = mic_icon!.frame;
            framer.origin.x = cameraView.frame.size.width - (mic_icon!.frame.size.width+20);
            mic_icon!.frame = framer;
            
            framer = location_icon!.frame;
            framer.origin.x = cameraView.frame.size.width - (location_icon!.frame.size.width+20);
            location_icon!.frame = framer;
            
            framer = closeView!.frame;
            framer.origin.x = 10;
            closeView!.frame = framer;
        }
        
        
        var framer:CGRect = mainView.frame;
        framer.origin.x = (UIScreen.main.bounds.size.width - 260) / 2;
        framer.origin.y = -272;
        mainView.frame = framer;
        
        lbl_title.text = "Start Broadcast".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_desc.text = "you can broadcast".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        camera_label.text = "Enable Camera".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        mic_label.text =  "Enable Microphone".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        location_label.text = "Enable Location".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
        var menuButtonFrame = menuButton!.frame
        menuButtonFrame.origin.y = (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 || UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2688) ? (view.bounds.height-34) - menuButtonFrame.height:view.bounds.height - menuButtonFrame.height
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        
        menuButton.backgroundColor = UIColor.white
        menuButton.layer.cornerRadius = menuButtonFrame.height/2;
        menuButton.layer.shadowColor = UIColor.black.cgColor
        menuButton.layer.shadowOpacity = 0.5
        menuButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        menuButton.layer.shadowRadius = 10
        //        //menuButton.layer.shadowPath = UIBezierPath(rect: menuButton.bounds).cgPath
        //
        let path = UIBezierPath()
        
        // Start at the Top Left Corner
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        
        // Move to the Top Right Corner
        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: 0.0))
        
        //        // Move to the Bottom Right Corner
        //        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: menuButton.frame.size.height))
        
        // This is the extra point in the middle :) Its the secret sauce.
        path.addLine(to: CGPoint(x: menuButton.frame.size.width/2.0, y: menuButton.frame.size.height/2.0))
        
        //        // Move to the Bottom Left Corner
        //        path.addLine(to: CGPoint(x: 0.0, y: menuButton.frame.size.height))
        
        path.close()
        
        menuButton.layer.shadowPath = path.cgPath
        menuButton.layer.masksToBounds = true;
        menuButton.layer.shouldRasterize = true
        view.layoutIfNeeded()
        
        close_icon.font = MaterialIcons.fontOfSize(size:20);
        close_icon.text = MaterialIcons.Close;
        
        self.updateFields();
        
    }
    func setupMiddleButton() {
        
        let menuButton = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 || UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2688) ? (view.bounds.height-34) - menuButtonFrame.height:view.bounds.height - menuButtonFrame.height
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        menuButton.tag = 101
        
        let cam_Icon = UIImageView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
        cam_Icon.image = UIImage(named:"Live_Selected.png")
        menuButton.addSubview(cam_Icon)
        
        menuButton.backgroundColor = UIColor.white
        menuButton.layer.cornerRadius = menuButtonFrame.height/2;
        
        menuButton.layer.shadowColor = UIColor.black.cgColor
        menuButton.layer.shadowOpacity = 0.5
        menuButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        menuButton.layer.shadowRadius = 10
       
        let path = UIBezierPath()
        
        // Start at the Top Left Corner
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        
        // Move to the Top Right Corner
        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: 0.0))
        
        //        // Move to the Bottom Right Corner
        //        path.addLine(to: CGPoint(x: menuButton.frame.size.width, y: menuButton.frame.size.height))
        
        // This is the extra point in the middle :) Its the secret sauce.
        path.addLine(to: CGPoint(x: menuButton.frame.size.width/2.0, y: menuButton.frame.size.height/2.0))
        
        //        // Move to the Bottom Left Corner
        //        path.addLine(to: CGPoint(x: 0.0, y: menuButton.frame.size.height))
        
        path.close()
        
        menuButton.layer.shadowPath = path.cgPath
        menuButton.layer.masksToBounds = true;
        menuButton.layer.shouldRasterize = true
        view.layoutIfNeeded()
        self.view.addSubview(menuButton);
    }
    func updateFields () -> Void {
        cameraView.layer.cornerRadius = 10.0;
        cameraView.layer.masksToBounds = true;
        cameraView.layer.borderColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor;
        cameraView.layer.borderWidth = 1.0;
        camera_icon.font = MaterialIcons.fontOfSize(size:20);
        
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1") {
            cameraView.backgroundColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            camera_label.textColor = .white;
            camera_icon.textColor = .white;
            camera_icon.text = MaterialIcons.Check;
        } else {
            cameraView.backgroundColor = .white
            camera_label.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            camera_icon.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            camera_icon.text = MaterialIcons.CameraAlt;
        }
        
        micView.layer.cornerRadius = 10.0;
        micView.layer.masksToBounds = true;
        micView.layer.borderColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor;
        micView.layer.borderWidth = 1.0;
        mic_icon.font = MaterialIcons.fontOfSize(size:20);
        
        if(getBSEValue(_keyname: MICPERMISSON) == "1") {
            micView.backgroundColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            mic_label.textColor = .white;
            mic_icon.textColor = .white;
            mic_icon.text = MaterialIcons.Check;
        } else {
            micView.backgroundColor = .white
            mic_label.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            mic_icon.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            mic_icon.text = MaterialIcons.Mic;
        }
        
        locationView.layer.cornerRadius = 10.0;
        locationView.layer.masksToBounds = true;
        locationView.layer.borderColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor;
        locationView.layer.borderWidth = 1.0;
        location_icon.font = MaterialIcons.fontOfSize(size:20);
        
        if(getBSEValue(_keyname: LOCATIONPERMISSON) == "1") {
            locationView.backgroundColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            location_label.textColor = .white;
            location_icon.textColor = .white;
            location_icon.text = MaterialIcons.Check;
        } else {
            locationView.backgroundColor = .white
            location_label.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            location_icon.textColor = UIColor.init(red: 89.0/255.0, green: 97.0/255.0, blue: 101.0/255.0, alpha: 1.0);
            location_icon.text = MaterialIcons.LocationOn;
        }
    }
    
    @IBAction func setPermission(sender: UIButton!) {
        if(sender.tag==0) {
            if(getBSEValue(_keyname: CAMERAPERMISSON)=="0") {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        setBSEValue(_keyname: CAMERAPERMISSON, _value: "1")
                        DispatchQueue.main.async {
                            self.checkAllPermission();
                        }
                        
                    } else {
                        setBSEValue(_keyname: CAMERAPERMISSON, _value: "2")
                    }
                })
            } else if (getBSEValue(_keyname: CAMERAPERMISSON)=="2") {
                UIView.animate(withDuration: 0.5, animations: {
                    var framer:CGRect = self.mainView.frame;
                    framer.origin.y = UIScreen.main.bounds.size.height;
                    self.mainView.frame = framer;
                }, completion: { (finished: Bool) in
                    self.dismiss(animated: false, completion: {
    UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    });
                })
               
            }
        } else if(sender.tag==1) {
            if(getBSEValue(_keyname: MICPERMISSON)=="0") {
                AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                    if granted {
                        setBSEValue(_keyname: MICPERMISSON, _value: "1")
                        DispatchQueue.main.async {
                            self.checkAllPermission();
                        }
                    } else {
                        setBSEValue(_keyname: MICPERMISSON, _value: "2")
                    }
                })
            } else if (getBSEValue(_keyname: MICPERMISSON)=="2") {
                UIView.animate(withDuration: 0.5, animations: {
                    var framer:CGRect = self.mainView.frame;
                    framer.origin.y = UIScreen.main.bounds.size.height;
                    self.mainView.frame = framer;
                }, completion: { (finished: Bool) in
                    self.dismiss(animated: false, completion: {
                       UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    });
                })
                
            }
        } else if (sender.tag==2) {
            if(getBSEValue(_keyname: LOCATIONPERMISSON)=="0") {
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
            } else if (getBSEValue(_keyname: LOCATIONPERMISSON)=="2") {
                UIView.animate(withDuration: 0.5, animations: {
                    var framer:CGRect = self.mainView.frame;
                    framer.origin.y = UIScreen.main.bounds.size.height;
                    self.mainView.frame = framer;
                }, completion: { (finished: Bool) in
                    self.dismiss(animated: false, completion: {
                        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    });
                })
                
            }
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            setBSEValue(_keyname: LOCATIONPERMISSON, _value: "1")
            DispatchQueue.main.async {
                self.checkAllPermission();
            }
        } else if status == .authorizedWhenInUse {
            setBSEValue(_keyname: LOCATIONPERMISSON, _value: "1")
            DispatchQueue.main.async {
                self.checkAllPermission();
            }
        } else if status == .denied {
            setBSEValue(_keyname: LOCATIONPERMISSON, _value: "2")
            DispatchQueue.main.async {
                self.checkAllPermission();
            }
        } else if status == .restricted {
            setBSEValue(_keyname: LOCATIONPERMISSON, _value: "2")
            DispatchQueue.main.async {
                self.checkAllPermission();
            }
        }
    }
    
    func checkAllPermission() -> Void {self.updateFields();
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1") {
            
            self.dismiss(animated: true, completion: nil);
            // showAddCamera()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showAddCamera()
            
        }
    }
    
    func showAddCamera() -> Void
    {
        if(getBSEValue(_keyname: CAMERAPERMISSON) == "1" && getBSEValue(_keyname: LOCATIONPERMISSON) == "1" && getBSEValue(_keyname: MICPERMISSON) == "1")
        {
            let discover = BSELiveVC(nibName: "BSELiveVC", bundle: nil)
            self.navigationController?.pushViewController(discover, animated: true)
        }
        else
        {
            let permission = BSEPermissionVC(nibName:"BSEPermissionVC",bundle:nil);
            permission.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(permission, animated: false, completion: nil)
    }
    }
 
    @IBAction func actionClose(sender: UIButton!) {
        UIView.animate(withDuration: 0.5, animations: {
            var framer:CGRect = self.mainView.frame;
            framer.origin.y = UIScreen.main.bounds.size.height;
            self.mainView.frame = framer;
        }, completion: { (finished: Bool) in
            self.dismiss(animated: false, completion: nil);
            
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) != nil {
                let foundView = window.viewWithTag(101)
                window.bringSubview(toFront: foundView!)
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

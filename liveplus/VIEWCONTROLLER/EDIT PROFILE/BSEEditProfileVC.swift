//
//  BSEEditProfileVC.swift
//  liveplus
//
//  Created by BSEtec on 13/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit
import CountryList
import SDWebImage
import MaterialIconsSwift

class BSEEditProfileVC: UIViewController, UITextFieldDelegate, CountryListDelegate {
    @IBOutlet weak var mainView: UIView!;
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var firstnameField: UITextField!;
    @IBOutlet weak var lastnameField: UITextField!;
    @IBOutlet weak var stateField: UITextField!;
    @IBOutlet weak var countryField: UITextField!;
    @IBOutlet weak var page_scroll: UIScrollView!;
    @IBOutlet weak var header_img: UIImageView!;
    @IBOutlet var submit_btn: UIButton!;
        @IBOutlet var lbl_menuTitle: UILabel!
    var progressBar:AMProgressBar!;
    var countryCode:String!;
    var profileImg:String!;
    var countryList = CountryList();
    
    @IBOutlet var avatarView : UIView?;
    @IBOutlet var avatarEditView : UIView?;
    @IBOutlet var camera_icon : UILabel?;
    @IBOutlet var avatarImage : UIImageView?;
      @IBOutlet var backView : UIView?;
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set { super.hidesBottomBarWhenPushed = newValue }
//    }
    // MARK: - Custom Function
    func initFields() -> Void {
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = UIScreen.main.bounds.size.width - (backView!.frame.size.width);
            backView!.frame = framer;
            
            firstnameField.textAlignment = .right
            lastnameField.textAlignment = .right
            stateField.textAlignment = .right
            countryField.textAlignment = .right
        }
        else
        {
            firstnameField.textAlignment = .left
            lastnameField.textAlignment = .left
            stateField.textAlignment = .left
            countryField.textAlignment = .left
        }
        
        lbl_menuTitle.text = "EDIT PROFILE".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        firstnameField.placeholder = "FirstName".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lastnameField.placeholder = "LastName".localized(lang: getBSEValue(_keyname: CURRENTLANG))
    
        
        stateField.placeholder = "State".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        countryField.placeholder = "Country".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        submit_btn?.setTitle("UPDATE".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
    
        
        profileImg = "";
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        countryField.inputView = nil;
        countryList.delegate = self;
        
        avatarView?.layer.borderColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0).cgColor;
        avatarView?.layer.borderWidth = 2.0
        avatarView?.layer.cornerRadius = 64;
        avatarView?.layer.masksToBounds = true;
        
        avatarImage?.layer.cornerRadius = 60;
        avatarImage?.layer.masksToBounds = true;
        
        avatarEditView?.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor;
        avatarEditView?.layer.borderWidth = 2.0
        avatarEditView?.layer.cornerRadius = 20;
        avatarEditView?.layer.masksToBounds = true;
        
        camera_icon?.font = MaterialIcons.fontOfSize(size:20);
        camera_icon?.text = MaterialIcons.CameraAlt
        
        firstnameField.text = getBSEValue(_keyname: FIRSTNAME);
        lastnameField.text = getBSEValue(_keyname: LASTNAME);
        stateField.text = getBSEValue(_keyname: STATE);
        countryField.text = getBSEValue(_keyname: COUNTRY);
        countryCode = getBSEValue(_keyname: COUNTRY);
        avatarImage?.sd_setImage(with: URL(string: USERIMGURL+getBSEValue(_keyname: USERID)), placeholderImage: UIImage(named: "placeholder.png"))
        
    }
    
    func setLoader() -> Void {
        headerView.isUserInteractionEnabled = false;
        submit_btn.isUserInteractionEnabled = false;
        avatarEditView?.isUserInteractionEnabled = false;
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
        

    }
    
    
    func hideLoader() -> Void {
        headerView.isUserInteractionEnabled = true;
        submit_btn.isUserInteractionEnabled = true;
        avatarEditView?.isUserInteractionEnabled = true;
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    

    func validateFields() -> Bool {
        if  FormValidation().notEmpty(msg: firstnameField.text!) && FormValidation().notEmpty(msg: lastnameField.text!) && FormValidation().notEmpty(msg: stateField.text!) && FormValidation().notEmpty(msg: countryField.text!) {
            return true;
        }
        else {
            
SwiftMessageBar.showMessageWithTitle( "Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);

        }
        return false
    }
    
    func saveUser(model:BSEUserModel) -> Void {
        setBSEValue(_keyname:USERID, _value:String(model.user_id))
        setBSEValue(_keyname:ACCESSTOKEN, _value:model.access_token);
        setBSEValue(_keyname:USERNAME, _value:model.username);
        setBSEValue(_keyname:FIRSTNAME, _value:model.first_name);
        setBSEValue(_keyname:LASTNAME, _value:model.last_name);
        setBSEValue(_keyname:COUNTRY, _value:model.country);
        setBSEValue(_keyname:STATE, _value:model.state);
        setBSEValue(_keyname:EMAIL, _value:model.email);
        setBSEValue(_keyname:FOLLOWERCOUNT, _value:String(model.follower_count));
        setBSEValue(_keyname:FOLLOWINGCOUNT, _value:String(model.following_count));
        setBSEValue(_keyname:GROUPCOUNT, _value:String(model.group_count));
        setBSEValue(_keyname:BROADCASTCOUNT, _value:String(model.broadcast_count));
        setBSEValue(_keyname:ISNOTIFY, _value:String(model.is_notify));
        setBSEValue(_keyname:ISPRIVATE, _value:String(model.is_private));
        setBSEValue(_keyname:AUTOINVITE, _value:String(model.auto_invite));
        setBSEValue(_keyname:ROLE, _value:String(model.role));
        setBSEValue(_keyname:ISBROADCASTID, _value:String(model.is_broadcast_id));
    }
    
    func selectedCountry(country: Country) {
        countryCode = country.countryCode;
        countryField.text = country.name;
    }
    
    
    // MARK: - Control function
    
    @IBAction func actionAvatarUpload(sender: UIButton!) {
        
        let alert = UIAlertController(title:"Choose Image".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , message: nil, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title:   "Camera".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
            self.openCamera()
        }))
        
        
        alert.addAction(UIAlertAction(title:   "Gallery".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title:   "Cancel".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
let alert  = UIAlertController(title: "Warning".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "No camera".localized(lang: getBSEValue(_keyname: CURRENTLANG)), preferredStyle: .alert)
            
alert.addAction(UIAlertAction(title: "OK".localized(lang: getBSEValue(_keyname: CURRENTLANG)), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func actionRegister(sender: UIButton!) {
        self.view .endEditing(true)
        if self.validateFields(){
            setLoader();
            let parameter = "userid="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&first_name="+(firstnameField.text)!+"&&last_name="+(lastnameField.text)!+"&&state="+(stateField.text)!+"&&country="+(countryCode)+"&&profile_pic="+profileImg+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODEDITPROFILE,  andCompletionBlock: { (success) -> Void in
                let commonModel : BSECommonModel = success as! BSECommonModel;
                if(commonModel.status == "true") {
                    let userModel : BSEUserModel = commonModel.temparray[0] as! BSEUserModel;
                    self.saveUser(model: userModel);
                    NotificationCenter.default.post(name: Notification.Name("profileNotify"), object: nil, userInfo: ["type":"updateprofile", "value":userModel])
    SwiftMessageBar.showMessageWithTitle("Success".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Profile Updated".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .success);

                } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);
                }
                self.hideLoader();
            }) { (failure) -> Void in
                self.hideLoader();
            }
        }
    }
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionShowCountry(sender: UIButton!) {
        let navController = UINavigationController(rootViewController: countryList);
        self.present(navController, animated: true, completion: nil);
    }
    
    func uploadImage(image: UIImage) -> Void {
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        var request = URLRequest(url: URL(string: APIURL + METHODUPLOAD)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        
        request.httpMethod = "POST"
        let boundaryConstant = "----------------12345";
        let contentType = "multipart/form-data;boundary=" + boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        let filename = randomString(length: 8) + ".jpg"
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"type\";\r\n\r\n".data(using: .utf8)!)
        uploadData.append("user".data(using: .utf8)!);
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
        uploadData.append(imageData!)
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        request.httpBody = uploadData as Data
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoader();
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: error?.localizedDescription , type: .error);
            } else {
                self.hideLoader();
                let Dic = apiservice().convertToDictionary(response: data!);
                self.profileImg = Dic!["fileurl"] as! String;
            }
        })
        
        task.resume()
    }
    
    func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }

    
    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==firstnameField{
            lastnameField.becomeFirstResponder()
        } else if textField==lastnameField{
            stateField.becomeFirstResponder()
        } else if textField==stateField{
            textField.resignFirstResponder();
            let navController = UINavigationController(rootViewController: countryList);
            self.present(navController, animated: true, completion: nil);
            
        } else{
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == countryField) {
            countryField.resignFirstResponder();
            let navController = UINavigationController(rootViewController: countryList);
            self.present(navController, animated: true, completion: nil);
        }
    }
    
    
    // MARK: - Keyboard Function
    override func viewDidAppear(_ animated: Bool)
    {
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK: - UIImagePickerControllerDelegate
extension BSEEditProfileVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.avatarImage?.image = editedImage
            
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: {
            self.setLoader();
            self.uploadImage(image: (self.avatarImage?.image)!);
            })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: {
        });
    }
    
}

//
//  NotifyCell.swift
//  liveplus
//
//  Created by BseTec on 2/24/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class NotifyCell: UITableViewCell {
    @IBOutlet var title : UILabel?;
      @IBOutlet var message : UILabel?;
    @IBOutlet var date : UILabel?;
    @IBOutlet var userimage : UIImageView?;
    override func awakeFromNib() {
        super.awakeFromNib()
        userimage?.layer.cornerRadius = 25.0;
        userimage?.layer.masksToBounds = true;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDefaultValue(model:BSENotificationModel) -> Void {
        userimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.sender_id)), placeholderImage: UIImage(named: "placeholder.png"))
        title?.text = model.title;
        message?.text = model.message;
        date?.text = model.date;
    }
}

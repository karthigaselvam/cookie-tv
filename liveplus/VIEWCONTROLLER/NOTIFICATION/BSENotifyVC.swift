
//
//  BSENotifyVC.swift
//  liveplus
//
//  Created by BSEtec on 10/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit


class BSENotifyVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var headerView: UIView!;
    @IBOutlet weak var tblHeaderView: UIView!;
    @IBOutlet var tblView : UITableView?;
    var tableData = [AnyObject]();
    var isAPILoading: Bool!;
    var isFirstTime: Bool!;
    var currentpage: Int!;
    var task: URLSessionDataTask!;
    var isInfiniteScrolling: Bool!;
    var isPullToRefresh: Bool!
       var refreshControl: UIRefreshControl!
    var progressBar:AMProgressBar!;
    var currentIndex :Int!;
    @IBOutlet var header_Title:UILabel!;
    @IBOutlet var backView : UIView?;
    override func viewDidLoad() {
        super.viewDidLoad()
       initFields();
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        if(self.task==nil) {
        } else {
            if(self.task.state == .running) {
                self.task.cancel();
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - custom function
    
    func initFields() -> Void {
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            var framer: CGRect = backView!.frame;
            framer.origin.x = 10;
            backView!.frame = framer;
            backView?.autoresizingMask = [.flexibleTopMargin,.flexibleBottomMargin];
            
        }
        
        isPullToRefresh = false;
        isInfiniteScrolling = false;
        isFirstTime = true;
        currentpage = 1;
        getNotificationList();
        tblView?.tableFooterView = UIView (frame: CGRect.zero)
        header_Title? .text = "notification".localized(lang: getBSEValue(_keyname: CURRENTLANG))
         NotificationCenter.default.addObserver(self, selector: #selector(self.profileNotify), name: Notification.Name("Notify"), object: nil)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        self.tblView?.insertSubview(refreshControl, at: 0)
        
    }

    @objc func onRefresh() {
        if(!self.isPullToRefresh) {
            self.isFirstTime = true;
            self.currentpage = 1;
            self.getNotificationList();
            self.isPullToRefresh = true;
        }
    }
    
    /**
     This is the function that help to get more data when user scroll down end of UITableView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if(!self.isFirstTime &&  !self.isAPILoading) {
                    self.tblView?.tableFooterView = self.tableCustomFooterLoader();
                    self.currentpage = self.currentpage + 1;
                    self.getNotificationList();
                }
            }
        }
    }
    
    /**
     This is the function that help to show loader from UITableView scroll to end
     */
    func tableCustomFooterLoader() -> UIView {
        let footerLoader : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 45));
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = .gray
        myActivityIndicator.center = footerLoader.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        footerLoader.addSubview(myActivityIndicator)
        return footerLoader
    }
    
    /**
     This is the function that help to show empty message in UITableView, if api response has empty data
     */
    func noDataView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Items Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }

    
     @objc func profileNotify(notification: Notification){
        isFirstTime = true;
        currentpage = 1;
        getNotificationList();
    }
    
    func getNotificationList() -> Void
    {
        isAPILoading = true;
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&page_no="+String(currentpage)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
        setLoader();
        
        apiservice().apiGetRequest(params:parameter, withMethod: METHODNOTIFICATIONLIST,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.tabcontroller?.tabBar.items?[4].badgeValue = (commonModel.return_id == "0") ? nil : commonModel.return_id;
                
                
                
                if(self.isFirstTime) {
                    self.tableData.removeAll();
                    self.isFirstTime = false;
                }
                if(commonModel.temparray.count>0) {
                    self.tableData.append(contentsOf:commonModel.temparray);
                    self.isAPILoading = false;
                }
            } else {
                self.isAPILoading = false;
               // SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            
            if(self.tableData.count==0) {
                self.tblView?.tableFooterView = self.noDataView();
            } else {
                self.tblView?.tableFooterView = UIView.init();
            }
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.tblView?.reloadData();
            self.hideLoader();
        }) { (failure) -> Void in
            if(self.isPullToRefresh) {
                self.isPullToRefresh = false;
                self.refreshControl.endRefreshing()
            }
            self.isAPILoading = false;
            self.hideLoader();
        }
    }
    
    func setLoader() -> Void {
        
        progressBar = AMProgressBar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: (headerView?.frame.size.height)!))
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 0
        progressBar.barColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = .white
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = ""
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        headerView?.addSubview(progressBar)
        headerView?.sendSubview(toBack: progressBar);
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if headerView.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    // MARK: table Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uidentifier = "NotifyCell"
        var cell: NotifyCell! = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? NotifyCell
        if cell == nil {
            tableView.register(UINib(nibName:(getBSEValue(_keyname: CURRENTLANG)=="ar") ? "NotifyCellRTL" : "NotifyCell", bundle: nil), forCellReuseIdentifier: uidentifier);
            cell = tableView.dequeueReusableCell(withIdentifier: uidentifier) as? NotifyCell;
        }
        cell.setDefaultValue(model: tableData[indexPath.row] as! BSENotificationModel)
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model : BSENotificationModel = tableData[indexPath.row] as! BSENotificationModel;
        currentIndex=indexPath.row;
        
        
        let parameter = "user_id="+getBSEValue(_keyname: USERID)+"&&access_token="+getBSEValue(_keyname: ACCESSTOKEN)+"&&notification_id="+String(model.notification_id)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
       self.NotificationUpdate(parameter: parameter);
        
        if model.type=="add_group" {
            let Jgroup = BSEJoinedGroupVC(nibName: "BSEJoinedGroupVC", bundle: nil)
            self.navigationController?.pushViewController(Jgroup, animated: true)
        }
        else if( model.type=="follow" || model.type=="request_accept"){
            let userModel : BSEUserModel = BSEUserModel();
            userModel.user_id = model.sender_id;
            userModel.username = model.authorname;
            let profile = BSEProfileVC(nibName: "BSEProfileVC", bundle: nil)
            profile.model = userModel;
            self.navigationController?.pushViewController(profile, animated: true)
        }
        else if model.type=="request_sent" {
            let request = BSERequestVC(nibName: "BSERequestVC", bundle: nil)
            self.navigationController?.pushViewController(request, animated: true)
        }
        else if model.type=="start_broadcast" {
            
            let window = UIApplication.shared.keyWindow!
            if window.viewWithTag(101) != nil {
                window.viewWithTag(101)?.removeFromSuperview()
            }
            
            if(String(model.sender_id) == getBSEValue(_keyname: USERID)) {
                let presenter = BSESchedulePresenterVC(nibName: "BSESchedulePresenterVC", bundle: nil)
                presenter.model = ["broadcast_id":String(model.objetc_id),"broadcast_status":model.status];
                 presenter.page_from = "detail"
                presenter.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
                presenter.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(presenter, animated: true)
            } else {
                let viewer = BSEScheduleViewerVC(nibName: "BSEScheduleViewerVC", bundle: nil)
                viewer.model = ["broadcast_id":String(model.objetc_id),"broadcast_status":model.status];
                viewer.user = ["user_id":getBSEValue(_keyname: USERID), "access_token":getBSEValue(_keyname: ACCESSTOKEN), "username": getBSEValue(_keyname: USERNAME)]
                viewer.current_lang = getBSEValue(_keyname: CURRENTLANG); self.navigationController?.pushViewController(viewer, animated: true)
            }
            
        }
    }
    
    // MARK: - control function
    
    @IBAction func actionBack(sender: UIButton!) {
        self.view .endEditing(true);
        self.navigationController?.popViewController(animated: true);
    }
    func NotificationUpdate(parameter:String) -> Void {
        print("parameter are: \(parameter)")
        apiservice().apiPostRequest(params:parameter, withMethod: METHODNOTIFICATIONUPDATE,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.tabcontroller?.tabBar.items?[4].badgeValue = (commonModel.return_id == "0") ? nil : commonModel.return_id;
//                self.tableData.remove(at: self.currentIndex);
//               self.tblView?.reloadData()
            } else {
                SwiftMessageBar.showMessageWithTitle("Error", message: commonModel.status_Msg, type: .error);
            }
            self.hideLoader();
        }) { (failure) -> Void in
            self.hideLoader();
        }
    }
    
    @IBAction func actionSearch(sender: UIButton!) {
        let discover = BSEDiscoverVC(nibName: "BSEDiscoverVC", bundle: nil)
        self.navigationController?.pushViewController(discover, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func noHeaderView() -> UIView {
        let noView:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView.backgroundColor = .clear;
        let noView_label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height:40.0));
        noView_label.font = UIFont(name: "Poppins-SemiBold", size: 15)!
        noView_label.text = "No Data Available";
        noView_label.textAlignment = .center;
        noView_label.textColor = UIColor.init(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0);
        noView.addSubview(noView_label);
        return noView;
    }

}

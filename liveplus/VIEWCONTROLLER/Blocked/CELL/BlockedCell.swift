//
//  RequestedCell.swift
//  liveplus
//
//  Created by BSEtec on 21/02/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import UIKit

class BlockedCell: UITableViewCell {
    
    @IBOutlet var fullname : UILabel?;
    @IBOutlet var userimage : UIImageView?;
    @IBOutlet var accept_btn: UIButton?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userimage?.layer.cornerRadius = 25.0;
        userimage?.layer.masksToBounds = true;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDefaultValue(model:BSEUserModel) -> Void {
        userimage?.sd_setImage(with: URL(string: USERIMGURL+String(model.user_id)), placeholderImage: UIImage(named: "placeholder.png"))
        fullname?.text = model.username.capitalized;
        
  accept_btn?.setTitle("UNBLOCK".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
    }
}

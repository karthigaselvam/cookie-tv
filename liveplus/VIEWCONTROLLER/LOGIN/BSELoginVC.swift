//
//  BSELoginVC.swift
//  Liveplus
//
//  Created by BseTec on 2/10/18.
//  Copyright © 2018 Pothi. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class BSELoginVC: UIViewController,UITextFieldDelegate, UITextViewDelegate,GIDSignInUIDelegate {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var page_scroll: UIScrollView!
    @IBOutlet weak var header_img: UIImageView!
    @IBOutlet var submit_btn: UIButton!
    @IBOutlet var forPass_btn: UIButton!
    @IBOutlet var or_btn: UIButton!
    @IBOutlet var lbl_newuser: UILabel!
    @IBOutlet var lbl_signUp: UILabel!
    @IBOutlet var facebook_btn: UIButton!
    @IBOutlet var google_btn: UIButton!
    var progressBar:AMProgressBar!
    var currentType: NSString!;
    @IBOutlet var termsView:UITextView!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFields()
        // Do any additional setup after loading the view.
    }
   // MARK: - Custom Function
    func initFields() -> Void {
        print("current lag is :",getBSEValue(_keyname: CURRENTLANG));
        if(getBSEValue(_keyname: CURRENTLANG)=="ar")
        {
            emailField.textAlignment = .right
             passField.textAlignment = .right
        }
            
        else
        {
            emailField.textAlignment = .left
             passField.textAlignment = .left
        }
        
        emailField.placeholder =  "Username or Email Address".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        passField.placeholder =  "Password".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        submit_btn?.setTitle("Login".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        facebook_btn?.setTitle( "Signup with FACEBOOK".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
       // google_btn?.setTitle( "Signup with GOOGLE+".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        forPass_btn?.setTitle("Forgot".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        
        or_btn?.setTitle("or".localized(lang: getBSEValue(_keyname: CURRENTLANG)), for: UIControlState.normal)
        
        
        lbl_signUp.text = "Sign Up".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        lbl_newuser.text = "New User".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        
        
 let termString : String = "Terms and Privacy Policy".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        let myAttribute = [NSAttributedStringKey.font: UIFont(name: "Poppins-Regular", size: 13.0)!];
        let myString = NSMutableAttributedString(string: termString, attributes: myAttribute)
        myString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: termString.count))
        if(NSLocale.current.languageCode=="en")
        {
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 40, length: 5))
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 50, length: 14))
        }
        else  if(NSLocale.current.languageCode=="ar")
        {
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 47, length: 6))
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 59, length: 20))
        }
        else
        {
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 40, length: 9))
            myString.addAttribute(NSAttributedStringKey.link, value: PRIVACYURL, range: NSRange(location: 54, length: 17))
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.profileNotify(notification:)), name: Notification.Name("twitterOauthCallback"), object: nil)
        
        GIDSignIn.sharedInstance().uiDelegate = self

        termsView.attributedText = myString;
        
        header_img.layer.masksToBounds=false
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    func setLoader() -> Void {
        progressBar = AMProgressBar(frame: submit_btn.bounds)
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 10
        progressBar.barColor = .white
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = UIColor.init(red: 165.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 0.5)
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = "LOGGING IN".localized(lang: getBSEValue(_keyname: CURRENTLANG))

        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        submit_btn.addSubview(progressBar)
    }
    
    func setLoaderForFacebook() -> Void {
        progressBar = AMProgressBar(frame: submit_btn.bounds)
        
        progressBar.cornerRadius = 0
        progressBar.borderColor = .clear
        progressBar.borderWidth = 0
        
        progressBar.barCornerRadius = 10
        
        progressBar.barColor = (currentType == "facebook") ? UIColor.init(red: 39.0/255.0, green: 85.0/255.0, blue: 183.0/255.0, alpha: 1.0) : UIColor.init(red: 217.0/255.0, green: 68.0/255.0, blue: 55.0/255.0, alpha: 1.0)
        progressBar.barMode = AMProgressBarMode.determined.rawValue
        
        progressBar.hideStripes = false
        progressBar.stripesColor = (currentType == "facebook") ? UIColor.init(red: 39.0/255.0, green: 113.0/255.0, blue: 183.0/255.0, alpha: 1.0) : UIColor.init(red: 217.0/255.0, green: 110.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        progressBar.stripesWidth = 30
        progressBar.stripesDelta = 100
        progressBar.stripesMotion = AMProgressBarStripesMotion.right.rawValue
        progressBar.stripesOrientation = AMProgressBarStripesOrientation.diagonalRight.rawValue
        progressBar.stripesSpacing = 30
        
        progressBar.textColor = .white
        progressBar.textFont = UIFont(name: "Poppins-SemiBold", size: 15)!
        progressBar.textPosition = AMProgressBarTextPosition.middle.rawValue
        progressBar.loaderText = "LOGGING IN".localized(lang: getBSEValue(_keyname: CURRENTLANG))
        progressBar.progressValue = 1
        progressBar.setProgress(progress: 1, animated: true)
        if(currentType == "facebook"){
            facebook_btn.addSubview(progressBar)}
        else
        {
          //  google_btn.addSubview(progressBar)
            
        }
    }
    
    func hideLoader() -> Void {
        if progressBar != nil { // Make sure the view exists
            if submit_btn.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
            }
        }
    }
    
    func hideLoaderForFacebook() -> Void {
        if progressBar != nil { // Make sure the view exists
//              if(currentType == "facebook"){
            if facebook_btn.subviews.contains(progressBar) {
                progressBar.removeFromSuperview() // Remove it
                currentType=""
            }
           // }
//            else
//              {
//                if google_btn.subviews.contains(progressBar) {
//                    progressBar.removeFromSuperview() // Remove it
//                    currentType=""
//                }
//            }
        }
    }
    
    func validateFields() -> Bool {
        if FormValidation().notEmpty(msg: emailField.text!) && FormValidation().notEmpty(msg: passField.text!) {
            if !FormValidation().isValidEmail(testStr: emailField.text!){
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:"InValid Email".localized(lang: getBSEValue(_keyname: CURRENTLANG))  , type: .error);
                return false;
            }
            if !FormValidation().passwordLength(msg: passField.text!,totalCount:6){
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:  "InValid Password".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                return false;
            }
             return true;
        }
        else {
SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: "Fields Empty".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
        }
        return false
    }
    
    func saveUser(model:BSEUserModel) -> Void {
        setBSEValue(_keyname:USERID, _value:String(model.user_id))
        setBSEValue(_keyname:ACCESSTOKEN, _value:model.access_token);
        setBSEValue(_keyname:USERNAME, _value:model.username);
        setBSEValue(_keyname:FIRSTNAME, _value:model.first_name);
        setBSEValue(_keyname:LASTNAME, _value:model.last_name);
        setBSEValue(_keyname:COUNTRY, _value:model.country);
        setBSEValue(_keyname:STATE, _value:model.state);
        setBSEValue(_keyname:EMAIL, _value:model.email);
        setBSEValue(_keyname:FOLLOWERCOUNT, _value:String(model.follower_count));
        setBSEValue(_keyname:FOLLOWINGCOUNT, _value:String(model.following_count));
        setBSEValue(_keyname:GROUPCOUNT, _value:String(model.group_count));
        setBSEValue(_keyname:BROADCASTCOUNT, _value:String(model.broadcast_count));
        setBSEValue(_keyname:ISNOTIFY, _value:String(model.is_notify));
        setBSEValue(_keyname:ISPRIVATE, _value:String(model.is_private));
        setBSEValue(_keyname:AUTOINVITE, _value:String(model.auto_invite));
        setBSEValue(_keyname:ROLE, _value:String(model.role));
        setBSEValue(_keyname:ISBROADCASTID, _value:String(model.is_broadcast_id));
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func uploadImage(facebookresponse: AnyObject, image: UIImage) -> Void {
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        let config = URLSessionConfiguration.default;
        let session = URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main);
        var request = URLRequest(url: URL(string: APIURL + METHODUPLOAD)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0);
        
        request.httpMethod = "POST"
        let boundaryConstant = "----------------12345";
        let contentType = "multipart/form-data;boundary=" + boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        let filename = randomString(length: 8) + ".jpg"
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"type\";\r\n\r\n".data(using: .utf8)!)
        uploadData.append("user".data(using: .utf8)!);
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
        uploadData.append(imageData!)
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        request.httpBody = uploadData as Data
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if error != nil {
                self.hideLoaderForFacebook();
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: error?.localizedDescription , type: .error);
            } else {
                let Dic = apiservice().convertToDictionary(response: data!);
                print("upload file",Dic);
                let profile_pic = Dic!["fileurl"] as! String;
                
                let device_token = getBSEValue(_keyname: DEVICETOKEN).isEmpty ? "" :  getBSEValue(_keyname: DEVICETOKEN)
           
                let FacebookDic = facebookresponse as! Dictionary<String,Any>;
      
                let username = (FacebookDic["username"] as! String).replacingOccurrences(of: " ", with: "")
                
                let parameter = "auth_id="+(FacebookDic["facebook_ID"] as! String)+"&&auth_type="+(FacebookDic["type"] as! String)+"&&first_name="+(FacebookDic["first_name"] as! String)+"&&last_name="+(FacebookDic["last_name"] as! String)+"&&username="+(username)+"&&email="+(FacebookDic["email"] as! String)+"&&state=&&country=&&profile_pic="+profile_pic+"&&device_type=iphone&&device_token="+device_token+"&&language="+getBSEValue(_keyname: CURRENTLANG);
                print("params are ",parameter);
                self.facebookSignup(params:parameter);
            }
        })
        
        task.resume()
    }
    
    func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func facebookSignup(params:String) -> Void {
        apiservice().apiPostRequest(params:params, withMethod: METHODFACEBOOK,  andCompletionBlock: { (success) -> Void in
            let commonModel : BSECommonModel = success as! BSECommonModel;
            if(commonModel.status == "true") {
                let userModel : BSEUserModel = commonModel.temparray[0] as! BSEUserModel;
                self.saveUser(model: userModel);
                setBSEValue(_keyname:LOGINFROM, _value:"fblogin");
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.login();
            } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

            }
            self.hideLoaderForFacebook();
            self.submit_btn.isUserInteractionEnabled=true;
            self.facebook_btn.isUserInteractionEnabled=true;
            //self.google_btn.isUserInteractionEnabled=true;
        }) { (failure) -> Void in
            self.hideLoaderForFacebook();
            self.submit_btn.isUserInteractionEnabled=true;
            self.facebook_btn.isUserInteractionEnabled=true;
          //  self.google_btn.isUserInteractionEnabled=true;
           
        }
    }
    
   // MARK: - Control function
    @IBAction func actionForgto(sender: UIButton!) {
        self.view .endEditing(true)
        let forgot = BSEForgotVC(nibName: "BSEForgotVC", bundle: nil)
        self.navigationController?.pushViewController(forgot, animated: true)
    }
    @IBAction func actionRegister(sender: UIButton!) {
        self.view .endEditing(true)
        let vc = BSERegisterVC(nibName: "BSERegisterVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func actionLogin(sender: UIButton!) {
        
        self.view .endEditing(true)
        if self.validateFields()
        {
            submit_btn.isUserInteractionEnabled=false;
            facebook_btn.isUserInteractionEnabled=false;
            //google_btn.isUserInteractionEnabled=false;
            setLoader();
            let device_token = getBSEValue(_keyname: DEVICETOKEN).isEmpty ? "" :  getBSEValue(_keyname: DEVICETOKEN)
            let parameter = "username="+(emailField.text)!+"&&password="+(passField.text)!+"&&device_type=iphone&&device_token="+(device_token)+"&&language="+getBSEValue(_keyname: CURRENTLANG);
            print("parameter are: \(parameter)")
            
            apiservice().apiPostRequest(params:parameter, withMethod: METHODLOGIN,  andCompletionBlock: { (success) -> Void in
                    let commonModel : BSECommonModel = success as! BSECommonModel;
                    if(commonModel.status == "true") {
                        let userModel : BSEUserModel = commonModel.temparray[0] as! BSEUserModel;
                        self.saveUser(model: userModel);
                        setBSEValue(_keyname:LOGINFROM, _value:"userlogin")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.login();
                    } else {
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message: commonModel.status_Msg , type: .error);

                   }
                   self.hideLoader();
                self.submit_btn.isUserInteractionEnabled=true;
                self.facebook_btn.isUserInteractionEnabled=true;
               // self.google_btn.isUserInteractionEnabled=true;
                }) { (failure) -> Void in
                   self.hideLoader();
                    self.submit_btn.isUserInteractionEnabled=true;
                   self.facebook_btn.isUserInteractionEnabled=true;
                  //  self.google_btn.isUserInteractionEnabled=true;
                }
        }
    }
    
    @IBAction func actionFacebookLogin(sender: UIButton!) {
        currentType="facebook"
        submit_btn.isUserInteractionEnabled=false;
        facebook_btn.isUserInteractionEnabled=false;
      //  google_btn.isUserInteractionEnabled=false;
        setLoaderForFacebook();
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                self.hideLoaderForFacebook();
    SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)), message:error.localizedDescription, type: .error);
                self.submit_btn.isUserInteractionEnabled=true;
                self.facebook_btn.isUserInteractionEnabled=true;
              //  self.google_btn.isUserInteractionEnabled=true;

            case .cancelled:
                self.hideLoaderForFacebook();
                self.submit_btn.isUserInteractionEnabled=true;
                self.facebook_btn.isUserInteractionEnabled=true;
             //   self.google_btn.isUserInteractionEnabled=true;

SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)) ,message:"cancelled login".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                let connection = GraphRequestConnection()
                connection.add(GraphRequest(graphPath: "/me",parameters:["fields": "id, name, first_name, last_name, email, gender, picture, birthday, locale"])) { httpResponse, result in
                    switch result {
                    case .success(let response):
                        print("Graph Request Succeeded: \(response)")
                        self.submit_btn.isUserInteractionEnabled=true;
                        self.facebook_btn.isUserInteractionEnabled=true;
                      //  self.google_btn.isUserInteractionEnabled=true;

                        let facebook_ID = response.dictionaryValue?["id"] as! String;
                        let dummyEmail = response.dictionaryValue?["id"] as! String + "@liveplus.com"
                        let first_name = ((response.dictionaryValue?["first_name"]) != nil) ? response.dictionaryValue?["first_name"] as! String : "";
                        let last_name = ((response.dictionaryValue?["last_name"]) != nil) ? response.dictionaryValue?["last_name"] as! String : "";
                        let username = ((response.dictionaryValue?["name"]) != nil) ? response.dictionaryValue?["name"] as! String : "";
                        let email = ((response.dictionaryValue?["email"]) != nil) ? response.dictionaryValue?["email"] as! String : dummyEmail;
                        let url = URL(string:"https://graph.facebook.com/"+facebook_ID+"/picture?type=large&width=720&height=720")!;
                        self.getDataFromUrl(url: url) { data, response, error in
                            guard let data = data, error == nil else { return }
                            print(response?.suggestedFilename ?? url.lastPathComponent)
                            print("Download Finished")
                            DispatchQueue.main.async() {

                                let downloadImage = UIImage(data: data);

                                self.uploadImage(facebookresponse: ["facebook_ID":facebook_ID, "first_name":first_name, "last_name":last_name, "username":username,"type":"facebook", "email":email] as AnyObject, image: downloadImage!);
                            }
                        }
                    case .failed(let error):
                        self.hideLoaderForFacebook();
                        SwiftMessageBar.showMessageWithTitle("Error".localized(lang: getBSEValue(_keyname: CURRENTLANG)) ,message:"cancelled login".localized(lang: getBSEValue(_keyname: CURRENTLANG)) , type: .error);
                        print("Graph Request Failed: \(error)")
                        self.submit_btn.isUserInteractionEnabled=true;
                        self.facebook_btn.isUserInteractionEnabled=true;
                       // self.google_btn.isUserInteractionEnabled=true;
                    }
                }
                connection.start()
            }
        }
        
    }
        

    // MARK: - TextField Deleagte
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==emailField{
            passField .becomeFirstResponder()}
        else{
            textField.resignFirstResponder()}
        return true
    }
      // MARK: - Keyboard Function
    override func viewWillAppear(_ animated: Bool)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
}
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        page_scroll.contentInset=contentInset;
        page_scroll.scrollIndicatorInsets=contentInset
        
        page_scroll.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: mainView.frame.size.height + mainView.frame.origin.y + 10)
        
    }
    
    

     // MARK: - Memory Waring
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - textview
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("testing clicked url",URL.absoluteString);
        return true;
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true;
    }
    private func signInWillDispatch(signIn: GIDSignIn!, error: Error!) {

    }
     //MARK: - Google +
    @IBAction func actionGoogleLogin(sender: UIButton!) {
        GIDSignIn.sharedInstance().signIn()
    }


    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    // Dismiss the "Sign in with Google" view
    internal func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
     @objc func profileNotify(notification: Notification){
         currentType="google"
         setLoaderForFacebook();
        submit_btn.isUserInteractionEnabled=false;
        facebook_btn.isUserInteractionEnabled=false;
      //  google_btn.isUserInteractionEnabled=false;

        let type : String = notification.userInfo!["type"] as! String
        if( type == "google") {
//             print("id  are ",notification.userInfo!["id"] as! String);
//              print("name  are ",notification.userInfo!["name"] as! String);
//             print("email  are ",notification.userInfo!["email"] as! String);
//               print("image  are ",notification.userInfo!["image"] as! NSURL);
            let username = (notification.userInfo!["name"] as! String).replacingOccurrences(of: " ", with: "")
//              print("email  are ",username);
//
//            let parameter = "auth_id="+(notification.userInfo!["id"] as! String)+"&&auth_type=google&&first_name="+(notification.userInfo!["first_name"] as! String)+"&&last_name="+(notification.userInfo!["last_name"] as! String)+"&&username="+(username)+"&&email="+(notification.userInfo!["email"] as! String)+"&&state=&&country=&&profile_pic=&&device_type=iphone&&device_token="+device_token+"&&language="+getBSEValue(_keyname: CURRENTLANG);
//            print("params are ",parameter);
          // self.facebookSignup(params:parameter);

            self.getDataFromUrl(url: notification.userInfo!["image"] as! URL) { data, response, error in
                guard let data = data, error == nil else { return }
               // print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
                DispatchQueue.main.async() {

                    let downloadImage = UIImage(data: data);

                    self.uploadImage(facebookresponse:["facebook_ID":(notification.userInfo!["id"] as! String), "first_name":(notification.userInfo!["first_name"] as! String), "last_name":(notification.userInfo!["last_name"] as! String), "username":username, "email":(notification.userInfo!["email"] as! String),"type":"google"] as AnyObject, image: downloadImage!);

                }
            }

        }
    }
}

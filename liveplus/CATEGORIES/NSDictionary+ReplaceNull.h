//
//  NSDictionary+ReplaceNull.h
//  Your Show
//
//  Created by Siba Prasad Hota on 16/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ReplaceNull)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings;

@end

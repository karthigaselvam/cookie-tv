//
//  NSString+LocaliseLanguage.h
//  majlis
//
//  Created by BseTec on 4/20/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LocaliseLanguage)
-(NSString*) LocalString :(NSString *)InputStr;
@end

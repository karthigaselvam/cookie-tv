//
//  NSString+LocaliseLanguage.m
//  majlis
//
//  Created by BseTec on 4/20/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

#import "NSString+LocaliseLanguage.h"

@implementation NSString (LocaliseLanguage)
-(NSString*) LocalString :(NSString *)InputStr
{
    NSString *path = [[NSBundle mainBundle] pathForResource:InputStr ofType:@"lproj"];
    NSBundle  *myBundle = [NSBundle bundleWithPath:path];
    return NSLocalizedStringWithDefaultValue(self, nil, myBundle, @"", @"");
}
@end

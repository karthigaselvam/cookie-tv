//
//  String+Language.swift
//  majlis
//
//  Created by BseTec on 4/18/18.
//  Copyright © 2018 BSEtec. All rights reserved.
//

import Foundation
extension String {
    func localized(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
       return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }}
